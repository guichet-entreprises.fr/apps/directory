@page ADD_AUTHORITY
Directory : Gestion des autorités
===================================

\tableofcontents

\section compoBulle Ajouter une autorité dans Directory

\subsection autorite_ge Autorité "Guichet-Entreprises"

Il s'agit ici de créer une autorité appelée "Guichet-Entreprises" pouvant administer toutes les autorités compétentes

![Autorité Guichet-Entreprises](@ref autorite_guichet-entreprises.jpg)

Les données à saisir pour la création de cette autorité sont les suivantes :
* Chemin : Niveau de hierarchie de l'autorité (ici "GE")
* Nom : Libellé de l'autorité compétente
* Informations : Informations complémentaires de l'autorité compétente
    * parent :  Niveau de hierarchie de l'autorité parente (ici chaîne vide)]
    * users : Liste des utilsateurs référents ou simples liés à l'autorité compétente. Un utilisateur est référencé par son identifiant et ses rôles ("referent" ou <liste vide>)

\subsection autorite_fille Autorité "fille"

Il s'agit ici de créer une autorité appelée "DRAAF75" en lien avec l'autorité Guichet-Entreprises

![Autorité DRAAF75](@ref autorite_draaf.jpg)

Les données à saisir pour la création de cette autorité sont les suivantes :
* Chemin : Niveau de hierarchie de l'autorité (ici "GE/DRAAF/DRAAF75")
* Nom : Libellé de l'autorité compétente
* Informations : Informations complémentaires de l'autorité compétente
    * parent :  Niveau de hierarchie de l'autorité parente (renseigné automatiquement après validation du formulaire)
    * users : Liste des utilsateurs référents ou simples liés à l'autorité compétente. Un utilisateur est référencé par son identifiant et ses rôles ("referent" ou <liste vide>).
    * Autres informations : saisie libre

