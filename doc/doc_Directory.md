Documentation Directory                         {#mainpage}
============

\tableofcontents

\section intro_sec Introduction

Bienvenue dans la documentation Directory de Guichet-Entreprises !

# Utilisation
## Gestion des autorités compétentes : 
  \ref ADD_AUTHORITY "Ajouter une autorité"

# Référentiel générique
[Référentiel générique](generic_repositories.md)

# Gestion des autorités
## Définition des champs de la table authority 

- **id** : identifiant technique lié à la base de données
- **entity_id** : nom de référence de l'entité
- **label** : nom de l'autorité (exemple CCI MOULINS-VICHY - CFE DE VICHY) <br/>
- **details** : un champ Json qui contient les informations détaillées de l'autorité <br/>
- **updated** : un champ time stamp qui contient le date time de la mise à jour de l'autorité <br/>

### Définition du champ details
 
Le champ *details* de la table authority contient les détails de l'autorité compétente, il est sous format Json, défini comme suit : 
- **ediCode**: code EDI de l'autorité compétente 
- **parent** : path du parent ; ex: "GE","GE/CMA", "GE/CCI/", ""
- **profile**: le profil de l'autorité compétente, elle contient les champs suivants
  -	**tel**: numéro de téléphone de l'autorité compétente 
  -	**fax**: numéro de fax de l'autorité compétente 
  - **email**:adresse e-mail de l'autorité compétente 
  - **url**: url du site web de l'autorité compétente
  - **observation**: informations complémentaires 
  - **address**: l'adresse postale contient les champs suivants : 
    - **recipientName**: le nom postal de l'autorité compétente
    - **addressName**: une concaténation de plusieurs champs : numéro de la voie, indice de répétition si présent, type de la voie et nom de la voie, (type de voie ; ALL : ALLEE, AV : AVENUE, BD : BOULEVARD,... réf : table supra_type_voie de la base ge_referentiel) 
    - **addressNameCompl**: complément d'adresse ; par ex: *Bâtiment H, allée 4, entrée 2*
    - **special**: distribution spéciale ; par ex: *BP 123*
    - **postalCode**: code postal 
    - **cedex**: cedex 
    - **cityNumber**: le code commune ; le nom de la commune n'est pas utile car il est résolu avec le référentiel des communes
- **notifications**: une section qui contient la liste des adresses courriel à notifier quand un dossier est déposé dans le backoffice
- **users**: une section qui contient la liste des utilisateurs liés à l'autorité compétente avec leurs rôles 
  - **roles**: liste des rôles d'un utilisateur pour une autorité compétente (*referent* : utilisateur référent, *user* : utilisateur simple, *accountant* : utilisateur référent du système de paiement), un même utilisateur peut avoir plusieurs rôles pour une même autorité compétente 
  - **id**: identifiant de l'utilisateur dans Account 
- **payment**: section contenant les informations relative au système de paiement
  - **paymentPartnerId**: identifiant de l'autorité compétente généré par le système de paiement 
- **transferChannels**: les préférences de l'utilisateur pour ses canaux d'envoi des dossiers, chaque canal décrit ci-dessous contient un champ "state" qui contient la valeur "enabled" / "disabled", selon que l'utilisateur a choisi et configuré le canal de son choix.
  - **backoffice**: contient seulement le champ "state" pour savoir si l'utilisateur a choisit ce canal ou pas.
    - **state** : [enabled|disabled]
  - **email**: le canal courrier électronique
    - **emailAddress** : l'adresse e-mail que le référent a renseigné lors de la configuration du canal courriel.
    - **state** : [enabled|disabled]
  - **address**:l'adresse que le référent a configuré pour l'envoi du dossier via  le canal courrier.
    - **addressDetail** : le détail de l'adresse
      - **recipientName**: Nom du destinataire
      - **recipientNameCompl**: Complément du nom du destinataire
      - **addressName**: adresse de destination
      - **addressNameCompl**: complément d'adresse du destinataire
      - **cityName**: ville du destinataire
      - **postalCode**: code postal du destinataire 
    - **state** : [enabled|disabled]
  - **ftp**: le canal FTP géré par EDDIE.
    - **token** : l'identifiant du  canal ftp généré par EDDIE.
    - **ftpMode** : Le mode de configuration choisie pour le canal FTP [delegatedChannel|pushPassword|pushKey|pullPassword|pullKey].
    - **pathFolder** : Le répértoire que le référent a renseigné lors de la configuration du canal ftp.
    - **comment** : Un commentaire contenant des informations sur la configuration.
    - **delegationAuthority** : Dans le cas de l'utilisation du canal ftp d'une autre autorité, l'entityId de celle-ci doit figurer dans ce champs là (cas de ftpMode = delegatedChannel)
    - **token** : L'identifiant du canal FTP configuré
    - **state** : [enabled|disabled]

Ci dessous un exemple d'un Json contenant des informations d'une autorité compétente  <br/>

~~~~~~~~~~~~~{.json}
{
    "ediCode": "X7501",
    "parent": "GE/CA",
    "profile": {
        "tel": "0139234239",
        "fax": "0139234242",
        "email": "cfe@ile-de-france.chambagri.fr",
        "url": "http://www.ile-de-france.chambagri.fr/",
        "observation": "",
        "address": {
            "recipientName": "CA DE L'ILE DE FRANCE",
            "addressNameCompl": "2 AV JEANNE D'ARC",
            "compl": "",
            "special": "",
            "postalCode": "78153",
            "cedex": "",
            "cityNumber": "78158"
        }
    },
    "notifications": [
		"toto@yopmail.com",
		"tata@yopmail.com"
	],
    "users": [
        {
            "roles": [
                "referent",
                "user",
                "all"
            ],
            "id": "2018-01-RDH-KRW-24",
            "email": "admin-GP-qual@yopmail.com"
        },
        {
            "roles": [
                "referent",
                "user",
                "all"
            ],
            "id": "2018-03-KPH-YMV-30",
            "email": "arnaud.boidard@gmail.com"
        }
    ],
    "payment": {
        "paymentPartnerId": "8fa92a4d-bd9e-45b6-b3d1-c9f8d459c022"
    },
    "transferChannels": {
        "backoffice": {
            "state": "disabled"
        },
        "email": {
            "emailAddress": "caIleFrance@yopmail.com",
            "state": "disabled"
        },
        "address": {
            "addressDetail": {
                "recipientName": "Ca de l'ile de france",
                "recipientNameCompl": "",
                "addressName": "Rue du general de gaulle",
                "addressNameCompl": "2 AV JEANNE D'ARC",
                "postalCode": "Paris",
                "cityName": "75001"
            },
            "state": "disabled"
        },
        "ftp": {
            "state": "enabled",
            "ftpMode": "pushKey",
            "pathFolder": "/eddie/testPathFolder",
            "comment": "FTP SERVER URL : 2.3.4.5:22",
            "delegationAuthority": "CMA31555",
            "token": "79b7c1ba-543c-4f1a-afda-ca0782f9c00b"
        }
    }
}
~~~~~~~~~~~~~

## Algorithme
### Calcul de l'autorité destinataire 

Pour calculer le destinataire et récupérer son identifiant il suffit d’appeler le web service de calcule des destinataires, comme dans l’exemple suivant 

~~~~~~~~~~~~~{.js}



////////////////////////////////
/// CALCUL DU DESTINATAIRE
////////////////////////////////


//Informations à récupérer de la formalité pour calculer le destinataire 

var secteur1 = "COMMERCIAL"; // secteur d'activité principal

var secteur2 = "ARTISANAL"; // secteur d'acitivité secondaire
 
var typePersonne = "PP";  // type de personne (personne physique : PP, personne morale : PM)

var formJuridique = "EIRL"; // forme juridique  ('AE', 'AEIRL', 'EI', 'EIRL')

var optionCMACCI = "NON"; // nombre de salariés supérieur à 10 (OUI, NON)

var codeCommune = "13208"; // code commune

// nom de l'algo a appeler 
var algo = "trouver Cfe"; // nom de l'algorithme appelé


//préparation de l'appel 
var args = {
    "secteur1" : secteur1,
	"secteur2" : secteur2,
	"typePersonne" : typePersonne,
	"formJuridique" : formJuridique,
	"optionCMACCI":optionCMACCI,
	"codeCommune": codeCommune
};

//appel à l'algo de directory pour calculer le destinataire
var response = nash.service.request('${directory.baseUrl}/v1/algo/{code}/execute',algo) //
        .connectionTimeout(10000) //
        .receiveTimeout(10000)
        .accept('text') //
        .post(args);
		

// Récupérer l'identifiant de l'autorité 
var codeAuthority = response.asString();


~~~~~~~~~~~~~

### Récupérer les données d'une autorité avec son ID 

Pour récupérer les données d'une autorité compétente il suffit d'appeler le web service de récupération d'une autorité et d'exploiter le retour comme dans l'exemple suivant


~~~~~~~~~~~~~{.js}

/////////////////////////////////////////////////////////
/// Récupération des informations d'une autorité
/////////////////////////////////////////////////////////



// identifiant de l'autorité récupéré dans l'étape de calcule
var destFuncId = codeAuthority;

// faire appel à directory avec l'identifiant de l'autorité pour récupérer toutes les informations

var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', destFuncId) //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
	           .get();

// récupérer le résultat dans un objet
			   
var receiverInfo = response.asObject();

// récupérer les détails de l'autorités (toutes les information) dans un json 

var contactInfo = !receiverInfo.contactInfo ? null : JSON.parse(receiverInfo.contactInfo);


//Récupérer les informations nécessaire, exemple ici : le code EDI

var ediCode = !contactInfo.ediCode ? null : contactInfo.ediCode;
~~~~~~~~~~~~~
