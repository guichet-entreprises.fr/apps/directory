# Directory : Documentation of the web services
Documentation des services web de directory

## web service to compute the actif channel of an authority

**Version:** 1.1

**BaseUrl:**  

property : ${ws.directory.private.url}

value : http://directory-ws.dev.guichet-partenaires.loc:12030/api/private/v1

** Interface contract's Path **

/services/authority/{entityId}/channel/active

** Interface contract ** 

JsonNode computeActiveChannel(@PathParam("entityId") String entityId) throws JsonProcessingException;

**Parameters**

| Name 		| Description 			| Required |
| ---- 		| ---------- 			| -------- | 
|entityId	| Authority Entity Id	|  Yes     |

** Example ** 

- Call of the ws on local : 

http://localhost:30080/ge-directory-ws-server/api/private/v1/services/authority/2018-06-VMB-AM7501/channel/active

- Result :

-- Case of when there is no authority with entityID given as a parameter: 

	Etat HTTP 404 - no configuration of the transferChannels for the authority with the entityId {entityId}
	
-- Case of when thedelegated authority have the same entityId as a previous authority (Loop case): 

	Etat HTTP 503 - The delegated Authority that have the entityId {entityId}, have the same entityId of an authority that was referred as a delegation authority which is not correct.

-- Case of when there is no EntityID found in a delegated ftp configuration: 

	Etat HTTP 404 - No delegated authority entityID found in the ftp configuration for the authority with the entityId {entityId}.
	
-- Case of when there is no delegated authority with entityID mentioned in the ftp configuration: 

	Etat HTTP 404 - No delegated authority exists with the entityId {entityId}
	
-- Case of when there is no enabled ftp configuration in the delegated authority : 

	Etat HTTP 404 - The delegated Authority that have the entityId {entityId}, have no Ftp channel enabled. 
	
-- Case of when there is no transferChannel bloc in the delegated authority : 

	Etat HTTP 404 - The delegated Authority that have the entityId {entityId}, have no configuration set. 	

-- Case of no configuration present in the authority: 

	Etat HTTP 404 - no configuration of the transferChannels for the authority with the entityId {entityId}

-- Case of no ENABLED channel in the transfer channel bloc :

	Etat HTTP 400 - the channels configured are not Activated for the authority that have entityId : {entityId}

-- Case of an enabled configuration : 

	Etat HTTP 200 - 
	
	Will return the bloc present in the transferChannel directory + a field channel that contains one of the following values depending on the enabled channel : backOffice, email, address, ftp. 
	
	Example of the JSON returned when the email channel is configured & enabled: 
	
	{
    "emailAddress": "kamal@yopmail.com",
    "state": "enabled",
    "channel": "email"
	}
	
	Example of the JSON returned when the ftp channel is configured & enabled (either there is a redirection or without redirection):
	
	{
	  "state": "enabled",
	  "ftpMode": "delegatedChannel",
	  "pathFolder": "/eddie/testPathFolder",
	  "comment": null,
	  "delegationAuthority": "2018-06-VMB-XXXXXX",
	  "token": "79b7c1ba-543c-4f1a-afda-ca0782f9c00b",
	  "channel": "ftp"
	}
	
	PS : In this example, the ftpMode is equal to "delegatedChannel", because there was a redirection in the configured authority, once we found the token and pathFolder we update the inital configuration and return it.
	