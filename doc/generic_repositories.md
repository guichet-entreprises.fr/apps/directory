# Les référentiels génériques
Les référentiels génériques sont des référentiels de données qui utilisent la même structure de données et les mêmes outils de gestion.

# Structure de stockage

# Outils d'accès
## Web services CRUD

## Détail sur le web service de recherche
Le web service de recherche comporte plusieurs catégories d'éléments :
- un filtre sur le référentiel 
- une contrainte sur le nombre d'éléments retournés avec gestion de la pagination
- une liste de filtres sur les éléments
- une liste de tris à appliquer sur le résultat
~~~~
  Exemple d'URL de recherche
  http://localhost:10080/ge-directory-ws-server/api/v1/repository/authority?startIndex=0&maxResults=20&filters=details.transferChannels.email..emails~cma-paris
~~~~

### Le filtre du référentiel
Il est obligatoire, il fait parti de l'URL de recherche

### la contrainte sur le nombre d'élément retournés
Il est important de ne pas pouvoir charger toute la base de données, il existe une contrainte logique permettant de préciser le nombre d'éléments que l'on veut récupérer. Il y a une gestion de la pagination.\
Paramètre de la requête : 
- startIndex : n-ième page de la pagination
- maxResults : nombre maximal de résultat sur la page

### une liste de filtres
La liste des filtres est une liste de paramètre de filtre tel que : 
- filters = _dataToFilter_-_operateur_-_valeur_
avec : 
- _dataToFilter_ qui est structuré de la façon suivante : champ[[.|..]champ] \
  L'opérateur _._ correspond à l'accès à un sous champ \
  L'opérateur _.._ correspond à l'accès à un sous champ de type tableau \
  Les sous-champs sont utilsable dans les champs de type JSON.
- _operateur_ dans la liste suivante :
  - _:_ : opérateur égal (=)
  - _>_ : opérateur supérieur à
  - _>=_ : opérateur supérieur ou égal à
  - _<_ : opérateur inférieur à
  - _>=_ : opérateur inférieur ou égal à
  - _~_ : opérateur est contenu dans (texte)
  - _~:_ : opérateur commence par (texte)
  - _:~_ : opérateur termine par (texte)

### une liste de tri
Le tri des éléments est uniquement par défaut pour le moment et se fait par ordre alphanumérique sur les champs suivants : 
- repositoryId
- label
- entityId



