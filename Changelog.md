# Changelog

Ce fichier contient les modifications techniques du module.

- Projet : [Directory](https://tools.projet-ge.fr/gitlab/minecraft/ge-directory)

## [2.16.1.0] - 2020-09-11

###  Ajout

- Modifier le logo de la bannière

__Properties__ :

### Modification

Concerne le module directory-ui :

| fichier   |      nom      | nouvelle valeur |
|-----------|:-------------:|----------------:|
| application.properties | ui.theme.default | default |

## [2.13.2.1] - 2020-01-07
- Ajout Algorithme "trouver RM"

## [2.13.2.0] - 2020-01-04
- Evol d'API de recherche des autorités

## [2.12.5.1] - 2019-11-29
- Mise à jour des nouveaux layouts et Ajout des bandeau bleu

###  Ajout

  __Properties__ :

Concerne uniquement l'application : directory-ui

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | ui.theme.default | Version du thème | base-v3 |
| application.properties | ui.theme.defaultSubTheme | Thème appliqué | guichet-partenaires |


## [2.12.4.0] - 2019-11-05

###  Corrections
- Mise à jour de l'algorithme de génération du nom de la liasse à transmettre aux autorités compétentes

## [2.11.9.0] - 2019-08-01

###  Corrections
Faille de sécurité injection SQL sur les services de recherche MINE-889

## [2.11.3.0] - 2019-05-23
---

###  Corrections
- Gestion de la mise à jour du block users via une formalité


## [2.11.2.1] - 2019-05-13
---

### Ajout 

- Nouveau fichier _Changelog.md_

###  Corrections
- suppression du bloc payment de la configuration directory

