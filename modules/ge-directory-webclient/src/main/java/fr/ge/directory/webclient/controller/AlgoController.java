/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.webclient.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.core.exception.FunctionalException;
import fr.ge.core.rest.exception.RestResponseException;
import fr.ge.directory.ws.v1.bean.ResponseAlgoBean;
import fr.ge.directory.ws.v1.service.IAlgoService;

/**
 * {@link ResponseAlgoBean} search controller.
 *
 * @author Christian Cougourdan
 */
@Controller
@RequestMapping("/algo")
public class AlgoController extends AbstractSearchController<ResponseAlgoBean> {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AlgoController.class);

    /** Algo service. */
    @Autowired
    private IAlgoService service;

    /**
     * Edits an algorithm.
     *
     * @param model
     *            the model
     * @return the template name
     */
    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public String edit(final Model model) {
        model.addAttribute("bean", new ResponseAlgoBean());

        return "algo/edit/main";
    }

    /**
     * Edits an algorithm.
     *
     * @param model
     *            the model
     * @param id
     *            the id
     * @return the template name
     */
    @RequestMapping(value = "/{id:\\d+}/view", method = RequestMethod.GET)
    public String edit(final Model model, @PathVariable("id") final long id) {
        final ResponseAlgoBean bean = this.service.get(id);

        model.addAttribute("bean", bean);

        return "algo/edit/main";
    }

    /**
     * Updates an algorithm.
     *
     * @param model
     *            the model
     * @param beanFromRequest
     *            the bean
     * @return the redirect URL
     */
    @RequestMapping(value = { "", "/" }, method = RequestMethod.POST)
    public String update(final Model model, final ResponseAlgoBean beanFromRequest) {
        this.service.save(beanFromRequest);

        return "redirect:/algo";
    }

    /**
     * Updates an algorithm.
     *
     * @param model
     *            the model
     * @param id
     *            the id
     * @param beanFromRequest
     *            the bean
     * @return the redirect URL
     */
    @RequestMapping(value = "/{id:\\d+}", method = RequestMethod.POST)
    public String update(final Model model, @PathVariable("id") final Long id, final ResponseAlgoBean beanFromRequest) {
        this.service.save(beanFromRequest);

        return "redirect:/algo";
    }

    /**
     * Removes an algorithm.
     *
     * @param id
     *            the id
     * @return the redirect URL
     */
    @RequestMapping(value = "/{id:\\d+}/remove", method = RequestMethod.GET)
    public String remove(@PathVariable("id") final Long id) {
        this.service.remove(id);
        return "redirect:/algo";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String templatePrefix() {
        return "algo";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SearchResult<ResponseAlgoBean> search(final SearchQuery query) {
        return this.service.search(query.getStartIndex(), query.getMaxResults(), query.getFilters(), query.getOrders());
    }

    /**
     * Prepares the algorithms to import.
     *
     * @param model
     *            the model
     * @return the algorithms display page
     */
    @RequestMapping(value = "/import", method = RequestMethod.GET)
    public String prepareImport(final Model model) {
        return "algo/import/main";
    }

    /**
     * Uploads algorithms.
     *
     * @param model
     *            the model
     * @return the redirect string
     */
    @RequestMapping(method = RequestMethod.POST, path = "/import")
    public String upload(final Model model, final HttpServletRequest request, final RedirectAttributes redirectAttributes, //
            @RequestParam("algorithms") final MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                this.service.importAlgo(file.getBytes(), true);
            } catch (final FunctionalException | RestResponseException | IOException ex) {
                LOGGER.error(String.format("An error occurs when sending the file for importation : %s.", ex.getMessage()), ex);
                return "algo/import/errors";
            }
        }
        return "redirect:/algo";
    }
}
