/**
 * 
 */
package fr.ge.directory.webclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.ws.v1.bean.ResponseRefPriceBean;
import fr.ge.directory.ws.v1.service.IRefService;

/**
 * @author $Author: sarahman $
 * @version $Revision: 0 $
 */

@Controller
@RequestMapping("/referentiels/prices")
public class PriceController extends AbstractSearchController<ResponseRefPriceBean> {

    @Autowired
    private IRefService service;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String templatePrefix() {

        return "referentiels/prices";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SearchResult<ResponseRefPriceBean> search(SearchQuery query) {
        return service.searchPrice(query.getStartIndex(), query.getMaxResults(), query.getFilters(), query.getOrders(), query.getSearchTerms());
    }

}
