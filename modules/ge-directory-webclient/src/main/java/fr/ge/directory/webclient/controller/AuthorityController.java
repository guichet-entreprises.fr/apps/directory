package fr.ge.directory.webclient.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.QueryParam;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.core.rest.exception.RestResponseException;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityBean;
import fr.ge.directory.ws.v1.service.IAuthorityService;
import fr.ge.directory.ws.v1.service.IImportAuthoritiesWebService;

/**
 * Authority controller.
 *
 * @author Christian Cougourdan
 */
@Controller
@RequestMapping("/authority")
public class AuthorityController extends AbstractSearchController<ResponseAuthorityBean> {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorityController.class);

    /** Authority service. */
    @Autowired
    private IAuthorityService service;

    @Autowired
    IImportAuthoritiesWebService iImportAuthoritiesWebService;

    /**
     * Edits an authority.
     *
     * @param model
     *            the model
     * @return the template name
     * @throws IOException
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public String edit(final Model model) throws JsonProcessingException, IOException {
        final ResponseAuthorityBean authority = new ResponseAuthorityBean();
        ObjectMapper mapper = new ObjectMapper();
        authority.setDetails(mapper.readTree("{}"));

        model.addAttribute("bean", authority);

        return "authority/edit/main";
    }

    /**
     * Edits an authority.
     *
     * @param model
     *            the model
     * @param id
     *            the id
     * @return the template name
     */
    @RequestMapping(value = "/{id:\\d+}/view", method = RequestMethod.GET)
    public String edit(final Model model, @PathVariable("id") final long id) {
        final ResponseAuthorityBean bean = this.service.get(id);

        model.addAttribute("bean", bean);

        return "authority/edit/main";
    }

    /**
     * Prepares the authorities import.
     *
     * @param model
     *            the model
     * @return the authorities display page
     */
    @RequestMapping(value = "/import", method = RequestMethod.GET)
    public String prepareImport(final Model model) {
        return "authority/import/main";
    }

    /**
     * Actually import the authorities.
     * 
     * @param autorities
     *            the authorities file
     * @param dryRun
     *            is ti a dry run, false by default
     * @return the authorities display page
     */
    @RequestMapping(method = RequestMethod.POST, path = "/import")
    public String importAuthorities(@RequestParam("autorities") final MultipartFile autorities, //
            @QueryParam("dryRun") final boolean dryRun) {

        try {
            List<Attachment> attachments = new ArrayList<Attachment>() {
                /** serial */
                private static final long serialVersionUID = -1473854898356828037L;

                {
                    add(new Attachment(autorities.getName(), autorities.getInputStream(), null));
                }
            };
            String result = iImportAuthoritiesWebService.importAuthorities(attachments, dryRun);
            LOGGER.info("Imported authorities : {}.", result);
        } catch (RestResponseException | IOException e) {
            LOGGER.error(String.format("An error occurs when sending the file for importation : %s.", e.getMessage()), e);
            // TODO gérer les erreurs à afficher sur la page
        }

        return "redirect:/authority/import";
    }

    /**
     * Updates an authority.
     *
     * @param model
     *            the model
     * @param id
     *            the id
     * @param beanFromRequest
     *            the bean
     * @return the redirect URL
     */
    @RequestMapping(value = { "", "/", "/{id:\\d+}" }, method = RequestMethod.POST)
    public String update(final Model model, @PathVariable(value = "id", required = false) final Long id, final ResponseAuthorityBean beanFromRequest) {

        if (StringUtils.isBlank(beanFromRequest.getEntityId())) {
            if (StringUtils.isNotBlank(beanFromRequest.getPath())) {
                // generate the functional identifier from the path
                final String[] pathSplit = beanFromRequest.getPath().split("/");
                beanFromRequest.setEntityId(pathSplit[pathSplit.length - 1]);
            }
        }
        LOGGER.info("Update the entityId {} from the path {}, and sending the update to the WS.", beanFromRequest.getEntityId(), beanFromRequest.getPath());
        Long authorityId = this.service.save(beanFromRequest);

        return String.format("redirect:/authority/%s/view", authorityId);
    }

    /**
     * Removes an authority.
     *
     * @param id
     *            the id
     * @return the redirect URL
     */
    @RequestMapping(value = "/{id:\\d+}/remove", method = RequestMethod.GET)
    public String remove(@PathVariable("id") final Long id) {
        this.service.remove(id);
        return "redirect:/authority";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String templatePrefix() {
        return "authority";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SearchResult<ResponseAuthorityBean> search(final SearchQuery query) {
        return this.service.search(query.getStartIndex(), query.getMaxResults(), query.getFilters(), query.getOrders(), query.getSearchTerms());
    }

}
