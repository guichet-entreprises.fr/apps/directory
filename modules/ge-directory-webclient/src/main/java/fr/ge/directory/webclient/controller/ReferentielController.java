/**
 *
 */
package fr.ge.directory.webclient.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.ws.v1.bean.ResponseReferentialItemBean;
import fr.ge.directory.ws.v2.service.IRefService;

/**
 * @author bsadil
 *
 */
@Controller
// @RequestMapping(value = "/referentiels")
public class ReferentielController extends AbstractSearchRefController<JsonNode> {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReferentielController.class);

    @Autowired
    private IRefService refRestServiceV2;

    /**
     * Edits an algorithm.
     *
     * @param model
     *            the model
     * @return the template name
     */
    @RequestMapping(method = RequestMethod.GET, value = "/referentiels")
    public String edit(final Model model, @ModelAttribute("refName") final String refName) {
        model.addAttribute("refName", refName);

        return "referentiels/referentiels";
    }

    @RequestMapping(value = "/referentiels/{refName}/view/{id:\\d+}", method = RequestMethod.GET)
    public String edit(final Model model, @PathVariable("id") final long id, @PathVariable("refName") final String refName) {
        final ResponseReferentialItemBean bean = this.refRestServiceV2.get(id, refName);

        model.addAttribute("bean", bean);
        model.addAttribute("refName", refName);

        return "referentiels/edit/main";
    }

    @RequestMapping(value = "/referentiels/{refName}", method = RequestMethod.GET)
    public String display(final Model model, @PathVariable("refName") final String refName) {

        model.addAttribute("refName", refName);
        return "referentiels/search/main";
    }

    /**
     * Uploads a referential data.
     *
     * @param model
     *            the model
     * @return the redirect string
     */
    @RequestMapping(method = RequestMethod.POST, path = "/referentiels/upload")
    public String upload(final Model model, final HttpServletRequest request, final RedirectAttributes redirectAttributes, //
            @RequestParam("file") final MultipartFile file, @QueryParam("refName") final String refName, @QueryParam("truncate") final boolean truncate) {
        model.addAttribute("refName", refName);
        if (StringUtils.endsWithIgnoreCase(file.getOriginalFilename(), ".csv")) {
            redirectAttributes.addAttribute("refName", refName);
            List<JsonNode> items = null;
            try (Scanner scanner = new Scanner(file.getInputStream(), StandardCharsets.UTF_8.name());) {
                final Map<Integer, String> columnNames = this.getColumnNames(scanner);
                if (columnNames.size() > 0) {
                    items = this.readItems(scanner, columnNames);
                }
            } catch (final IOException e) {
                LOGGER.warn("Error reading the CSV file {}", file.getName(), e);
            }
            if (truncate) {
                this.refRestServiceV2.truncate(refName);
                LOGGER.debug("All data are truncate for {}", refName);
            }
            this.refRestServiceV2.save(refName, items);
        }
        return "redirect:/referentiels";
    }

    /**
     * Gets the CSV column names.
     *
     * @param scanner
     *            the scanner
     * @return the column names
     */
    private Map<Integer, String> getColumnNames(final Scanner scanner) {
        final Map<Integer, String> columnNames = new HashMap<>();
        if (scanner.hasNextLine()) {
            final String[] columns = scanner.nextLine().split(";");
            for (int i = 0; i < columns.length; ++i) {
                columnNames.put(i, columns[i]);
            }
        }
        return columnNames;
    }

    /**
     * Reads CSV items.
     *
     * @param scanner
     *            the scanner
     * @param columnNames
     *            the column names
     * @return the items
     */
    private List<JsonNode> readItems(final Scanner scanner, final Map<Integer, String> columnNames) {

        final List<JsonNode> items = new ArrayList<JsonNode>();

        while (scanner.hasNextLine()) {
            final ObjectNode item = JsonNodeFactory.instance.objectNode();
            final String line = scanner.nextLine();
            final String[] columns = line.split(";");
            for (int i = 0; i < columnNames.size(); ++i) {
                JsonNode column;
                try {
                    if (i < columns.length) {
                        column = this.mapper.readTree(columns[i]);
                    } else {
                        column = JsonNodeFactory.instance.textNode(StringUtils.EMPTY);
                    }
                } catch (final IOException e) {
                    LOGGER.info("Parsing column value [{}] : {}", columns[i], e.getMessage());
                    column = JsonNodeFactory.instance.textNode(columns[i]);
                }
                item.set(columnNames.get(i), column);
            }
            items.add(item);
        }
        return items;
    }

    private final ObjectMapper mapper = new ObjectMapper();

    /**
     * {@inheritDoc}
     */
    @Override
    protected String templatePrefix() {
        return "referentiels";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SearchResult<JsonNode> search(final String refName, final SearchQuery query) {
        final SearchResult<JsonNode> search = new SearchResult<>();
        if (StringUtils.isEmpty(refName)) {
            return search;
        }
        return this.refRestServiceV2.findAll(refName, query.getStartIndex(), query.getMaxResults(), query.getFilters(), query.getOrders(), query.getSearchTerms());
    }

}
