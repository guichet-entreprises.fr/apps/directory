/**
 * 
 */
package fr.ge.directory.webclient.support.converter;

import java.io.IOException;

import org.springframework.core.convert.converter.Converter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.utils.exception.TechnicalException;

/**
 * @author $Author: sarahman $
 * @version $Revision: 0 $
 */
public class StringToJsonNodeConverter implements Converter<String, JsonNode> {

    /**
     * {@inheritDoc}
     */
    @Override
    public JsonNode convert(String source) {

        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode actualObj = mapper.readTree(source);
            return actualObj;
        } catch (TechnicalException | IOException e) {
            throw new TechnicalException();

        }

    }

}
