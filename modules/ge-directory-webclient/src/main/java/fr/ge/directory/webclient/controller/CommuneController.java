/**
 * 
 */
package fr.ge.directory.webclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.ws.v1.bean.ResponseRefCommuneBean;
import fr.ge.directory.ws.v1.service.IRefService;

/**
 * @author bsadil
 *
 */
@Controller
@RequestMapping("/referentiels/communess")
public class CommuneController extends AbstractSearchController<ResponseRefCommuneBean> {

    @Autowired
    private IRefService service;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String templatePrefix() {
        return "referentiels/communes";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SearchResult<ResponseRefCommuneBean> search(SearchQuery query) {

        return this.service.search(query.getStartIndex(), query.getMaxResults(), query.getFilters(), query.getOrders(), query.getSearchTerms());

    }

}
