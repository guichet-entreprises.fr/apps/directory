/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.webclient.bean.legacy;

/**
 * Legacy referential element.
 *
 * @author jpauchet
 */
public class LegacyRefElement {

    /** EDI code. */
    private String ediCode;

    /** Département. */
    private String dep;

    /** Commune. */
    private String commune;

    /** Other authorities at the same place ?. */
    private boolean others;

    /** Number of other authorities. */
    private String nbOthers;

    /** Name of other authorities. */
    private String nameOthers;

    /**
     * Constructor.
     */
    public LegacyRefElement() {
        // Nothing to do.
    }

    /**
     * Gets the edi code.
     *
     * @return the edi code
     */
    public String getEdiCode() {
        return this.ediCode;
    }

    /**
     * Sets the edi code.
     *
     * @param ediCode
     *            the new edi code
     */
    public LegacyRefElement setEdiCode(final String ediCode) {
        this.ediCode = ediCode;
        return this;
    }

    /**
     * Gets the dep.
     *
     * @return the dep
     */
    public String getDep() {
        return this.dep;
    }

    /**
     * Sets the dep.
     *
     * @param dep
     *            the new dep
     */
    public LegacyRefElement setDep(final String dep) {
        this.dep = dep;
        return this;
    }

    /**
     * Gets the commune.
     *
     * @return the commune
     */
    public String getCommune() {
        return this.commune;
    }

    /**
     * Sets the commune.
     *
     * @param commune
     *            the new commune
     */
    public LegacyRefElement setCommune(final String commune) {
        this.commune = commune;
        return this;
    }

    /**
     * Checks if is others.
     *
     * @return true, if is others
     */
    public boolean isOthers() {
        return this.others;
    }

    /**
     * Sets the others.
     *
     * @param others
     *            the new others
     */
    public LegacyRefElement setOthers(final boolean others) {
        this.others = others;
        return this;
    }

    /**
     * Gets the nb others.
     *
     * @return the nb others
     */
    public String getNbOthers() {
        return this.nbOthers;
    }

    /**
     * Sets the nb others.
     *
     * @param nbOthers
     *            the new nb others
     */
    public LegacyRefElement setNbOthers(final String nbOthers) {
        this.nbOthers = nbOthers;
        return this;
    }

    /**
     * Gets the name others.
     *
     * @return the name others
     */
    public String getNameOthers() {
        return this.nameOthers;
    }

    /**
     * Sets the name others.
     *
     * @param nameOthers
     *            the new name others
     */
    public LegacyRefElement setNameOthers(final String nameOthers) {
        this.nameOthers = nameOthers;
        return this;
    }

}
