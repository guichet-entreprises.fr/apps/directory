/**
 *
 */
package fr.ge.directory.webclient.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.webclient.bean.datatable.DatatableSearchQuery;
import fr.ge.directory.webclient.bean.datatable.DatatableSearchResult;
import fr.ge.directory.ws.v1.model.RepositoryElement;
import fr.ge.directory.ws.v1.service.RepositoryWebService;

/**
 * Generic access to the repositories.
 */
@Controller
public class RepositoryController {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryController.class);

    @Autowired
    private RepositoryWebService repositoryWebService;

    /**
     * Access to the repository page.
     *
     * @param model
     *            the model
     * @return the template name
     */
    @RequestMapping(method = RequestMethod.GET, value = "/repository")
    public String edit(final Model model, @ModelAttribute("refName") final String refName) {

        List<String> listRepositoryId = new ArrayList<String>();

        listRepositoryId = repositoryWebService.listRepositories();

        model.addAttribute("listRepositoryId", listRepositoryId);
        model.addAttribute("refName", refName);
        return "repository/repository";
    }

    /**
     * Access to add (import a generic repository) repository page.
     *
     * @param model
     *            the model
     * @return the template name
     */
    @RequestMapping(value = "/repository/addRepository", method = RequestMethod.GET)
    public String addRepository(final Model model) {

        return "/repository/add/main";
    }

    /**
     * Display the details of a selected element.
     * 
     * @param model
     *            the model
     * @param id
     *            the id of the element
     * @param repositoryId
     *            id of the repository
     * @return the template name
     */
    @RequestMapping(value = "/repository/{refName}/view/{id:\\d+}", method = RequestMethod.GET)
    public String edit(final Model model, @PathVariable("id") final long id, @PathVariable("refName") final String repositoryId) {
        final RepositoryElement bean = this.repositoryWebService.read(repositoryId, id);

        model.addAttribute("bean", bean);
        model.addAttribute("refName", repositoryId);

        return "repository/edit/main";
    }

    /**
     * Display the elements of a repository.
     * 
     * @param model
     *            the model
     * @param refName
     *            the Id of the repository
     * @return the template name
     */
    @RequestMapping(value = "/repository/{refName}", method = RequestMethod.GET)
    public String display(final Model model, @PathVariable("refName") final String refName) {

        model.addAttribute("refName", refName);
        return "repository/search/main";
    }

    /**
     * Uploads a repository data.
     *
     * @param model
     *            the model
     * @return the redirect string
     */
    @RequestMapping(method = RequestMethod.POST, path = "/repository/upload")
    public String upload(final Model model, final HttpServletRequest request, final RedirectAttributes redirectAttributes, //
            @RequestParam("file") final MultipartFile file, @QueryParam("refName") final String refName, @QueryParam("truncate") final boolean truncate) {
        model.addAttribute("refName", refName);
        if (StringUtils.endsWithIgnoreCase(file.getOriginalFilename(), ".csv")) {
            redirectAttributes.addAttribute("refName", refName);
            List<RepositoryElement> items = null;
            try (Scanner scanner = new Scanner(file.getInputStream(), StandardCharsets.UTF_8.name());) {
                final Map<Integer, String> columnNames = this.getColumnNames(scanner);
                if (columnNames.size() > 0) {
                    items = this.readItems(scanner, columnNames);
                }
            } catch (final IOException e) {
                LOGGER.warn("Error reading the CSV file {}", file.getName(), e);
            }
            this.repositoryWebService.add(refName, truncate, items);
            LOGGER.debug("Upload data for the repository {} , the existing data are deleted ? {}", refName, truncate);
        }
        return "redirect:/repository";
    }

    /**
     * Gets the CSV column names.
     *
     * @param scanner
     *            the scanner
     * @return the column names
     */
    private Map<Integer, String> getColumnNames(final Scanner scanner) {
        final Map<Integer, String> columnNames = new HashMap<>();
        if (scanner.hasNextLine()) {
            final String[] columns = scanner.nextLine().split(";");
            for (int i = 0; i < columns.length; ++i) {
                columnNames.put(i, columns[i]);
            }
        }
        return columnNames;
    }

    /**
     * Reads CSV items.
     *
     * @param scanner
     *            the scanner
     * @param columnNames
     *            the column names
     * @return the items
     */
    private List<RepositoryElement> readItems(final Scanner scanner, final Map<Integer, String> columnNames) {

        final List<JsonNode> items = new ArrayList<JsonNode>();

        while (scanner.hasNextLine()) {
            final ObjectNode item = JsonNodeFactory.instance.objectNode();
            final String line = scanner.nextLine();
            final String[] columns = line.split(";");
            for (int i = 0; i < columnNames.size(); ++i) {
                JsonNode column;
                try {
                    if (i < columns.length) {
                        column = this.mapper.readTree(columns[i]);
                    } else {
                        column = JsonNodeFactory.instance.textNode(StringUtils.EMPTY);
                    }
                } catch (final IOException e) {
                    LOGGER.info("Parsing column value [{}] : {}", columns[i], e.getMessage());
                    column = JsonNodeFactory.instance.textNode(columns[i]);
                }
                item.set(columnNames.get(i), column);
            }
            items.add(item);
        }

        return extractRepositoryElemetsFromCsv(items);
    }

    private final ObjectMapper mapper = new ObjectMapper();

    /**
     * {@inheritDoc}
     */
    protected String templatePrefix() {
        return "repository";
    }

    /**
     * jQuery datatable search mapping.
     *
     * @param criteria
     *            jQuery datatable search criteria
     * @param filters
     *            the filters
     * @return search result object
     */
    @RequestMapping(value = "/repository/search/data", method = RequestMethod.GET)
    @ResponseBody
    public DatatableSearchResult<RepositoryElement> searchData(final DatatableSearchQuery criteria, @QueryParam("filter") final String[] filters, @QueryParam("refName") final String refName) {
        final DatatableSearchResult<RepositoryElement> datatableSearchResult = new DatatableSearchResult<>(criteria.getDraw());

        final SearchQuery query = new SearchQuery(criteria.getStart(), criteria.getLength());

        if (null != criteria.getOrder()) {
            final List<SearchQueryOrder> orders = criteria.getOrder().stream() //
                    .map(src -> new SearchQueryOrder(criteria.getColumns().get(src.getColumn()).getData(), src.getDir())) //
                    .collect(Collectors.toList());

            query.setOrders(orders);
        }

        if (StringUtils.isNotEmpty(criteria.getSearch().getValue())) {
            // query.setSearchTerms(criteria.getSearch().getValue());
            // added a label search, since the legacy one is deprecated
            query.addFilter("label", "~", criteria.getSearch().getValue());
        }
        if (null != filters) {
            for (final String filter : filters) {
                query.addFilter(filter);
            }
        }

        // Searching the elements
        final SearchResult<RepositoryElement> searchResult = this.search(refName, query);

        datatableSearchResult.setRecordsFiltered((int) searchResult.getTotalResults());
        datatableSearchResult.setRecordsTotal((int) searchResult.getTotalResults());
        datatableSearchResult.setData(Optional.ofNullable(searchResult.getContent()).orElse(Collections.emptyList()));

        return datatableSearchResult;
    }

    /**
     * Do the real search, call the search web service.
     */
    protected SearchResult<RepositoryElement> search(final String refName, final SearchQuery query) {
        final SearchResult<RepositoryElement> search = new SearchResult<>();
        if (StringUtils.isEmpty(refName)) {
            return search;
        }
        return this.repositoryWebService.search(refName, query.getStartIndex(), query.getMaxResults(), query.getFilters(), query.getOrders());
    }

    /**
     * Extract a list of #RepositoryElement frome a list of #JsonNode.
     * 
     * @param items
     *            a list of #JsonNode representig #RepositoryElement
     * @return a list of #RepositoryElement
     */
    private List<RepositoryElement> extractRepositoryElemetsFromCsv(List<JsonNode> items) {
        List<RepositoryElement> repositoryElements = new ArrayList<RepositoryElement>();

        for (final JsonNode jsonDetail : items) {
            final RepositoryElement repositoryElement = new RepositoryElement();
            final String label = Optional.ofNullable(jsonDetail) //
                    .filter(json -> json.has("label")) //
                    .map(json -> json.get("label").asText()) //
                    .orElseGet(() -> StringUtils.join(this.jsonValues(jsonDetail), StringUtils.SPACE));
            final String entityId = Optional.ofNullable(jsonDetail) //
                    .filter(json -> json.has("entityId")) //
                    .map(json -> json.get("entityId").asText()) //
                    .orElseGet(() -> StringUtils.join(this.jsonValues(jsonDetail), StringUtils.SPACE));
            repositoryElement.setLabel(label);
            repositoryElement.setEntityId(entityId);
            repositoryElement.setDetails(jsonDetail);
            repositoryElements.add(repositoryElement);
        }
        return repositoryElements;

    }

    private List<String> jsonValues(final JsonNode root) {
        final List<String> lst = new ArrayList<>();

        if (root.isValueNode()) {
            lst.add(root.asText());
        } else if (root.isContainerNode()) {
            root.forEach(node -> lst.addAll(this.jsonValues(node)));
        }

        return lst;
    }

}
