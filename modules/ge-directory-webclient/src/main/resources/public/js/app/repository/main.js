require([ 'jquery', 'lib/fileUpload' ], function($) {

    function showReferential(name) {
        $('#refName').val(name);
        var referentials = $('.referential');
        var referential = $('.referential[data-name="' + name + '"]');
        var uploadForm = $('.upload-form');
        referentials.removeClass('selected');
        referential.addClass('selected');
        if (referential.data('list')) {
            uploadForm.addClass('hidden');
            window.location.replace(referential.data('list'));
        } else {
            uploadForm.removeClass('hidden');
        }
    }

    var routes = {};
    function route(path, fn) {
        var re = '^#' + path + '$';
        routes[re] = {
            re : new RegExp(re),
            fn : fn
        };
    }

    route('show\/(.*)', function(m) {
    	console.log(window.location);
        showReferential(m[1]);
    });

    $(function () {
        $(window).on("hashchange", function(evt) {
            var hash = window.location.hash;
            Object.keys(routes).forEach(function (re) {
                var route = routes[re];
                var m = route.re.exec(hash);
                if (m) {
                    route.fn(m);
                    return false;
                }
            });
        });

        $('input[type="file"][data-toggle="file"]').fileUpload();

        var refName = $('#refName').val();
        if (refName) {
            window.location.hash = 'show/' + refName;
        } 
        
        var url = new URL(window.location.href);
        var refName = url.searchParams.get("refName");;
        console.log(refName);
        var $this = $('#upload'), url = window.location.origin + window.location.pathname +'/'+refName
        if(refName != null){
        $.ajax(url, {
            dataType : 'html'
        }).done(function(html) {
            $this.html(html);
            $('input[type="file"][data-toggle="file"]').fileUpload();
        }).fail(function(jqXHR, textStatus, errorThrown) {
            $this.html(jqXHR.responseText);
        }).always(function(jqXHR, textStatus) {
            console.log(textStatus);
        });
        }

        $('.referential').click(function() {
              window.location.hash = 'show/' + $(this).data('name');
//            $('#referentiel').DataTable().ajax.url(window.location.origin + window.location.pathname+'/search/data?refName='+$(this).data('name')).load();
        	   var refName = $(this).data('name');
               var $this = $('#upload'), url = window.location.origin + window.location.pathname +'/'+refName
               $.ajax(url, {
                   dataType : 'html'
               }).done(function(html) {
            	   console.log(this);
                   $this.html(html);
                   $('input[type="file"][data-toggle="file"]').fileUpload();
                   $('#repository').DataTable();
                   console.log(html);
               }).fail(function(jqXHR, textStatus, errorThrown) {
                   $this.html(jqXHR.responseText);
               }).always(function(jqXHR, textStatus) {
                   console.log(textStatus);
               });
        });
        
         
        
    });
    
    

});
