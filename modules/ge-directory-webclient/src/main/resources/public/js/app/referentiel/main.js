/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
require([ 'jquery', 'lib/fileUpload' ], function($) {

    function showReferential(name) {
        $('#refName').val(name);
        var referentials = $('.referential');
        var referential = $('.referential[data-name="' + name + '"]');
        var uploadForm = $('.upload-form');
        referentials.removeClass('selected');
        referential.addClass('selected');
        if (referential.data('list')) {
            uploadForm.addClass('hidden');
            window.location.replace(referential.data('list'));
        } else {
            uploadForm.removeClass('hidden');
        }
    }

    var routes = {};
    function route(path, fn) {
        var re = '^#' + path + '$';
        routes[re] = {
            re : new RegExp(re),
            fn : fn
        };
    }

    route('show\/(.*)', function(m) {
    	console.log(window.location);
        showReferential(m[1]);
    });

    $(function () {
        $(window).on("hashchange", function(evt) {
            var hash = window.location.hash;
            Object.keys(routes).forEach(function (re) {
                var route = routes[re];
                var m = route.re.exec(hash);
                if (m) {
                    route.fn(m);
                    return false;
                }
            });
        });

        $('input[type="file"][data-toggle="file"]').fileUpload();

        var refName = $('#refName').val();
        if (refName) {
            window.location.hash = 'show/' + refName;
        } 
        
        var url = new URL(window.location.href);
        var refName = url.searchParams.get("refName");;
        console.log(refName);
        var $this = $('#upload'), url = window.location.origin + window.location.pathname +'/'+refName
        if(refName != null){
        $.ajax(url, {
            dataType : 'html'
        }).done(function(html) {
            $this.html(html);
            $('input[type="file"][data-toggle="file"]').fileUpload();
        }).fail(function(jqXHR, textStatus, errorThrown) {
            $this.html(jqXHR.responseText);
        }).always(function(jqXHR, textStatus) {
            console.log(textStatus);
        });
        }

        $('.referential').click(function() {
              window.location.hash = 'show/' + $(this).data('name');
//            $('#referentiel').DataTable().ajax.url(window.location.origin + window.location.pathname+'/search/data?refName='+$(this).data('name')).load();
        	   var refName = $(this).data('name');
               var $this = $('#upload'), url = window.location.origin + window.location.pathname +'/'+refName
               $.ajax(url, {
                   dataType : 'html'
               }).done(function(html) {
            	   console.log(this);
                   $this.html(html);
                   $('input[type="file"][data-toggle="file"]').fileUpload();
                   $('#referentiel').DataTable();
                   console.log(html);
               }).fail(function(jqXHR, textStatus, errorThrown) {
                   $this.html(jqXHR.responseText);
               }).always(function(jqXHR, textStatus) {
                   console.log(textStatus);
               });
        });
        
         
        
    });
    
    

});
