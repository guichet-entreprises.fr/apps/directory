require([ 'jquery', 'ace/ace', 'lib/confirm', 'bootstrap', 'ace/worker-javascript', 'ace/mode-javascript', 'ace/theme-eclipse' ], function($, ace) {

    $(function () {
        var frm = $('form[name="algorithm"]');
        var editor = ace.edit('algo_content');
        editor.setTheme('ace/theme/eclipse');
        editor.getSession().setMode('ace/mode/javascript');

        frm.on('submit', function () {
            $('input[name="content"]', frm).val(editor.getValue());
        })
    });

});
