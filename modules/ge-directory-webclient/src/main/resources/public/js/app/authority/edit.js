require([ 'jquery', 'ace/ace', 'lib/confirm', 'bootstrap', 'ace/worker-json', 'ace/mode-json', 'ace/theme-eclipse' ], function($, ace) {

    $(function () {
        var frm = $('form[name="authority"]');
        var editor = ace.edit('authority_details');
        editor.setValue(JSON.stringify(JSON.parse(editor.getValue()),undefined,4));
        editor.setTheme('ace/theme/eclipse');
        editor.getSession().setMode('ace/mode/json');

        frm.on('submit', function () {
            $('input[name="details"]', frm).val(editor.getValue());
        })
    });

});
