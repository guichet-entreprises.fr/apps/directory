requirejs.config({
    baseUrl: baseUrl.replace(/\/$/, '') + '/js',
    paths: {
        app: 'app',
        i18n: 'i18n',
        rest: '../rest',
        'ace/theme/eclipse': 'ace/theme-eclipse',
        'ace/mode/javascript': 'ace/mode-javascript'
    },
    map: {
        '*': {
            'intlTelInput': 'intl-tel-input',
            'parsley': 'lib/parsley'
        }
    }
});

console.log('Go go go');

requirejs([ 'jquery', 'bootstrap', 'lib/polyfill', 'lib/datatables' ], function($) {

    $('table[data-toggle="datatable"]').DataTable();

});
