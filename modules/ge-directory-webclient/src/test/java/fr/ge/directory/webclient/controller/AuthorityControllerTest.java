/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.webclient.controller;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.core.rest.exception.RestResponseException;
import fr.ge.ct.authentification.facade.IAccountFacade;
import fr.ge.directory.webclient.AbstractTest;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityBean;
import fr.ge.directory.ws.v1.service.IAuthorityService;
import fr.ge.directory.ws.v1.service.IImportAuthoritiesWebService;

/**
 * The Class AuthorityControllerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class AuthorityControllerTest extends AbstractTest {

    /** mvc. */
    private MockMvc mvc;

    /** web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /** The service. */
    @Autowired
    private IAuthorityService service;

    /** Account facade. */
    @Autowired
    private IAccountFacade accountFacade;

    @Autowired
    private IImportAuthoritiesWebService importAuthorityWebService;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
        reset(this.service, this.importAuthorityWebService, this.accountFacade);
    }

    /**
     * Test search main.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSearchMain() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/authority")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("authority/search/main"));
    }

    /**
     * Test search empty data.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSearchEmptyData() throws Exception {
        final SearchResult<ResponseAuthorityBean> searchResult = new SearchResult<>(0, 10);
        when(this.service.search(any(long.class), any(long.class), any(), any(), eq(null))).thenReturn(searchResult);

        this.mvc.perform( //
                get("/authority/search/data") //
                        .param("draw", "1") //
                        .param("start", "0") //
                        .param("length", "10") //
                        .param("filters", "entityId!") //
        ) //
                .andExpect(status().isOk());

        final ArgumentCaptor<List<SearchQueryFilter>> filtersCaptor = ArgumentCaptor.forClass(List.class);
        final ArgumentCaptor<List<SearchQueryOrder>> ordersCaptor = ArgumentCaptor.forClass(List.class);

        verify(this.service).search(eq(0L), eq(10L), filtersCaptor.capture(), ordersCaptor.capture(), eq(null));

        assertThat(filtersCaptor.getValue(), hasSize(1));
        assertThat(ordersCaptor.getValue(), empty());
    }

    /**
     * Test search data.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSearchData() throws Exception {
        final SearchResult<ResponseAuthorityBean> searchResult = new SearchResult<>(0, 10);
        searchResult.setContent(Collections.singletonList(new ResponseAuthorityBean()));

        when(this.service.search(any(long.class), any(long.class), any(), any(), eq(null))).thenReturn(searchResult);

        this.mvc.perform( //
                get("/authority/search/data") //
                        .param("draw", "1") //
                        .param("start", "0") //
                        .param("length", "10") //
        ) //
                .andExpect(status().isOk());

        final ArgumentCaptor<List<SearchQueryFilter>> filtersCaptor = ArgumentCaptor.forClass(List.class);
        final ArgumentCaptor<List<SearchQueryOrder>> ordersCaptor = ArgumentCaptor.forClass(List.class);

        verify(this.service).search(eq(0L), eq(10L), filtersCaptor.capture(), ordersCaptor.capture(), eq(null));

        assertThat(filtersCaptor.getValue(), nullValue());
        assertThat(ordersCaptor.getValue(), empty());
    }

    /**
     * Test edit.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEdit() throws Exception {
        when(this.service.get(any(long.class))).thenReturn(new ResponseAuthorityBean());

        this.mvc.perform(get("/authority/{id}/view", 1L)) //
                .andExpect(status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("authority/edit/main"));
    }

    /**
     * Test edit new one.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEditNewOne() throws Exception {
        when(this.service.get(any(long.class))).thenReturn(new ResponseAuthorityBean());

        this.mvc.perform(get("/authority/view")) //
                .andExpect(status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("authority/edit/main"));
    }

    /**
     * Test update.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testUpdate() throws Exception {
        final Long authorityId = 42L;
        final String authorityEntityId = "authorityFunctionalId";
        String authorityEntityPath = "parentPath/" + authorityEntityId;
        final String authorityLabel = "Authority Label";
        final String authorityInfoEmail = "authority@project.com";
        final String authorityInfoSlack = "authority@project.slack.com";
        final String authorityInfo = String.format("{\"e-mail\":\"%s\",\"slack\":\"%s\"}", authorityInfoEmail, authorityInfoSlack);

        when(this.service.get(any(long.class))).thenReturn(new ResponseAuthorityBean());
        when(this.service.save(any(ResponseAuthorityBean.class))).thenReturn(42l);

        this.mvc.perform( //
                post("/authority/{id}", authorityId) //
                        .param("entityId", authorityEntityId) //
                        .param("path", authorityEntityPath) //
                        .param("label", authorityLabel) //
                        .param("details", authorityInfo) //
        ) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.view().name("redirect:/authority/42/view"));

        final ArgumentCaptor<ResponseAuthorityBean> data = ArgumentCaptor.forClass(ResponseAuthorityBean.class);

        verify(this.service).save(data.capture());

        final ResponseAuthorityBean actual = data.getValue();

        assertThat(actual, allOf( //
                hasProperty("id", equalTo(authorityId)), //
                hasProperty("entityId", equalTo(authorityEntityId)), //
                hasProperty("label", equalTo(authorityLabel)) //
        ) //
        );

        assertThat(actual.getDetails().get("e-mail").textValue(), equalTo(authorityInfoEmail));
        assertThat(actual.getDetails().get("slack").textValue(), equalTo(authorityInfoSlack));
    }

    /**
     * Test create.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testCreate() throws Exception {
        final String authorityEntityId = "authorityFunctionalId";
        final String authorityLabel = "Authority Label";
        final String authorityInfoEmail = "authority@project.com";
        final String authorityInfoSlack = "authority@project.slack.com";
        final String authorityInfo = String.format("{\"e-mail\":\"%s\",\"slack\":\"%s\"}", authorityInfoEmail, authorityInfoSlack);

        when(this.service.get(any(long.class))).thenReturn(new ResponseAuthorityBean());
        when(this.service.save(any(ResponseAuthorityBean.class))).thenReturn(43l);

        this.mvc.perform( //
                post("/authority/") //
                        .param("entityId", "") //
                        .param("path", authorityEntityId) //
                        .param("label", authorityLabel) //
                        .param("details", authorityInfo) //
        ) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(MockMvcResultMatchers.view().name("redirect:/authority/43/view"));

        final ArgumentCaptor<ResponseAuthorityBean> data = ArgumentCaptor.forClass(ResponseAuthorityBean.class);

        verify(this.service).save(data.capture());

        final ResponseAuthorityBean actual = data.getValue();

        assertThat(actual, allOf( //
                hasProperty("entityId", equalTo(authorityEntityId)), //
                hasProperty("label", equalTo(authorityLabel)) //
        ) //
        );

        assertThat(actual.getDetails().get("e-mail").textValue(), equalTo(authorityInfoEmail));
        assertThat(actual.getDetails().get("slack").textValue(), equalTo(authorityInfoSlack));
    }

    /**
     * Test remove.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testRemove() throws Exception {
        when(this.service.remove(any(long.class))).thenReturn(true);

        this.mvc.perform(get("/authority/{id}/remove", 1001L)) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(view().name("redirect:/authority"));

        verify(this.service).remove(eq(1001L));
    }

    @Test
    public void testImportAuthorities() throws Exception, RestResponseException {

        // prepare
        when(this.importAuthorityWebService.importAuthorities(any(List.class), any(Boolean.class))).thenReturn("102");

        // call
        this.mvc.perform(MockMvcRequestBuilders.fileUpload("/authority/import").file(new MockMultipartFile("autorities", "ca.csv", "text/csv", this.resourceAsBytes("ca.csv")))) //
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())//
                .andExpect(MockMvcResultMatchers.redirectedUrl("/authority/import"));
    }
}
