/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.webclient.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JsonNode;

import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.ws.v2.service.IRefService;

/**
 * Tests {@link ReferentielController}.
 *
 * @author jpauchet
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class ReferentielControllerTest {

    /** Mvc. */
    private MockMvc mvc;

    /** Web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /** The service. */
    @Autowired
    private IRefService service;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
        reset(this.service);
    }

    /**
     * Tests
     * {@link ReferentielController#upload(org.springframework.ui.Model, javax.servlet.http.HttpServletRequest, String, org.springframework.web.multipart.MultipartFile)}.
     */
    @Test
    public void testUpload() throws Exception {
        final String refCode = "requirements";
        final StringBuilder content = new StringBuilder().append("attr1;attr2;attr3;attr4\r\n") //
                .append("val1-1;val2-1;val3-1\r\n") //
                .append("val1-2;{\"code\":\"XXX\",\"label\":\"Awesome value\"};val3-2\r\n") //
                .append("val1-3;val2-3;val3-3\r\n");

        final MockMultipartFile csvFile = new MockMultipartFile("file", "ref_requirements.csv", "application/vnd.ms-excel", content.toString().getBytes());
        this.mvc.perform(fileUpload("/referentiels/upload").file(csvFile).param("refName", refCode)) //
                .andExpect(status().isFound());

        final ArgumentCaptor<List<JsonNode>> captor = ArgumentCaptor.forClass(List.class);

        verify(this.service).save(eq(refCode), captor.capture());

        final List<JsonNode> items = captor.getValue();
        assertThat(items, hasSize(3));

        assertEquals("\"val1-1\"", items.get(0).get("attr1").toString());
        assertEquals("\"val2-1\"", items.get(0).get("attr2").toString());
        assertEquals("\"val3-1\"", items.get(0).get("attr3").toString());
        assertEquals("\"\"", items.get(0).get("attr4").toString());

        assertEquals("\"val1-2\"", items.get(1).get("attr1").toString());
        assertEquals("{\"code\":\"XXX\",\"label\":\"Awesome value\"}", items.get(1).get("attr2").toString());
        assertEquals("\"val3-2\"", items.get(1).get("attr3").toString());
        assertEquals("\"\"", items.get(1).get("attr4").toString());

        assertEquals("\"val1-3\"", items.get(2).get("attr1").toString());
        assertEquals("\"val2-3\"", items.get(2).get("attr2").toString());
        assertEquals("\"val3-3\"", items.get(2).get("attr3").toString());
        assertEquals("\"\"", items.get(2).get("attr4").toString());
    }

    @Test
    public void testUploadWithPurge() throws Exception {
        final String refCode = "fragments";
        final StringBuilder content = new StringBuilder().append("attr1;attr2;attr3\r\n") //
                .append("val1-1;val2-1;val3-1\r\n") //
                .append("val1-2;val2-2;val3-2\r\n") //
                .append("val1-3;val2-3;val3-3\r\n");

        final MockMultipartFile csvFile = new MockMultipartFile("file", "ref_requirements.csv", "application/vnd.ms-excel", content.toString().getBytes());
        this.mvc.perform(fileUpload("/referentiels/upload").file(csvFile).param("refName", refCode).param("truncate", "yes")) //
                .andExpect(status().isFound());

        verify(this.service).truncate(eq(refCode));

        final ArgumentCaptor<List<JsonNode>> itemsCaptor = ArgumentCaptor.forClass(List.class);
        verify(this.service).save(eq(refCode), itemsCaptor.capture());

        final List<JsonNode> items = itemsCaptor.getValue();
        assertThat(items, hasSize(3));
    }

    @Test
    public void testEdit() throws Exception {
        this.mvc.perform(get("/referentiels").param("refName", "fragments")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("referentiels/referentiels"));
    }

    @Test
    public void testSearchData() throws Exception {
        final String refCode = "fragments";
        final SearchResult<JsonNode> searchResult = new SearchResult<>(0, 10);
        when(this.service.findAll(eq(refCode), eq(searchResult.getStartIndex()), eq(searchResult.getMaxResults()), any(), any(), any())) //
                .thenReturn(searchResult);

        this.mvc.perform( //
                get("/referentiels/search/data") //
                        .param("refName", refCode) //
                        .param("draw", "1") //
                        .param("start", "0") //
                        .param("length", "10") //
        ) //
                .andExpect(status().isOk()); //
    }

}
