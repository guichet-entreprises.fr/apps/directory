package fr.ge.directory.webclient.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.ws.v1.model.RepositoryElement;
import fr.ge.directory.ws.v1.service.RepositoryWebService;

/**
 * Test {@link RepositoryController}.
 * 
 * @author mtakerra
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class RepositoryControllerTest {

    /** Mvc. */
    private MockMvc mvc;

    /** Web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /** The service. */
    @Autowired
    private RepositoryWebService repositoryWebService;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
        reset(this.repositoryWebService);
    }

    /**
     * Tests
     * {@link ReferentielController#upload(org.springframework.ui.Model, javax.servlet.http.HttpServletRequest, String, org.springframework.web.multipart.MultipartFile)}.
     */
    @Test
    public void testUpload() throws Exception {
        final String refCode = "organisme_conventionne";
        final StringBuilder content = new StringBuilder().append("entityId;label;attr3;attr4\r\n") //
                .append("val1-1;val2-1;val3-1;val4-1\r\n") //
                .append("val1-2;val2-2;{\"code\":\"XXX\",\"label\":\"Awesome value\"};val4-2\r\n") //
                .append("val1-3;val2-3;val3-3;val4-3\r\n");

        final MockMultipartFile csvFile = new MockMultipartFile("file", "organisme_conventionne.csv", "application/vnd.ms-excel", content.toString().getBytes());
        this.mvc.perform(fileUpload("/repository/upload").file(csvFile).param("refName", refCode).param("truncate", "false")) //
                .andExpect(status().isFound());

        final ArgumentCaptor<List<RepositoryElement>> captorElementsList = ArgumentCaptor.forClass(List.class);

        verify(this.repositoryWebService).add(eq(refCode), eq(false), captorElementsList.capture());

        final List<RepositoryElement> items = captorElementsList.getValue();
        assertThat(items, hasSize(3));

        assertEquals("val1-1", items.get(0).getEntityId());
        assertEquals("val2-1", items.get(0).getLabel());
        assertEquals("{\"entityId\":\"val1-1\",\"label\":\"val2-1\",\"attr3\":\"val3-1\",\"attr4\":\"val4-1\"}", items.get(0).getDetails().toString());

        assertEquals("val1-2", items.get(1).getEntityId());
        assertEquals("val2-2", items.get(1).getLabel());
        assertEquals("{\"entityId\":\"val1-2\",\"label\":\"val2-2\",\"attr3\":{\"code\":\"XXX\",\"label\":\"Awesome value\"},\"attr4\":\"val4-2\"}", items.get(1).getDetails().toString());

        assertEquals("val1-3", items.get(2).getEntityId());
        assertEquals("val2-3", items.get(2).getLabel());
        assertEquals("{\"entityId\":\"val1-3\",\"label\":\"val2-3\",\"attr3\":\"val3-3\",\"attr4\":\"val4-3\"}", items.get(2).getDetails().toString());
    }

    @Test
    public void testUploadWithReset() throws Exception {
        final String refCode = "organisme_conventionne";
        final StringBuilder content = new StringBuilder().append("entityId;label;attr3;attr4\r\n") //
                .append("val1-1;val2-1;val3-1;val4-1\r\n") //
                .append("val1-2;val2-2;{\"code\":\"XXX\",\"label\":\"Awesome value\"};val4-2\r\n") //
                .append("val1-3;val2-3;val3-3;val4-3\r\n");

        final MockMultipartFile csvFile = new MockMultipartFile("file", "organisme_conventionne.csv", "application/vnd.ms-excel", content.toString().getBytes());
        this.mvc.perform(fileUpload("/repository/upload").file(csvFile).param("refName", refCode).param("truncate", "yes")) //
                .andExpect(status().isFound());

        final ArgumentCaptor<List<RepositoryElement>> captorElementsList = ArgumentCaptor.forClass(List.class);

        verify(this.repositoryWebService).add(eq(refCode), eq(true), captorElementsList.capture());

        final List<RepositoryElement> items = captorElementsList.getValue();
        assertThat(items, hasSize(3));
    }

    @Test
    public void testEdit() throws Exception {
        this.mvc.perform(get("/repository").param("refName", "organisme_conventionne")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("repository/repository"));
    }

    @Test
    public void testEditElement() throws Exception {
        RepositoryElement bean = new RepositoryElement();
        String repositoryId = "organisme_conventionne";
        long elementId = 1;

        when(this.repositoryWebService.read(repositoryId, elementId)) //
                .thenReturn(bean);

        this.mvc.perform(get("/repository/organisme_conventionne/view/1")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("repository/edit/main"));
    }

    @Test
    public void testSearchData() throws Exception {
        final String refCode = "organisme_conventionne";
        final SearchResult<RepositoryElement> searchResult = new SearchResult<>(0, 10);
        when(this.repositoryWebService.search(eq(refCode), eq(searchResult.getStartIndex()), eq(searchResult.getMaxResults()), any(), any())) //
                .thenReturn(searchResult);

        this.mvc.perform( //
                get("/repository/search/data") //
                        .param("refName", refCode) //
                        .param("draw", "1") //
                        .param("start", "0") //
                        .param("length", "10") //
        ) //
                .andExpect(status().isOk()); //
    }

}
