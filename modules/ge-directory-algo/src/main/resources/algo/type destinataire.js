var activiteIndustrielle = "";
if(args.activiteIndustrielle!==null){
   activiteIndustrielle = args.activiteIndustrielle.toUpperCase();
}
var secteur1 = args.secteur1.toUpperCase();
var secteur2="";
if(args.secteur2!==null){
	secteur2 = args.secteur2.toUpperCase();
}
var typePersonne = args.typePersonne.toUpperCase();

var formJuridique = "";
if(args.formJuridique!==null){
	formJuridique=args.formJuridique.toUpperCase();
}
//Boolean pour savoir le nombre de salariés [OUI|NON]
var optionCMACCI = args.optionCMACCI.toUpperCase();

var codeCommune = args.codeCommune;

var isAlsaceMoselle = codeCommune.substring(0, 2);
logger.info("isAlsaceMoselle is {}", isAlsaceMoselle);
var listeTypeDestinatairesByRole = [];
var jsonResult = {};

var departementAlsaceMoselle =["57", "67", "68"];
var pmCommerciales=["EURL", "SAS" ,"SASU" ,"SCS" ,"SCA", "SARL", "SA", "SNC"];
var pmCiviles =["SCI", "SCM", "SCP", "SOCIETES CIVILES", "SEL"];
var pmAgricoles=["GAEC", "EARL", "SCEA", "GFA"];


if(typePersonne == 'PP'){
	switch (secteur1){	
		case 'LIBERAL':
			//CFE
			
			listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'URSSAF'});
		
			//TDR
			if(formJuridique == 'EIRL'){
				listeTypeDestinatairesByRole.push({'role':'TDR', 'authority': 'GREFFE'}); 
			}		
			jsonResult['listAuthorities'] = listeTypeDestinatairesByRole;
			return JSON.stringify(jsonResult);
			
		
		case 'AGRICOLE':
			listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'CA'});
			jsonResult['listAuthorities'] = listeTypeDestinatairesByRole;
			return JSON.stringify(jsonResult);
		
		case 'COMMERCIAL':
			//CFE
			if(secteur2 == 'AGRICOLE'){
					listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'CA'});
			}else if(secteur2 == 'ARTISANAL'){
					if(optionCMACCI=='OUI' && departementAlsaceMoselle.indexOf(isAlsaceMoselle)== -1){
					listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'CCI'});
					listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'GREFFE'});
				}else if(departementAlsaceMoselle.indexOf(isAlsaceMoselle)!== -1 && activiteIndustrielle=="OUI"){
					logger.info("activite industrielle + alsace moselle");
					listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'CCI'});
					listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'GREFFE'});
				}else{	
					listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'CMA'});
					listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'CMA'});
					listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'GREFFE'});
					}
			}else{
				listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'CCI'});
				listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'GREFFE'});
			}
						
			jsonResult['listAuthorities'] = listeTypeDestinatairesByRole;
			return JSON.stringify(jsonResult);		
			
		case 'ARTISANAL':   
			//CFE
			if(optionCMACCI=='OUI' && departementAlsaceMoselle.indexOf(isAlsaceMoselle)== -1){
				listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'CCI'});
				listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'GREFFE'});
			}else if(departementAlsaceMoselle.indexOf(isAlsaceMoselle)!== -1 && activiteIndustrielle=="OUI"){
				logger.info("activite industrielle + alsace moselle");
				listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'CCI'});
				listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'GREFFE'});
			}else{
				listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'CMA'});	
				listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'CMA'});
			}
			//TDR			
			if (secteur2 == 'COMMERCIAL' && !((optionCMACCI=='OUI' && departementAlsaceMoselle.indexOf(isAlsaceMoselle)== -1) || (departementAlsaceMoselle.indexOf(isAlsaceMoselle)!== -1 && activiteIndustrielle=="OUI"))){
				listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'GREFFE'});
			}
			jsonResult['listAuthorities'] = listeTypeDestinatairesByRole;
			return JSON.stringify(jsonResult);
			
		case 'AGENT COMMERCIAL':   
			//CFE			
				listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'GREFFE'});
						
			//TDR			
				listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'GREFFE'});
				
			jsonResult['listAuthorities'] = listeTypeDestinatairesByRole;
			return JSON.stringify(jsonResult);
	}	
	
}else if(typePersonne == 'PM') {
	if(pmCommerciales.indexOf(formJuridique)!== -1){
	//Cas des PM sociétés commerciales Seulement	
		if((secteur1 == 'LIBERAL' || secteur1 == 'COMMERCIAL') && secteur2 != 'ARTISANAL'){
				//Cas activité liberal, commercial	
				listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'CCI'});
				listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'GREFFE'});

			jsonResult['listAuthorities'] = listeTypeDestinatairesByRole;
			return JSON.stringify(jsonResult);				
		}else if(secteur1 == 'ARTISANAL' || secteur2 == 'ARTISANAL'){
			//Cas activité ARTISANAL
						
			if(optionCMACCI=='OUI' && departementAlsaceMoselle.indexOf(isAlsaceMoselle)== -1){
				listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'CCI'});
				listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'GREFFE'});
			}else if(departementAlsaceMoselle.indexOf(isAlsaceMoselle)!== -1 && activiteIndustrielle=="OUI"){
				logger.info("activite industrielle & alsace moselle");
				listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'CCI'});
				listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'GREFFE'});
			}else{
				listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'CMA'});
				listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'CMA'});
				listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'GREFFE'});
			}

			jsonResult['listAuthorities'] = listeTypeDestinatairesByRole;
			return JSON.stringify(jsonResult);		
		}else if(secteur1 == 'AGRICOLE'){
			//Cas activité AGRICOLE (cas de "SARL", "SA", "SNC" agricoles)
			listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'CA'});
			listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'GREFFE'});
			
			jsonResult['listAuthorities'] = listeTypeDestinatairesByRole;
			return JSON.stringify(jsonResult);
		}	
	}else if(pmCiviles.indexOf(formJuridique)!== -1){
		
			listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'GREFFE'});
			listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'GREFFE'});
			
			jsonResult['listAuthorities'] = listeTypeDestinatairesByRole;
			return JSON.stringify(jsonResult);	
	}else if(pmAgricoles.indexOf(formJuridique)!== -1){
		
			listeTypeDestinatairesByRole.push({'role':'CFE', 'authority':'CA'});
			listeTypeDestinatairesByRole.push({'role':'TDR', 'authority':'GREFFE'});
			
			jsonResult['listAuthorities'] = listeTypeDestinatairesByRole;
			return JSON.stringify(jsonResult);
	}else{
	    logger.error(" The argument 'formJuridique' does not represent any type of PM !");
		return "ERROR";
	}
}else{
    logger.error(" The argument 'typePersonne' is mandatory !");
	return "ERROR";
}