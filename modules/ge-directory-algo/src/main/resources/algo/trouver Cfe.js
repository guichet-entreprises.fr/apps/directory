logger.info("serviceParams  {}",args);

// call directory service to find type cfe
var response = service.request('${urlDirectory}/v1/algo/{code}/execute', "type de cfe") //
        .accept('text') //
        .post(args);
logger.info("response  {}",response);
// Result value of type cfe
var typeCfe = response.asString();

logger.info("args is {}",args)
args.typeCfe =typeCfe;

// call directory service to find secteur cfe
var response = service.request('${urlDirectory}/v1/algo/{code}/execute',"secteur cfe") //
        .accept('text') //
        .post(args);
logger.info("response  {}",response);
// Result value of functional id 
var secteurCFe = response.asString();
args.secteurCfe = secteurCFe

var codeCommune =  String(args.codeCommune);
var response = service.request('${urlDirectory}/v1/authority/{entityId}', codeCommune) //
	.accept('json') //
	.get();
	
var receiverInfo = response.asObject();

logger.info("receiverInfo is  {}", receiverInfo);
logger.info("type of details of receiveInfo is  {}",typeof receiverInfo.details);


var details = !receiverInfo.details ? null : receiverInfo.details;

// convert map to json
var input = {}
details.forEach(function(key, value){
        input[key] = value
});

args.details = JSON.stringify(input);
logger.info("args passed to trouver autorite code {}",args);

// call directory service to find codeAuthority
var response = service.request('${urlDirectory}/v1/algo/{code}/execute',"trouver AutoriteCode") //
        .accept('text') //
        .post(args);
// Result value of functional id 
var codeAuthority = response.asString();


logger.info("codeAuthority  {}",codeAuthority);

return codeAuthority