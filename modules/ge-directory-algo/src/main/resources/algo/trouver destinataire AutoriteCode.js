logger.info("args received from trouver cfe {}",args);

var typeDestinataire = args.typeDestinataire;

logger.info("typeDestinataire is {}", typeDestinataire);

var contact = JSON.parse(args.details);

typeDestinataire = JSON.parse(typeDestinataire);

logger.info(" parsed typeDestinataire is {}", typeDestinataire);

logger.info("contact is {}", contact);

for(var i=0; i<typeDestinataire.listAuthorities.length; i++){

logger.info(" authority {} is {}", parseInt(i), typeDestinataire.listAuthorities[i]);

	switch (typeDestinataire.listAuthorities[i]['authority']) {
		case 'CA': typeDestinataire.listAuthorities[i]['authority'] = contact.CA;break;
		
		case 'CMA': typeDestinataire.listAuthorities[i]['authority'] = contact.CMA;break;
		
		case 'CCI': typeDestinataire.listAuthorities[i]['authority'] = contact.CCI;break;
		
		case 'URSSAF': typeDestinataire.listAuthorities[i]['authority'] = contact.URSSAF; break;
		
		case 'GREFFE' :	typeDestinataire.listAuthorities[i]['authority'] = contact.GREFFE; break;
		
		case 'RM' :	typeDestinataire.listAuthorities[i]['authority'] = contact.RM; break;
	} 
}
logger.info("typeDestinataire is {}", typeDestinataire);
return JSON.stringify(typeDestinataire);