logger.info("secteur1 received is  {}",args.secteur1);
if(args.secteur1 === null){
    return ERROR;
}

// codeSecteurPrincipal COMMERCIAL ou AGRICOLE
var secteur1 = args.secteur1.toUpperCase();


// codeSecteurSecondaire COMMERCIAL ou AGRICOLE
var secteur2 = "";
if(args.secteur2!==null){
secteur2 = args.secteur2.toUpperCase();
}

// typePersonne exp PP ou PM
var typePersonne = args.typePersonne.toUpperCase();

switch (secteur1) {

case 'AGRICOLE': return 'CA' ;
case 'ARTISANAL':   return 'CMA' ;
case 'COMMERCIAL':
	if (secteur2 == 'AGRICOLE')  return 'CA'; 
	if (secteur2 == 'ARTISANAL') return 'CMA';
	return 'CCI';
case 'LIBERAL':
	if (secteur2 == 'COMMERCIAL') {return 'CCI'; };
	if (typePersonne == 'PP') {return 'URSSAF';} else {return 'GREFFE'; };
}