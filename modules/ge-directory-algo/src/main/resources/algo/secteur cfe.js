//type du CFE
var typeCfe = args.typeCfe.toUpperCase();

// formeJuridique exp AE ou EIRL:
var formJuridique = args.formJuridique.toUpperCase();

// indique si on a une option CMACCI (L'effectif de l'entreprise est-il > 10 ? )
var optionCMACCI = args.optionCMACCI.toUpperCase();

// type de personne PP pour les formes juridiques suivantes
var formJuridiques = ['AE', 'AEIRL', 'EI', 'EIRL'];

switch (typeCfe) {
case 'GREFFE':	if (formJuridiques.indexOf(formJuridique) === 0) { return 'CCI';} return typeCfe;
case 'CMA':  if(optionCMACCI == 'OUI') {return 'CCI';}  return typeCfe;
case 'CCI': return typeCfe;
case 'URSSAF': return typeCfe;
default: return typeCfe;
}