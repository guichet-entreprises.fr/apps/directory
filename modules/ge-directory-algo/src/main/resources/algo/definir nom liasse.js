function pad(s) { return (s < 10) ? '0' + s : s; }

logger.info("funcId received is  {}", args.funcId);
logger.info("numeroLiasse received is  {}",args.numeroLiasse);
if (null === args.funcId || null === args.numeroLiasse ){
    return null;
}

var response = service.request('${urlDirectory}/v1/authority/{entityId}', args.funcId) //
	.accept('json') //
	.get();
	
var authority = response.asObject();
var details = !authority.details ? null : authority.details;
var referentials = {
    'G' : {
        'emetteur' : '1000A1001',
        'type' : 'DMTDU',
        'version' : 'GUEN',
        'extension' : {
        	'zip' : '.zip',
        	'fini' : '.zip.fini'
        }
    },
    'C' : {
        'emetteur' : '0016A1002',
        'type' : 'DEMAT',
        'version' : 'MGUN',
        'extension' : {
        	'zip' : '.zip',
        	'fini' : '.fini'
        }
    },
    'M' : {
        'emetteur' : '0016A1003',
        'type' : 'DEMAT',
        'version' : 'MGUN',
        'extension' : {
        	'zip' : '.zip',
        	'fini' : '.fini'
        }
    },
    'X' : {
        'emetteur' : '0016A1004',
        'type' : 'DEMAT',
        'version' : 'MGUN',
        'extension' : {
        	'zip' : '.zip',
        	'fini' : '.fini'
        }
    },
    'U' : {
        'emetteur' : '0016A1005',
        'type' : 'DEMAT',
        'version' : 'MGUN',
        'extension' : {
        	'zip' : '.zip',
        	'fini' : '.zip.fini'
        }
    }
}

var referential = referentials[details.ediCode.toUpperCase().charAt(0)];

var currentDate = new Date();
var month = currentDate.getMonth() + 1;

var nomFichierLiasse = [
    'C',
    referential.emetteur,
    'L',
    args.numeroLiasse.substring(6, 12),
    'D',
    currentDate.getFullYear(),
    pad(month.toString()),
    pad(currentDate.getDate().toString()),
    'H',
    pad(currentDate.getHours().toString()),
    pad(currentDate.getMinutes().toString()),
    pad(currentDate.getSeconds().toString()),
    'T',
    referential.type,
    'P',
    referential.version
].join('');

return JSON.stringify({
	'zip' : nomFichierLiasse.concat('', referential.extension.zip),
	'fini' : nomFichierLiasse.concat('', referential.extension.fini)
});