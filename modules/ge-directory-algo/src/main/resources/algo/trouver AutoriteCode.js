logger.info("args received from trouver cfe {}",args);

var secteurCfe = args.secteurCfe.toUpperCase();

logger.info("secteurCfe is {}", secteurCfe);

var contact = JSON.parse(args.details);

logger.info("contact is {}", contact);
logger.info("contact is {}", contact.CCI);

switch (secteurCfe) {
    case 'CA': return contact.CA ;
    case 'CMA':   return contact.CMA ;
    case 'CCI': return contact.CCI;
    case 'URSSAF': return contact.URSSAF;
    case 'GREFFE' : return contact.GREFFE;
    case 'RM' : return contact.RM;
}