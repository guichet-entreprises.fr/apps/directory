logger.info("serviceParams  {}",args);

// call directory service to find type cfe
var response = service.request('${urlDirectory}/v1/algo/{code}/execute', "trouver destinataire") //
        .accept('string') //
        .post(args);

var authoritiesString = response.asString();
logger.info("Response sent from trouver destinataire {}", authoritiesString);
var authorities = JSON.parse(authoritiesString);
var destinataires = [];
var clickerEvents = [];
var jsonResult = {};
var authoritiesNoDuplicate = [];
var clickerBaseEvent = "DOSSIER_TRANSMIS";

if(authorities !== null && authorities.listAuthorities !== null) {
	
	//Get each unique authority code
	var duplicatedAuthorities = [];
	authorities.listAuthorities.forEach(function(value) {
		if(authoritiesNoDuplicate.indexOf(value.authority) < 0) {
			authoritiesNoDuplicate.push(value.authority);
		} else {
			duplicatedAuthorities.push(value.authority);
		}
	});
	
	authoritiesNoDuplicate.length = 0;
	authorities.listAuthorities.forEach(function(value) { 
		var destinataire = value;
		if(duplicatedAuthorities.indexOf(destinataire.authority) >= 0) {
			destinataire.role = "CFE-TDR";
		}
		if(authoritiesNoDuplicate.indexOf(destinataire.authority) < 0) {
			authoritiesNoDuplicate.push(destinataire.authority);
			destinataires.push(destinataire);
		}
		
	});
}

logger.info("List of unique authorities {}", authoritiesNoDuplicate);
logger.info("Duplicated authorities {}", duplicatedAuthorities);

if(authoritiesNoDuplicate.length > 0) {
	
	destinataires.forEach(function(value){
		var response = service.request('${urlDirectory}/v1/authority/{entityId}', value.authority) //
		.accept('json') //
		.get();
		
		var receiverInfo = response.asObject();
		var details = receiverInfo.details || undefined;
		if(details) {
			var edi = details.ediCode;
			var reseau = "";
			switch (edi.charAt(0)) {
				case "G" : reseau = "GREFFE"; break;
				case "M" : reseau = "CMA"; break;
				case "C" : reseau = "CCI"; break;
				case "X" : reseau = "CA"; break;				
			}
			clickerEvents.push({
				"ref" : clickerBaseEvent,
				"role" : value.role
			});					
			clickerEvents.push({
				"ref" : clickerBaseEvent + "_" + reseau,
				"role" : value.role
			});
			clickerEvents.push({
				"ref" : clickerBaseEvent + "_" + reseau + "_" + value.role,
				"role" : value.role
			});
			clickerEvents.push({
				"ref" : clickerBaseEvent + "_" + reseau + "_" + value.role + "_" + edi,
				"role" : value.role
			});
		}
	});
}
jsonResult['listEvents'] = clickerEvents;
logger.info("List of event names to click is {}", jsonResult);
return JSON.stringify(jsonResult);