//this algorithm allows to find the business directory (REPERTOIRE DES METIERS)  from the number of the town (code commune)

logger.info("serviceParams  {}",args);

//set recipient type

args.secteurCfe = "RM";


// call directory service to find business directory (REPERTOIRE DES METIERS)
var codeCommune =  String(args.codeCommune);
var response = service.request('${urlDirectory}/v1/authority/{entityId}', codeCommune) //
	.accept('json') //
	.get();

var receiverInfo = response.asObject();
logger.info("receiverInfo is  {}", receiverInfo);
logger.info("type of details of receiveInfo is  {}",typeof receiverInfo.details);	


var details = !receiverInfo.details ? null : receiverInfo.details;
// convert map to json
var input = {}
details.forEach(function(key, value){
        input[key] = value
});

args.details = JSON.stringify(input);
logger.info("args passed to get the authorities code {}",args);

// call directory service to find codeAuthority
var response = service.request('${urlDirectory}/v1/algo/{code}/execute',"trouver AutoriteCode") //
        .accept('text') //
        .post(args);

var codeAuthorities = response.asString();
logger.info("authorities code computed are {}",codeAuthorities);

return codeAuthorities;