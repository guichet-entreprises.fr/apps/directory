CREATE TABLE IF NOT EXISTS "ref_countries" (
   id BIGINT NOT NULL,
   entity_id text,
   label text,
   details json,
   created timestamp NOT NULL DEFAULT now(),
   PRIMARY KEY (id)
);
CREATE SEQUENCE sq_ref_countries START 1 MINVALUE 1 CACHE 1;


CREATE TABLE IF NOT EXISTS "ref_departments" (
   id BIGINT NOT NULL,
   entity_id text,
   label text,
   details json,
   created timestamp NOT NULL DEFAULT now(),
   PRIMARY KEY (id)
);
CREATE SEQUENCE sq_ref_departments START 1 MINVALUE 1 CACHE 1;