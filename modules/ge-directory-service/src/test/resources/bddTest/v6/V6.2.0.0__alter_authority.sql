-- Suppression de la table autority suivi d'une nouvelle création dans un nouveau format.
-- Attention la table autority_User est vidée pour respect des contraintes d'integrité.

DROP TABLE IF EXISTS authority_user;
	
DROP TABLE IF EXISTS authority;	

CREATE TABLE authority (
    id              BIGINT          PRIMARY KEY,
    entity_id       VARCHAR(100)    UNIQUE NOT NULL,
    label           VARCHAR(100)    NOT NULL,
    details         json,
    updated         TIMESTAMP NOT NULL DEFAULT now()); 
    
CREATE TABLE authority_user (
    authority_id    BIGINT          NOT NULL,
    user_id         VARCHAR(100)    NOT NULL
);

ALTER TABLE authority_user
    ADD CONSTRAINT fk_authority_user_authority
    FOREIGN KEY (authority_id)
    REFERENCES authority(id);