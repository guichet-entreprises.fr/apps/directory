CREATE TABLE authority (
    id              BIGINT          PRIMARY KEY,
    func_id         VARCHAR(100)    UNIQUE NOT NULL,
    label           VARCHAR(100)    NOT NULL,
    contact_info    BYTEA
);

CREATE SEQUENCE sq_authority
    START 1
    MINVALUE 1
    CACHE 1;

CREATE TABLE predicate (
    id              BIGINT          PRIMARY KEY,
    attribute       VARCHAR(50)     NOT NULL,
    operator        VARCHAR(5)      NOT NULL,
    val             VARCHAR(50)     NOT NULL,
    authority_id    BIGINT          NOT NULL
);

CREATE SEQUENCE sq_predicate
    START 1
    MINVALUE 1
    CACHE 1;

CREATE TABLE algo (
    id              BIGINT          PRIMARY KEY,
    code            VARCHAR(100)    UNIQUE NOT NULL,
    content         BYTEA           NOT NULL
);

CREATE SEQUENCE sq_algo
    START 1
    MINVALUE 1
    CACHE 1;

ALTER TABLE predicate
    ADD CONSTRAINT fk_predicate_authority
    FOREIGN KEY (authority_id)
    REFERENCES authority(id);
