/**
 *
 */
package fr.ge.directory.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.IAuthorityDataService;
import fr.ge.directory.service.bean.AuthorityBean;
import fr.ge.directory.service.bean.AuthorityRoleBean;
import fr.ge.directory.service.data.AbstractDbTest;
import fr.ge.directory.service.mapper.AlgoMapper;
import fr.ge.directory.service.mapper.AuthorityMapper;

/**
 * Tests {@link AuthorityDataServiceImpl}.
 *
 * @author jpauchet
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml", "classpath:spring/test-persistence-context.xml" })
public class AuthorityDataServiceImplTest extends AbstractDbTest {

    /** Authority data service. */
    @Autowired
    IAuthorityDataService authorityDataService;

    /** Authority mapper. */
    @Autowired
    AuthorityMapper authorityMapper;

    /** Algo mapper. */
    @Autowired
    AlgoMapper algoMapper;

    /**
     * Setup.
     *
     * @throws Exception
     *             exception
     */
    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception {
        Mockito.reset(this.authorityMapper, this.algoMapper);
    }

    /**
     * Formats a String to be at the JSON format.
     *
     * @param str
     *            the input string
     * @return the string with the good format
     */
    private String json(final String str) {
        return str.replaceAll("'", "\"");
    }

    /**
     * Tests {@link AuthorityDataServiceImpl#get(long)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    public void testGet() throws JsonProcessingException, IOException {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean authority = new AuthorityBean();
        authority.setId(42L);
        authority.setEntityId("Mairie78646");
        authority.setLabel("Mairie de Versailles");
        authority.setPath("GE/Mairies/Mairie78646");
        authority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646}}")));
        when(this.authorityMapper.get(eq(42L))).thenReturn(authority);

        // call
        final AuthorityBean result = this.authorityDataService.get(42L);

        // verify
        verify(this.authorityMapper).get(eq(42L));
        assertThat(result, allOf( //
                hasProperty("id", equalTo(42L)), //
                hasProperty("entityId", equalTo("Mairie78646")), //
                hasProperty("label", equalTo("Mairie de Versailles")), //
                hasProperty("path", equalTo("GE/Mairies/Mairie78646")), //
                hasProperty("details", equalTo(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646}}")))) //
        ) //
        );
    }

    /**
     * Tests {@link AuthorityDataServiceImpl#getByEntityId(String)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    public void testgetByEntityId() throws JsonProcessingException, IOException {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean authority = new AuthorityBean();
        authority.setId(42L);
        authority.setEntityId("Mairie78646");
        authority.setLabel("Mairie de Versailles");
        authority.setPath("GE/Mairies/Mairie78646");
        authority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646}}")));
        when(this.authorityMapper.getByEntityId(eq("Mairie78646"))).thenReturn(authority);

        // call
        final AuthorityBean result = this.authorityDataService.getByEntityId("Mairie78646");

        // verify
        verify(this.authorityMapper).getByEntityId(eq("Mairie78646"));
        assertThat(result, allOf( //
                hasProperty("id", equalTo(42L)), //
                hasProperty("entityId", equalTo("Mairie78646")), //
                hasProperty("label", equalTo("Mairie de Versailles")), //
                hasProperty("details", equalTo(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646}}").getBytes()))) //
        ) //
        );
    }

    /**
     * Tests
     * {@link AuthorityDataServiceImpl#save(fr.ge.directory.service.bean.AuthorityBean)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    public void testSaveExists() throws JsonProcessingException, IOException {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean dbAuthority = new AuthorityBean();
        dbAuthority.setId(42L);
        dbAuthority.setEntityId("Mairie78646");
        dbAuthority.setLabel("Mairie de Versailles");
        dbAuthority.setDetails(mapper.readTree(this.json("{'parent':'GE/Mairies'}")));
        when(this.authorityMapper.getByEntityId(eq("Mairie78646"))).thenReturn(dbAuthority);

        // call
        final AuthorityBean authority = new AuthorityBean();
        authority.setEntityId("Mairie78646");
        authority.setLabel("Mairie de Versailles");
        authority.setDetails(mapper.readTree(this.json("{'email':'yyy@yyy.com','address':{'zipCode':78646},'parent':'GE/Mairies'}")));
        this.authorityDataService.save(authority);

        // verify
        final AuthorityBean updatedAuthority = new AuthorityBean();
        updatedAuthority.setEntityId("Mairie78646");
        updatedAuthority.setLabel("Mairie de Versailles");
        updatedAuthority.setDetails(mapper.readTree(this.json("{'email':'yyy@yyy.com','address':{'zipCode':78646},'parent':'GE/Mairies'}")));
        verify(this.authorityMapper).update(refEq(updatedAuthority));
    }

    /**
     * Tests
     * {@link AuthorityDataServiceImpl#save(fr.ge.directory.service.bean.AuthorityBean)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    public void testSaveNotExists() throws JsonProcessingException, IOException {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        when(this.authorityMapper.getByEntityId(eq("Mairie78646"))).thenReturn(null);

        // call
        final AuthorityBean authority = new AuthorityBean();
        authority.setEntityId("Mairie78646");
        authority.setLabel("Mairie de Versailles");
        authority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646},'parent':'GE/Mairies'}")));
        this.authorityDataService.save(authority);

        // verify
        final AuthorityBean createdAuthority = new AuthorityBean();
        createdAuthority.setEntityId("Mairie78646");
        createdAuthority.setLabel("Mairie de Versailles");
        createdAuthority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646},'parent':'GE/Mairies'}")));
        verify(this.authorityMapper).create(refEq(createdAuthority));
    }

    /**
     * Tests
     * {@link AuthorityDataServiceImpl#save(fr.ge.directory.service.bean.AuthorityBean)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    public void testSaveAddUser() throws JsonProcessingException, IOException {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean dbAuthority = new AuthorityBean();
        dbAuthority.setId(42L);
        dbAuthority.setEntityId("Mairie78646");
        dbAuthority.setLabel("Mairie de Versailles");

        dbAuthority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646},'parent':'GE/Mairies'}")));
        when(this.authorityMapper.getByEntityId(eq("Mairie78646"))).thenReturn(dbAuthority);

        // call
        final AuthorityBean authority = new AuthorityBean();
        authority.setEntityId("Mairie78646");
        authority.setLabel("Mairie de Versailles");
        authority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646},'parent':'GE/Mairies'," //
                + "'users':[{'roles':['referent'],'id':'2017-10-USR-XXX-42'}]}")));
        this.authorityDataService.save(authority);

        // verify
        final AuthorityBean updatedAuthority = new AuthorityBean();
        updatedAuthority.setEntityId("Mairie78646");
        updatedAuthority.setLabel("Mairie de Versailles");
        updatedAuthority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646},'parent':'GE/Mairies'," //
                + "'users':[{'roles':['referent'],'id':'2017-10-USR-XXX-42'}]}")));
        verify(this.authorityMapper).update(refEq(updatedAuthority));
        verify(this.authorityMapper).createLink(eq(42L), eq("2017-10-USR-XXX-42"));
    }

    /**
     * Tests
     * {@link AuthorityDataServiceImpl#save(fr.ge.directory.service.bean.AuthorityBean)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    public void testSaveRemoveUser() throws JsonProcessingException, IOException {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean dbAuthority = new AuthorityBean();
        dbAuthority.setId(42L);
        dbAuthority.setEntityId("Mairie78646");
        dbAuthority.setLabel("Mairie de Versailles");
        dbAuthority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646},'parent':'GE/Mairies'," //
                + "'users':[{'roles':['referent'],'id':'2017-10-USR-XXX-42'}]}")));
        when(this.authorityMapper.getByEntityId(eq("Mairie78646"))).thenReturn(dbAuthority);

        // call
        final AuthorityBean authority = new AuthorityBean();
        authority.setEntityId("Mairie78646");
        authority.setLabel("Mairie de Versailles");
        authority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646},'parent':'GE/Mairies'}")));
        this.authorityDataService.save(authority);

        // verify
        final AuthorityBean updatedAuthority = new AuthorityBean();
        updatedAuthority.setEntityId("Mairie78646");
        updatedAuthority.setLabel("Mairie de Versailles");
        updatedAuthority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646},'parent':'GE/Mairies'}")));
        verify(this.authorityMapper).update(refEq(updatedAuthority));
        verify(this.authorityMapper).removeLink(eq(42L), eq("2017-10-USR-XXX-42"));
    }

    /**
     * Tests
     * {@link AuthorityDataServiceImpl#merge(fr.ge.directory.service.bean.AuthorityBean)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    public void testMergeExists() throws JsonProcessingException, IOException {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean oldAuthority = new AuthorityBean();
        oldAuthority.setId(42L);
        oldAuthority.setEntityId("2018-06-VMB-AM7501");
        oldAuthority.setLabel("CMA DE PARIS");
        oldAuthority.setPath("GE/CMA/2018-06-VMB-AM7501");
        oldAuthority.setDetails(mapper.readTree(this.json(
                "{'ediCode': 'M7501','parent': 'GE/CMA','users': [{'roles': ['referent','accountant','all'],'id': '2018-01-AZV-EPW-25'},{'roles': ['referent','accountant','all'],'id': '2018-01-DYE-XCH-26'},{'roles': ['referent'],'id': '2018-07-ZKS-DNH-67','email': 'test-referent-cma-paris@yopmail.com'},{'roles': ['accountant'],'id': '2018-07-ZJW-WRE-33','email': 'test-accountant-cma-paris@yopmail.com'}],'profile': {'tel': '0153335333','fax': '0143431232','email': 'cmaParis@yopmail.com','url': '','observation': '','address': {'recipientName': 'CMA DE PARIS','addressNameCompl': '72 RUE DE REUILLY','compl': '','special': '','postalCode': '75592','cedex': '12','cityNumber': '75112'}},'payment': 'docomo','transferChannels': {'backoffice': {'state': 'disabled'},'email': {'emails': ['kamal@yopmail.com','kamal2@yopmail.com'],'state': 'enabled'},'address': {'addressDetail': {'recipientName': null,'recipientNameCompl': null,'addressName': null,'addressNameCompl': null,'cityName': null,'postalCode': null},'state': 'disabled'},'ftp': {'state': 'disabled','token': '','folderPath': null}}}")));
        when(this.authorityMapper.getByEntityId(eq("2018-06-VMB-AM7501"))).thenReturn(oldAuthority);

        // call
        final AuthorityBean newAuthority = new AuthorityBean();
        newAuthority.setEntityId("2018-06-VMB-AM7501");
        // simple email addresses update
        newAuthority.setDetails(mapper.readTree(this.json(
                "{'transferChannels': {'backoffice': {'state': 'disabled'},'email': {'emails': ['kama2@yopmail.com','kamal3@yopmail.com'],'state': 'enabled'},'address': {'addressDetail': {'recipientName': null,'recipientNameCompl': null,'addressName': null,'addressNameCompl': null,'cityName': null,'postalCode': null},'state': 'disabled'},'ftp': {'state': 'disabled','token': '','folderPath': null}}}")));
        this.authorityDataService.merge(newAuthority);

        // verify
        ArgumentCaptor<AuthorityBean> authorityCaptor = ArgumentCaptor.forClass(AuthorityBean.class);
        verify(this.authorityMapper).update(authorityCaptor.capture());
        AuthorityBean savedAuthority = authorityCaptor.getValue();
        assertThat(savedAuthority.getEntityId(), equalTo("2018-06-VMB-AM7501"));
        assertThat(savedAuthority.getDetails().get("users").getClass().toString(), equalTo(ArrayNode.class.toString()));

        verify(this.authorityMapper, never()).createLink(any(Long.class), any(String.class));
        verify(this.authorityMapper, never()).removeLink(any(Long.class), any(String.class));
    }

    /**
     * Tests
     * {@link AuthorityDataServiceImpl#merge(fr.ge.directory.service.bean.AuthorityBean)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    @Ignore("méthode merge à supprimer suite aux modification du modèle de directory")
    public void testMergeNotExists() throws JsonProcessingException, IOException {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        when(this.authorityMapper.getByEntityId(eq("Mairie78646"))).thenReturn(null);

        // call
        final AuthorityBean srcAuthority = new AuthorityBean();
        srcAuthority.setEntityId("Mairie78646");
        srcAuthority.setLabel("Mairie de Paris");
        srcAuthority.setPath("GE/Mairies/Mairie78646");
        srcAuthority.setDetails(mapper.readTree(this.json("{'srcAttr':'srcVal','email':'xxx@xxx.com','address':{'zipCode':75012}}")));
        this.authorityDataService.merge(srcAuthority);

        // verify
        verify(this.authorityMapper).create(refEq(srcAuthority));
    }

    /**
     * Tests {@link AuthorityDataServiceImpl#remove(long)}.
     */
    @Test
    public void testRemove() {
        // prepare
        when(this.authorityMapper.removeLinks(eq(42L))).thenReturn(2);
        when(this.authorityMapper.remove(eq(42L))).thenReturn(1);

        // call
        final boolean result = this.authorityDataService.remove(42L);

        // verify
        verify(this.authorityMapper).removeLinks(eq(42L));
        verify(this.authorityMapper).remove(eq(42L));
        assertThat(result, equalTo(true));
    }

    /**
     * Tests {@link AuthorityDataServiceImpl#remove(long)}.
     */
    @Test
    public void testRemoveNoElement() {
        // prepare
        when(this.authorityMapper.removeLinks(eq(42L))).thenReturn(0);
        when(this.authorityMapper.remove(eq(42L))).thenReturn(0);

        // call
        final boolean result = this.authorityDataService.remove(42L);

        // verify
        verify(this.authorityMapper).removeLinks(eq(42L));
        verify(this.authorityMapper).remove(eq(42L));
        assertThat(result, equalTo(false));
    }

    /**
     * Tests
     * {@link AuthorityDataServiceImpl#createLink(String, String, String)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    @Ignore("Problème de mapping dozer, compliqué à résoudre")
    public void testCreateLink() throws JsonProcessingException, IOException {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean authority = new AuthorityBean();
        authority.setId(42L);
        authority.setEntityId("Mairie78646");
        authority.setLabel("Mairie de Versailles");
        authority.setPath("GE/Mairies/Mairie78646");
        authority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646}," //
                + "'users':[{'roles':['referent'],'id':'2017-10-USR-XXX-41'}]}")));
        when(this.authorityMapper.getByEntityId(eq("Mairie78646"))).thenReturn(authority);

        // call
        this.authorityDataService.createLink("Mairie78646", "2017-10-USR-XXX-42", "referent");

        // verify
        final AuthorityBean mergedAuthority = new AuthorityBean();
        mergedAuthority.setId(42L);
        mergedAuthority.setEntityId("Mairie78646");
        mergedAuthority.setLabel("Mairie de Versailles");
        mergedAuthority.setPath("GE/Mairies/Mairie78646");
        mergedAuthority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646}," //
                + "'users':[{'roles':['referent'],'id':'2017-10-USR-XXX-41'},{'roles':['referent'],'id':'2017-10-USR-XXX-42'}]}")));
        verify(this.authorityMapper).update(refEq(mergedAuthority));
        verify(this.authorityMapper).createLink(eq(42L), eq("2017-10-USR-XXX-42"));
    }

    /**
     * Tests {@link AuthorityDataServiceImpl#findAuthoritiesByUserId(String)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    public void testFindAuthoritiesByUserId() throws JsonProcessingException, IOException {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean bean = new AuthorityBean();
        bean.setId(42L);
        bean.setEntityId("Mairie78646");
        bean.setLabel("Mairie de Versailles");
        bean.setPath("GE/Mairies/Mairie78646");
        bean.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646},'users':[{'roles':['user','referent'],'id':'2017-10-USR-XXX-42'}]}")));
        final List<AuthorityBean> authorities = Arrays.asList(bean, bean, bean);
        when(this.authorityMapper.findAuthoritiesByUserId(eq("2017-10-USR-XXX-42"))).thenReturn(authorities);

        // call
        final List<AuthorityRoleBean> result = this.authorityDataService.findAuthoritiesByUserId("2017-10-USR-XXX-42");

        // verify
        verify(this.authorityMapper).findAuthoritiesByUserId(eq("2017-10-USR-XXX-42"));
        assertThat(result, hasSize(3));
        assertThat(result, hasItem( //
                allOf( //
                        hasProperty("id", equalTo(42L)), //
                        hasProperty("entityId", equalTo("Mairie78646")), //
                        hasProperty("label", equalTo("Mairie de Versailles")), //
                        hasProperty("roles", contains("user", "referent")) //
                ) //
        ) //
        );
    }

    /**
     * Tests
     * {@link AuthorityDataServiceImpl#hasRoleForAuthority(String, String, String)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    public void testHasRoleForAuthority() throws JsonProcessingException, IOException {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean bean = new AuthorityBean();
        bean.setId(42L);
        bean.setEntityId("Mairie78646");
        bean.setLabel("Mairie de Versailles");
        bean.setPath("GE/Mairies/Mairie78646");
        bean.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','parent':'GE/Mairies','address':{'zipCode':78646},'users':[{'roles':['referent'],'id':'2017-10-USR-XXX-42'}]}")));
        final List<AuthorityBean> authorities = Arrays.asList(bean, bean, bean);
        when(this.authorityMapper.getByEntityId(eq("Mairie78646"))).thenReturn(bean);
        when(this.authorityMapper.findAuthoritiesByUserId(eq("2017-10-USR-XXX-42"))).thenReturn(authorities);

        // call
        final boolean result = this.authorityDataService.hasRoleForAuthority("2017-10-USR-XXX-42", "referent", "Mairie78646");

        // verify
        verify(this.authorityMapper).findAuthoritiesByUserId(eq("2017-10-USR-XXX-42"));
        assertThat(result, equalTo(true));
    }

    /**
     * Tests
     * {@link AuthorityDataServiceImpl#hasRoleForAuthority(String, String, String)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    public void testHasRoleForAuthorityWrongRole() throws JsonProcessingException, IOException {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean bean = new AuthorityBean();
        bean.setId(42L);
        bean.setEntityId("Mairie78646");
        bean.setLabel("Mairie de Versailles");
        bean.setPath("GE/Mairies/Mairie78646");
        bean.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com', 'parent':'GE/Mairies','address':{'zipCode':78646},'users':[{'roles':['user'],'id':'2017-10-USR-XXX-42'}]}")));
        final List<AuthorityBean> authorities = Arrays.asList(bean, bean, bean);
        when(this.authorityMapper.getByEntityId(eq("Mairie78646"))).thenReturn(bean);
        when(this.authorityMapper.findAuthoritiesByUserId(eq("2017-10-USR-XXX-42"))).thenReturn(authorities);

        // call
        final boolean result = this.authorityDataService.hasRoleForAuthority("2017-10-USR-XXX-42", "referent", "Mairie78646");

        // verify
        verify(this.authorityMapper).findAuthoritiesByUserId(eq("2017-10-USR-XXX-42"));
        assertThat(result, equalTo(false));
    }

    /**
     * Tests
     * {@link AuthorityDataServiceImpl#hasRoleForAuthority(String, String, String)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    public void testHasRoleForAuthorityPathHierarchy() throws JsonProcessingException, IOException {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean typedAuthority = new AuthorityBean();
        typedAuthority.setId(42L);
        typedAuthority.setEntityId("Mairie78646");
        typedAuthority.setLabel("Mairie de Versailles");
        typedAuthority.setPath("GE/Mairies/Mairie78646");
        typedAuthority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com', 'parent':'GE/Mairies','address':{'zipCode':78646},'users':[{'roles':['referent'],'id':'2017-10-USR-XXX-42'}]}")));
        when(this.authorityMapper.getByEntityId(eq("Mairie78646"))).thenReturn(typedAuthority);

        final AuthorityBean referentAuthority = new AuthorityBean();
        referentAuthority.setId(43L);
        referentAuthority.setEntityId("Mairies");
        referentAuthority.setLabel("Mairie de Versailles");
        referentAuthority.setPath("GE/Mairies");
        referentAuthority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','parent':'GE','address':{'zipCode':78646},'users':[{'roles':['referent'],'id':'2017-10-USR-XXX-42'}]}")));
        final List<AuthorityBean> referentAuthorities = Arrays.asList(referentAuthority, referentAuthority, referentAuthority);
        when(this.authorityMapper.findAuthoritiesByUserId(eq("2017-10-USR-XXX-42"))).thenReturn(referentAuthorities);

        // call
        final boolean result = this.authorityDataService.hasRoleForAuthority("2017-10-USR-XXX-42", "referent", "Mairie78646");

        // verify
        verify(this.authorityMapper).findAuthoritiesByUserId(eq("2017-10-USR-XXX-42"));
        assertThat(result, equalTo(true));
    }

    /**
     * Tests
     * {@link AuthorityDataServiceImpl#hasRoleForAuthority(String, String, String)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    public void testHasRoleForAuthorityPathWrongHierarchy() throws JsonProcessingException, IOException {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean typedAuthority = new AuthorityBean();
        typedAuthority.setId(42L);
        typedAuthority.setEntityId("Mairies");
        typedAuthority.setLabel("Mairie de Versailles");
        typedAuthority.setPath("GE/Mairies");
        typedAuthority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','parent':'GE','address':{'zipCode':78646},'users':[{'roles':['referent'],'id':'2017-10-USR-XXX-42'}]}")));
        when(this.authorityMapper.getByEntityId(eq("Mairie78646"))).thenReturn(typedAuthority);

        final AuthorityBean referentAuthority = new AuthorityBean();
        referentAuthority.setId(43L);
        referentAuthority.setEntityId("Mairie78646");
        referentAuthority.setLabel("Mairie de Versailles");
        referentAuthority.setPath("GE/Mairies/Mairie78646");
        referentAuthority
                .setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','parent':'GE/Mairies','address':{'zipCode':78646},'users':[{'roles':['referent'],'id':'2017-10-USR-XXX-42'}]}")));
        final List<AuthorityBean> referentAuthorities = Arrays.asList(referentAuthority, referentAuthority, referentAuthority);
        when(this.authorityMapper.findAuthoritiesByUserId(eq("2017-10-USR-XXX-42"))).thenReturn(referentAuthorities);

        // call
        final boolean result = this.authorityDataService.hasRoleForAuthority("2017-10-USR-XXX-42", "referent", "Mairie78646");

        // verify
        verify(this.authorityMapper).findAuthoritiesByUserId(eq("2017-10-USR-XXX-42"));
        assertThat(result, equalTo(false));
    }

    /**
     * Tests
     * {@link AuthorityDataServiceImpl#search(fr.ge.directory.bean.SearchQuery, Class)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @SuppressWarnings("unchecked")
    @Test
    // @Ignore("Problème de mapping dozer, compliqué à résoudre")
    public void testSearch() throws JsonProcessingException, IOException {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean bean = new AuthorityBean();
        bean.setId(42L);
        bean.setEntityId("Mairie78646");
        bean.setLabel("Mairie de Versailles");
        bean.setPath("GE/Mairies/Mairie78646");
        bean.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','parent':'GE/Mairies'}")));
        final List<AuthorityBean> dataSearchResult = Arrays.asList(bean, bean, bean);
        when(this.authorityMapper.findAll(any(Map.class), eq(null), any(RowBounds.class), eq(null))).thenReturn(dataSearchResult);
        when(this.authorityMapper.count(eq(new HashMap<>()), eq(null), eq(null))).thenReturn(3L);

        // call
        final SearchQuery searchQuery = new SearchQuery(0, 5);
        final SearchResult<AuthorityBean> result = this.authorityDataService.search(searchQuery, AuthorityBean.class);

        // verify
        verify(this.authorityMapper).findAll(any(Map.class), eq(null), any(RowBounds.class), eq(null));
        verify(this.authorityMapper).count(eq(new HashMap<>()), eq(null), eq(null));
        assertThat(result, allOf( //
                hasProperty("totalResults", equalTo(3L)), //
                hasProperty("content", hasSize(3)), hasProperty("content", hasItem( //
                        allOf( //
                                hasProperty("id", equalTo(42L)), //
                                hasProperty("entityId", equalTo("Mairie78646")), //
                                hasProperty("label", equalTo("Mairie de Versailles")), //
                                hasProperty("path", equalTo("GE/Mairies/Mairie78646")), //
                                hasProperty("details", equalTo(mapper.readTree(this.json("{'email':'xxx@xxx.com','parent':'GE/Mairies'}")))) //
                        ) //
                ) //
                ) //
        ) //
        );
    }

}
