package fr.ge.directory.service.mapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mybatis.spring.MyBatisSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.directory.service.bean.ReferentialItemBean;
import fr.ge.directory.service.data.AbstractDbTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class ReferentialItemMapperTest extends AbstractDbTest {

    /** The algo mapper. */
    @Autowired
    private ReferentialItemMapper referentialItemMapper;

    /**
     * Tests {@link ReferentialItemMapper#get(String,Long)}.
     * 
     * null String correct Long
     */
    @Test(expected = BadSqlGrammarException.class)
    public void testGetBadSqlGrammarException() {

        /**
         * null String correct Long
         */
        this.referentialItemMapper.get(null, 1L);

        /**
         * null String wrong Long
         */
        this.referentialItemMapper.get(null, 0L);

        /**
         * null String null Long
         */
        this.referentialItemMapper.get(null, null);

        /**
         * wrong String correct Long
         */
        this.referentialItemMapper.get("", 1L);

        /**
         * wrong String wrong Long
         */
        this.referentialItemMapper.get("", 0L);

        /**
         * wrong String null Long
         */
        this.referentialItemMapper.get("", null);

        /**
         * correct String wrong Long
         */
        this.referentialItemMapper.get("ref_economic_zone", 0L);

        /**
         * correct String null Long
         */
        this.referentialItemMapper.get("ref_economic_zone", null);
    }

    /**
     * Tests {@link ReferentialItemMapper#get(String,Long)}.
     * 
     * correct String correct Long
     */
    @Test
    public void testGetNotNullResult() {
        final String tableName = "ref_economic_zone";
        final long id = 1L;

        final ReferentialItemBean result = this.referentialItemMapper.get(tableName, id);

        assertThat(result,
                allOf( //
                        notNullValue(), //
                        hasProperty("id", equalTo(id)), //
                        hasProperty("label", equalTo("Andorre AD")), //
                        hasProperty("detail", equalTo("{ \"code\":\"AD\", \"label\":\"Andorre\"}")) //
                ));
    }

    /**
     * Tests
     * {@link ReferentialItemMapper#findReferentialItemBySearchedValue(String,Long)}.
     * 
     */
    @Test(expected = MyBatisSystemException.class)
    public void testfindReferentialItemBySearchTextNullResult() {
        this.referentialItemMapper.find(null, null);
        this.referentialItemMapper.find(null, Arrays.asList());
        this.referentialItemMapper.find("", null);
        this.referentialItemMapper.find("", Arrays.asList());
    }

    /**
     * Tests
     * {@link ReferentialItemMapper#findReferentialItemBySearchedValue(String,Long)}.
     * 
     */
    @Test
    public void testfindReferentialItemBySearchTextResult() {

        // test one result
        final List<ReferentialItemBean> referentialOneResult = this.referentialItemMapper.find("ref_economic_zone", Arrays.asList("Emirats"));
        assertThat(referentialOneResult.size(), equalTo(1));

        // test two results
        final List<ReferentialItemBean> referentialTwoResults = this.referentialItemMapper.find("ref_economic_zone", Arrays.asList("Andorre"));
        assertThat(referentialTwoResults.size(), equalTo(4));

        // test many results
        final List<ReferentialItemBean> referentialManyResults = this.referentialItemMapper.find("ref_economic_zone", Arrays.asList());
        assertThat(referentialManyResults.size(), notNullValue());
    }

    private final ObjectMapper mapper = new ObjectMapper();

    /**
     * Tests {@link ReferentialItemMapper#create(String,ReferentialItemBean)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    public void testCreateReferentialItemBean() throws JsonProcessingException, IOException {
        final List<JsonNode> itemsList = Arrays.asList( //
                this.mapper.readTree("{\"key1\":\"Value 1.1\",\"key2\":\"Value 1.2\"}"), //
                this.mapper.readTree("{\"key1\":\"Value 2.1\",\"key2\":\"Value 2.2\",\"label\":\"Item label\"}") //
        );

        // verify
        final ReferentialItemBean item = new ReferentialItemBean();
        item.setLabel("Value 1.1 Value 1.2");
        item.setDetails(itemsList.get(0));

        this.referentialItemMapper.create("ref_economic_zone", item);

        assertThat(this.referentialItemMapper.get("ref_economic_zone", 292L),
                allOf( //
                        notNullValue(), //
                        hasProperty("id", notNullValue()), //
                        hasProperty("label", equalTo("Value 1.1 Value 1.2")), //
                        hasProperty("detail", notNullValue())));
    }

}
