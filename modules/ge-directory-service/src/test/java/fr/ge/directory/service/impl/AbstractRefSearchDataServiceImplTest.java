/**
 *
 */
package fr.ge.directory.service.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.directory.service.ISearchV2DataService;
import fr.ge.directory.service.bean.ReferentialItemBean;
import fr.ge.directory.service.mapper.ReferentialItemMapper;

/**
 * Tests {@link AlgoDataServiceImpl}.
 *
 * @author ijijon
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml", "classpath:spring/test-persistence-context.xml" })
public class AbstractRefSearchDataServiceImplTest {

    /** AbstractRefSearch data service. */
    @Autowired
    ISearchV2DataService AbstractRefSearchDataServiceImpl;

    @Autowired
    private ReferentialItemMapper referentialMapper;

    /**
     * Setup.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        Mockito.reset(this.referentialMapper);
    }

    @Test
    public void testSearch() throws Exception {

        // prepare
        SearchQuery searchQuery = new SearchQuery();
        searchQuery.setStartIndex(0);
        searchQuery.setMaxResults(1);
        searchQuery.setSearchTerms("searchedValue");
        SearchQueryFilter filter = new SearchQueryFilter("un.filtre");
        filter.setPath("un.chemin.avec.points");
        searchQuery.addFilter(filter);
        SearchQueryFilter filter2 = new SearchQueryFilter("un.autre.filtre");
        filter2.setPath("uncheminsanspoints");
        searchQuery.addFilter(filter2);

        final Map<String, Object> filters = new HashMap<>();
        final RowBounds rowBounds = new RowBounds();

        when(this.referentialMapper.findAll(null, filters, null, rowBounds, StringUtils.upperCase(searchQuery.getSearchTerms()))).thenReturn(new ArrayList<ReferentialItemBean>());

        // call
        AbstractRefSearchDataServiceImpl.search(searchQuery, null, "table");

        // verify
        verify(this.referentialMapper, atLeast(1)).findAll(any(), any(), any(), any(RowBounds.class), eq("SEARCHEDVALUE"));
    }
}
