/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.directory.service.mapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.directory.service.bean.AuthorityBean;
import fr.ge.directory.service.data.AbstractDbTest;

/**
 * Tests {@link AuthorityMapper}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class AuthorityMapperTest extends AbstractDbTest {

    /** The mapper mapper. */
    @Autowired
    private AuthorityMapper mapper;

    /**
     * Setup.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
    }

    /**
     * Tests {@link AuthorityMapper#get(Long)}.
     */
    @Test
    public void testGet() {
        assertThat(this.mapper.get(1002L), allOf( //
                notNullValue(), //
                hasProperty("id", equalTo(1002L)), //
                hasProperty("entityId", equalTo("CCI78")), //
                hasProperty("label", equalTo("CCI des Yvelines - Service 1")) //
        // hasProperty("details", equalTo("info1")) // non testable
        ));
    }

    /**
     * Tests {@link AuthorityMapper#getByFuncId(String)}.
     */
    @Test
    public void testGetByFuncId() {
        assertThat(this.mapper.getByEntityId("CCI78"), allOf( //
                notNullValue(), //
                hasProperty("id", equalTo(1002L)), //
                hasProperty("entityId", equalTo("CCI78")), //
                hasProperty("label", equalTo("CCI des Yvelines - Service 1")) //
        // hasProperty("details", equalTo("info1".getBytes())) //non testable
        ));
    }

    /**
     * Tests {@link AuthorityMapper#create(AuthorityBean)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    public void testCreate() throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean authority = new AuthorityBean();
        authority.setEntityId("Mairie92062");
        authority.setLabel("Mairie de Puteaux");
        authority.setDetails(mapper.readTree("{\"info3\":true}"));
        authority.setPath("GE/Mairies/Mairie92062");
        this.mapper.create(authority);
        assertThat(this.mapper.get(1L), allOf( //
                notNullValue(), //
                hasProperty("id", equalTo(1L)), //
                hasProperty("entityId", equalTo("Mairie92062")), //
                hasProperty("label", equalTo("Mairie de Puteaux")) //
        // hasProperty("details", equalTo("info3")), // non testable
        ));
    }

    /**
     * Tests {@link AuthorityMapper#update(AuthorityBean)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    public void testUpdate() throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean authority = new AuthorityBean();
        authority.setEntityId("CCI75");
        authority.setLabel("Chambre de Commerce et d'Industrie de Paris");
        authority.setDetails(mapper.readTree("{\"info4\":true}"));
        authority.setPath("GE/CCI/CCI75");
        this.mapper.update(authority);
        assertThat(this.mapper.getByEntityId("CCI75"), allOf( //
                notNullValue(), //
                hasProperty("id", equalTo(1001L)), //
                hasProperty("entityId", equalTo("CCI75")), //
                hasProperty("label", equalTo("Chambre de Commerce et d'Industrie de Paris")) //
        // hasProperty("details", equalTo("info4")), // non testable
        ));
    }

    /**
     * Tests {@link AuthorityMapper#checkExistsByEntityId(String)}.
     */
    @Test
    public void testCheckExistsByEntityId() {
        assertThat(this.mapper.checkExistsByEntityId("CCI75"), equalTo(true));
        assertThat(this.mapper.checkExistsByEntityId("CCI57"), equalTo(false));
    }

    /**
     * Tests {@link AuthorityMapper#remove(long)}.
     */
    @Test
    public void testRemove() {
        assertThat(this.mapper.get(1006L), notNullValue());
        this.mapper.remove(1006L);
        assertThat(this.mapper.get(1006L), nullValue());
    }

    /**
     * Tests {@link AuthorityMapper#createLink(Long, String)}.
     */
    @Test
    public void testCreateLink() {
        assertThat(this.mapper.findAuthoritiesByUserId("2017-10-XXX-XXX-44"), empty());
        this.mapper.createLink(1003L, "2017-10-XXX-XXX-44");
        assertThat(this.mapper.findAuthoritiesByUserId("2017-10-XXX-XXX-44"), //
                contains(Arrays.asList( //
                        allOf( //
                                hasProperty("id", equalTo(1003L)), //
                                hasProperty("entityId", equalTo("CMA78")) //
                        ) //
                )) //
        );
    }

    /**
     * Tests {@link AuthorityMapper#findAuthoritiesByUserId(String)}.
     */
    @Test
    public void testFindAuthoritiesByUserId() {
        assertThat(this.mapper.findAuthoritiesByUserId("2017-10-XXX-XXX-42"), contains(Arrays.asList( //
                allOf( //
                        hasProperty("id", equalTo(1001L)), //
                        hasProperty("entityId", equalTo("CCI75")), //
                        hasProperty("label", equalTo("CCI de Paris")) //
                // hasProperty("details", equalTo("info1")), //non testable
                ), //
                allOf( //
                        hasProperty("id", equalTo(1002L)), //
                        hasProperty("entityId", equalTo("CCI78")), //
                        hasProperty("label", equalTo("CCI des Yvelines - Service 1")) //
                // hasProperty("details", equalTo("info1")), //non testable
                ) //
        )) //
        );
    }

    /**
     * Tests {@link AuthorityMapper#findAuthoritiesByUserId(String)}.
     */
    @Test
    public void testFindAuthoritiesByUserIdNoAuthority() {
        assertThat(this.mapper.findAuthoritiesByUserId("2017-10-XXX-XXX-43"), empty());
    }

    /**
     * Tests {@link AuthorityMapper#removeLink(Long, String)}.
     */
    @Test
    public void testRemoveLink() {
        assertThat(this.mapper.findAuthoritiesByUserId("2017-10-XXX-XXX-42"), hasSize(2));
        this.mapper.removeLink(1002L, "2017-10-XXX-XXX-42");
        assertThat(this.mapper.findAuthoritiesByUserId("2017-10-XXX-XXX-42"), hasSize(1));
    }

    /**
     * Tests {@link AuthorityMapper#removeLinks(long)}.
     */
    @Test
    public void testRemoveLinks() {
        assertThat(this.mapper.findAuthoritiesByUserId("2017-10-XXX-XXX-42"), hasSize(2));
        this.mapper.removeLinks(1002L);
        assertThat(this.mapper.findAuthoritiesByUserId("2017-10-XXX-XXX-42"), hasSize(1));
    }

}
