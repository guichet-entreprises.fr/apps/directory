package fr.ge.directory.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.query.sql.QueryBuilder;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.mapper.RepositoryDaoMapper;
import fr.ge.directory.ws.v1.model.RepositoryElement;
import fr.ge.directory.ws.v1.model.RepositoryId;

/**
 * Test {@link RepositoryService}.
 * 
 * @author mtakerra
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class RepositoryServiceTest {

    @Autowired
    private RepositoryDaoMapper mapper;

    /** The repository service. */
    @Autowired
    private RepositoryService repositoryService;

    /**
     * Setup.
     */
    @Before
    public void setup() {

        Mockito.reset(this.mapper);

    }

    @Test
    public void testlistRepositories() throws Exception {
        List<String> repositories = new ArrayList<String>();
        repositories.add("organisme_conventionne");

        when(this.mapper.listRepositories()).thenReturn(repositories);

        List<String> returnedRepositories = this.repositoryService.listRepositories();

        // verify
        assertThat(returnedRepositories.get(0), equalTo(repositories.get(0)));
        verify(this.mapper).listRepositories();
    }

    @Test
    public void testAddElement() throws Exception {
        String repositoryId = "organisme_conventionne";
        RepositoryId repositoryName = new RepositoryId();
        repositoryName.setRepositoryId(repositoryId);
        RepositoryElement element = new RepositoryElement();
        element.setRepositoryId(repositoryId);

        Mockito.doNothing().when(mapper).create(repositoryId, element);

        RepositoryId returnedRepositoryId = this.repositoryService.add(repositoryId, element);

        // verify
        assertThat(element.getRepositoryId(), equalTo(returnedRepositoryId.getRepositoryId()));
        verify(this.mapper).create(eq(repositoryId), eq(element));
    }

    @Test
    public void testReadElement() throws Exception {
        String repositoryId = "organisme_conventionne";
        RepositoryElement element = new RepositoryElement();
        element.setId(1L);
        element.setRepositoryId(repositoryId);

        when(this.mapper.read(repositoryId, 1L)).thenReturn(element);

        RepositoryElement returnedElement = this.repositoryService.read(repositoryId, 1L);

        // verify
        assertThat(element.getRepositoryId(), equalTo(returnedElement.getRepositoryId()));
        assertThat(element.getId(), equalTo(returnedElement.getId()));
        verify(this.mapper).read(eq(repositoryId), eq(1L));
    }

    @Test
    public void testAddListElementS() throws Exception {
        String repositoryId = "organisme_conventionne";
        RepositoryId repositoryName = new RepositoryId();
        repositoryName.setRepositoryId(repositoryId);
        RepositoryElement element1 = new RepositoryElement();
        element1.setId(1L);
        element1.setRepositoryId(repositoryId);
        List<RepositoryElement> listElements = new ArrayList<RepositoryElement>();
        listElements.add(element1);

        when(this.mapper.delete(repositoryId)).thenReturn(1);
        Mockito.doNothing().when(mapper).create(repositoryId, element1);

        this.repositoryService.injectRepository(repositoryId, listElements, true);

        // verify
        verify(this.mapper).delete(eq(repositoryId));
    }

    @Test
    public void testSearchElementS() throws Exception {
        String repositoryId = "organisme_conventionne";
        RepositoryId repositoryName = new RepositoryId();
        repositoryName.setRepositoryId(repositoryId);
        RepositoryElement element1 = new RepositoryElement();
        element1.setId(1L);
        element1.setRepositoryId(repositoryId);
        List<RepositoryElement> listElements = new ArrayList<RepositoryElement>();
        listElements.add(element1);
        SearchQuery searchQuery = new SearchQuery();
        searchQuery.addFilter("label", ":", "CMA de l'Aude");
        searchQuery.addFilter("details.label", ":", "CMA de l'Aude");

        SearchResult<RepositoryElement> returnedResult = this.repositoryService.search(repositoryId, searchQuery);

        final ArgumentCaptor<RowBounds> captorRow = ArgumentCaptor.forClass(RowBounds.class);
        final ArgumentCaptor<QueryBuilder> captorQuery = ArgumentCaptor.forClass(QueryBuilder.class);

        // verify
        verify(this.mapper).search(eq(repositoryId), captorQuery.capture(), captorRow.capture());
    }

    @Test
    public void testUpdateElement() throws Exception {
        String repositoryId = "organisme_conventionne";
        long id = 13L;
        RepositoryId repositoryName = new RepositoryId();
        repositoryName.setRepositoryId("invitations");
        RepositoryElement element1 = new RepositoryElement();
        element1.setId(13L);

        Mockito.doNothing().when(mapper).update(Mockito.anyString(), Mockito.any(RepositoryElement.class));

        (this.repositoryService).updateElement(repositoryId, element1);

        verify(this.mapper).update(eq(repositoryId), eq(element1));
    }

    @Test
    public void testListRepositories() throws Exception {

        List<String> listRepositories = new ArrayList<>();
        listRepositories.add("IRCS001");
        listRepositories.add("IRSEI01");
        listRepositories.add("NRCDP01");
        listRepositories.add("DDMEI01");
        listRepositories.add("IRCSB08");
        listRepositories.add("IRCS002");

        when(this.mapper.listRepositories()).thenReturn(listRepositories);

        List<String> listFoundRepositories = (this.repositoryService).listRepositories();

        verify(this.mapper).listRepositories();
    }
}
