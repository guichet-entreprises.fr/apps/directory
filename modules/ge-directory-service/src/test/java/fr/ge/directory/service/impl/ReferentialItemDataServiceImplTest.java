package fr.ge.directory.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.directory.service.IPriceDataService;
import fr.ge.directory.service.IReferentialItemDataService;
import fr.ge.directory.service.bean.ReferentialItemBean;
import fr.ge.directory.service.bean.ReferentialTableEnum;
import fr.ge.directory.service.data.AbstractDbTest;
import fr.ge.directory.service.mapper.ReferentialItemMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml", "classpath:spring/test-persistence-context.xml" })
public class ReferentialItemDataServiceImplTest extends AbstractDbTest {

    /**
     * {@link IPriceDataService}
     */
    @Autowired
    private IReferentialItemDataService referentialItemDataService;

    /**
     * Mapper Of Referential item.
     */
    @Autowired
    private ReferentialItemMapper referentialItemMapper;

    /**
     * Setup.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        Mockito.reset(this.referentialItemMapper);
    }

    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testSave() throws Exception {

        final List<JsonNode> itemsList = Arrays.asList( //
                this.mapper.readTree("{\"key1\":\"Value 1.1\",\"key2\":\"Value 1.2\"}"), //
                this.mapper.readTree("{\"key1\":\"Value 2.1\",\"key2\":\"Value 2.2\",\"label\":\"Item label\"}") //
        );

        // call to save
        this.referentialItemDataService.save(ReferentialTableEnum.COUNTRIES.getFunctionalTable(), itemsList);

        // verify
        final ReferentialItemBean item = new ReferentialItemBean();
        item.setLabel("Value 1.1 Value 1.2");
        item.setDetails(itemsList.get(0));

        final ArgumentCaptor<ReferentialItemBean> captor = ArgumentCaptor.forClass(ReferentialItemBean.class);

        verify(this.referentialItemMapper, times(2)).create(eq(ReferentialTableEnum.COUNTRIES.getTechnicalTable()), captor.capture());

        final List<ReferentialItemBean> actuals = captor.getAllValues();
        final ReferentialItemBean actual = actuals.get(0);

        assertThat(actuals.get(0).getLabel(), equalTo(item.getLabel()));
        assertThat(actuals.get(0).getDetails().get("key1").asText(), equalTo("Value 1.1"));
        assertThat(actuals.get(0).getDetails().get("key2").asText(), equalTo("Value 1.2"));

        assertThat(actuals.get(1).getLabel(), equalTo("Item label"));
        assertThat(actuals.get(1).getDetails().get("key1").asText(), equalTo("Value 2.1"));
        assertThat(actuals.get(1).getDetails().get("key2").asText(), equalTo("Value 2.2"));
        assertThat(actuals.get(1).getDetails().get("label").asText(), equalTo("Item label"));
    }

    @Test
    public void testSaveEmptyDetails() throws Exception {
        final List<JsonNode> itemsList = Arrays.asList();

        this.referentialItemDataService.save(ReferentialTableEnum.COUNTRIES.getFunctionalTable(), itemsList);
        verify(this.referentialItemMapper, times(0)).create(eq(ReferentialTableEnum.COUNTRIES.getFunctionalTable()), any());
    }

    @Test(expected = TechnicalException.class)
    public void testSaveWrongTable() throws Exception {
        final List<JsonNode> itemsList = Arrays.asList( //
                this.mapper.readTree("{\"key1\":\"Value 1.1\",\"key2\":\"Value 1.2\"}"), //
                this.mapper.readTree("{\"key1\":\"Value 2.1\",\"key2\":\"Value 2.2\",\"label\":\"Item label\"}") //
        );

        this.referentialItemDataService.save("ref_communes", itemsList);
        verify(this.referentialItemMapper, times(0)).create(eq("ref_communes"), any());
    }
}
