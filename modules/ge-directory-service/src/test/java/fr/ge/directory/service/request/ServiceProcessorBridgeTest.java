/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.service.request;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.test.AbstractTest;
import fr.ge.directory.bean.Configuration;
import fr.ge.directory.core.exception.TechnicalException;

/**
 * The Class ServiceProcessorBridgeTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/engine-context.xml", "classpath:spring/test-rest-context.xml" })
public class ServiceProcessorBridgeTest extends AbstractTest {

    /** The Constant ENDPOINT. */
    protected static final String ENDPOINT = "http://localhost:8888/test";

    /** The server. */
    protected Server server;

    /** The service. */
    protected ITestService service;

    protected Configuration configuration;

    /**
     * Sets the up.
     */
    @Before
    public void setUp() {
        this.service = mock(ITestService.class);

        final Properties properties = new Properties();
        properties.put("ws.external.url", ENDPOINT);

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        final Object jsonProvider = new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });

        final JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setAddress(ENDPOINT);
        serverFactory.setServiceBean(this.service);
        serverFactory.setProvider(jsonProvider);
        this.server = serverFactory.create();

        this.configuration = new Configuration(properties);
    }

    /**
     * Tear down.
     *
     * @throws Exception
     *             the exception
     */
    @After
    public void tearDown() throws Exception {
        this.server.stop();
        this.server.destroy();
    }

    /**
     * Test get string.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetString() throws Exception {
        final String expectedResponseAsString = "Hello Bob from Luc";

        when(this.service.hello(any(), any())).thenReturn(Response.ok(expectedResponseAsString).build());

        final ServiceProcessorBridge bridge = new ServiceProcessorBridge(this.configuration);
        final ServiceResponse response = bridge //
                .request("${ws.external.url}/v1/test/hello/{name}", "Bob") //
                .accept("text").param("from", "Luc").get();

        final String actualAsString = response.asString();

        verify(this.service).hello(eq("Bob"), eq("Luc"));

        assertEquals(expectedResponseAsString, actualAsString);
    }

    /**
     * Test get object.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetObject() throws Exception {
        final Map<String, String> obj = new HashMap<>();
        obj.put("firstname", "john");
        obj.put("lastname", "doe");

        when(this.service.whoAmI(any(), any())).thenReturn(Response.ok(obj).build());

        final ServiceProcessorBridge bridge = new ServiceProcessorBridge(this.configuration);
        final ServiceResponse response = bridge //
                .request("${ws.external.url}/v1/test/whoami/{name}", "Bob") //
                .accept("json").param("from", "Luc").get();

        final Map<String, String> actual = CoreUtil.cast(response.asObject());

        verify(this.service).whoAmI(eq("Bob"), eq("Luc"));

        assertThat(actual, //
                allOf( //
                        hasEntry("firstname", obj.get("firstname")), //
                        hasEntry("lastname", obj.get("lastname")) //
                ) //
        );
    }

    /**
     * Test get object twice.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetObjectTwice() throws Exception {
        final Map<String, String> obj = new HashMap<>();
        obj.put("firstname", "john");
        obj.put("lastname", "doe");

        when(this.service.whoAmI(any(), any())).thenReturn(Response.ok(obj).build());

        final ServiceProcessorBridge bridge = new ServiceProcessorBridge(this.configuration);
        final ServiceResponse response = bridge //
                .request("${ws.external.url}/v1/test/whoami/{name}", "Bob") //
                .accept("json").param("from", "Luc").get();

        final byte[] responseAsBytes = response.asBytes();
        final Map<String, String> actual = CoreUtil.cast(response.asObject());

        verify(this.service).whoAmI(eq("Bob"), eq("Luc"));

        final Map<String, String> responseAsObject = CoreUtil.cast(new ObjectMapper().readValue(responseAsBytes, Map.class));

        assertThat(responseAsObject, //
                allOf( //
                        hasEntry("firstname", obj.get("firstname")), //
                        hasEntry("lastname", obj.get("lastname")) //
                ) //
        );

        assertThat(actual, //
                allOf( //
                        hasEntry("firstname", obj.get("firstname")), //
                        hasEntry("lastname", obj.get("lastname")) //
                ) //
        );
    }

    /**
     * Test post object.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPostObject() throws Exception {
        final Map<String, String> obj = new HashMap<>();
        obj.put("firstname", "john");
        obj.put("lastname", "doe");

        when(this.service.update(any())).thenReturn(Response.ok(obj).build());

        final ServiceProcessorBridge bridge = new ServiceProcessorBridge(this.configuration);
        final ServiceResponse response = bridge.request("${ws.external.url}/v1/test/update").accept("json").post(obj);

        final Map<String, String> actual = CoreUtil.cast(response.asObject());

        final ArgumentCaptor<Map<String, String>> data = CoreUtil.cast(ArgumentCaptor.forClass(Map.class));

        verify(this.service).update(data.capture());
        final Map<String, String> sendData = data.getValue();

        assertThat(actual, //
                allOf( //
                        hasEntry("firstname", obj.get("firstname")), //
                        hasEntry("lastname", obj.get("lastname")) //
                ) //
        );

        assertThat(sendData, //
                allOf( //
                        hasEntry("firstname", obj.get("firstname")), //
                        hasEntry("lastname", obj.get("lastname")) //
                ) //
        );
    }

    /**
     * Test put object.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPutObject() throws Exception {
        final Map<String, String> obj = new HashMap<>();
        obj.put("firstname", "john");
        obj.put("lastname", "doe");

        when(this.service.update(any())).thenReturn(Response.ok(obj).build());

        final ServiceProcessorBridge bridge = new ServiceProcessorBridge(this.configuration);
        bridge.request("${ws.external.url}/v1/test/putObject").accept("json").put(obj);

        final ArgumentCaptor<Map<String, String>> data = CoreUtil.cast(ArgumentCaptor.forClass(Map.class));

        verify(this.service).putObject(data.capture());
        final Map<String, String> sendData = data.getValue();

        assertThat(sendData, //
                allOf( //
                        hasEntry("firstname", obj.get("firstname")), //
                        hasEntry("lastname", obj.get("lastname")) //
                ) //
        );
    }

    /**
     * Test upload.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testUpload() throws Exception {
        final String expected = "Uploaded content";

        when(this.service.upload(any())).thenReturn(Response.ok().build());

        final ServiceProcessorBridge bridge = new ServiceProcessorBridge(this.configuration);
        bridge //
                .request("${ws.external.url}/v1/test/upload") //
                .accept("json").post(expected.getBytes(StandardCharsets.UTF_8));

        final ArgumentCaptor<byte[]> arg = ArgumentCaptor.forClass(byte[].class);
        verify(this.service).upload(arg.capture());
        assertEquals(expected, new String(arg.getValue(), StandardCharsets.UTF_8));
    }

    /**
     * Test download.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDownload() throws Exception {
        final String expected = "Downloaded content";

        when(this.service.download()).thenReturn(Response.ok(expected.getBytes(StandardCharsets.UTF_8)).build());

        final ServiceProcessorBridge bridge = new ServiceProcessorBridge(this.configuration);
        final ServiceResponse response = bridge //
                .request("${ws.external.url}/v1/test/download") //
                .connectionTimeout(500).receiveTimeout(1000) //
                .accept("bytes").get();

        verify(this.service).download();

        assertNull(response.asObject());
        assertEquals(expected, new String(response.asBytes(), StandardCharsets.UTF_8));
        assertEquals(200, response.getStatus());
    }

    /**
     * Test not found.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testNotFound() throws Exception {
        try {
            final ServiceProcessorBridge bridge = new ServiceProcessorBridge(this.configuration);
            bridge //
                    .request("${ws.external.url}/v1/test/error") //
                    .connectionTimeout(500).receiveTimeout(1000) //
                    .get();
            fail("Expected technical exception");
        } catch (final TechnicalException ex) {
            assertEquals(String.format("HTTP error 404 on '%s/v1/test/error' : Not Found", ENDPOINT), ex.getMessage());
        }
    }

    /**
     * Test error.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testError() throws Exception {
        when(this.service.download()).thenReturn(Response.serverError().build());

        try {
            final ServiceProcessorBridge bridge = new ServiceProcessorBridge(this.configuration);
            bridge //
                    .request("${ws.external.url}/v1/test/download") //
                    .connectionTimeout(500).receiveTimeout(1000) //
                    .accept("bytes").get();
            fail("Expected technical exception");
        } catch (final TechnicalException ex) {
            assertEquals(String.format("HTTP error 500 on '%s/v1/test/download' : Internal Server Error", ENDPOINT), ex.getMessage());
        }
    }

    /**
     * Test multipart as bytes.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testMultipartAsBytes() throws Exception {
        final String sender = "leia.organa@rebellion.org";
        final String receiver = "scn-test@yopmail.com";
        final String object = "Test message";
        final String content = "This a test message.";
        final String attachmentContent = "Content of the attachment";

        when(this.service.mail(any(), any(), any(), any(), any())).thenReturn(Response.ok().build());

        final Map<String, Object> params = new HashMap<>();
        params.put("sender", sender);
        params.put("receiver", receiver);
        params.put("object", object);
        params.put("content", content);
        params.put("file", attachmentContent.getBytes(StandardCharsets.UTF_8));

        final ServiceProcessorBridge bridge = new ServiceProcessorBridge(this.configuration);
        bridge //
                .request("${ws.external.url}/v1/test/mail") //
                .connectionTimeout(500).receiveTimeout(1000) //
                .dataType("form") //
                .post(params);

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.service).mail(eq(sender), eq(receiver), eq(object), eq(content), captor.capture());

        final byte[] actual = captor.getValue();

        assertEquals(attachmentContent, new String(actual));
    }

    /**
     * Test post string.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPostString() throws Exception {
        final String sendedParam = "a simple parameter";

        when(this.service.postString(any())).thenReturn(Response.ok().build());

        final ServiceProcessorBridge bridge = new ServiceProcessorBridge(this.configuration);
        bridge //
                .request("${ws.external.url}/v1/test/postString") //
                .connectionTimeout(500).receiveTimeout(1000) //
                .post(sendedParam);

        verify(this.service).postString(eq(sendedParam));
    }

    /**
     * Test post long.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPostLong() throws Exception {
        final long sendedParam = 42L;

        when(this.service.postString(any())).thenReturn(Response.ok().build());

        final ServiceProcessorBridge bridge = new ServiceProcessorBridge(this.configuration);
        bridge //
                .request("${ws.external.url}/v1/test/postString") //
                .connectionTimeout(500).receiveTimeout(1000) //
                .post(sendedParam);

        verify(this.service).postString(eq("" + sendedParam));
    }

    /**
     * The Interface ITestService.
     */
    @Path("/v1/test")
    public static interface ITestService {

        /**
         * Hello.
         *
         * @param name
         *            the name
         * @param from
         *            the from
         * @return the response
         */
        @GET
        @Path("/hello/{name}")
        @Produces(MediaType.TEXT_PLAIN)
        Response hello(@PathParam("name") String name, @QueryParam("from") String from);

        /**
         * Who am I.
         *
         * @param name
         *            the name
         * @param from
         *            the from
         * @return the response
         */
        @GET
        @Path("/whoami/{name}")
        @Produces(MediaType.APPLICATION_JSON)
        Response whoAmI(@PathParam("name") String name, @QueryParam("from") String from);

        /**
         * Update.
         *
         * @param data
         *            the data
         * @return the response
         */
        @POST
        @Path("/update")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        Response update(Map<String, String> data);

        /**
         * Put object.
         *
         * @param data
         *            the data
         * @return the response
         */
        @PUT
        @Path("/putObject")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        Response putObject(Map<String, String> data);

        /**
         * Upload.
         *
         * @param resourceAsBytes
         *            the resource as bytes
         * @return the response
         */
        @POST
        @Path("/upload")
        @Consumes(MediaType.APPLICATION_OCTET_STREAM)
        Response upload(byte[] resourceAsBytes);

        /**
         * Download.
         *
         * @return the response
         */
        @GET
        @Path("/download")
        @Produces(MediaType.APPLICATION_OCTET_STREAM)
        Response download();

        /**
         * Mail.
         *
         * @param sender
         *            the sender
         * @param receiver
         *            the receiver
         * @param object
         *            the object
         * @param content
         *            the content
         * @param attachment
         *            the attachment
         * @return the response
         */
        @POST
        @Path("/mail")
        @Consumes(MediaType.MULTIPART_FORM_DATA)
        Response mail(@Multipart("sender") String sender, @Multipart("receiver") String receiver, @Multipart("object") String object, @Multipart("content") String content,
                @Multipart("file") byte[] attachment);

        /**
         * Post string.
         *
         * @param input
         *            the input
         * @return the response
         */
        @POST
        @Path("/postString")
        @Consumes(MediaType.TEXT_PLAIN)
        Response postString(String input);

        /**
         * Execute.
         *
         * @param code
         *            the code
         * @param app
         *            the app
         * @param params
         *            the params
         * @return the response
         */
        @POST
        @Path("/{code}/execute")
        @Consumes(MediaType.APPLICATION_JSON)
        Response execute(@PathParam("code") String code, @HeaderParam("application") String app, Map<String, Object> params);

    }

}
