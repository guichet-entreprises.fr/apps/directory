/**
 *
 */
package fr.ge.directory.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.ICommuneDataService;
import fr.ge.directory.service.bean.RefCommuneBean;
import fr.ge.directory.service.data.AbstractDbTest;
import fr.ge.directory.service.mapper.CommuneMapper;

/**
 * Tests {@link AuthorityDataServiceImpl}.
 *
 * @author jpauchet
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml", "classpath:spring/test-persistence-context.xml" })
public class CommuneDataServiceImplTest extends AbstractDbTest {

    /** Algo data service. */
    @Autowired
    ICommuneDataService communeDataService;

    /** Algo mapper. */
    @Autowired
    CommuneMapper communeMapper;

    /**
     * Setup.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        Mockito.reset(this.communeMapper);
    }

    /**
     * Tests {@link CommuneDataServiceImpl#search(SearchQuery, Class)}.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void search() {
        // prepare
        final RefCommuneBean bean = new RefCommuneBean("01001", "01400", "L'ABERGEMENT-CLÉMENCIAT");

        final List<RefCommuneBean> dataSearchResult = Arrays.asList(bean, bean, bean);
        when(this.communeMapper.findAll(any(Map.class), eq(null), any(RowBounds.class), eq(null))).thenReturn(dataSearchResult);
        when(this.communeMapper.count(eq(new HashMap<>()), eq(null), eq(null))).thenReturn(3L);

        // call
        final SearchQuery searchQuery = new SearchQuery(0, 5);
        final SearchResult<RefCommuneBean> result = this.communeDataService.search(searchQuery, RefCommuneBean.class);

        // verify
        verify(this.communeMapper).findAll(any(Map.class), eq(null), any(RowBounds.class), eq(null));
        verify(this.communeMapper).count(eq(new HashMap<>()), eq(null), eq(null));
        assertThat(result, allOf( //
                hasProperty("totalResults", equalTo(3L)), //
                hasProperty("content", hasSize(3))) //
        );
    }

}
