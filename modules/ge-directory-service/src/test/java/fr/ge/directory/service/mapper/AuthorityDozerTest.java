package fr.ge.directory.service.mapper;

import static org.fest.assertions.Assertions.assertThat;

import java.sql.Timestamp;
import java.util.Date;

import org.dozer.DozerBeanMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.directory.service.bean.AuthorityBean;

/**
 *
 * @author $Author: jpauchet $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/dozer-context.xml" })
public class AuthorityDozerTest {

    /** The dozer jsonMapper. */
    @Autowired
    private DozerBeanMapper dozerBeanMapper;

    /** simple json mapper */
    private ObjectMapper jsonMapper = new ObjectMapper();

    /**
     * 
     * @throws Exception
     */
    @Test
    public void testMappingAuthorityToItself() throws Exception {
        AuthorityBean authority = new AuthorityBean();
        authority.setEntityId("U10101");
        authority.setId(10L);
        authority.setLabel("URSSAF de l'Aude");
        authority.setUpdated(new Timestamp(new Date().getTime()));
        authority.setDetails(jsonMapper.readTree(
                "{    \"CCI\": \"CCI01053\",    \"CMA\": \"CMA01053\",    \"GREFFE\": \"GREFFE01053\",    \"CA\": \"CA01053\",    \"URSSAF\": \"URSSAF01053\",    \"parent\": \"GE/communes\"}"));
        AuthorityBean authorityBeanMapped = this.dozerBeanMapper.map(authority, AuthorityBean.class);
        assertThat(authorityBeanMapped).isNotNull();
        assertThat(authorityBeanMapped.getEntityId()).isEqualTo("U10101");
        assertThat(authorityBeanMapped.getId()).isEqualTo(10L);
        assertThat(authorityBeanMapped.getLabel()).isEqualTo("URSSAF de l'Aude");
        assertThat(authorityBeanMapped.getDetails().findValue("CCI").asText()).isEqualTo("CCI01053");
        assertThat(authorityBeanMapped.getDetails().findValue("URSSAF").asText()).isEqualTo("URSSAF01053");
        assertThat(authorityBeanMapped.getDetails().findValue("parent").asText()).isEqualTo("GE/communes");
    }

}
