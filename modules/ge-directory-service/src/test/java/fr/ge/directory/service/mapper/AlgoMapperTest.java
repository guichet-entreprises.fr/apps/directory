/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.directory.service.mapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.directory.service.bean.AlgoBean;
import fr.ge.directory.service.data.AbstractDbTest;

/**
 * Tests {@link AlgoMapper}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class AlgoMapperTest extends AbstractDbTest {

    /** The algo mapper. */
    @Autowired
    private AlgoMapper mapper;

    /**
     * Setup.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
    }

    /**
     * Tests {@link AlgoMapper#get(Long)}.
     */
    @Test
    public void testGet() {
        assertThat(this.mapper.get(1002L),
                allOf( //
                        notNullValue(), //
                        hasProperty("id", equalTo(1002L)), //
                        hasProperty("code", equalTo("debitBoisson")), //
                        hasProperty("content", equalTo("return \"mairie\" + args.codeInsee;".getBytes())) //
                ));
    }

    /**
     * Tests {@link AlgoMapper#getByCode(String)}.
     */
    @Test
    public void testGetByCode() {
        assertThat(this.mapper.getByCode("debitBoisson"),
                allOf( //
                        notNullValue(), //
                        hasProperty("id", equalTo(1002L)), //
                        hasProperty("code", equalTo("debitBoisson")), //
                        hasProperty("content", equalTo("return \"mairie\" + args.codeInsee;".getBytes())) //
                ));
    }

    /**
     * Tests {@link AlgoMapper#create(AlgoBean)}.
     */
    @Test
    public void testCreate() {
        final AlgoBean algo = new AlgoBean();
        algo.setCode("cma");
        algo.setContent("return \"cma\" + args.departement;".getBytes());
        this.mapper.create(algo);
        assertThat(this.mapper.get(1L),
                allOf( //
                        notNullValue(), //
                        hasProperty("id", equalTo(1L)), //
                        hasProperty("code", equalTo("cma")), //
                        hasProperty("content", equalTo("return \"cma\" + args.departement;".getBytes())) //
                ));
    }

    /**
     * Tests {@link AlgoMapper#update(AlgoBean)}.
     */
    @Test
    public void testUpdate() {
        final AlgoBean algo = new AlgoBean();
        algo.setCode("cci");
        algo.setContent("return \"cma\" + args.departement;".getBytes());
        this.mapper.update(algo);
        assertThat(this.mapper.getByCode("cci"),
                allOf( //
                        notNullValue(), //
                        hasProperty("id", equalTo(1001L)), //
                        hasProperty("code", equalTo("cci")), //
                        hasProperty("content", equalTo("return \"cma\" + args.departement;".getBytes())) //
                ));
    }

    /**
     * Tests {@link AlgoMapper#checkExistsByCode(String)}.
     */
    @Test
    public void testCheckExistsByCode() {
        assertThat(this.mapper.checkExistsByCode("cci"), equalTo(true));
        assertThat(this.mapper.checkExistsByCode("ca"), equalTo(false));
    }

}
