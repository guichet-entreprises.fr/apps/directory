package fr.ge.directory.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.IPriceDataService;
import fr.ge.directory.service.bean.RefPriceBean;
import fr.ge.directory.service.data.AbstractDbTest;
import fr.ge.directory.service.mapper.PriceMapper;

/**
 * Test Class of {@link PriceDataServiceImpl}.
 * 
 * @author $Author: sarahman $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml", "classpath:spring/test-persistence-context.xml" })
public class PriceDataServiceImplTest extends AbstractDbTest {

    /**
     * {@link IPriceDataService}
     */
    @Autowired
    private IPriceDataService priceDataService;

    /**
     * Mapper Of Prices.
     */
    @Autowired
    private PriceMapper priceMapper;

    /**
     * Setup.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        Mockito.reset(this.priceMapper);
    }

    @Test
    public void testSearch() throws Exception {
        // prepare
        final RefPriceBean bean = new RefPriceBean("IRC001", "Inscription au Registre du Commerce et des Sociétés d une entreprise individuelle (par création)", "13,00", null, null);

        final List<RefPriceBean> dataSearchResult = Arrays.asList(bean, bean, bean);
        when(this.priceMapper.findAll(any(Map.class), eq(null), any(RowBounds.class), eq(null))).thenReturn(dataSearchResult);
        when(this.priceMapper.count(eq(new HashMap<>()), eq(null), eq(null))).thenReturn(3L);

        // call
        final SearchQuery searchQuery = new SearchQuery(0, 5);
        final SearchResult<RefPriceBean> result = this.priceDataService.search(searchQuery, RefPriceBean.class);

        // verify
        verify(this.priceMapper).findAll(any(Map.class), eq(null), any(RowBounds.class), eq(null));
        verify(this.priceMapper).count(eq(new HashMap<>()), eq(null), eq(null));
        assertThat(result,
                allOf( //
                        hasProperty("totalResults", equalTo(3L)), //
                        hasProperty("content", hasSize(3))) //
        );
    }

}
