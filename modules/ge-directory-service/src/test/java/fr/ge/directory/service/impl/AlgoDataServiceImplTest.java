/**
 *
 */
package fr.ge.directory.service.impl;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.directory.service.IAlgoDataService;
import fr.ge.directory.service.IAuthorityDataService;
import fr.ge.directory.service.bean.AlgoBean;
import fr.ge.directory.service.mapper.AlgoMapper;
import fr.ge.directory.service.mapper.AuthorityMapper;

/**
 * Tests {@link AlgoDataServiceImpl}.
 *
 * @author ijijon
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml", "classpath:spring/test-persistence-context.xml" })
public class AlgoDataServiceImplTest {

    /** Algo data service. */
    @Autowired
    IAlgoDataService algoDataService;

    /** Authority data service. */
    @Autowired

    IAuthorityDataService authorityDataService;

    /** Authority mapper. */
    @Autowired
    AuthorityMapper authorityMapper;

    /** Algo mapper. */
    @Autowired
    AlgoMapper algoMapper;

    /**
     * Setup.
     *
     * @throws Exception
     *             exception
     */
    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception {
        Mockito.reset(this.authorityMapper, this.algoMapper);
    }

    @Test
    public void testExecute() throws Exception {
        // prepare
        byte[] content = "content".getBytes();
        AlgoBean algo = new AlgoBean();
        algo.setContent(content);

        when(this.algoMapper.getByCode("code")).thenReturn(algo);

        // call
        this.algoDataService.execute("code", null);

        // verify
        verify(this.algoMapper).getByCode("code");
    }

    @Test
    public void testGet() throws Exception {
        // prepare
        when(this.algoMapper.get(1L)).thenReturn(new AlgoBean());

        // call
        this.algoDataService.get(1L);

        // verify
        verify(this.algoMapper).get(1L);
    }

    @Test
    public void testGetByCode() throws Exception {
        // prepare
        when(this.algoMapper.getByCode("authorityType")).thenReturn(new AlgoBean());

        // call
        this.algoDataService.getByCode("authorityType");

        // verify
        verify(this.algoMapper).getByCode("authorityType");
    }

    /**
     * Tests {@link AlgoDataServiceImpl#removeAll()}.
     */
    @Test
    public void testRemoveAll() {
        // prepare
        doNothing().when(this.algoMapper).removeAll();

        // call
        this.algoDataService.removeAll();

        // verify
        verify(this.algoMapper).removeAll();
    }

    @Test
    public void testRemoveFalse() throws Exception {
        // prepare
        when(this.algoMapper.remove(1L)).thenReturn(0);

        // call
        this.algoDataService.remove(1L);

        // verify
        verify(this.algoMapper).remove(1L);
    }

    @Test
    public void testRemoveTrue() throws Exception {
        // prepare
        when(this.algoMapper.remove(1L)).thenReturn(1);

        // call
        this.algoDataService.remove(1L);

        // verify
        verify(this.algoMapper).remove(1L);
    }

    @Test
    public void testSaveFalse() throws Exception {
        // prepare
        final AlgoBean algo = new AlgoBean();
        algo.setCode("code");
        when(this.algoMapper.checkExistsByCode("code")).thenReturn(false);
        doNothing().when(this.algoMapper).create(algo);

        // call
        this.algoDataService.save(algo);

        // verify
        verify(this.algoMapper).create(algo);
    }

    @Test
    public void testSaveTrue() throws Exception {
        // prepare
        final AlgoBean algo = new AlgoBean();
        algo.setCode("code");
        when(this.algoMapper.checkExistsByCode("code")).thenReturn(true);
        doNothing().when(this.algoMapper).update(algo);

        // call
        this.algoDataService.save(algo);

        // verify
        verify(this.algoMapper).update(algo);
    }
}
