/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.directory.service.mapper;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.directory.service.bean.RefCommuneBean;
import fr.ge.directory.service.data.AbstractDbTest;

/**
 * Tests {@link AlgoMapper}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class CommuneMapperTest extends AbstractDbTest {

    /** The algo mapper. */
    @Autowired
    private CommuneMapper mapper;

    /**
     * Setup.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
    }

    @Test
    public void testSearch() {

        final List<SearchQueryOrder> orders = new ArrayList<>();
        final List<SearchQueryFilter> filters = new ArrayList<>();
        orders.add(new SearchQueryOrder("codeCommune", "asc"));
        final SearchQuery search = new SearchQuery(0, 5L);
        search.setOrders(orders);
        final SearchQueryFilter filter = new SearchQueryFilter("codeCommune", ":", "01001");
        final SearchQueryFilter filter1 = new SearchQueryFilter("codePostal", ":", "01400");
        filters.add(filter);
        filters.add(filter1);
        search.setFilters(filters);

        final RowBounds rowBounds = new RowBounds(0, 5);
        final Map<String, Object> map = new HashMap<>();
        search.getFilters().forEach(t -> map.put(t.getColumn(), t));

        final List<RefCommuneBean> actual = this.mapper.findAll(map, search.getOrders(), rowBounds, null);

        assertEquals(actual.size(), 1);
    }

    @Test
    public void testSearchLabel() {

        final List<SearchQueryOrder> orders = new ArrayList<>();
        final List<SearchQueryFilter> filters = new ArrayList<>();
        orders.add(new SearchQueryOrder("codecommune", "asc"));
        final SearchQuery search = new SearchQuery(0, 5L);
        search.setOrders(orders);
        // SearchQueryFilter filter = new SearchQueryFilter("libelle", "%",
        // "AMBÉRIEU");
        //
        // //filters.add(filter);
        // search.setFilters(filters);

        final RowBounds rowBounds = new RowBounds(0, 10);
        final Map<String, Object> map = new HashMap<>();
        // search.getFilters().forEach(t -> map.put(t.getColumn(), t));

        final List<RefCommuneBean> actual = this.mapper.findAll(map, search.getOrders(), rowBounds, null);

        assertEquals(actual.size(), 10);
    }

}
