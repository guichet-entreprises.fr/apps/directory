--Renommer les identifiants des autorités RM pour avoir un total de 18 caractères
update authority
set entity_id = replace(
	regexp_replace(entity_id, '^\d{4}-\d{2}-', to_char(now(),'YYYY') || '-' || to_char(now(),'MM') || '-', 'g'), '-RM', ''
),
details = 
replace(
( 	'{' ||
	'"parent":' || (details->'parent')::text || ', ' ||
	'"profile":' || (details->'profile')::text || ', ' || 
	'"transferChannels":' || (details->'transferChannels')::text
	|| '}' ),
'}{', 
', ')::json 
where 1=1
and entity_id like '%-RM';

--Renommer les identifiants des autorités RM dans les zones géographiques
update authority set details = 
replace(
	( 	
		'{"parent":' || (details->'parent')::text || 	
		', "codeCommune":' || (details->'codeCommune')::text || 	
		', "CCI":' || (details->'CCI')::text || 	
		', "CMA":' || (details->'CMA')::text || 	
		', "GREFFE":' || (details->'GREFFE')::text || 	
		', "CA":' || (details->'CA')::text || 	
		', "URSSAF":' || (details->'URSSAF')::text || 	
		', "CNBA":' || (details->'CNBA')::text ||  	
		', "RM":"' || replace(
			regexp_replace((details->>'RM'), '^\d{4}-\d{2}-', to_char(now(),'YYYY') || '-' || to_char(now(),'MM') || '-', 'g'), '-RM', ''
		) || '"'

		'}' ),
	'}{', 
	', ')::json 
where 1=1
and (details->'parent')::text = '"ZONES"'
and (details->'RM')::text is not null
;
