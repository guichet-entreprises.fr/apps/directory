
-- -------------------------------------------------------------------------------------
--
-- repositories
--
-- New unique generic table for data repositories
--
-- --------------------------------------------------------------------------------------
CREATE TABLE repositories (
   id BIGINT PRIMARY KEY, --unique id incremented with the sequence sq_repositories
   repository_id VARCHAR(25) NOT NULL, -- repository name
   entity_id VARCHAR(50) NOT NULL, -- id of the entity
   label VARCHAR(250) NOT NULL, -- label of the entity
   details JSON, -- details of the entity in JSON
   updated timestamp without time zone NOT NULL DEFAULT now() -- date of last update on the entity
);
CREATE SEQUENCE sq_repositories START 1 MINVALUE 1 CACHE 1;