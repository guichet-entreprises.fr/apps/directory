-- ----------------------------------------------------------------------------
--Table: ref_fragment_complex
-- create table ref_fragment_complex
--
-- ----------------------------------------------------------------------------



CREATE TABLE IF NOT EXISTS "ref_fragment_complex" (
   id BIGINT NOT NULL,
   label VARCHAR(250),
   detail JSON,
   created timestamp without time zone NOT NULL DEFAULT now(),
   PRIMARY KEY (id)
);

   
CREATE SEQUENCE sq_ref_fragment_complex
   START 1
   MINVALUE 1
   CACHE 1;