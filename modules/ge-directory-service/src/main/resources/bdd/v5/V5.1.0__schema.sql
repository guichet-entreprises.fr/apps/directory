
-- ----------------------------------------------------------------------------
--Table: grille_tarifaire
-- create table grille_tarifaire
--
-- ----------------------------------------------------------------------------
CREATE TABLE grille_tarifaire (
    id              BIGINT PRIMARY KEY,
    code            VARCHAR(100) 	NOT NULL,
    produit         VARCHAR(500) 	NOT NULL,
    tarif           money		NOT NULL,   
    date_debut      TIMESTAMP ,
    date_fin        TIMESTAMP 
    
);
CREATE SEQUENCE sq_grille_tarifaire
    START 1
    MINVALUE 1
    CACHE 1;