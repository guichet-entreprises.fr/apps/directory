-- ----------------------------------------------------------------------------
--Table: ref_requirement
-- create table ref_requirement
--
--Table: ref_economic_zone
-- create table ref_economic_zone
--
--Table: ref_merchandise
-- create table ref_merchandise
--
-- ----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS "ref_requirement" (
    id BIGINT NOT NULL,
    label text,
    detail JSON,
    created timestamp without time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS "ref_economic_zone" (
    id BIGINT NOT NULL,
    label VARCHAR(250),
    detail JSON,
    created timestamp without time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS "ref_merchandise" (
    id BIGINT NOT NULL,
    label VARCHAR(250),
    detail JSON,
    created timestamp without time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (id)
);

CREATE SEQUENCE sq_ref_requirement
    START 1
    MINVALUE 1
    CACHE 1;
    
CREATE SEQUENCE sq_ref_economic_zone
    START 1
    MINVALUE 1
    CACHE 1;
    
CREATE SEQUENCE sq_ref_merchandise
    START 1
    MINVALUE 1
    CACHE 1;