-- ----------------------------------------------------------------------------
--Table: prices
-- create table prices
--
-- ----------------------------------------------------------------------------



DROP TABLE grille_tarifaire;

DROP SEQUENCE sq_grille_tarifaire;


CREATE TABLE prices (
    id              BIGINT PRIMARY KEY,
    code            VARCHAR(100) 	NOT NULL,
    product         VARCHAR(500) 	NOT NULL,
    price           money		NOT NULL,   
    start_date      TIMESTAMP ,
    end_date        TIMESTAMP 
    
);

CREATE SEQUENCE sq_prices
    START 1
    MINVALUE 1
    CACHE 1;