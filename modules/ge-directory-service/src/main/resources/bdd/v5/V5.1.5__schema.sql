CREATE TABLE IF NOT EXISTS "ref_fragment" (
   id BIGINT NOT NULL,
   label text,
   detail JSON,
   created timestamp without time zone NOT NULL DEFAULT now(),
   PRIMARY KEY (id)
);
CREATE SEQUENCE sq_ref_fragment START 1 MINVALUE 1 CACHE 1;