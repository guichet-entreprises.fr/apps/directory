-- -------------------------------------------------------------------------------------
--
-- Remove payment attribute into authority details
--
--
-- --------------------------------------------------------------------------------------
UPDATE authority SET details = REPLACE(details::text, ',"payment":' || (details->'payment')::text, '')::json WHERE cast(details->'payment' as json) is not null;
