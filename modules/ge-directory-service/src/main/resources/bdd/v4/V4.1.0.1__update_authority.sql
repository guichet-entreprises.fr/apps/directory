SET client_encoding = 'UTF8';


UPDATE authority
   SET func_id = 'COMMUNE_' || func_id
 WHERE func_id NOT LIKE 'C%' AND func_id NOT LIKE 'G%' AND func_id NOT LIKE 'U%';