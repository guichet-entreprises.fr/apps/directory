CREATE TABLE authority_user (
    authority_id    BIGINT          NOT NULL,
    user_id         VARCHAR(100)    NOT NULL
);

ALTER TABLE authority_user
    ADD CONSTRAINT fk_authority_user_authority
    FOREIGN KEY (authority_id)
    REFERENCES authority(id);
