CREATE TABLE refcommune
(
  codecommune character varying(5) NOT NULL,
  codepostal character varying(9) NOT NULL,
  libelle character varying(200),
  codedepartement character varying(3),
  CONSTRAINT refcommune_pkey PRIMARY KEY (codecommune, codepostal)
)