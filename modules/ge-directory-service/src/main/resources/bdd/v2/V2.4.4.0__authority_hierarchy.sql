DROP TABLE IF EXISTS predicate;
DROP SEQUENCE IF EXISTS sq_predicate;

ALTER TABLE authority
ADD COLUMN path VARCHAR(300);

UPDATE authority
SET path = func_id;

ALTER TABLE authority 
ALTER COLUMN path SET NOT NULL;

ALTER TABLE authority
ADD CONSTRAINT path_unique UNIQUE(path);
