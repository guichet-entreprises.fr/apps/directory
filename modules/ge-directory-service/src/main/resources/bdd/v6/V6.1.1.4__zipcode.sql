ALTER TABLE ref_countries
	RENAME created TO updated ;
	
ALTER TABLE ref_departments 
	RENAME created TO updated ;
	
ALTER TABLE ref_communes 
	RENAME created TO updated ;


CREATE TABLE IF NOT EXISTS "ref_zipcode" (
   id BIGINT NOT NULL,
   entity_id text,
   label text,
   details JSON,
   updated timestamp without time zone NOT NULL DEFAULT now(),
   PRIMARY KEY (id)
);
CREATE SEQUENCE sq_ref_zipcode START 1 MINVALUE 1 CACHE 1;