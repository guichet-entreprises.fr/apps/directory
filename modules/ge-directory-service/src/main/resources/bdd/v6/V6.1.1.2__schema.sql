CREATE TABLE IF NOT EXISTS "ref_communes" (
   id BIGINT NOT NULL,
   entity_id text,
   label text,
   details JSON,
   created timestamp without time zone NOT NULL DEFAULT now(),
   PRIMARY KEY (id)
);
CREATE SEQUENCE sq_ref_communes START 1 MINVALUE 1 CACHE 1;