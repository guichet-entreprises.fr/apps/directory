/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.directory.service.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import fr.ge.directory.service.bean.AuthorityBean;

/**
 * {@link AuthorityBean} mapper.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public interface AuthorityMapper extends SearchMapper<AuthorityBean> {

    /**
     * Gets an authority.
     *
     * @param id
     *            the id
     * @return the authority
     */
    AuthorityBean get(@Param("id") Long id);

    /**
     * Gets an authority by its functional id.
     *
     * @param entityId
     *            the authority id
     * @return the authority
     */
    AuthorityBean getByEntityId(@Param("entityId") String entityId);

    /**
     * Gets an authority by its path.
     *
     * @param path
     *            the path
     * @return the authority
     */
    AuthorityBean getByPath(@Param("path") String path);

    /**
     * Creates an authority.
     *
     * @param authority
     *            the authority
     */
    void create(@Param("authority") AuthorityBean authority);

    /**
     * Updates an authority.
     *
     * @param authority
     *            the authority
     */
    void update(@Param("authority") AuthorityBean authority);

    /**
     * Checks an authority exists.
     *
     * @param entityId
     *            the autority id
     * @return a boolean
     */
    boolean checkExistsByEntityId(@Param("entityId") String entityId);

    /**
     * Checks an authority exists.
     *
     * @param path
     *            the path
     * @return a boolean
     */
    boolean checkExistsByPath(@Param("path") String path);

    /**
     * Remove an authority by its ID.
     *
     * @param authorityId
     *            the authority id
     * @return removed element count
     */
    int remove(@Param("authorityId") Long authorityId);

    /**
     * Creates a link between an authority and a user.
     *
     * @param authorityId
     *            the authority id
     * @param userId
     *            the user id
     */
    void createLink(@Param("authorityId") Long authorityId, @Param("userId") String userId);

    /**
     * Gets authorities linked to an user.
     *
     * @param userId
     *            the user id
     * @return the authorities
     */
    List<AuthorityBean> findAuthoritiesByUserId(@Param("userId") String userId);

    /**
     * Removes a link between an authority and a user.
     *
     * @param authorityId
     *            the authority id
     * @param userId
     *            the user id
     */
    int removeLink(@Param("authorityId") Long authorityId, @Param("userId") String userId);

    /**
     * Remove links between authorities and users.
     *
     * @param authorityId
     *            the authority id
     * @return removed element count
     */
    int removeLinks(@Param("authorityId") Long authorityId);

}
