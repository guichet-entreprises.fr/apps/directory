/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.service.request;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.StatusType;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.utils.CoreUtil;
import fr.ge.directory.core.exception.TechnicalException;

/**
 * The Class ServiceRequest.
 *
 * @author Christian Cougourdan
 */
public class ServiceRequest {

    /** HTTP bad request. */
    private static final int HTTP_BAD_REQUEST = 400;

    /** The Constant OBJECT_MAPPER. */
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    /** Data types. */
    private static final Map<String, MediaType> DATA_TYPES;

    static {
        final Map<String, MediaType> m = new HashMap<>();

        m.put("json", MediaType.APPLICATION_JSON_TYPE);
        m.put("text", MediaType.TEXT_PLAIN_TYPE);
        m.put("bytes", MediaType.APPLICATION_OCTET_STREAM_TYPE);

        DATA_TYPES = Collections.unmodifiableMap(m);
    }

    /** Attachment builder. */
    private static final Map<Predicate<Object>, BiFunction<String, Object, Attachment>> ATTACHMENT_BUILDER;

    static {
        final Map<Predicate<Object>, BiFunction<String, Object, Attachment>> m = new LinkedHashMap<>();

        m.put(value -> value instanceof byte[], (name, value) -> buildAttachment(name, MediaType.APPLICATION_OCTET_STREAM, value));
        m.put(value -> null == value, (name, value) -> buildAttachment(name, MediaType.TEXT_PLAIN, ""));
        m.put(value -> value instanceof String || value instanceof Number, (name, value) -> buildAttachment(name, MediaType.TEXT_PLAIN, value));
        m.put(value -> true, (name, value) -> buildAttachment(name, MediaType.APPLICATION_JSON, marshall(value)));

        ATTACHMENT_BUILDER = Collections.unmodifiableMap(m);
    }

    /**
     * Marshall.
     *
     * @param src
     *            the src
     * @return the object
     */
    private static Object marshall(final Object src) {
        String valueAsJson = null;
        final Object value = src;

        if (src instanceof String || src instanceof byte[]) {
            return src;
        }

        try {
            valueAsJson = OBJECT_MAPPER.writeValueAsString(value);
        } catch (final JsonProcessingException ex) {
            throw new TechnicalException(ex);
        }

        return valueAsJson;
    }

    /** The target. */
    private WebTarget target;

    /** The expected data type. */
    private String expectedDataType;

    /** Sender data type. */
    private String sendedDataType = null;

    /** The headers. */
    private final MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();

    /**
     * Instantiates a new service request.
     *
     * @param url
     *            the url
     */
    public ServiceRequest(final String url) {
        this.target = ClientBuilder.newClient().target(url);
    }

    /**
     * Post.
     *
     * @param content
     *            the content
     * @return the service response
     */
    public ServiceResponse post(final Object content) {
        final Entity<Object> entity;

        if ("form".equals(this.sendedDataType) && content instanceof Map) {
            final Map<String, Object> m = CoreUtil.cast(content);
            final List<Attachment> attachments = m.entrySet().stream() //
                    .map(entry -> this.buildAttachment(entry.getKey(), entry.getValue())) //
                    .filter(value -> null != value) //
                    .collect(Collectors.toList());
            final MultipartBody body = new MultipartBody(attachments);
            entity = Entity.entity(body, MediaType.MULTIPART_FORM_DATA_TYPE);
        } else {
            entity = null == content ? null : Entity.entity(marshall(content), Optional.ofNullable(this.sendedDataType).map(MediaType::valueOf).orElse(this.mediaType(content)));
        }

        final Response response = this.builder().post(entity);

        this.check(response);

        return new ServiceResponse(response);
    }

    /**
     * Put.
     *
     * @param content
     *            the content
     * @return the service response
     */
    public ServiceResponse put(final Object content) {
        final Entity<Object> entity;

        if ("form".equals(this.sendedDataType) && content instanceof Map) {
            final Map<String, Object> m = CoreUtil.cast(content);
            final List<Attachment> attachments = m.entrySet().stream() //
                    .map(entry -> this.buildAttachment(entry.getKey(), entry.getValue())) //
                    .filter(value -> null != value) //
                    .collect(Collectors.toList());
            entity = Entity.entity(new MultipartBody(attachments), MediaType.MULTIPART_FORM_DATA_TYPE);
        } else {
            entity = null == content ? null : Entity.entity(marshall(content), Optional.ofNullable(this.sendedDataType).map(MediaType::valueOf).orElse(this.mediaType(content)));
        }

        final Response response = this.builder().put(entity);

        this.check(response);

        return new ServiceResponse(response);
    }

    /**
     * Builds the attachment.
     *
     * @param name
     *            the name
     * @param value
     *            the value
     * @return the attachment
     */
    private Attachment buildAttachment(final String name, final Object value) {
        return ATTACHMENT_BUILDER.entrySet().stream().filter(entry -> entry.getKey().test(value)).findFirst().map(entry -> entry.getValue().apply(name, value)).orElse(null);
    }

    /**
     * Builds the attachment.
     *
     * @param name
     *            the name
     * @param mimeType
     *            the mime type
     * @param data
     *            the data
     * @return the attachment
     */
    private static Attachment buildAttachment(final String name, final String mimeType, final Object data) {
        final MultivaluedMap<String, String> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-ID", name);
        headers.putSingle("Content-Type", mimeType);
        headers.putSingle("Content-Disposition", String.format("form-data; name=\"%s\"", name));

        return new Attachment(headers, data);
    }

    /**
     * Gets the.
     *
     * @return the service response
     */
    public ServiceResponse get() {
        final Response response = this.builder().get();

        this.check(response);

        return new ServiceResponse(response);
    }

    /**
     * Builder.
     *
     * @return the builder
     */
    protected Builder builder() {
        Builder builder = this.target.request().headers(this.headers);

        if (null != this.expectedDataType && DATA_TYPES.containsKey(this.expectedDataType)) {
            builder = builder.accept(DATA_TYPES.get(this.expectedDataType));
        }

        return builder;
    }

    /**
     * Check.
     *
     * @param response
     *            the response
     */
    protected void check(final Response response) {
        final StatusType statusType = response.getStatusInfo();

        if (statusType.getStatusCode() >= HTTP_BAD_REQUEST) {
            throw new TechnicalException(String.format("HTTP error %d on '%s' : %s", statusType.getStatusCode(), this.target.getUri(), statusType.getReasonPhrase()));
        }
    }

    /**
     * Connection timeout.
     *
     * @param timeout
     *            the timeout
     * @return the service request
     */
    public ServiceRequest connectionTimeout(final long timeout) {
        this.target = this.target.property("http.connection.timeout", timeout);
        return this;
    }

    /**
     * Receive timeout.
     *
     * @param timeout
     *            the timeout
     * @return the service request
     */
    public ServiceRequest receiveTimeout(final long timeout) {
        this.target = this.target.property("http.receive.timeout", timeout);
        return this;
    }

    /**
     * Specify returned data type. Accepted values are 'json', 'text', 'binary'.
     *
     * @param dataType
     *            expected data type
     * @return the service request
     */
    public ServiceRequest accept(final String dataType) {
        this.expectedDataType = dataType;
        return this;
    }

    /**
     * Specify query parameter.
     *
     * @param key
     *            parameter name
     * @param values
     *            parameter value
     * @return the service request
     */
    public ServiceRequest param(final String key, final Object... values) {
        this.target = this.target.queryParam(key, values);
        return this;
    }

    /**
     * Data type.
     *
     * @param type
     *            the type
     * @return the service request
     */
    public ServiceRequest dataType(final String type) {
        this.sendedDataType = type;
        return this;
    }

    /**
     * Header.
     *
     * @param name
     *            the name
     * @param value
     *            the value
     * @return the service request
     */
    public ServiceRequest header(final String name, final String value) {
        this.headers.putSingle(name, value);
        return this;
    }

    /**
     * Media type.
     *
     * @param content
     *            the content
     * @return the media type
     */
    private MediaType mediaType(final Object content) {
        if (content instanceof byte[]) {
            return MediaType.APPLICATION_OCTET_STREAM_TYPE;
        } else if (null == content || content instanceof String || content instanceof Number) {
            return MediaType.TEXT_PLAIN_TYPE;
        } else {
            return MediaType.APPLICATION_JSON_TYPE;
        }
    }

}
