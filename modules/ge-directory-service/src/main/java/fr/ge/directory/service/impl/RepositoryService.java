package fr.ge.directory.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.query.sql.Operator;
import fr.ge.common.query.sql.QueryBuilder;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.mapper.RepositoryDaoMapper;
import fr.ge.directory.ws.v1.model.RepositoryElement;
import fr.ge.directory.ws.v1.model.RepositoryId;

/**
 * Service to access the generic repository.
 * 
 * @author $Author: LABEMONT $
 */
@Service
public class RepositoryService {

    @Autowired
    private RepositoryDaoMapper mapper;

    /**
     * List the currents repositories.
     * 
     * @return a not null list of ids.
     */
    public List<String> listRepositories() {
        List<String> repositories = this.getMapper().listRepositories();
        return repositories;
    }

    /**
     * Adding an element in the repository.
     * 
     * @param repositoryId
     *            the repository id, not null
     * @param element
     *            the element to add, not null
     * @return
     */
    public RepositoryId add(String repositoryId, RepositoryElement element) {
        this.getMapper().create(repositoryId, element);
        return element;
    }

    /**
     * Read an element from the repository.
     * 
     * @param repositoryId
     *            repository id
     * @param id
     *            element id
     * @return the element, null if not found
     */
    public RepositoryElement read(String repositoryId, long id) {
        RepositoryElement element = this.getMapper().read(repositoryId, id);
        return element;
    }

    /**
     * Multiple criteria search on generic repository.
     * 
     * @param repositoryId
     *            the repository id
     * @param searchQuery
     *            the search criteria
     * @return the result
     */
    public SearchResult<RepositoryElement> search(String repositoryId, SearchQuery searchQuery) {

        QueryBuilder queryBuilder = new QueryBuilder();
        if (null != searchQuery.getFilters()) {
            for (SearchQueryFilter filter : searchQuery.getFilters()) {
                if (StringUtils.isNoneBlank(filter.getPath())) {
                    queryBuilder.addFilter(String.format("%s.%s", filter.getColumn(), filter.getPath()), filter.getOperator(), filter.getValue());
                } else {
                    queryBuilder.addFilter(filter.getColumn(), filter.getOperator(), filter.getValue());
                }
            }
        }

        if (StringUtils.isNotBlank(searchQuery.getSearchTerms())) {
            queryBuilder.addFilter("label", Operator.LIKE_ALL, searchQuery.getSearchTerms());
        }

        final RowBounds rowBounds = new RowBounds((int) searchQuery.getStartIndex(), (int) searchQuery.getMaxResults());
        final List<RepositoryElement> entities = this.getMapper().search(repositoryId, queryBuilder, rowBounds);

        final SearchResult<RepositoryElement> searchResult = new SearchResult<>(searchQuery.getStartIndex(), searchQuery.getMaxResults());
        searchResult.setContent(entities);
        searchResult.setTotalResults(this.getMapper().count(repositoryId, queryBuilder, rowBounds));

        return searchResult;
    }

    /**
     * Adding a list of elements to a repository with possibility to delete the
     * existing content.
     * 
     * @param repositoryId
     *            the Id of the repository to upload
     * @param repositoryElements
     *            the elements to insert in the repository
     * @param truncate
     *            if true delete existing content before inserting new elements
     */
    @Transactional(value = "ge_directory")
    public void injectRepository(String repositoryId, List<RepositoryElement> repositoryElements, Boolean truncate) {
        if (truncate) {
            this.getMapper().delete(repositoryId);
        }
        for (RepositoryElement repositoryElement : repositoryElements) {
            this.getMapper().create(repositoryId, repositoryElement);
        }
    };

    /**
     * Gets the repository mapper.
     * 
     * @return the mapper
     */
    protected RepositoryDaoMapper getMapper() {
        return this.mapper;
    }

    /**
     * 
     * Update a repository element
     * 
     * @param repositoryId
     *            the repository Id
     * 
     * @param repositoryElement
     *            The repository element to update
     */
    public void updateElement(String repositoryId, RepositoryElement repositoryElement) {
        this.getMapper().update(repositoryId, repositoryElement);
    }
}
