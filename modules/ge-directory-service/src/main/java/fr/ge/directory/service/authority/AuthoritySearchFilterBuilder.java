/**
 * 
 */
package fr.ge.directory.service.authority;

import java.util.ArrayList;
import java.util.List;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.directory.service.bean.AuthorityBean;

/**
 * Filter for authorities.
 * 
 * @author $Author: sarahman $
 */
public class AuthoritySearchFilterBuilder {

    /** The inner search query. */
    protected List<SearchQueryFilter> filters;

    /** The main object used as search criteria. */
    protected AuthorityBean authority;

    /**
     * 
     * Constructeur de la classe.
     *
     * @param authority
     *            the authority used as search criteria
     */
    public AuthoritySearchFilterBuilder(AuthorityBean authority) {
        this.authority = authority;
        this.filters = new ArrayList<>();
    }

    /**
     * Filter by edi code.
     * 
     * @param ediCode
     *            the edi code
     * @return the builder
     */
    public AuthoritySearchFilterBuilder filterByFunctionalCode() {
        SearchQueryFilter filter = new SearchQueryFilter();
        filter.setColumn("details").setPath("ediCode").setOperator(":").setValue(authority.getDetails().get("ediCode").asText());
        filters.add(filter);

        return this;
    }

    /**
     * Generate the search query.
     * 
     * @return the search query, not null
     */
    public SearchQuery build() {
        return new SearchQuery(0, 1).setFilters(filters).setOrders(new ArrayList<SearchQueryOrder>()).setSearchTerms("");
    }
}
