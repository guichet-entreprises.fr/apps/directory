/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.service.bean;

/**
 * Enum matching functional and technical tables from database.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public enum ReferentialTableEnum {

    AUTHORITY("authority", "authority"), ///
    COUNTRIES("countries", "ref_countries"), ////
    COMMUNES("communes", "ref_communes"), ////
    ZIPCODE("zipcode", "ref_zipcode"), DEPARTEMENT("departments", "ref_departments");

    /** The functional table name. */
    private String functionalTable;

    /** The technical table name. */
    private String technicalTable;

    /**
     * Constructor.
     *
     * @param code
     *            the code
     * @param codeRegex
     *            the code regex
     * @param message
     *            the message
     * @param display
     *            display ?
     */
    ReferentialTableEnum(final String functionalTable, final String technicalTable) {
        this.functionalTable = functionalTable;
        this.technicalTable = technicalTable;
    }

    /**
     * Accesseur sur l'attribut {@link #functionalTable}.
     *
     * @return String functionalTable
     */
    public String getFunctionalTable() {
        return functionalTable;
    }

    /**
     * Mutateur sur l'attribut {@link #functionalTable}.
     *
     * @param functionalTable
     *            la nouvelle valeur de l'attribut functionalTable
     */
    public void setFunctionalTable(String functionalTable) {
        this.functionalTable = functionalTable;
    }

    /**
     * Accesseur sur l'attribut {@link #technicalTable}.
     *
     * @return String technicalTable
     */
    public String getTechnicalTable() {
        return technicalTable;
    }

    /**
     * Mutateur sur l'attribut {@link #technicalTable}.
     *
     * @param technicalTable
     *            la nouvelle valeur de l'attribut technicalTable
     */
    public void setTechnicalTable(String technicalTable) {
        this.technicalTable = technicalTable;
    }

    /**
     * Find a referential enum from a function table.
     * 
     * @param functionalTable
     *            the functional table name
     * @return a referential enum
     */
    public static String findTechnicalTable(final String functionalTable) {
        for (final ReferentialTableEnum referentialTable : ReferentialTableEnum.values()) {
            if (functionalTable.equals(referentialTable.getFunctionalTable()))
                return referentialTable.getTechnicalTable();
        }
        return null;
    }
}
