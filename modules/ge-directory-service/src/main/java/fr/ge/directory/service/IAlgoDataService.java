/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.service;

import java.util.Map;

import fr.ge.directory.service.bean.AlgoBean;

/**
 * The Interface IAlgoDataService.
 *
 * @author Christian Cougourdan
 */
public interface IAlgoDataService extends ISearchDataService {

    /**
     * Retrieves an algo from its `id`.
     *
     * @param id
     *            the id
     * @return found issue
     */
    AlgoBean get(long id);

    /**
     * Retrieves an algo from its authority type.
     *
     * @param authorityType
     *            the authority type
     * @return found algo
     */
    AlgoBean getByCode(String authorityType);

    /**
     * Saves an algo.
     *
     * @param algo
     *            the algo
     */
    void save(final AlgoBean algo);

    /**
     * Remove algorithm by its ID.
     *
     * @param id
     *            algorithm ID
     * @return true, if successful
     */
    boolean remove(long id);

    /**
     * Retrieves an authority functional id from an algo.
     *
     * @param code
     *            the algo code, which is the authority type
     * @param metas
     *            the metas
     * @return found authority functional id
     */
    String execute(String code, Map<String, String> metas);

    /**
     * Remove all algorithms.
     */
    void removeAll();

}
