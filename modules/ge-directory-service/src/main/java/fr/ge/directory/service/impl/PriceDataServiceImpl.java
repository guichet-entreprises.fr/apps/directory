
package fr.ge.directory.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.ibatis.session.RowBounds;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.IPriceDataService;
import fr.ge.directory.service.bean.RefPriceBean;
import fr.ge.directory.service.mapper.PriceMapper;

/**
 * Tarif Data Service.
 * 
 * @author $Author: sarahman $
 * @version $Revision: 0 $
 */
@Service("priceDataService")
public class PriceDataServiceImpl implements IPriceDataService {

    /**
     * Mapper Of Prices.
     */
    @Autowired
    private PriceMapper priceMapper;

    /**
     * Dozer Maper.
     */
    @Autowired
    private DozerBeanMapper dozer;

    /**
     * {@inheritDoc}
     */
    @Override
    public <R> SearchResult<R> search(SearchQuery searchQuery, Class<R> expectedClass) {
        final Map<String, Object> filters = new HashMap<>();
        if (null != searchQuery.getFilters()) {
            searchQuery.getFilters().forEach(filter -> filters.put(filter.getColumn(), filter));
        }

        final RowBounds rowBounds = new RowBounds((int) searchQuery.getStartIndex(), (int) searchQuery.getMaxResults());
        final List<RefPriceBean> entities = this.priceMapper.findAll(filters, searchQuery.getOrders(), rowBounds, searchQuery.getSearchTerms());

        final SearchResult<R> searchResult = new SearchResult<>(searchQuery.getStartIndex(), searchQuery.getMaxResults());
        if (null != entities) {
            searchResult.setContent(entities.stream().map(elm -> this.dozer.map(elm, expectedClass)).collect(Collectors.toList()));
        }

        searchResult.setTotalResults(this.priceMapper.count(filters, searchQuery.getOrders(), searchQuery.getSearchTerms()));

        return searchResult;
    }

}
