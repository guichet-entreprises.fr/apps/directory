/**
 * 
 */
package fr.ge.directory.dozer;

import org.dozer.BeanFactory;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * TODO
 * 
 * @author $Author: sarahman $
 */
public class JsonNodeDozerFactory implements BeanFactory {

    /**
     * {@inheritDoc} <br/>
     * Création d'un JsonNode avec un object dans la hiérarchie d'un JsonNode
     */
    @Override
    public Object createBean(Object source, Class<?> sourceClass, String targetBeanId) {
        if (source != null && source instanceof JsonNode) {
            return ((JsonNode) source).deepCopy();
        }
        return null;
    }

}
