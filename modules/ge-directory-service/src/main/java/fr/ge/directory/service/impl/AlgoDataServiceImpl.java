/**
 *
 */
package fr.ge.directory.service.impl;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.directory.bean.Configuration;
import fr.ge.directory.service.IAlgoDataService;
import fr.ge.directory.service.bean.AlgoBean;
import fr.ge.directory.service.mapper.AlgoMapper;
import fr.ge.directory.service.mapper.SearchMapper;
import fr.ge.directory.service.request.ServiceProcessorBridge;

/**
 * Algo service.
 *
 * @author jpauchet
 */
@Service
public class AlgoDataServiceImpl extends AbstractSearchDataServiceImpl<AlgoBean> implements IAlgoDataService {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AlgoDataServiceImpl.class);

    @Autowired
    private AlgoMapper mapper;

    @Resource(name = "engineProperties")
    private Properties engineProperties;

    /**
     * {@inheritDoc}
     */
    @Override
    protected SearchMapper<AlgoBean> getMapper() {
        return this.mapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AlgoBean get(final long id) {
        return this.mapper.get(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AlgoBean getByCode(final String authorityType) {
        return this.mapper.getByCode(authorityType);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_directory")
    public void save(final AlgoBean algo) {
        final boolean exists = this.mapper.checkExistsByCode(algo.getCode());
        if (exists) {
            this.mapper.update(algo);
        } else {
            this.mapper.create(algo);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_directory")
    public boolean remove(final long id) {
        return this.mapper.remove(id) > 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String execute(final String code, final Map<String, String> metas) {
        final AlgoBean algo = this.mapper.getByCode(code);
        String entityId = StringUtils.EMPTY;
        if (null == algo) {
            LOGGER.info("Authority type '{}' not found", code);
        } else {
            final ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");
            try {
                engine.put("logger", LOGGER);
                engine.put("service", new ServiceProcessorBridge(new Configuration(engineProperties)));
                engine.eval("function execute(args) {" + new String(algo.getContent(), StandardCharsets.UTF_8) + "}");
                entityId = (String) ((Invocable) engine).invokeFunction("execute", metas);
            } catch (final ScriptException | NoSuchMethodException e) {
                LOGGER.warn("Error on authority algorithm '{}' execution", code, e);
            }
        }
        return entityId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_directory")
    public void removeAll() {
        this.mapper.removeAll();
    }

}
