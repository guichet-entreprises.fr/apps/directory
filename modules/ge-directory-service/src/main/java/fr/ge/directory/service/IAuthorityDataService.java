/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.directory.service;

import java.util.List;
import java.util.Map;

import fr.ge.directory.service.bean.AuthorityBean;
import fr.ge.directory.service.bean.AuthorityRoleBean;

/**
 * Authority services.
 *
 * @author Christian Cougourdan
 */
public interface IAuthorityDataService extends ISearchDataService {

    /**
     * Retrieves an authority from its `id`.
     *
     * @param id
     *            the id
     * @return found authority
     */
    AuthorityBean get(long id);

    /**
     * Retrieves an authority from its functional id.
     *
     * @param entityId
     *            the entityId
     * @return found authority
     */
    AuthorityBean getByEntityId(String entityId);

    /**
     * Saves an authority.
     *
     * @param authority
     *            the authority
     * @return authority technical ID
     */
    Long save(AuthorityBean authority);

    /**
     * Merge an authority.
     *
     * @param authority
     *            the authority
     * @return authority technical ID
     */
    Long merge(AuthorityBean authority);

    /**
     * Remove an authority by its ID.
     *
     * @param id
     *            authority ID
     * @return true, if successful
     */
    boolean remove(long id);

    /**
     * Creates a link between an authority and a user.
     *
     * @param authorityFuncId
     *            the authority functional id
     * @param userId
     *            the user id
     * @param role
     *            the role within the authority
     * @throws Exception
     */
    void createLink(String authorityFuncId, final String userId, final String role);

    /**
     * Checks a user has a role for an authority.
     *
     * @param userId
     *            the user id
     * @param role
     *            the role to check
     * @param authorityId
     *            the authority functional id
     * @return a boolean
     */
    boolean hasRoleForAuthority(final String userId, final String role, final String authorityId);

    /**
     * Find authorities for a user.
     *
     * @param userId
     *            the user id
     * @return authorities role list
     */
    List<AuthorityRoleBean> findAuthoritiesByUserId(final String userId);

    /**
     * Find rights from an user identifier.
     * 
     * @param userId
     *            The user identifier
     * @return roles as map
     */
    Map<String, List<String>> findUserRights(final String userId);

}
