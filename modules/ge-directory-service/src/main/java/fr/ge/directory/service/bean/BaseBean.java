/**
 *
 */
package fr.ge.directory.service.bean;

/**
 * The Class AbstractDatedBean.
 *
 * @author Christian Cougourdan
 * @param <T>
 *            the generic type
 */
public class BaseBean<T extends BaseBean<T>> {

    /** The id. */
    private Long id;

    /**
     * Constructor
     */
    protected BaseBean() {
        // Nothing to do.
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     * @return the t
     */
    @SuppressWarnings("unchecked")
    public T setId(final Long id) {
        this.id = id;
        return (T) this;
    }

}
