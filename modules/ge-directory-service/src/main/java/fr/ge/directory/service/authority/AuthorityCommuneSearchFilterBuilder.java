/**
 * 
 */
package fr.ge.directory.service.authority;

import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.directory.service.bean.AuthorityBean;

/**
 * Filter for communes handled like authorities.
 */
public class AuthorityCommuneSearchFilterBuilder extends AuthoritySearchFilterBuilder {
    /** {@inheritDoc} */
    public AuthorityCommuneSearchFilterBuilder(AuthorityBean authority) {
        super(authority);
    }

    /** {@inheritDoc} */
    @Override
    public AuthorityCommuneSearchFilterBuilder filterByFunctionalCode() {
        SearchQueryFilter filter = new SearchQueryFilter();
        filter.setColumn("details").setPath("codeCommune").setOperator(":").setValue(authority.getDetails().get("codeCommune").asText());
        filters.add(filter);

        return this;
    }
}
