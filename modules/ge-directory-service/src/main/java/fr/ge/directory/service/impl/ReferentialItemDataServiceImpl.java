/**
 *
 */
package fr.ge.directory.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;

import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.directory.service.IReferentialItemDataService;
import fr.ge.directory.service.bean.ReferentialItemBean;
import fr.ge.directory.service.bean.ReferentialTableEnum;
import fr.ge.directory.service.mapper.ReferentialItemMapper;
import fr.ge.directory.service.mapper.ReferentialSearchMapper;

/**
 * Authority service.
 *
 * @author aakkou
 */
@Service
public class ReferentialItemDataServiceImpl extends AbstractRefSearchDataServiceImpl<ReferentialItemBean> implements IReferentialItemDataService {

    @Autowired
    private ReferentialItemMapper mapper;

    /** The repository service. */
    @Autowired
    private RepositoryService repositoryService;

    /**
     * {@inheritDoc}
     */
    @Override
    protected ReferentialSearchMapper<ReferentialItemBean> getMapper() {
        return this.mapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReferentialItemBean get(final String tableName, final long id) {
        return this.mapper.get(tableName, id);
    }

    @Override
    @Transactional(value = "ge_directory")
    public void save(final String tableName, final List<JsonNode> jsonList) {
        final String techninalTable = ReferentialTableEnum.findTechnicalTable(tableName);
        if (StringUtils.isBlank(techninalTable)) {
            throw new TechnicalException(String.format("Cannot get a referential table from table name input", tableName));
        }
        for (final JsonNode jsonDetail : jsonList) {
            final ReferentialItemBean item = new ReferentialItemBean();
            final String label = Optional.ofNullable(jsonDetail) //
                    .filter(json -> json.has("label")) //
                    .map(json -> json.get("label").asText()) //
                    .orElseGet(() -> StringUtils.join(this.jsonValues(jsonDetail), StringUtils.SPACE));
            final String entityId = Optional.ofNullable(jsonDetail) //
                    .filter(json -> json.has("entityId")) //
                    .map(json -> json.get("entityId").asText()) //
                    .orElseGet(() -> StringUtils.join(this.jsonValues(jsonDetail), StringUtils.SPACE));
            item.setLabel(label);
            item.setEntityId(entityId);
            item.setDetails(jsonDetail);
            this.mapper.create(techninalTable, item);
        }
    }

    private List<String> jsonValues(final JsonNode root) {
        final List<String> lst = new ArrayList<>();

        if (root.isValueNode()) {
            lst.add(root.asText());
        } else if (root.isContainerNode()) {
            root.forEach(node -> lst.addAll(this.jsonValues(node)));
        }

        return lst;
    }

    @Override
    public List<ReferentialItemBean> find(final String tableName, final String searchText) {

        // Split the searchText
        String[] searchTermsList = StringUtils.upperCase(searchText).split(" ");

        if (searchTermsList.length == 0) {
            searchTermsList = new String[] { searchText };
        }

        return this.mapper.find(tableName, Arrays.asList(searchTermsList));
    }

    @Override
    public void truncate(final String tableName) {
        this.mapper.truncate(tableName);
    }

}
