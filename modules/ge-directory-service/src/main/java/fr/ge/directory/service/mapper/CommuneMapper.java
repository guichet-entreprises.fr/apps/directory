/**
 * 
 */
package fr.ge.directory.service.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.directory.service.bean.RefCommuneBean;

/**
 * @author bsadil
 *
 */
public interface CommuneMapper {
    /**
     * Search for {code}T{code} entities.
     *
     * @param filters
     *            the filters
     * @param orders
     *            the orders
     * @param rowBounds
     *            pagination
     * @return entity list
     */
    List<RefCommuneBean> findAll(@Param("filters") Map<String, Object> filters, @Param("orders") List<SearchQueryOrder> orders, RowBounds rowBounds, @Param("searchedValue") String searchedValue);

    /**
     * Count entities.
     *
     * @param filters
     *            filters
     * @return total elements corresponding
     */
    long count(@Param("filters") Map<String, Object> filters, @Param("orders") List<SearchQueryOrder> orders, @Param("searchedValue") String searchedValue);

}
