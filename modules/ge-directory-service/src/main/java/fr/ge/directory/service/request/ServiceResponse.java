/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.service.request;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The Class ServiceResponse.
 *
 * @author Christian Cougourdan
 */
public class ServiceResponse {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceResponse.class);

    /** The Constant OBJECT_MAPPER. */
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    /** The response. */
    private final Response response;

    /**
     * Instantiates a new service response.
     *
     * @param response
     *            the response
     */
    public ServiceResponse(final Response response) {
        this.response = response;
        this.response.bufferEntity();
    }

    /**
     * Status code.
     *
     * @return the int
     */
    public int getStatus() {
        return this.response.getStatus();
    }

    /**
     * As bytes.
     *
     * @return the byte[]
     */
    public byte[] asBytes() {
        return this.response.readEntity(byte[].class);
    }

    /**
     * As object.
     *
     * @return the object
     */
    public Object asObject() {
        return this.asObject(Map.class);
    }

    /**
     * As object.
     *
     * @param <R>
     *            the generic type
     * @param expected
     *            the expected
     * @return the r
     */
    public <R> R asObject(final Class<R> expected) {
        if (Optional.ofNullable(this.response).map(Response::getMediaType).map(MediaType.APPLICATION_JSON_TYPE::isCompatible).orElse(false)) {
            final String str = this.response.readEntity(String.class);
            try {
                return OBJECT_MAPPER.readValue(str, expected);
            } catch (final IOException ex) {
                throw new fr.ge.directory.core.exception.TechnicalException("Unable to parse value [" + str + "]", ex);
            }
        } else {
            LOGGER.info("Invalide response type ('{}'), expected '{}'", this.response.getMediaType(), MediaType.APPLICATION_JSON_TYPE);
        }

        return null;
    }

    /**
     * As string.
     *
     * @return the string
     */
    public String asString() {
        return new String(this.asBytes(), StandardCharsets.UTF_8);
    }

    /**
     * As list.
     *
     * @return the list of values
     */
    public List<?> asList() {
        return this.response.readEntity(List.class);
    }

}
