
package fr.ge.directory.service.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.session.RowBounds;

import fr.ge.common.query.sql.QueryBuilder;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.directory.ws.v1.model.RepositoryElement;

/**
 * Repository Dao.
 */
public interface RepositoryDaoMapper {

    /**
     * Search for the repositories present.
     * 
     * @return a not null list of repositories ids.
     */
    @ResultType(java.lang.String.class)
    List<String> listRepositories();

    /**
     * Create an element in the repository.
     * 
     * @param repositoryId
     *            the repository id, not null
     * @param element
     *            the element to add, not null
     */
    void create(@Param("repositoryId") String repositoryId, @Param("item") RepositoryElement element);

    /**
     * Read an element in the repository.
     * 
     * @param repositoryId
     *            the repository id, not null
     * @param id
     *            the element id to read, not null
     */
    RepositoryElement read(@Param("repositoryId") String repositoryId, @Param("id") long id);

    /**
     * Search for XXX écrire.
     *
     * @param filters
     *            the filters
     * @param orders
     *            the orders
     * @param rowBounds
     *            pagination
     * @return entity list
     */
    @Deprecated
    List<RepositoryElement> search(@Param("repositoryId") String repositoryId, @Param("filters") Map<String, Object> filters, @Param("orders") List<SearchQueryOrder> orders, RowBounds rowBounds,
            @Param("searchedValue") String searchedValue);

    /**
     * Search for XXX écrire.
     *
     * @param filters
     *            the filters
     * @param orders
     *            the orders
     * @param rowBounds
     *            pagination
     * @return entity list
     */
    List<RepositoryElement> search(@Param("repositoryId") String repositoryId, @Param("query") QueryBuilder query, RowBounds rowBounds);

    /**
     * Count entities.
     *
     * @param filters
     *            filters
     * @return total elements corresponding
     */
    @Deprecated
    int count(@Param("repositoryId") String repositoryId, @Param("filters") Map<String, Object> filters, @Param("orders") List<SearchQueryOrder> orders, @Param("searchedValue") String searchedValue);

    /**
     * Count entities.
     *
     * @param filters
     *            filters
     * @return total elements corresponding
     */
    int count(@Param("repositoryId") String repositoryId, @Param("query") QueryBuilder query, RowBounds rowBounds);

    /**
     * Truncate repository.
     * 
     * @param repositoryId
     *            the Id of the repository
     */
    int delete(@Param("repositoryId") String repositoryId);

    /**
     * Update a repositoryElement
     * 
     * @param repositoryId
     *            The repository Id
     * @param element
     *            The element to update
     */
    void update(@Param("repositoryId") String repositoryId, @Param("element") RepositoryElement element);
}
