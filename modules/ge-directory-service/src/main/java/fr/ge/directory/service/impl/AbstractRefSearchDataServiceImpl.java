/**
 *
 */
package fr.ge.directory.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.ISearchV2DataService;
import fr.ge.directory.service.bean.ReferentialTableEnum;
import fr.ge.directory.service.mapper.ReferentialSearchMapper;

/**
 * @author bsadil
 *
 */
public abstract class AbstractRefSearchDataServiceImpl<T> implements ISearchV2DataService {

    /** Dozer. */
    /**
     * {@inheritDoc}
     */
    @Override
    public <R> SearchResult<R> search(final SearchQuery searchQuery, final Class<R> expectedClass, final String refName) {
        final Map<String, Object> filters = new HashMap<>();
        if (null != searchQuery.getFilters()) {
            searchQuery.getFilters().forEach(filter -> {
                filter.setPath(null == filter.getPath() ? null : this.convertPath(filter.getPath()));
                cleanColumnName(filter);
                filters.put(filter.getColumn(), filter);
            });
        }

        final RowBounds rowBounds = new RowBounds((int) searchQuery.getStartIndex(), (int) searchQuery.getMaxResults());
        final String table = ReferentialTableEnum.findTechnicalTable(refName);
        final List<R> entities = CoreUtil.cast(this.getMapper().findAll(table, filters, searchQuery.getOrders(), rowBounds, StringUtils.upperCase(searchQuery.getSearchTerms())));

        final SearchResult<R> searchResult = new SearchResult<>(searchQuery.getStartIndex(), searchQuery.getMaxResults());
        if (null != entities) {
            // searchResult.setContent(entities.stream().map(elm ->
            // this.dozer.map(elm,
            // expectedClass)).collect(Collectors.toList()));
            searchResult.setContent(entities);
        }

        searchResult.setTotalResults(this.getMapper().count(table, filters, searchQuery.getOrders(), StringUtils.upperCase(searchQuery.getSearchTerms())));

        return searchResult;
    }

    private static SearchQueryFilter cleanColumnName(final SearchQueryFilter filter) {
        final String columnName = filter.getColumn();
        if (null != columnName) {
            filter.setColumn(columnName.replaceAll("([A-Z])", "_$1").toLowerCase());
        }
        return filter;
    }

    /**
     * this methos converts a string like test.test.test to >test>test>>test and if
     * the String doesn't . the returned value is >>path
     *
     * @param path
     * @returnand
     */
    private String convertPath(final String path) {

        String transformedPath = null;
        final String[] pathWithoutPoint = StringUtils.split(path, ".");

        if (pathWithoutPoint.length == 1) {
            transformedPath = path;
        } else {
            final String replacedString = StringUtils.replace(path, ".", ">");
            transformedPath = StringUtils.substringBeforeLast(replacedString, ">").concat(">>") + StringUtils.substringAfterLast(replacedString, ">");
            transformedPath = "->".concat(transformedPath);
        }

        return transformedPath;
    }

    /**
     * Gets the mapper.
     *
     * @return the mapper
     */
    protected abstract ReferentialSearchMapper<T> getMapper();

}
