/**
 * 
 */
package fr.ge.directory.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.ICommuneDataService;
import fr.ge.directory.service.bean.RefCommuneBean;
import fr.ge.directory.service.mapper.CommuneMapper;

/**
 * @author bsadil
 *
 */
@Service("communeDataService")
public class CommuneDataServiceImpl implements ICommuneDataService {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(CommuneDataServiceImpl.class);

    /** Dozer. */
    @Autowired
    private DozerBeanMapper dozer;

    @Autowired
    private CommuneMapper mapper;

    /**
     * {@inheritDoc}
     */
    @Override
    public <R> SearchResult<R> search(SearchQuery searchQuery, Class<R> expectedClass) {
        final Map<String, Object> filters = new HashMap<>();
        if (null != searchQuery.getFilters()) {
            searchQuery.getFilters().forEach(filter -> filters.put(filter.getColumn(), filter));
        }

        final RowBounds rowBounds = new RowBounds((int) searchQuery.getStartIndex(), (int) searchQuery.getMaxResults());
        final List<RefCommuneBean> entities = this.mapper.findAll(filters, searchQuery.getOrders(), rowBounds, StringUtils.upperCase(searchQuery.getSearchTerms()));

        final SearchResult<R> searchResult = new SearchResult<>(searchQuery.getStartIndex(), searchQuery.getMaxResults());
        if (null != entities) {
            searchResult.setContent(entities.stream().map(elm -> this.dozer.map(elm, expectedClass)).collect(Collectors.toList()));
        }

        searchResult.setTotalResults(this.mapper.count(filters, searchQuery.getOrders(), StringUtils.upperCase(searchQuery.getSearchTerms())));

        return searchResult;
    }

}
