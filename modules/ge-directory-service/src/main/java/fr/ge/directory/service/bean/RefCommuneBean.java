/**
 * 
 */
package fr.ge.directory.service.bean;

/**
 * @author bsadil
 *
 */
public class RefCommuneBean {

    /**
     * the code of town
     */
    private String codeCommune;

    /**
     * the code of the city
     */
    private String codePostal;

    /**
     * the label of town
     */
    private String label;

    /**
     * 
     */
    public RefCommuneBean() {
        super();
        // Nothing to do
    }

    /**
     * @param codeCommune
     * @param codePostal
     * @param label
     */
    public RefCommuneBean(String codeCommune, String codePostal, String label) {
        super();
        this.codeCommune = codeCommune;
        this.codePostal = codePostal;
        this.label = label;
    }

    /**
     * @return the codeCommune
     */
    public String getCodeCommune() {
        return codeCommune;
    }

    /**
     * @param codeCommune
     *            the codeCommune to set
     */
    public void setCodeCommune(String codeCommune) {
        this.codeCommune = codeCommune;
    }

    /**
     * @return the codePostal
     */
    public String getCodePostal() {
        return codePostal;
    }

    /**
     * @param codePostal
     *            the codePostal to set
     */
    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label
     *            the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

}
