package fr.ge.directory.support.mybatis;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonNodeTypeHandler extends BaseTypeHandler<JsonNode> {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonNodeTypeHandler.class);

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Override
    public void setNonNullParameter(final PreparedStatement ps, final int i, final JsonNode parameter, final JdbcType jdbcType) throws SQLException {
        try {
            ps.setString(i, MAPPER.writeValueAsString(parameter));
        } catch (final JsonProcessingException e) {
            LOGGER.info("Unable to serialize JSON value {} : {}", parameter, e.getMessage());
        }
    }

    @Override
    public JsonNode getNullableResult(final ResultSet rs, final String columnName) throws SQLException {
        final String value = rs.getString(columnName);
        try {
            return null == value ? null : MAPPER.readTree(value);
        } catch (final IOException e) {
            LOGGER.info("Unable to parse JSON value {} : {}", value, e.getMessage());
            return null;
        }
    }

    @Override
    public JsonNode getNullableResult(final ResultSet rs, final int columnIndex) throws SQLException {
        final String value = rs.getString(columnIndex);
        try {
            return null == value ? null : MAPPER.readTree(value);
        } catch (final IOException e) {
            LOGGER.info("Unable to parse JSON value {} : {}", value, e.getMessage());
            return null;
        }
    }

    @Override
    public JsonNode getNullableResult(final CallableStatement cs, final int columnIndex) throws SQLException {
        final String value = cs.getString(columnIndex);
        try {
            return null == value ? null : MAPPER.readTree(value);
        } catch (final IOException e) {
            LOGGER.info("Unable to parse JSON value {} : {}", value, e.getMessage());
            return null;
        }
    }

}
