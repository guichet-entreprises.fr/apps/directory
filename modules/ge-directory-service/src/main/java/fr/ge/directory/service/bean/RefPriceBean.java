/**
 * 
 */
package fr.ge.directory.service.bean;

import java.util.Date;

/**
 * @author $Author: sarahman $
 * @version $Revision: 0 $
 */
public class RefPriceBean {
    /**
     * product code
     */
    private String productCode;
    /**
     * the product
     * 
     */
    private String product;
    /**
     * the price
     */
    private String price;
    /**
     * start date
     */
    private Date startDate;

    /**
     * end date
     * 
     */
    private Date endDate;

    /**
     * Constructor
     *
     */
    public RefPriceBean() {
        super();
    }

    /**
     * Constructor .
     *
     * @param codeProduct
     * @param product
     * @param price
     * @param startDate
     * @param startEnd
     */
    public RefPriceBean(String productCode, String product, String price, Date startDate, Date endDate) {
        super();
        this.productCode = productCode;
        this.product = product;
        this.price = price;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /**
     * Getter {@link #productCode}.
     *
     * @return String productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Setter {@link #productCode}.
     *
     * @param productCode
     *            productCode
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * Getter {@link #product}.
     *
     * @return String product
     */
    public String getProduct() {
        return product;
    }

    /**
     * Setter {@link #product}.
     *
     * @param product
     * 
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * Getter {@link #price}.
     *
     * @return String price
     */
    public String getPrice() {
        return price;
    }

    /**
     * Setter {@link #price}.
     *
     * @param price
     *            the price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * Getter {@link #startDate}.
     *
     * @return Date startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Setter {@link #startDate}.
     *
     * @param startDate
     *            the startDate
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Getter {@link #endDate}.
     *
     * @return Date endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Mutateur sur l'attribut {@link #endDate}.
     *
     * @param endDate
     *            the endDate
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

}
