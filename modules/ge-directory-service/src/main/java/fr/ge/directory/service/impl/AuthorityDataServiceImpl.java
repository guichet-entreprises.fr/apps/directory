/**
 *
 */
package fr.ge.directory.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.directory.service.IAuthorityDataService;
import fr.ge.directory.service.bean.AuthorityBean;
import fr.ge.directory.service.bean.AuthorityContactInfoUserBean;
import fr.ge.directory.service.bean.AuthorityDetailsBean;
import fr.ge.directory.service.bean.AuthorityRoleBean;
import fr.ge.directory.service.mapper.AuthorityMapper;
import fr.ge.directory.service.mapper.SearchMapper;

/**
 * Authority service.
 *
 * @author jpauchet
 */
@Service("authorityDataService")
public class AuthorityDataServiceImpl extends AbstractSearchDataServiceImpl<AuthorityBean> implements IAuthorityDataService {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorityDataServiceImpl.class);

    /** User id attribute */
    private static final String USER_ID_ATTRIBUTE = "id";

    /** User roles attribute. */
    private static final String USER_ROLES_ATTRIBUTE = "roles";

    /** Contact info users. */
    private static final String CONTACT_INFO_USERS = "users";

    @Autowired
    private AuthorityMapper mapper;

    /** Dozer. */
    @Autowired
    private DozerBeanMapper dozer;

    /**
     * {@inheritDoc}
     */
    @Override
    protected SearchMapper<AuthorityBean> getMapper() {
        return this.mapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AuthorityBean get(final long id) {
        return this.mapper.get(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AuthorityBean getByEntityId(final String entityId) {
        return this.mapper.getByEntityId(entityId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_directory")
    public Long save(final AuthorityBean authority) {
        // generate the functional identifier from the path
        // final String[] pathSplit = authority.getPath().split("/");
        // authority.setEntityId(pathSplit[pathSplit.length - 1]);

        // add the parent path to the JSON
        try {
            final ObjectMapper jsonMapper = new ObjectMapper();
            final Map<String, Object> details = this.mapDetails(jsonMapper, authority);
            // details.put("parent", StringUtils.join(pathSplit, "/", 0,
            // pathSplit.length - 1));
            authority.setDetails(jsonMapper.readTree(jsonMapper.writeValueAsString(details)));
        } catch (final IOException e) {
            LOGGER.warn("Could not map contact info", e);
        }

        return this.saveDb(authority);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_directory")
    public Long merge(final AuthorityBean authority) {
        AuthorityBean dstAuthority = this.mapper.getByEntityId(authority.getEntityId());
        if (dstAuthority == null) {
            // create
            dstAuthority = authority;
        } else {
            // merge
            try {
                final ObjectMapper jsonMapper = new ObjectMapper();
                final Map<String, Object> srcDetails = this.mapDetails(jsonMapper, authority);
                final Map<String, Object> dstDetails = this.mapDetails(jsonMapper, dstAuthority);

                if (srcDetails.containsKey(CONTACT_INFO_USERS) && !(srcDetails.get(CONTACT_INFO_USERS) instanceof ArrayList)) {
                    Object usersObject = srcDetails.get(CONTACT_INFO_USERS);
                    List<Map<String, Object>> users = new ArrayList<>();
                    users.addAll(((Map) usersObject).values());

                    srcDetails.put(CONTACT_INFO_USERS, users);
                }
                dstDetails.putAll(srcDetails);
                dstAuthority.setDetails(jsonMapper.readTree(jsonMapper.writeValueAsString(dstDetails)));
            } catch (final IOException e) {
                LOGGER.warn("Could not map contact info", e);
            }
        }
        return this.saveDb(dstAuthority);
    }

    /**
     * Saves an authority to the database, after all the JSON transformations
     * are done.
     *
     * @param authority
     *            the authority
     * @return the authority technical id
     */
    @Transactional(value = "ge_directory")
    private Long saveDb(final AuthorityBean authority) {
        Long authorityId;
        final AuthorityBean dbAuthority = this.mapper.getByEntityId(authority.getEntityId());
        final ObjectMapper jsonMapper = new ObjectMapper();
        final Map<String, Object> dbDetails = this.mapDetails(jsonMapper, dbAuthority);
        final Map<String, Object> currDetails = this.mapDetails(jsonMapper, authority);
        final List<Map<String, Object>> dbUsers = this.mapDetailsUsers(jsonMapper, dbDetails);
        final List<Map<String, Object>> currUsers = this.mapDetailsUsers(jsonMapper, currDetails);
        final Map<String, Map<String, Object>> dbUsersIndexed = this.indexUsers(dbUsers);
        final Map<String, Map<String, Object>> currUsersIndexed = this.indexUsers(currUsers);

        if (dbAuthority == null) {
            this.mapper.create(authority);
            authorityId = authority.getId();
        } else {
            this.mapper.update(authority);
            authorityId = dbAuthority.getId();
        }
        for (final Map<String, Object> currUser : currUsers) {
            final String currUserId = (String) currUser.get(USER_ID_ATTRIBUTE);
            if (dbUsersIndexed.get(currUserId) == null) {
                this.mapper.createLink(authorityId, currUserId);
            }
        }
        for (final Map<String, Object> dbUser : dbUsers) {
            final String dbUserId = (String) dbUser.get(USER_ID_ATTRIBUTE);
            if (currUsersIndexed.get(dbUserId) == null) {
                this.mapper.removeLink(authorityId, dbUserId);
            }
        }
        return authorityId;
    }

    /**
     * Maps an authority contact information.
     *
     * @param jsonMapper
     *            a JSON mapper
     * @param authority
     *            the authority
     * @return the mapping
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> mapDetails(final ObjectMapper jsonMapper, final AuthorityBean authority) {
        Map<String, Object> details = null;
        if (authority == null || authority.getDetails() == null || authority.getDetails().toString().length() == 0) {
            details = new HashMap<>();
        } else {
            try {
                details = jsonMapper.readValue(authority.getDetails().toString(), Map.class);
            } catch (final IOException e) {
                LOGGER.warn("Could not map contact info", e);
                details = new HashMap<>();
            }
        }
        return details;
    }

    /**
     * Maps an authority contact information users attribute.
     *
     * @param jsonMapper
     *            a JSON mapper
     * @param details
     *            the contact information
     * @return the mapping
     */
    @SuppressWarnings("unchecked")
    private List<Map<String, Object>> mapDetailsUsers(final ObjectMapper jsonMapper, final Map<String, Object> details) {
        List<Map<String, Object>> users = new ArrayList<>();
        if (details != null) {
            Object usersObject = details.get(CONTACT_INFO_USERS);
            if (usersObject instanceof List) {
                users = (List<Map<String, Object>>) usersObject;
            } else if (usersObject instanceof Map) {
                users.addAll(((Map) usersObject).values());
            }

            // if the list does not exist yet in the JSON, create it
            if (users.isEmpty()) {
                details.put(CONTACT_INFO_USERS, users);
            }
        }
        return users;
    }

    /**
     * Indexes an authority contact information users attribute.
     *
     * @param users
     *            the users attribute
     * @return an index of the users
     */
    private final Map<String, Map<String, Object>> indexUsers(final List<Map<String, Object>> users) {
        final Map<String, Map<String, Object>> indexedUsers = new HashMap<>();
        for (final Map<String, Object> user : users) {
            final String userId = (String) user.get(USER_ID_ATTRIBUTE);
            indexedUsers.put(userId, user);
        }
        return indexedUsers;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(value = "ge_directory")
    public boolean remove(final long id) {
        this.mapper.removeLinks(id);
        return this.mapper.remove(id) > 0;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    @Transactional(value = "ge_directory")
    public void createLink(final String authorityFuncId, final String userId, final String role) {
        // myBatis uses by default localCacheScope="SESSION", so we have to copy
        // the object before modifying it, so that we don't get the modified
        // object in any future call of the same database request
        final AuthorityBean authority = this.dozer.map(this.mapper.getByEntityId(authorityFuncId), AuthorityBean.class);
        if (authority != null && userId != null) {
            try {
                final ObjectMapper jsonMapper = new ObjectMapper();
                final Map<String, Object> details = this.mapDetails(jsonMapper, authority);
                final List<Map<String, Object>> users = this.mapDetailsUsers(jsonMapper, details);
                // search the user we want to link to the authority
                boolean userAlreadyExists = false;
                for (final Map<String, Object> user : users) {
                    if (userId.equals(user.get(USER_ID_ATTRIBUTE))) {
                        userAlreadyExists = true;
                        // create the list of roles for the user
                        List<String> roles = (List<String>) user.get(USER_ROLES_ATTRIBUTE);
                        if (roles == null) {
                            roles = new ArrayList<>();
                            user.put(USER_ROLES_ATTRIBUTE, roles);
                        }
                        // add the role to the user
                        if (StringUtils.isNotBlank(role) && !roles.contains(role)) {
                            roles.add(role);
                        }
                        break;
                    }
                }
                // if the user is not linked to the authority, create the link
                if (!userAlreadyExists) {
                    final Map<String, Object> newUser = new HashMap<>();
                    newUser.put(USER_ID_ATTRIBUTE, userId);
                    // create the list of roles for the user
                    final List<String> roles = new ArrayList<>();
                    newUser.put(USER_ROLES_ATTRIBUTE, roles);
                    // add the role to the user
                    if (StringUtils.isNotBlank(role)) {
                        roles.add(role);
                    }
                    users.add(newUser);
                }
                // update the JSON
                authority.setDetails(jsonMapper.readTree(jsonMapper.writeValueAsString(details)));
            } catch (final IOException e) {
                LOGGER.warn("Could not map contact info", e);
            }
            this.saveDb(authority);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasRoleForAuthority(final String userId, final String role, final String authorityId) {
        final AuthorityBean authority = this.mapper.getByEntityId(authorityId);

        if (authority == null) {
            return false;
        }

        AuthorityDetailsBean authorityDetails = null;
        if (authority.getDetails() != null && authority.getDetails().toString().length() > 0) {
            final ObjectMapper jsonMapper = new ObjectMapper();
            jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            try {
                authorityDetails = jsonMapper.readValue(authority.getDetails().toString(), AuthorityDetailsBean.class);
                // Build the path with the parent and entityId
                authority.setPath(buildPath(authorityDetails.getParent(), authority.getEntityId()));
            } catch (final IOException e) {
                LOGGER.warn("Could not map contact info of authority with id {}", authorityId, e);
            }
        }

        final List<AuthorityRoleBean> authoritiesRoles = this.findAuthoritiesByUserId(userId);
        for (final AuthorityRoleBean authorityRole : authoritiesRoles) {
            if (authority.getPath().startsWith(authorityRole.getPath()) && authorityRole != null && authorityRole.getRoles().contains(role)) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AuthorityRoleBean> findAuthoritiesByUserId(final String userId) {
        final List<AuthorityBean> authorities = this.mapper.findAuthoritiesByUserId(userId);
        final List<AuthorityRoleBean> authoritiesRoles = new ArrayList<>();
        for (final AuthorityBean authority : authorities) {
            final AuthorityRoleBean authorityRole = this.dozer.map(authority, AuthorityRoleBean.class);

            AuthorityDetailsBean authorityDetails = null;
            if (authority.getDetails() != null && authority.getDetails().toString().length() > 0) {
                final ObjectMapper jsonMapper = new ObjectMapper();
                jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                try {
                    authorityDetails = jsonMapper.readValue(authority.getDetails().toString(), AuthorityDetailsBean.class);
                } catch (final IOException e) {
                    LOGGER.warn("Could not map contact info", e);
                }
            }
            if (authorityDetails != null) {
                // ADD Users
                if (!authorityDetails.getUsers().isEmpty()) {
                    for (final AuthorityContactInfoUserBean user : authorityDetails.getUsers()) {
                        if (userId.equals(user.getId())) {
                            authorityRole.setRoles(user.getRoles());
                            break;
                        }
                    }
                }

                // Build the path with the parent and entityId
                authorityRole.setPath(buildPath(authorityDetails.getParent(), authorityRole.getEntityId()));

            }

            authoritiesRoles.add(authorityRole);
        }
        return authoritiesRoles;
    }

    /**
     * Build the path from the parent and entityId
     * 
     * @param parent
     *            the parent's path of the authority
     * @param entityId
     *            th entityId of the authority
     * @return the path of the authority
     */
    private String buildPath(String parent, String entityId) {
        if (StringUtils.isNotBlank(parent)) {
            return String.join("/", parent, entityId);
        } else {
            return entityId;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, List<String>> findUserRights(final String userId) {
        LOGGER.info("Get authorities rights for user {}", userId);
        final List<AuthorityRoleBean> authoritiesRoles = this.findAuthoritiesByUserId(userId);
        LOGGER.info("The user {} is referenced in {} athorities", userId, authoritiesRoles.size());
        HashMap<String, List<String>> authorityRightsUser = new HashMap<String, List<String>>();
        for (final AuthorityRoleBean authorityRole : authoritiesRoles) {
            authorityRightsUser.put(authorityRole.getPath(), authorityRole.getRoles());
        }
        return authorityRightsUser;
    }

}
