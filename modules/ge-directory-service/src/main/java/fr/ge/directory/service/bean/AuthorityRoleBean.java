/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.directory.service.bean;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * The authority.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class AuthorityRoleBean extends BaseBean<AuthorityRoleBean> {

    /** Functional id. */
    private String entityId;

    /** Label. */
    private String label;

    /** Path. */
    private String path;

	/** Roles. */
    private List<String> roles;

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

    /**
     * Getter on attribute {@link #entityId}.
     *
     * @return String entityId
     */
    public String getEntityId() {
        return this.entityId;
    }

    /**
     * Setter on attribute {@link #entityId}.
     *
     * @param entityId
     *            the new value of attribute entityId
     */
    public void setEntityId(final String entityId) {
        this.entityId = entityId;
    }

    /**
     * Getter on attribute {@link #label}.
     *
     * @return String label
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Setter on attribute {@link #label}.
     *
     * @param label
     *            the new value of attribute label
     */
    public void setLabel(final String label) {
        this.label = label;
    }

    /**
     * Gets the path.
     *
     * @return the path
     */
    public String getPath() {
        return this.path;
    }

    /**
     * Sets the path.
     *
     * @param path
     *            the new path
     */
    public void setPath(final String path) {
        this.path = path;
    }

    /**
     * Gets the roles.
     *
     * @return the roles
     */
    public List<String> getRoles() {
        return this.roles;
    }

    /**
     * Sets the roles.
     *
     * @param roles
     *            the new roles
     */
    public void setRoles(final List<String> roles) {
        this.roles = roles;
    }


}
