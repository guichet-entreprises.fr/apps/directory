/**
 *
 */
package fr.ge.directory.core.exception;

/**
 * Class TemporaryException.
 *
 * @author bsadil
 */
public class TemporaryException extends Exception {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new Temporary Exception.
     */
    public TemporaryException() {
        super();
    }

    /**
     * Instantiates a new new Temporary Exception.
     *
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public TemporaryException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new new Temporary Exception.
     *
     * @param message
     *            the message
     */
    public TemporaryException(final String message) {
        super(message);
    }

    /**
     * Instantiates a new Temporary Exception.
     *
     * @param cause
     *            the cause
     */
    public TemporaryException(final Throwable cause) {
        super(cause);
    }

}
