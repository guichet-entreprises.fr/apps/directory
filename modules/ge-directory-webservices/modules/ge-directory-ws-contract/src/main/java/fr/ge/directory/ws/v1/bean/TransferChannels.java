/**
 * 
 */
package fr.ge.directory.ws.v1.bean;

/**
 * TransferChannels section of #Details, contains informations about the
 * activation and details of different channels for the partner
 * 
 * 
 * @author $Author: mtakerra $
 * @version $Revision: 0 $
 */
public class TransferChannels {

    /**
     * Attribute that indicates the activation or not of the backoffice channel
     */
    Backoffice backoffice;

    /**
     * Attribute that indicates the activation or not and the configuration of
     * the ftp channel
     */
    Ftp ftp;

    /**
     * Accesseur sur l'attribut {@link #backoffice}.
     *
     * @return Backoffice backoffice
     */
    public Backoffice getBackoffice() {
        return backoffice;
    }

    /**
     * Mutateur sur l'attribut {@link #backoffice}.
     *
     * @param backoffice
     *            la nouvelle valeur de l'attribut backoffice
     */
    public void setBackoffice(Backoffice backoffice) {
        this.backoffice = backoffice;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public TransferChannels() {
        super();
    }

    /**
     * Accesseur sur l'attribut {@link #ftp}.
     *
     * @return Ftp ftp
     */
    public Ftp getFtp() {
        return ftp;
    }

    /**
     * Mutateur sur l'attribut {@link #ftp}.
     *
     * @param ftp
     *            la nouvelle valeur de l'attribut ftp
     */
    public void setFtp(Ftp ftp) {
        this.ftp = ftp;
    }

}
