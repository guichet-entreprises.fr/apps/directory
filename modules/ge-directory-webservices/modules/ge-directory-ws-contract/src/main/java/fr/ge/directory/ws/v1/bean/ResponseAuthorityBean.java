/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.directory.ws.v1.bean;

import java.sql.Timestamp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * The authority.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "authority", namespace = "http://v1.ws.directory.ge.fr")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseAuthorityBean {

    /** Id. */
    private Long id;

    /** Authority id. */
    private String entityId;

    /** Label. */
    private String label;

    /** Contact information. */
    private JsonNode details;

    /** Path. */
    private String path;

    /** Updated time stamp */
    private Timestamp updated;

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    /**
     * Getter on attribute {@link #id}.
     *
     * @return Long id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Setter on attribute {@link #id}.
     *
     * @param id
     *            the new value of attribute id
     * @return the response authority bean
     */
    public ResponseAuthorityBean setId(final Long id) {
        this.id = id;
        return this;
    }

    /**
     * Getter on attribute {@link #label}.
     *
     * @return String label
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Setter on attribute {@link #label}.
     *
     * @param label
     *            the new value of attribute label
     * @return the response authority bean
     */
    public ResponseAuthorityBean setLabel(final String label) {
        this.label = label;
        return this;
    }

    /**
     * Gets the path.
     *
     * @return the path
     */
    public String getPath() {
        return this.path;
    }

    /**
     * Sets the path.
     *
     * @param path
     *            the new path
     */
    public ResponseAuthorityBean setPath(final String path) {
        this.path = path;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #entityId}.
     *
     * @return String entityId
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * Mutateur sur l'attribut {@link #entityId}.
     *
     * @param entityId
     *            la nouvelle valeur de l'attribut entityId
     */
    public ResponseAuthorityBean setEntityId(String entityId) {
        this.entityId = entityId;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #details}.
     *
     * @return String details
     */
    public JsonNode getDetails() {
        return details;
    }

    /**
     * Mutateur sur l'attribut {@link #details}.
     *
     * @param details
     *            la nouvelle valeur de l'attribut details
     */
    public ResponseAuthorityBean setDetails(JsonNode details) {
        this.details = details;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #updated}.
     *
     * @return Timestamp updated
     */
    public Timestamp getUpdated() {
        return updated;
    }

    /**
     * Mutateur sur l'attribut {@link #updated}.
     *
     * @param updated
     *            la nouvelle valeur de l'attribut updated
     */
    public ResponseAuthorityBean setUpdated(Timestamp updated) {
        this.updated = updated;
        return this;
    }

}
