/**
 * 
 */
package fr.ge.directory.ws.v1.model;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * A Repository Element.
 */
public class RepositoryElement extends RepositoryId {

    /** Element's business id. */
    private String entityId;

    /** Element's label. */
    private String label;

    /** Element's details, stored as json. */
    private JsonNode details;

    /** Element's last updated date. */
    private Date updatedDate;

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

    /**
     * Accesseur sur l'attribut {@link #entityId}.
     *
     * @return String entityId
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * Mutateur sur l'attribut {@link #entityId}.
     *
     * @param entityId
     *            la nouvelle valeur de l'attribut entityId
     */
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    /**
     * Accesseur sur l'attribut {@link #label}.
     *
     * @return String label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Mutateur sur l'attribut {@link #label}.
     *
     * @param label
     *            la nouvelle valeur de l'attribut label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Accesseur sur l'attribut {@link #details}.
     *
     * @return JsonNode details
     */
    public JsonNode getDetails() {
        return details;
    }

    /**
     * Mutateur sur l'attribut {@link #details}.
     *
     * @param details
     *            la nouvelle valeur de l'attribut details
     */
    public void setDetails(JsonNode details) {
        this.details = details;
    }

    /**
     * Accesseur sur l'attribut {@link #updatedDate}.
     *
     * @return Date updatedDate
     */
    public Date getUpdatedDate() {
        return updatedDate;
    }

    /**
     * Mutateur sur l'attribut {@link #updatedDate}.
     *
     * @param updatedDate
     *            la nouvelle valeur de l'attribut updatedDate
     */
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

}
