
package fr.ge.directory.ws.v1.service;

import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import fr.ge.core.rest.exception.RestResponseException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * A set of services around the authority functionalities.
 * 
 * @author mtakerra
 *
 */
public interface AuthorityServicesWebService {

    /**
     * Retrieve and check the active transfer channels for FTP redirect. If a
     * FTP channel is parameterized to use the one of another authority, the
     * final value is retrieve and stored in the first entity.
     * 
     * @param entityId
     *            the entity id of the authority, must be consistent
     * @return the computed transfer channel json node
     * @throws JsonProcessingException
     * @throws RestResponseException
     */
    @GET
    @Path("/services/authority/{entityId}/channel/active")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get authority channels.", tags = { "Private Authority Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{ \"entityId\": \"2020-01-AUT-HORITY\", \"label\": \"Authority label\", \"path\": null, \"ftp\": { \"state\": \"enabled\", \"token\": \"1234567890\", \"pathFolder\": null, \"ftpMode\": \"delegatedChannel\", \"comment\": null, \"delegationAuthority\": \"Root Authority\" } }") //
                    ) //
            ), ///
    })
    JsonNode computeActiveChannel( //
            @Parameter(description = "Authority identifier", required = true) @PathParam("entityId") String entityId) throws RestResponseException;
    // TODO remettre en cause l'objet retour : faire évoluer le bean
    // TransferChannels ou pas?

    /**
     * Authority rights services. <br/>
     * An authority has a hierarchical path. <br/>
     * A user has a list of roles by authority
     */

    /**
     * Get the rights of a user for authorities. <br/>
     * If User has rights returns a map of <path, List<roles>>. <br/>
     * If user does not exist or has no roles returns an empty map.
     *
     * @param userId
     *            the user id
     * @return a map of : authority path and user roles.
     */
    @GET
    @Path("/services/authority/user/{userId}/rights")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get the rights of a user for authorities.", tags = { "Private Authority Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{ \"ROOT/GROUP/AUTHORITY\": [ \"role1\", \"role2\" ] }") //
                    ) //
            ), ///
    })
    Map<String, List<String>> findUserRights( //
            @Parameter(description = "User identifier", required = true) @PathParam("userId") final String userId) throws TechniqueException, FunctionalException;

}
