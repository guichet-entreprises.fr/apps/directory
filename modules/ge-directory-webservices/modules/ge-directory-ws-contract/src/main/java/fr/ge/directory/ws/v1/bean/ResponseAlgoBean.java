/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.directory.ws.v1.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The algo.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "algo", namespace = "http://v1.ws.directory.ge.fr")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseAlgoBean {

    /** Id. */
    private Long id;

    /** Authority type. */
    private String code;

    /** Content. */
    private String content;

    /**
     * Getter on attribute {@link #id}.
     *
     * @return Long id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Setter on attribute {@link #id}.
     *
     * @param id
     *            the new value of attribute id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Getter on attribute {@link #code}.
     *
     * @return String code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Setter on attribute {@link #code}.
     *
     * @param code
     *            the new value of attribute code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Getter on attribute {@link #content}.
     *
     * @return String content
     */
    public String getContent() {
        return this.content;
    }

    /**
     * Setter on attribute {@link #content}.
     *
     * @param content
     *            the new value of attribute content
     */
    public void setContent(final String content) {
        this.content = content;
    }

}
