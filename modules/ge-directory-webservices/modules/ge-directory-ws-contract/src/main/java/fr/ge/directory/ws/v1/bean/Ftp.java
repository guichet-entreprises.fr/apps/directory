/**
 * 
 */
package fr.ge.directory.ws.v1.bean;

/**
 * Ftp section of #TransferChannels, contains information of activation of the
 * Ftp channel and details
 * 
 * @author $Author: mtakerra $
 * @version $Revision: 0 $
 */
public class Ftp {

    /** Id of the Ftp channel (EDDIE) */
    String token;

    /** state of activation of the channel Ftp (enabled, disabled) */
    String state;

    /** file storage path  */
    String pathFolder;
    
    /**
     * Accesseur sur l'attribut {@link #token}.
     *
     * @return String token
     */
    public String getToken() {
        return token;
    }

    /**
     * Mutateur sur l'attribut {@link #token}.
     *
     * @param token
     *            la nouvelle valeur de l'attribut token
     */

    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Accesseur sur l'attribut {@link #state}.
     *
     * @return String state
     */

    public String getState() {
        return state;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public Ftp() {
        super();
    }

    /**
     * Mutateur sur l'attribut {@link #state}.
     *
     * @param state
     *            la nouvelle valeur de l'attribut state
     */
    public void setState(String state) {
        this.state = state;
    }

	public String getPathFolder() {
		return pathFolder;
	}

	public void setPathFolder(String pathFolder) {
		this.pathFolder = pathFolder;
	}
}
