/**
 * 
 */
package fr.ge.directory.ws.v1.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;

import fr.ge.core.rest.exception.RestResponseException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
@Path("v1")
public interface IImportAuthoritiesWebService {

    /**
     * Import Authorities. 2 types : CFE and cities. TODO détailler
     * 
     * @param authorities
     *            the authorities csv files
     * @param dryRun
     *            true if dry run
     * @return empty string if nothing goes wrong, a result list on dry run mode
     * @throws RestResponseException
     *             if anything does not goes as expected
     */
    @POST
    @Path("/import/authority")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    @Operation(summary = "Import authorities", tags = { "Private Import Authority Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN //
                    ) //
            ), //
    })
    public String importAuthorities( //
            @Parameter(description = "Authorities as csv file", required = true) @Multipart List<Attachment> authorities, //
            @Parameter(description = "Simulate") @QueryParam("dryRun") boolean dryRun) throws RestResponseException;

}
