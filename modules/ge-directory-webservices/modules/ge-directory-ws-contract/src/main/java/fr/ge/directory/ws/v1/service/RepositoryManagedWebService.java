/**
 * 
 */
package fr.ge.directory.ws.v1.service;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.ws.v1.model.RepositoryElement;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Generic managed repository web services.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public interface RepositoryManagedWebService {

    /**
     * search some elements into a repository
     * 
     * @param repositoryId
     *            the repository id
     * @param startIndex
     *            start index at 0
     * @param maxResults
     *            max returned result
     * @param filters
     *            filters TODO
     * @param orders
     *            orders TODO
     * @param searchTerms
     *            text search
     * @return
     */
    @GET
    @Path("/repository/{repositoryId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Searches into a specific repository.", tags = { "Managed Repository Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    SearchResult<RepositoryElement> search( //
            @Parameter(description = "Repository identifier", required = true) @PathParam("repositoryId") String repositoryId, //
            @Parameter(description = "Search index") @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Maximum result") @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Query filters") @QueryParam("filters") List<SearchQueryFilter> filters, //
            @Parameter(description = "Query orders") @QueryParam("orders") List<SearchQueryOrder> orders);

}
