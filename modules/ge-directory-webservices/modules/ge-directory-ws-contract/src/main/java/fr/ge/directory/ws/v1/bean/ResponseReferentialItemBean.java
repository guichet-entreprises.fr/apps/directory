/**
 * 
 */
package fr.ge.directory.ws.v1.bean;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author bsadil
 *
 */

@XmlRootElement(name = "referentiel", namespace = "http://v1.ws.directory.ge.fr")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseReferentialItemBean {

    /** Id. */
    private Long id;

    private String entityId;

    /** Label. */
    private String label;

    /** Detail. */
    private JsonNode details;

    /** Created. */
    private Date created;

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public JsonNode getDetails() {
        return this.details;
    }

    public void setDetails(final JsonNode details) {
        this.details = details;
    }

    public Date getCreated() {
        return this.created;
    }

    public void setCreated(final Date created) {
        this.created = created;
    }

    /**
     * @return the entityId
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * @param entityId
     *            the entityId to set
     */
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

}
