/**
 * 
 */
package fr.ge.directory.ws.v1.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author $Author: LABEMONT $
 * @version $Revision: 0 $
 */
public class RepositoryId {

    /** Repository name. */
    private String repositoryId;

    /** Element internal Id. */
    private Long id;

    /**
     * Constructeur de la classe.
     *
     */
    public RepositoryId() {
        super();
    }

    /**
     * @return the repositoryId
     */
    public String getRepositoryId() {
        return repositoryId;
    }

    /**
     * @param repositoryId
     *            the repositoryId to set
     */
    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    /**
     * Accesseur sur l'attribut {@link #id}.
     *
     * @return Long id
     */
    public Long getId() {
        return id;
    }

    /**
     * Mutateur sur l'attribut {@link #id}.
     *
     * @param id
     *            la nouvelle valeur de l'attribut id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}