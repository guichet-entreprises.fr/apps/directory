/**
 * 
 */
package fr.ge.directory.ws.v1.bean;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author $Author: sarahman $
 * @version $Revision: 0 $
 */

@XmlRootElement(name = "refPrice", namespace = "http://v1.ws.directory.ge.fr")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseRefPriceBean {

    /**
     * the code of product
     */
    private String productCode;

    /**
     * the content of product
     */
    private String product;
    /**
     * the price of product
     */
    private String price;

    /**
     * start date
     */
    private Date startDate;

    /**
     * end date
     */
    private Date endDate;

    /**
     * Accesseur sur l'attribut {@link #productCode}.
     *
     * @return String productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Mutateur sur l'attribut {@link #productCode}.
     *
     * @param productCode
     *            la nouvelle valeur de l'attribut productCode
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * Accesseur sur l'attribut {@link #product}.
     *
     * @return String product
     */
    public String getProduct() {
        return product;
    }

    /**
     * Mutateur sur l'attribut {@link #product}.
     *
     * @param product
     *            la nouvelle valeur de l'attribut product
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * Accesseur sur l'attribut {@link #price}.
     *
     * @return String price
     */
    public String getPrice() {
        return price;
    }

    /**
     * Mutateur sur l'attribut {@link #price}.
     *
     * @param price
     *            la nouvelle valeur de l'attribut price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * Accesseur sur l'attribut {@link #startDate}.
     *
     * @return Date startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Mutateur sur l'attribut {@link #startDate}.
     *
     * @param startDate
     *            la nouvelle valeur de l'attribut startDate
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Accesseur sur l'attribut {@link #endDate}.
     *
     * @return Date endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Mutateur sur l'attribut {@link #endDate}.
     *
     * @param endDate
     *            la nouvelle valeur de l'attribut endDate
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

}
