/**
 * 
 */
package fr.ge.directory.ws.v1.bean;

/**
 * Details, contains details of the partner and his configuration
 * 
 * @author $Author: mtakerra $
 * @version $Revision: 0 $
 */
public class Details {

    /** contains partenaire's preferences of the transfer channels */
    TransferChannels transferChannels;

    /**
     * Constructeur de la classe.
     *
     */
    public Details() {
        super();
    }

    /**
     * Accesseur sur l'attribut {@link #transferChannels}.
     *
     * @return TransferChannels transferChannels
     */
    public TransferChannels getTransferChannels() {
        return transferChannels;
    }

    /**
     * Mutateur sur l'attribut {@link #transferChannels}.
     *
     * @param transferChannels
     *            la nouvelle valeur de l'attribut transferChannels
     */
    public void setTransferChannels(TransferChannels transferChannels) {
        this.transferChannels = transferChannels;
    }
}
