/**
 * 
 */
package fr.ge.directory.ws.v1.service;

import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.ws.v1.bean.ResponseRefCommuneBean;
import fr.ge.directory.ws.v1.bean.ResponseRefPriceBean;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * @author bsadil
 *
 */

@Path("/v1/refService")
public interface IRefService {

    /**
     * Search for Authority .
     *
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results per page
     * @param filters
     *            filters as string
     * @param orders
     *            orders as string
     * @return search result
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @GET
    @Path("/refCommune")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Searches into referential \"commune\"", tags = { "Private Referential Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    SearchResult<ResponseRefCommuneBean> search( //
            @Parameter(description = "Search index") @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Maximum result") @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Query filters") @QueryParam("filters") List<SearchQueryFilter> filters, //
            @Parameter(description = "Query orders") @QueryParam("orders") List<SearchQueryOrder> orders, //
            @Parameter(description = "Terms") @QueryParam("searchedValue") @DefaultValue("") String searchedValue);

    /**
     * 
     * @param startIndex
     * @param maxResults
     * @param filters
     * @param orders
     * @param searchedValue
     * @return
     */
    @GET
    @Path("/refPrice")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Searches into referential \"prices\"", tags = { "Private Referential Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    SearchResult<ResponseRefPriceBean> searchPrice( //
            @Parameter(description = "Search index") @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Maximum result") @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Query filters") @QueryParam("filters") List<SearchQueryFilter> filters, //
            @Parameter(description = "Query orders") @QueryParam("orders") List<SearchQueryOrder> orders, //
            @Parameter(description = "Terms") @QueryParam("searchedValue") @DefaultValue("") String searchedValue);

}
