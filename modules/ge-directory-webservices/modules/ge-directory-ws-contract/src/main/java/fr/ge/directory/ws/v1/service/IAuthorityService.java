/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.ws.v1.service;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityBean;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityRoleBean;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Authority services.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@Path("/v1/authority")
public interface IAuthorityService {

    /**
     * Retrieves an authority from its `id`.
     *
     * @param id
     *            the id
     * @return found authority
     * @deprecated WS not used
     */
    @GET
    @Path("/private/{id : \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Deprecated
    ResponseAuthorityBean get(@PathParam("id") long id);

    /**
     * Retrieves an authority from its functional id.
     *
     * @param entityId
     *            the entityId
     * @return found authority
     */
    @GET
    @Path("/{entityId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get authority by identifier.", tags = { "Private Authority Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    ResponseAuthorityBean getByEntityId( //
            @Parameter(description = "Authority identifier", required = true) @PathParam("entityId") String entityId);

    /**
     * Retrieves an authority from metas.
     *
     * @param metas
     *            the metas
     * @return found authority
     */
    @POST
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Retrieves an authority from metas.", tags = { "Private Authority Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    ResponseAuthorityBean find( //
            @Parameter(description = "Input metadata") Map<String, String> metas);

    /**
     * Search for Authority, paginated, filtered, ranked, on a full text value.
     *
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results per page
     * @param filters
     *            filters as string
     * @param orders
     *            orders as string
     * @param searchedValue
     *            the value
     * @return search result
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Search for Authority, paginated, filtered, ranked, on a full text value.", tags = { "Private Authority Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    SearchResult<ResponseAuthorityBean> search( //
            @Parameter(description = "Search index") @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Maximum result") @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Query filters") @QueryParam("filters") List<SearchQueryFilter> filters, //
            @Parameter(description = "Query orders") @QueryParam("orders") List<SearchQueryOrder> orders, //
            @Parameter(description = "Terms") @QueryParam("searchedValue") @DefaultValue("") String searchedValue);

    /**
     * Saves an authority.
     *
     * @param data
     *            the data
     * @return authority technical ID
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Saves an authority.", tags = { "Private Authority Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    Long save( //
            @Parameter(description = "Authority content", required = true) ResponseAuthorityBean data);

    /**
     * Merge an authority.
     *
     * @param data
     *            the data
     * @return authority technical ID
     */
    @PUT
    @Path("/merge")
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Merge an authority.", tags = { "Private Authority Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    Long merge( //
            @Parameter(description = "Authority content", required = true) ResponseAuthorityBean data);

    /**
     * Removes an authority by its ID.
     *
     * @param id
     *            algorithm ID
     * @return true if successful
     */
    @DELETE
    @Path("/{id : \\d+}")
    @Operation(summary = "Removes an authority by its ID.", tags = { "Private Authority Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    boolean remove( //
            @Parameter(description = "Authority identifier", required = true) @PathParam("id") long id);

    /**
     * Creates a link between an authority and a user.
     *
     * @param authorityFuncId
     *            the authority functional id
     * @param userId
     *            the user id
     */
    @POST
    @Path("/{authorityFuncId}/user/{userId}")
    @Operation(summary = "Creates a link between an authority and a user.", tags = { "Private Authority Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    void createLink( //
            @Parameter(description = "Authority target identifier", required = true) @PathParam("authorityFuncId") String authorityFuncId, //
            @Parameter(description = "User target identifier", required = true) @PathParam("userId") final String userId);

    /**
     * Creates a link between an authority and a user, with a role.
     *
     * @param authorityFuncId
     *            the authority functional id
     * @param userId
     *            the user id
     * @param role
     *            the role within the authority
     */
    @POST
    @Path("/{authorityFuncId}/user/{userId}/role/{role}")
    @Operation(summary = "Creates a link between an authority and a user, with a role.", tags = { "Private Authority Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    void createLinkWithRole( //
            @Parameter(description = "Authority target identifier", required = true) @PathParam("authorityFuncId") String authorityFuncId, //
            @Parameter(description = "User target identifier", required = true) @PathParam("userId") final String userId, //
            @Parameter(description = "Role target identifier", required = true) @PathParam("role") final String role);

    /**
     * Checks a user has a role for an authority.
     *
     * @param userId
     *            the user id
     * @param role
     *            the role to check
     * @param authorityId
     *            the authority functional id
     * @return a boolean
     */
    @GET
    @Path("/{authorityFuncId}/user/{userId}/role/{role}")
    @Operation(summary = "Checks a user has a role for an authority.", tags = { "Private Authority Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    boolean hasRoleForAuthority( //
            @Parameter(description = "User target identifier", required = true) @PathParam("userId") final String userId, //
            @Parameter(description = "Role target identifier", required = true) @PathParam("role") final String role, //
            @Parameter(description = "Authority target identifier", required = true) @PathParam("authorityFuncId") final String authorityId);

    /**
     * Find authorities for a user.
     *
     * @param userId
     *            the user id
     * @return authorities list
     */
    @GET
    @Path("/user/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Identifies the authorities which a user belongs.", tags = { "Private Authority Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    List<ResponseAuthorityRoleBean> findAuthoritiesByUserId( //
            @Parameter(description = "User target identifier", required = true) @PathParam("userId") final String userId) throws TechniqueException, FunctionalException;

}