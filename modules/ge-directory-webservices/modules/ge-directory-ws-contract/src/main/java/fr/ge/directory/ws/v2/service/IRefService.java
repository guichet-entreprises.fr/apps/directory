/**
 * 
 */
package fr.ge.directory.ws.v2.service;

import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.databind.JsonNode;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.ws.v1.bean.ResponseReferentialItemBean;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@Path("/v2/ref")
public interface IRefService {

    /**
     * Save a referential table.
     * 
     * @param refTableName
     *            the referential table name
     * @param details
     *            the list of json details
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{refTableName}")
    @Deprecated
    void save(@PathParam("refTableName") final String refTableName, final List<JsonNode> details);

    @GET
    @Path("/{id : \\d+}/{refTableName}")
    @Produces(MediaType.APPLICATION_JSON)
    @Deprecated
    ResponseReferentialItemBean get(@PathParam("id") long id, @PathParam("refTableName") final String refTableName);

    /**
     * 
     * @param startIndex
     * @param maxResults
     * @param filters
     * @param orders
     * @param searchedValue
     * @return
     */
    @GET
    @Path("/{refTableName}")
    @Produces(MediaType.APPLICATION_JSON)
    @Deprecated
    List<JsonNode> search(@PathParam("refTableName") final String refTableName, @QueryParam("searchedValue") @DefaultValue("") String searchedValue);

    /**
     * Truncate the table.
     * 
     * @param refTableName
     *            the referential table name
     */
    @DELETE
    @Path("/{refTableName}")
    @Deprecated
    void truncate(@PathParam("refTableName") final String refTableName);

    /**
     * Search for ref .
     *
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results per page
     * @param filters
     *            filters as string
     * @param orders
     *            orders as string
     * @return search result
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @GET
    @Path("/list/{refName}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Searches into specific referential", tags = { "Private Referential Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    SearchResult<JsonNode> findAll( //
            @Parameter(description = "Referential code identifier", required = true) @PathParam("refName") final String refTableName, //
            @Parameter(description = "Search index", required = true) @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Maximum result", required = true) @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Query filters", required = true) @QueryParam("filters") List<SearchQueryFilter> filters, //
            @Parameter(description = "Query orders", required = true) @QueryParam("orders") List<SearchQueryOrder> orders, //
            @Parameter(description = "Terms or key words", required = true) @QueryParam("searchTerms") @DefaultValue("") String searchTerms);
}
