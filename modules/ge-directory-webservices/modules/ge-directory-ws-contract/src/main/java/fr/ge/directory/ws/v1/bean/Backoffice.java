/**
 * 
 */
package fr.ge.directory.ws.v1.bean;

/**
 * Backoffice section of #TransferChannels, contains information of activation
 * of the backoffice channel
 * 
 * @author $Author: mtakerra $
 * @version $Revision: 0 $
 */
public class Backoffice {

    /** state of activation of the channel backoffice (enabled, disabled) */
    String State;

    /**
     * Accesseur sur l'attribut {@link #state}.
     *
     * @return String state
     */
    public String getState() {
        return State;
    }

    /**
     * Mutateur sur l'attribut {@link #state}.
     *
     * @param state
     *            la nouvelle valeur de l'attribut state
     */

    public void setState(String state) {
        State = state;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public Backoffice() {
        super();
    }
}
