/**
 * 
 */
package fr.ge.directory.ws.v1.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author bsadil
 *
 */
@XmlRootElement(name = "refCommune", namespace = "http://v1.ws.directory.ge.fr")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseRefCommuneBean {

    /**
     * the code of town
     */
    private String codeCommune;

    /**
     * the code of the city
     */
    private String codePostal;

    /**
     * the label of town
     */
    private String label;

    /**
     * 
     */
    public ResponseRefCommuneBean() {
        super();
        // Nothing to do
    }

    /**
     * @return the codeCommune
     */
    public String getCodeCommune() {
        return codeCommune;
    }

    /**
     * @param codeCommune
     *            the codeCommune to set
     */
    public void setCodeCommune(String codeCommune) {
        this.codeCommune = codeCommune;
    }

    /**
     * @return the codePostal
     */
    public String getCodePostal() {
        return codePostal;
    }

    /**
     * @param codePostal
     *            the codePostal to set
     */
    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label
     *            the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

}
