
package fr.ge.directory.ws.v1.service;

import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * A set of services around the authority functionalities.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Path("/public/services")
public interface AuthorityServicesWebPublicService {

    /**
     * Get the rights of a user for authorities. <br/>
     * If User has rights returns a map of <path, List<roles>>. <br/>
     * If user does not exist or has no roles returns an empty map.
     *
     * @param userId
     *            the user id
     * @return a map of : authority path and user roles.
     */
    @GET
    @Path("/authority/user/{userId}/rights")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get the rights of a user for authorities.", tags = { "Public Authority Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{ \"ROOT/GROUP/AUTHORITY\": [ \"role1\", \"role2\" ] }") //
                    ) //
            ), ///
    })
    Map<String, List<String>> findUserRights( //
            @Parameter(description = "User identifier", required = true) @PathParam("userId") final String userId) throws TechniqueException, FunctionalException;
}