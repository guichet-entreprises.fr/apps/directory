/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.ws.v1.service;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.core.exception.FunctionalException;
import fr.ge.core.rest.exception.RestResponseException;
import fr.ge.directory.ws.v1.bean.ResponseAlgoBean;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Algo services.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@Path("/v1/algo")
public interface IAlgoService {

    /**
     * Retrieves an algo from its `id`.
     *
     * @param id
     *            the id
     * @return found issue
     */
    @GET
    @Path("/{id : \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get algo by identifier.", tags = { "Private Algo Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    ResponseAlgoBean get( //
            @Parameter(description = "Algo identifier", required = true) @PathParam("id") long id);

    /**
     * Retrieves an algo from its authority type.
     *
     * @param code
     *            the code
     * @return found algo
     */
    @GET
    @Path("/{code : [a-zA-Z]([a-zA-Z_0-9])*}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get algo by code.", tags = { "Private Algo Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    ResponseAlgoBean getByCode( //
            @Parameter(description = "Algo code", required = true) @PathParam("code") String code);

    /**
     * Saves an algo.
     *
     * @param data
     *            the data
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Save algo.", tags = { "Private Algo Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    void save( //
            @Parameter(description = "Algo content as bean", required = true) ResponseAlgoBean data);

    /**
     * Remove an algorithm by its ID.
     *
     * @param id
     *            algorithm ID
     * @return true if successful
     */
    @DELETE
    @Path("/{id : \\d+}")
    @Operation(summary = "Removes an algo.", tags = { "Private Algo Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    boolean remove( //
            @Parameter(description = "Algo identifier", required = true) @PathParam("id") long id);

    /**
     * Search for algo.
     *
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results per page
     * @param filters
     *            filters as string
     * @param orders
     *            orders as string
     * @return search result
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Searches algo.", tags = { "Private Algo Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    SearchResult<ResponseAlgoBean> search( //
            @Parameter(description = "Start index", required = true) @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Maximum result", required = true) @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Query filters", required = true) @QueryParam("filters") List<SearchQueryFilter> filters, //
            @Parameter(description = "Query orders", required = true) @QueryParam("orders") List<SearchQueryOrder> orders //
    );

    /**
     * Retrieves an authority from algo.
     *
     * @param code
     *            the code
     * @param metas
     *            the metas
     * @return found authority
     */
    @POST
    @Path("/{code}/execute")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Operation(summary = "Executes an algo.", tags = { "Private Algo Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN //
                    ) //
            ), //
    })
    String execute( //
            @Parameter(description = "Algo code identifier", required = true) @PathParam("code") String code, //
            @Parameter(description = "Arguments", required = true) Map<String, String> metas);

    /**
     * Import Algorithms.
     * 
     * @param resourceAsBytes
     *            Resource as bytes
     * @param reset
     *            Reset existing data
     * @return
     * @throws FunctionalException
     * @throws RestResponseException
     */
    @POST
    @Path("/import")
    @Consumes({ "application/zip", MediaType.APPLICATION_OCTET_STREAM })
    @Produces(MediaType.TEXT_PLAIN)
    @Operation(summary = "Imports an algo.", tags = { "Private Algo Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN //
                    ) //
            ), //
    })
    public String importAlgo( //
            @Parameter(description = "Archives containing algo as bytes", required = true) byte[] resourceAsBytes, //
            @Parameter(description = "Reset existing data", required = true) @QueryParam("reset") boolean reset) throws FunctionalException, RestResponseException;
}
