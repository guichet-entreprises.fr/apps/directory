/**
 * 
 */
package fr.ge.directory.ws.v1.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.ws.v1.model.RepositoryElement;
import fr.ge.directory.ws.v1.model.RepositoryId;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Generic repository Seb Services.
 * 
 * @author $Author: LABEMONT $
 */
public interface RepositoryWebService {

    /**
     * Add an element in the repository.
     * 
     * @param repositoryId
     *            the repository id
     * @return the inserted element's id
     */
    @POST
    @Path("/repository/{repository}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Add a single element to the repository.", tags = { "Private Repository Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    RepositoryId add( //
            @Parameter(description = "Repository identifier", required = true) @PathParam("repository") String repositoryId, //
            @Parameter(description = "Repository element to add", required = true) RepositoryElement element);

    /**
     * Add a list of elements to the repository.
     * 
     * @param repositoryId
     *            the Id of the repository
     * @param reset
     *            if true reset the existing data in the repository
     * @param repositoryElements
     *            elements to add to the repository
     * @return
     */
    @PUT
    @Path("/repository/{repositoryId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Add a list of elements to the repository.", tags = { "Private Repository Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    void add( //
            @Parameter(description = "Repository identifier", required = true) @PathParam("repositoryId") String repositoryId, //
            @Parameter(description = "Removes existing data", required = true) @QueryParam("reset") boolean reset, //
            @Parameter(description = "Repository elements to add", required = true) List<RepositoryElement> repositoryElements);

    // update an element

    /**
     * Update an element in the repository.
     * 
     * @param repositoryId
     *            the Id of the repository
     * @param repositoryElement
     *            elements to add to the repository
     */
    @PUT
    @Path("/repository/{repositoryId}/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Update an element in the repository.", tags = { "Private Repository Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    void update( //
            @Parameter(description = "Repository identifier", required = true) @PathParam("repositoryId") String repositoryId, //
            @Parameter(description = "Repository content", required = true) RepositoryElement repositoryElement);

    // TODO : delete an element

    // TODO : delete a list of element

    /**
     * read an element
     * 
     * @param repositoryId
     *            the repository id
     * @param entityId
     *            the element id
     * @return
     */
    @GET
    @Path("/repository/{repositoryId}/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Searches a single element into a specific repository.", tags = { "Private Repository Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    RepositoryElement read( //
            @Parameter(description = "Repository identifier", required = true) @PathParam("repositoryId") String repositoryId, //
            @Parameter(description = "Element identifier") @PathParam("id") long id);

    /**
     * search some elements into a repository
     * 
     * @param repositoryId
     *            the repository id
     * @param startIndex
     *            start index at 0
     * @param maxResults
     *            max returned result
     * @param filters
     *            filters TODO
     * @param orders
     *            orders TODO
     * @param searchTerms
     *            text search
     * @return
     */
    @GET
    @Path("/repository/{repositoryId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Searches into a specific repository.", tags = { "Private Repository Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    SearchResult<RepositoryElement> search( //
            @Parameter(description = "Repository identifier", required = true) @PathParam("repositoryId") String repositoryId, //
            @Parameter(description = "Search index") @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Maximum result") @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Query filters") @QueryParam("filters") List<SearchQueryFilter> filters, //
            @Parameter(description = "Query orders") @QueryParam("orders") List<SearchQueryOrder> orders);

    /**
     * Return a distinct list of the repositories_id
     * 
     * @return list of string representing the repositoriesid present in the
     *         repository
     */
    @GET
    @Path("/repository/repositories")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Lists all repositories.", tags = { "Private Repository Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    List<String> listRepositories();

}
