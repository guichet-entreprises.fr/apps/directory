var listFraisDestinataires = [];

for(var i=0; i<_INPUT_.fraisInformation.fraisDestinations.size(); i++){
	
	var coupleFraisDestination = _INPUT_.fraisInformation.fraisDestinations[i];
	_log.info("coupleFraisDestination  is {}", coupleFraisDestination);

    var authorityCode = coupleFraisDestination.authorityCode;
	_log.info("authorityCode  is {}", authorityCode);	
	
	var authorityLabel = coupleFraisDestination.authorityLabel;
	_log.info("authorityLabel  is {}", authorityLabel);	
		
	var fraisCode = coupleFraisDestination.fraisCode;
	_log.info("fraisCode  is {}", fraisCode);	
		

	var fraisLabel = coupleFraisDestination.fraisLabel;
	_log.info("fraisLabel  is {}", fraisLabel);	
		
	var	fraisPrix = coupleFraisDestination.fraisPrix;
	_log.info("fraisPrix  is {}", fraisPrix);
	
	var fraisDestination = spec.createGroup({
							'id':'fraisDestinations'+i,
							'label':"Frais à payer pour l'autorité "+ authorityLabel,
							'data':[
									spec.createData({
										'id': 'authorityLabel',
										'label': 'Autorité à payer :',
										'type': 'StringReadOnly',
										'value': authorityLabel
										}),
									spec.createData({
										'id': 'frais',
										'label': 'Service à payer :',
										'type': 'StringReadOnly',
										'value': fraisLabel
										}),
									spec.createData({
										'id': 'fraisPrix',
										'label': 'Montant à payer (en euros) :',
										'type': 'StringReadOnly',
										'value': ""+fraisPrix/100
										})						
							]
						});
	listFraisDestinataires.push(fraisDestination);
}


return spec.create({
    id : 'listFrais',
    label : "Liste des services à payer",
	groups : [ spec.createGroup({
					id : 'fraisInformation',
					label:"Liste des frais à payer",
					description:'Cette liste représente tous les frais qui devront être payer aux autorités compétentes chargées du traitement de votre dossier.',
					data : listFraisDestinataires
				})
	]
});