// check inout information
var proxyResult = _INPUT_.proxyResult;

var recordUid = nash.record.description().recordUid;
var fileName = "proxyResult.files-1-"+recordUid+"_reçu.pdf";
var file = nash.doc //
		.load('/payment/generated/'+fileName);
var finalDoc = file.save(fileName);
return spec.create({
		id: 'review',
		label: "Génération du reçu de paiement",
		groups: [spec.createGroup({
				id: 'generatedFile',
				label: "Récapitulatif de votre paiement",
				description: "Votre dossier va être transmis au service compétent.",
				data: [ spec.createData({
						id: 'reviewMsg',
						description:'Veuillez trouver ci-dessous le récapitulatif de votre paiement.',
						type: 'FileReadOnly',
						value: [ finalDoc ]
					})]
			})]
	});
