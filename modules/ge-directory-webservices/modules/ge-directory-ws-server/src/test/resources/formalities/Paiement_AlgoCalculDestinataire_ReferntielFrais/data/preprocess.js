var algo = _INPUT_.infoAlgo.infos.algo;
_log.info("algo is {}",algo);
var secteur1 = _INPUT_.infoAlgo.infos.secteur1;
_log.info("secteur1 is {}",secteur1);
var secteur2 = _INPUT_.infoAlgo.infos.secteur2;
_log.info("secteur2 is {}",secteur2);
var typePersonne = _INPUT_.infoAlgo.infos.typePersonne;
_log.info("typePersonne is {}",typePersonne);
var formJuridique = _INPUT_.infoAlgo.infos.formJuridique;
_log.info("formJuridique is {}",formJuridique);
var optionCMACCI = _INPUT_.infoAlgo.infos.optionCMACCI;
_log.info("optionCMACCI is {}",optionCMACCI);
var codeCommune = _INPUT_.infoAlgo.infos.codeCommune.getId();
_log.info("codeCommune is {}",codeCommune);
var activiteIndustrielle = _INPUT_.infoAlgo.infos.activiteIndustrielle;
_log.info("activiteIndustrielle is {}",activiteIndustrielle);
//Devra etre generique apres
var listCodesFrais = ["IRCS001", "IRM0001", "NRMDP01", "DAGTC01","IRCS009", "DCPCV01"];

//=================================> Partie a garder pour apres : calcul des destinataires Début<=========================================
var args = {
    "secteur1" : secteur1,
	"secteur2" : secteur2,
	"typePersonne" : typePersonne,
	"formJuridique" : formJuridique,
	"optionCMACCI":optionCMACCI,
	"codeCommune": codeCommune,
	"activiteIndustrielle": activiteIndustrielle
};

_log.info("serviceParams  {}",JSON.stringify(args));

// call directory service to find functional id of authority 
var response = nash.service.request('${directory.baseUrl}/v1/algo/{code}/execute',algo) //
        .connectionTimeout(10000) //
        .receiveTimeout(10000)
        .accept('application/json') //
        .post(args);

var receiverInfo = JSON.parse(response.asString());

_log.info("receiverInfo  is {}", receiverInfo);

_log.info("listAuthorities  is {}", receiverInfo.listAuthorities);
_log.info("type of details of listAuthorities is  {}", typeof receiverInfo.listAuthorities);
_log.info("listAuthorities.length is {}",  receiverInfo.listAuthorities.length);

//=================================> Partie a garder pour apres : calcul des destinataires Fin <=========================================

//=================================> Partie qui sera mise par le rédacteur pour conditionner le couple frais autoprité Début<=========================================
var listCoupleAutoriteFrais=[];
for(var i=0; i<receiverInfo.listAuthorities.length; i++){

	//Récupérer les informations de l'autorité 
	_log.info("Authority Code is {}", receiverInfo.listAuthorities[i]['authority']);
	var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', receiverInfo.listAuthorities[i]['authority']) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();   
				   
	var authority = response.asObject();
	
	var funcLabel = !authority.label ? null :authority.label;

	_log.info("funcLabel is {}", funcLabel);
	var coupleAutoriteFrais = {
								"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
								"authorityLabel": funcLabel,
								"frais" : listCodesFrais[i]
								};
	_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);

	listCoupleAutoriteFrais.push(coupleAutoriteFrais);
}

_log.info("listCoupleAutoriteFrais  is {}", listCoupleAutoriteFrais);

//=================================> Partie qui sera mise par le rédacteur pour conditionner le couple frais autoprité Fin=========================================

//=================================> Partie générique pour générer le specCreate à passer à paiement Début=========================================
var listFraisDestinataires=[];
									
for(var i=0; i<listCoupleAutoriteFrais.length; i++){
	//Récupérer les informations du frais
	
	var filters = [];
	filters.push("details.entityId:"+listCoupleAutoriteFrais[i]['frais']);
	_log.info(" -- filters -- is {}", filters);
	var response = nash.service.request('${directory.baseUrl}/private/v1/repository/{repositoryId}', "frais") //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
			   .param("filters",filters)
	           .get();
			   
	var receiverInfo = response.asObject();
	var details = !receiverInfo.content[0].details ? null : receiverInfo.content[0].details;
	_log.info(" -- details -- is {}", details);
	
	if(details != null){
		
		var prix = !details.prix ? null : details.prix;
		_log.info(" -- prix -- is {}", prix);
		var reseau = !details.reseau ? null : details.reseau;
		_log.info(" -- reseau -- is {}", reseau);
		var label = !details.label ? null : details.label;
		_log.info(" -- label -- is {}", label);
	}else{
		_log.error(" -- Error : code with the id {} not found in the repository with the id {}", listCodesFrais[i], "frais");
		return spec.create({
							id : 'codeFraisNotFound',
							label : 'Erreur lors de la recherche des codes de frais',
							groups : [ spec.createGroup({
								id : 'error',
								label : 'Erreur lors de la recherche des codes de frais',
								description : "Une erreur lors de la recherche des codes de frais dans le référentiel, veuillez contacter le support à l'adresse: support@guichet-entreprises.fr",
								data : []
							}) ]
						});					
	}
		
	//Construction de la page à afficher au user
	
	var fraisDestination = spec.createGroup({
							'id':'fraisDestinations['+i+']',
							'label':"Frais à payer",
							'minOccurs':"0",
							'maxOccurs':listCoupleAutoriteFrais.length,
							'data':[
									spec.createData({
										'id': 'authorityCode',
										'label': 'Authorité code :',
										'type': 'StringReadOnly',
										'value':listCoupleAutoriteFrais[i]['authorityCode']
										}),
									spec.createData({
										'id': 'authorityLabel',
										'label': 'Authorité label :',
										'type': 'StringReadOnly',
										'value':listCoupleAutoriteFrais[i]['authorityLabel']
										}),
									spec.createData({
										'id': 'fraisCode',
										'label': 'frais code :',
										'type': 'StringReadOnly',
										'value':listCoupleAutoriteFrais[i]['frais']
										}),
									spec.createData({
										'id': 'fraisLabel',
										'label': 'frais label :',
										'type': 'StringReadOnly',
										'value':label
										}),
									spec.createData({
										'id': 'fraisPrix',
										'label': 'frais prix :',
										'type': 'StringReadOnly',
										'value':""+prix
										}),										
							]
						});
	listFraisDestinataires.push(fraisDestination);
}
_log.info("listFraisDestinataires  is {}", listFraisDestinataires);

//=================================> Partie générique pour générer le specCreate à passer à paiement Fin=========================================

return spec.create({
    id : 'step01',
    label : "Génération de la liste des frais pour le paiement",
	groups : [ spec.createGroup({
					id : 'fraisInformation',
					label:"Liste des frais à payer",
					data : listFraisDestinataires
				})
	]
});