var description = nash.record.description().title; 
var recordUid = nash.record.description().recordUid; 

_log.info("description is {}", description);
_log.info("recordUid is {}", recordUid);

var items = [];

for(var i=0; i<_INPUT_.fraisInformation.fraisDestinations.size(); i++){
	
	//Récupérer les informations Autorité + Frais

	var coupleFraisDestination = _INPUT_.fraisInformation.fraisDestinations[i];
	_log.info("coupleFraisDestination  is {}", coupleFraisDestination);

    var authorityCode = coupleFraisDestination.authorityCode;
	_log.info("authorityCode  is {}", authorityCode);	
	
	var authorityLabel = coupleFraisDestination.authorityLabel;
	_log.info("authorityLabel  is {}", authorityLabel);	
		
	var fraisCode = coupleFraisDestination.fraisCode;
	_log.info("fraisCode  is {}", fraisCode);	
		
	var fraisLabel = coupleFraisDestination.fraisLabel;
	_log.info("fraisLabel  is {}", fraisLabel);	
		
	var	fraisPrix = coupleFraisDestination.fraisPrix;
	_log.info("fraisPrix  is {}", fraisPrix);

	//Vérification du partner ID pour l'autorité 
	
	var partnerID = null;

	// call directory with funcId to find all information of Authorithy
	var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', authorityCode) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();

	//result			     
	var receiverInfo = response.asObject();
	_log.info("receiverInfo is  {}",receiverInfo);
	// prepare all information of receiver to create data.xml

	var funcId = !receiverInfo.entityId ? null : receiverInfo.entityId;
	var funcLabel = !receiverInfo.label ? null :receiverInfo.label;
	var details = !receiverInfo.details ? null : receiverInfo.details;
	_log.info("details is  {}",details);
	var payment = !details.payment ? null : details.payment;
	var email = !details.profile.email ? null : details.profile.email;
	var tel = !details.profile.tel ? null : details.profile.tel;

	_log.info("adress is  {}",details.profile.address);
	_log.info("email is  {}", email);
	_log.info("recipientName is  {}",details.profile.address.recipientName);
	_log.info("addressNameCompl is  {}",details.profile.address.addressNameCompl);
	_log.info("cityNumber is  {}",details.profile.address.cityNumber);

	var nomAuth = !details.profile.address.recipientName ? null : details.profile.address.recipientName;
	var authCP = !details.profile.address.cityNumber ? null : details.profile.address.cityNumber;

	var authAddressNameCompl = !details.profile.address.addressNameCompl ? null : details.profile.address.addressNameCompl;
	var authSpecial = !details.profile.address.special ? null : details.profile.address.special;
	var authCity = !details.profile.address.cityName ? null : details.profile.address.cityName;
	var postalCode = !details.profile.address.postalCode ? null : details.profile.address.postalCode;
	var authAdress = authAddressNameCompl + " " + authSpecial;

	var attachment = attachment ? null : attachment;

	var accountCreation = {
	  "businessKey" :funcId,
	  "name":funcLabel,
	  "type":"VENDOR",
	  "contacts":
		[
		  { "email":email,
			"phone":tel,
			"addresse": authAdress,
			"country": "FR",
			"name":funcLabel,
			"zip": postalCode,
			"city": authCity }
		]
	};
		
	if(null != payment){
		partnerID = details.payment.paymentPartnerId;
	}

	if(partnerID == null)
	{	
		/* var response = nash.service.request('${payment.proxy.api.private.baseUrl}/api/marketplace/v1/checkouts/partners') // */
		var response = nash.service.request('http://localhost:50080/proxy-payment/private/api/marketplace/v1/checkouts/partners') //
		.dataType('application/json') //
		.accept('json') //
		.post(JSON.stringify(accountCreation));
		
		partnerID = response.asString();

	var authorityObject = new Object();


	authorityObject.entityId = funcId
	authorityObject.label = funcLabel

	var contact = {};
	contact["payment"] = {"paymentPartnerId" : partnerID};
	authorityObject.details = contact;

	_log.info("contact is  {}",contact);
	_log.info("authorityObject is  {}",authorityObject);

	var response = nash.service.request('${directory.baseUrl}/v1/authority/merge') //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .dataType('application/json')//
				   .accept('json') //
				   .put(authorityObject);	
	}

	_log.info("after if response create partner account id is  {}", partnerID);
	
var item=	{  
				"name":fraisCode,
				"price": fraisPrix,
				"description":"Paiement du service :"+fraisLabel,
				"partnerId" : partnerID						
			};

items.push(item);

}

_log.info("items is {}", items);

//TODO : Les informations du payeur devront être issues de la formalité

var cart = {    

				"recordUid" : recordUid,  
				"dossierId" : recordUid,
				"userCivility": "MR",
				"userLastName": "EL MOURAHIB",
				"userFirstName": "Kamal",
				"description": description,
				"paymentType":"DB",
				"items":items,	                    
				"reference": recordUid
		};

_log.info("cart is {}", cart);
		
var nfo = nash.hangout.pay({'cart' : JSON.stringify(cart)});

return nfo;