package fr.ge.directory.ws.v1.service.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.ICommuneDataService;
import fr.ge.directory.service.IPriceDataService;
import fr.ge.directory.ws.v1.bean.ResponseRefCommuneBean;
import fr.ge.directory.ws.v1.bean.ResponseRefPriceBean;

/**
 * Class Test of {@link RefServiceImpl}.
 * 
 * @author $Author: sarahman $
 * @version $Revision: 0 $
 */
public class RefServiceImplTest {

    @Mock
    private IPriceDataService priceDataServie;

    @Mock
    private ICommuneDataService communeDataServie;

    @InjectMocks
    private RefServiceImpl refServiceImpl;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test of priceCreation.
     * 
     * @throws Exception
     */
    @Test
    public void testSearchPrice() throws Exception {

        long startIndex = 1;
        List<SearchQueryFilter> filters = new ArrayList<>();
        String searchedValue = "searchedValue";
        long maxResults = 10;
        List<SearchQueryOrder> orders = new ArrayList<>();

        SearchResult<ResponseRefPriceBean> res = new SearchResult<ResponseRefPriceBean>();
        res.setMaxResults(10);

        final SearchQuery searchQuery = new SearchQuery(startIndex, maxResults).setFilters(filters).setOrders(orders).setSearchTerms(searchedValue);
        when(priceDataServie.search(searchQuery, ResponseRefPriceBean.class)).thenReturn(res);

        SearchResult<ResponseRefPriceBean> result = refServiceImpl.searchPrice(startIndex, maxResults, filters, orders, searchedValue);
        Assert.assertNotNull(result);

    }

    @Test
    public void testSearch() throws Exception {
        // Prepare mock
        SearchResult<ResponseRefCommuneBean> results = new SearchResult<ResponseRefCommuneBean>();
        final SearchQuery searchQuery = new SearchQuery(0, 1).setFilters(null).setOrders(null).setSearchTerms("searchTerms");
        when(communeDataServie.search(searchQuery, ResponseRefCommuneBean.class)).thenReturn(results);

        // Execute
        refServiceImpl.search(0L, 1L, null, null, "searchTerms");

        // Verify
        verify(this.communeDataServie).search(searchQuery, ResponseRefCommuneBean.class);

    }

}
