/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.ws.v1.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.IAlgoDataService;
import fr.ge.directory.service.IAuthorityDataService;
import fr.ge.directory.service.bean.AuthorityBean;
import fr.ge.directory.service.bean.AuthorityRoleBean;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityBean;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityRoleBean;

/**
 * Tests {@link AuthorityServiceImpl}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(locations = { "classpath:spring/data-context.xml",
// "classpath:spring/test-context.xml",
// "classpath:spring/ws-server-cxf-context.xml" })
@ContextConfiguration(locations = { "classpath:spring/application-context.xml", "classpath:spring/test-context.xml" })
public class AuthorityServiceImplTest extends AbstractRestTest {

    /** Authority data service. */
    @Autowired
    private IAuthorityDataService authorityDataService;

    /** Algo data service. */
    @Autowired
    private IAlgoDataService algoDataService;

    /**
     * Setup.
     */
    @Before
    public void setup() {

        Mockito.reset(this.authorityDataService, this.algoDataService);

    }

    /**
     * Formats a String to be at the JSON format.
     *
     * @param str
     *            the input string
     * @return the string with the good format
     */
    private String json(final String str) {
        return str.replaceAll("'", "\"");
    }

    /**
     * Tests {@link AuthorityServiceImpl#get(long)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testGet() throws Exception {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean authority = new AuthorityBean();
        authority.setId(42L);
        authority.setEntityId("Mairie78646");
        authority.setLabel("Mairie de Versailles");
        authority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','parent':'GE/Mairies'}")));
        authority.setPath("GE/Mairies/Mairie78646");
        when(this.authorityDataService.get(eq(42L))).thenReturn(authority);

        // call
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/authority/private/42").get();

        // verify
        verify(this.authorityDataService).get(eq(42L));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(this.readAsBean(response, ResponseAuthorityBean.class), allOf( //
                hasProperty("id", equalTo(42L)), //
                hasProperty("entityId", equalTo("Mairie78646")), //
                hasProperty("label", equalTo("Mairie de Versailles")), //
                hasProperty("details", equalTo(mapper.readTree(this.json("{'email':'xxx@xxx.com','parent':'GE/Mairies'}")))), //
                hasProperty("path", equalTo("GE/Mairies/Mairie78646")) //
        ) //
        );
    }

    /**
     * Tests {@link AuthorityServiceImpl#get(long)}. Details :
     * 
     */
    @Test
    public void testGetPathInDetails() throws Exception {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean authority = new AuthorityBean();
        authority.setId(42L);
        authority.setEntityId("Mairie78646");
        authority.setLabel("Mairie de Versailles");
        // authority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646},
        // 'parent': 'GE/CA'}")));
        authority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','parent':'GE/CA'}")));

        authority.setPath("GE/CA/Mairie78646");

        when(this.authorityDataService.get(eq(42L))).thenReturn(authority);

        // call
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/authority/private/42").get();

        // verify
        verify(this.authorityDataService).get(eq(42L));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(this.readAsBean(response, ResponseAuthorityBean.class), allOf( //
                hasProperty("id", equalTo(42L)), //
                hasProperty("entityId", equalTo("Mairie78646")), //
                hasProperty("label", equalTo("Mairie de Versailles")), //

                hasProperty("details", equalTo(mapper.readTree(this.json("{'email':'xxx@xxx.com','parent':'GE/CA'}")))), //
                hasProperty("path", equalTo("GE/CA/Mairie78646")) //
        ) //
        );
    }

    /**
     * Tests {@link AuthorityServiceImpl#getByFuncId(String)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testGetByEntityId() throws Exception {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final AuthorityBean authority = new AuthorityBean();
        authority.setId(42L);
        authority.setEntityId("Mairie78646");
        authority.setLabel("Mairie de Versailles");
        authority.setPath("GE/Mairies/Mairie78646");
        // authority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646}}")));
        authority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','parent':'GE/CA'}")));
        when(this.authorityDataService.getByEntityId(eq("Mairie78646"))).thenReturn(authority);

        // call
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/authority/Mairie78646").get();

        // verify
        verify(this.authorityDataService).getByEntityId(eq("Mairie78646"));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(this.readAsBean(response, ResponseAuthorityBean.class), allOf( //
                hasProperty("id", equalTo(42L)), //
                hasProperty("entityId", equalTo("Mairie78646")), //
                hasProperty("label", equalTo("Mairie de Versailles")), //
                hasProperty("details", equalTo(mapper.readTree(this.json("{'email':'xxx@xxx.com','parent':'GE/CA'}")))), //
                hasProperty("path", equalTo("GE/Mairies/Mairie78646")) //
        ) //
        );
    }

    /**
     * Tests {@link AuthorityServiceImpl#find(Map)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testFind() throws Exception {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final Map<String, String> metas = new HashMap<>();
        metas.put("serviceCompetent", "debitBoisson");
        metas.put("codeInsee", "78646");
        when(this.algoDataService.execute(eq("debitBoisson"), eq(metas))).thenReturn("Mairie78646");
        final AuthorityBean authority = new AuthorityBean();
        authority.setId(42L);
        authority.setEntityId("Mairie78646");
        authority.setLabel("Mairie de Versailles");
        authority.setPath("GE/Mairies/Mairie78646");
        // authority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646}}")));
        authority.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','parent':'GE/CA'}")));
        when(this.authorityDataService.getByEntityId(eq("Mairie78646"))).thenReturn(authority);

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).path("/v1/authority/find").post(metas);

        // verify
        verify(this.algoDataService).execute(eq("debitBoisson"), eq(metas));
        verify(this.authorityDataService).getByEntityId(eq("Mairie78646"));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(this.readAsBean(response, ResponseAuthorityBean.class), allOf( //
                hasProperty("id", equalTo(42L)), //
                hasProperty("entityId", equalTo("Mairie78646")), //
                hasProperty("label", equalTo("Mairie de Versailles")), //
                hasProperty("details", equalTo(mapper.readTree(this.json("{'email':'xxx@xxx.com','parent':'GE/CA'}")))), //
                hasProperty("path", equalTo("GE/Mairies/Mairie78646")) //
        ) //
        );
    }

    /**
     * Tests {@link AuthorityServiceImpl#save(ResponseAuthorityBean)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    @Ignore("Refaire ce test avec des spy car la comparaison des objets ne fonctionne pas")
    public void testSave() throws JsonProcessingException, IOException {
        // call
        ObjectMapper mapper = new ObjectMapper();
        final Map<String, String> data = new HashMap<>();
        data.put("entityId", "Mairie78646");
        data.put("label", "Mairie de Versailles");
        data.put("details", this.json("{\"email\":\"xxx@xxx.com\",\"parent\":\"GE/Mairies\"}")); //

        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/authority").post(data);

        // verify
        final AuthorityBean authority = new AuthorityBean();
        authority.setEntityId("Mairie78646");
        authority.setLabel("Mairie de Versailles");
        authority.setDetails(mapper.readTree(("{\"email\":\"xxx@xxx.com\",\"parent\":\"GE/Mairies\"}")));
        verify(this.authorityDataService).save(refEq(authority));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(response.readEntity(Long.class), equalTo(0L));
    }

    /**
     * Tests {@link AuthorityServiceImpl#merge(ResponseAuthorityBean)}.
     * 
     * @throws IOException
     * @throws JsonProcessingException
     */
    @Test
    public void testMerge() throws JsonProcessingException, IOException {
        // call
        ObjectMapper mapper = new ObjectMapper();
        String jsonAuthority = "{\"entityId\":\"2018-06-VMB-AM7501\", \"details\":{\"transferChannels\":{\"backoffice\":{\"state\":\"disabled\"},\"email\":{\"emails\":[\"kamal@yopmail.com\",\"kamal2@yopmail.com\"],\"state\":\"enabled\"},\"address\":{\"addressDetail\":{\"recipientName\":null,\"recipientNameCompl\":null,\"addressName\":null,\"addressNameCompl\":null,\"cityName\":null,\"postalCode\":null},\"state\":\"disabled\"},\"ftp\":{\"state\":\"disabled\",\"token\":\"\",\"folderPath\":null}}}}";

        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/authority/merge").put(jsonAuthority);

        // verify
        ArgumentCaptor<AuthorityBean> authorityCaptor = ArgumentCaptor.forClass(AuthorityBean.class);
        verify(this.authorityDataService).merge(authorityCaptor.capture());
        AuthorityBean authorityBean = authorityCaptor.getValue();
        assertThat(authorityBean.getEntityId(), equalTo("2018-06-VMB-AM7501"));

    }

    /**
     * Tests {@link AuthorityServiceImpl#search(long, long, List, List)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testSearch() throws Exception {
        // prepare
        ObjectMapper mapper = new ObjectMapper();
        final ResponseAuthorityBean bean = new ResponseAuthorityBean();
        bean.setId(42L);
        bean.setEntityId("Mairie78646");
        bean.setLabel("Mairie de Versailles");
        bean.setPath("GE/Mairies/Mairie78646");
        // bean.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','address':{'zipCode':78646}}")));
        bean.setDetails(mapper.readTree(this.json("{'email':'xxx@xxx.com','parent':'GE/Mairies'}")));
        final SearchResult<ResponseAuthorityBean> dataSearchResult = new SearchResult<>(0, 5);
        dataSearchResult.setTotalResults(3L);
        dataSearchResult.setContent(Arrays.asList(bean, bean, bean));
        when(this.authorityDataService.search(any(SearchQuery.class), eq(ResponseAuthorityBean.class))).thenReturn(dataSearchResult);

        // call
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/authority").query("startIndex", 0).query("maxResults", 5).get();

        // verify
        verify(this.authorityDataService).search(any(SearchQuery.class), eq(ResponseAuthorityBean.class));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(this.readAsBean(response, new TypeReference<SearchResult<ResponseAuthorityBean>>() {
        }), allOf( //
                hasProperty("totalResults", equalTo(3L)), //
                hasProperty("content", hasSize(3)), //
                hasProperty("content", hasItem( //
                        allOf( //
                                hasProperty("id", equalTo(42L)), //
                                hasProperty("entityId", equalTo("Mairie78646")), //
                                hasProperty("label", equalTo("Mairie de Versailles")), //
                                hasProperty("details", equalTo((mapper.readTree(this.json("{'email':'xxx@xxx.com','parent':'GE/Mairies'}"))))), //
                                hasProperty("path", equalTo("GE/Mairies/Mairie78646")) //
                        ) //
                ) //
                ) //
        ));
    }

    /**
     * Tests {@link AuthorityServiceImpl#remove(long)}.
     */
    @Test
    public void testRemove() {
        // prepare
        when(this.authorityDataService.remove(eq(42L))).thenReturn(true);

        // call
        final Response response = this.client().path("/v1/authority/42").delete();

        // verify
        verify(this.authorityDataService).remove(eq(42L));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(response.readEntity(boolean.class), equalTo(true));
    }

    /**
     * Tests {@link AuthorityServiceImpl#createLink(String, String)}.
     */
    @Test
    public void testCreateLink() {
        // call
        final Response response = this.client().path("/v1/authority/Mairie78646/user/2017-10-USR-XXX-42").post(null);

        // verify
        verify(this.authorityDataService).createLink(eq("Mairie78646"), eq("2017-10-USR-XXX-42"), eq(null));
        assertThat(response.getStatus(), equalTo(Status.NO_CONTENT.getStatusCode()));
    }

    /**
     * Tests
     * {@link AuthorityServiceImpl#createLinkWithRole(String, String, String)}.
     */
    @Test
    public void testCreateLinkWithRole() {
        // call
        final Response response = this.client().path("/v1/authority/Mairie78646/user/2017-10-USR-XXX-42/role/referent").post(null);

        // verify
        verify(this.authorityDataService).createLink(eq("Mairie78646"), eq("2017-10-USR-XXX-42"), eq("referent"));
        assertThat(response.getStatus(), equalTo(Status.NO_CONTENT.getStatusCode()));
    }

    /**
     * Tests
     * {@link AuthorityServiceImpl#hasRoleForAuthority(String, String, String)}.
     */
    @Test
    public void testHasRoleForAuthority() {
        // prepare
        when(this.authorityDataService.hasRoleForAuthority(eq("2017-10-USR-XXX-42"), eq("referent"), eq("Mairie78646"))).thenReturn(true);

        // call
        final Response response = this.client().path("/v1/authority/Mairie78646/user/2017-10-USR-XXX-42/role/referent").get();

        // verify
        verify(this.authorityDataService).hasRoleForAuthority(eq("2017-10-USR-XXX-42"), eq("referent"), eq("Mairie78646"));
        assertThat(response.readEntity(boolean.class), equalTo(true));
    }

    /**
     * Tests {@link AuthorityServiceImpl#findAuthoritiesByUserId(String)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testFindAuthoritiesByUserId() throws Exception {
        // prepare
        final AuthorityRoleBean bean = new AuthorityRoleBean();
        bean.setEntityId("Mairie78646");
        bean.setLabel("Mairie de Versailles");
        bean.setPath("GE/Mairies/Mairie78646");
        bean.setRoles(Arrays.asList("user", "referent"));
        final List<AuthorityRoleBean> authoritiesRoles = Arrays.asList(bean, bean, bean);
        when(this.authorityDataService.findAuthoritiesByUserId("2017-10-USR-XXX-42")).thenReturn(authoritiesRoles);

        // call
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/authority/user/2017-10-USR-XXX-42").get();

        // verify
        verify(this.authorityDataService).findAuthoritiesByUserId(eq("2017-10-USR-XXX-42"));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        final List<ResponseAuthorityRoleBean> actual = this.readAsBean(response, new TypeReference<List<ResponseAuthorityRoleBean>>() {
        });
        assertThat(actual, hasSize(3));
        assertThat(actual, hasItem( //
                allOf( //
                        hasProperty("entityId", equalTo("Mairie78646")), //
                        hasProperty("path", equalTo("GE/Mairies/Mairie78646")), //
                        hasProperty("roles", contains("user", "referent")) //
                ) //
        ) //
        );
    }

    /**
     * Tests {@link AuthorityServiceImpl#findAuthoritiesByUserId(String)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testFindAuthoritiesByUserId_Payment() throws Exception {
        // prepare
        final AuthorityRoleBean bean = new AuthorityRoleBean();
        bean.setEntityId("Mairie78646");
        bean.setLabel("Mairie de Versailles");
        bean.setPath("GE/Mairies/Mairie78646");
        bean.setRoles(Arrays.asList("user", "referent"));
        
        final List<AuthorityRoleBean> authoritiesRoles = Arrays.asList(bean, bean, bean);
        when(this.authorityDataService.findAuthoritiesByUserId("2017-10-USR-XXX-42")).thenReturn(authoritiesRoles);

        // call
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/authority/user/2017-10-USR-XXX-42").get();

        // verify
        verify(this.authorityDataService).findAuthoritiesByUserId(eq("2017-10-USR-XXX-42"));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        final List<ResponseAuthorityRoleBean> actual = this.readAsBean(response, new TypeReference<List<ResponseAuthorityRoleBean>>() {
        });
        assertThat(actual, hasSize(3));
        assertThat(actual, hasItem( //
                allOf( //
                        hasProperty("entityId", equalTo("Mairie78646")), //
                        hasProperty("path", equalTo("GE/Mairies/Mairie78646")), //
                        hasProperty("roles", contains("user", "referent")) //
                        
                ) //
        ) //
        );
    }

}
