package fr.ge.directory.ws.v1.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.IOUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.IAuthorityDataService;
import fr.ge.directory.service.IReferentialItemDataService;
import fr.ge.directory.service.bean.AuthorityBean;
import fr.ge.directory.service.bean.ReferentialItemBean;
import fr.ge.tracker.facade.ITrackerFacade;

/**
 * Tests Authorities import
 *
 * @author lhoffste
 */
@RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(locations = { "classpath:spring/data-context.xml",
// "classpath:spring/test-context.xml",
// "classpath:spring/ws-server-cxf-context.xml" })
@ContextConfiguration(locations = { "classpath:spring/application-context.xml", "/spring/external-services-context-test.xml", "classpath:spring/test-context.xml" })
public class ImportAuthoritiesWebServiceTest extends AbstractRestTest {

    /** Authority data service. */
    @Autowired
    private IAuthorityDataService authoritiesDataService;

    @Autowired
    private IReferentialItemDataService referentialItemDataService;

    /** Mapper pour faire du Json */
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private ITrackerFacade trackerFacade;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        Mockito.reset(this.authoritiesDataService, this.trackerFacade);
        when(this.trackerFacade.createUid()).then(new Answer<String>() {
            private String[] trackerIds = new String[] { "2018-06-FGZ-CH-235", "2018-06-HJD-VE-234", "2018-06-KJK-QD-875", "2018-06-AFC-HD-264" };
            private int nbInvocation = 0;

            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                if (nbInvocation < trackerIds.length) {
                    return trackerIds[nbInvocation++];
                }
                return null;
            }
        });
    }

    /**
     * Tests import authorities avec un cas de merge
     *
     * @throws Exception
     *             whatever
     */
    @Test
    public void testImport2Authorities() throws Exception {
        // prepare data
        InputStream csvFileInputStream = this.getClass().getClassLoader().getResourceAsStream("authorities/urssaf_extrait.csv");
        byte[] csvFile = IOUtils.toByteArray(csvFileInputStream);

        SearchResult<Object> emptySearchResult = new SearchResult<>();
        emptySearchResult.setContent(new ArrayList<AuthorityBean>());
        emptySearchResult.setTotalResults(0);
        SearchResult<Object> u0101SearchResult = new SearchResult<>();
        u0101SearchResult.setContent(new ArrayList<ReferentialItemBean>() {
            /** serial */
            private static final long serialVersionUID = 6537325758068416168L;

            {
                ReferentialItemBean authorityItem = new ReferentialItemBean();
                authorityItem.setId(123L);
                authorityItem.setLabel("URSSAF DE L'AIN_TEST-label_stocké_qui_sera_modifié");
                authorityItem.setEntityId("2018-06-AAA-U0101");
                authorityItem.setDetails(objectMapper.readTree("{\"edicode\":\"U0101\",\"parent\":\"GE/URSSAF\"" //
                        + ", \"profile\" : {\"tel\":\"00000000000\"}" //
                        + ", \"payment\":\"docomo\"}"));
                this.add(authorityItem);
            }
        });
        u0101SearchResult.setTotalResults(1);

        // mock data service
        when(this.referentialItemDataService.search(any(SearchQuery.class), any(), anyString())).then(new Answer<SearchResult<Object>>() {

            private int nbInvocation = 0;

            @Override
            public SearchResult<Object> answer(InvocationOnMock invocation) throws Throwable {
                // retourne un résultat une fois sur 2
                // premier coup y'a un résultat
                return (nbInvocation++ % 2 == 1) ? emptySearchResult : u0101SearchResult;
            }
        });

        // ajouter les réponses du dataservice

        List<Attachment> attachments = new ArrayList<Attachment>() {
            /** serial */
            private static final long serialVersionUID = -3555935315175013727L;

            {
                add(new Attachment("files", MediaType.APPLICATION_OCTET_STREAM, csvFile));
            }
        };

        // call
        final Response response = this.client().accept(MediaType.TEXT_PLAIN).type(MediaType.MULTIPART_FORM_DATA).path("/v1/import/authority").query("dryRun", false)
                .post(new MultipartBody(attachments));

        // verify
        assertThat(response.getStatus()).isEqualTo(Status.OK.getStatusCode());
        // seuls les 4 premiers ont eu un id tracker, les autres sont morts
        assertThat(response.readEntity(String.class)).isEqualTo("2");

        // verify d'appel sur la couche dataservice
        ArgumentCaptor<AuthorityBean> savedAuthority = ArgumentCaptor.forClass(AuthorityBean.class);
        verify(this.authoritiesDataService, times(2)).save(savedAuthority.capture());
        List<AuthorityBean> storedAuthorities = savedAuthority.getAllValues();
        // authority U0101
        assertThat(storedAuthorities.get(0).getEntityId()).isEqualTo("2018-06-AAA-U0101");
        assertThat(storedAuthorities.get(0).getDetails().get("profile").get("tel").asText()).isEqualTo("0474456699");
        assertThat(storedAuthorities.get(0).getDetails().get("users")).isNull();
        assertThat(storedAuthorities.get(0).getDetails().get("payment")).isNotNull();
        assertThat(storedAuthorities.get(0).getDetails().get("payment").asText()).isEqualTo("docomo");
        assertThat(storedAuthorities.get(0).getLabel()).isEqualTo("URSSAF DE L'AIN");

        // authority U0202
        assertThat(storedAuthorities.get(1).getEntityId()).isEqualTo("2018-06-FGZ-CU0202");
        assertThat(storedAuthorities.get(1).getDetails().get("profile").get("tel").asText()).isEqualTo("0323273636");
        assertThat(storedAuthorities.get(1).getDetails().get("users")).isNull();
        assertThat(storedAuthorities.get(1).getDetails().get("payment")).isNull();
    }

    /**
     * Tests import authorities failing to get a tracker id.
     *
     * @throws Exception
     *             whatever
     */
    @Test
    public void testImportTooManyAuthorities() throws Exception {
        // prepare data
        InputStream csvFileInputStream = this.getClass().getClassLoader().getResourceAsStream("authorities/urssaf_10elements.csv");
        byte[] csvFile = IOUtils.toByteArray(csvFileInputStream);

        SearchResult<Object> emptySearchResult = new SearchResult<>();
        emptySearchResult.setContent(new ArrayList<AuthorityBean>());
        emptySearchResult.setTotalResults(0);

        // mock data service
        when(this.referentialItemDataService.search(any(SearchQuery.class), any(), anyString())).thenReturn(emptySearchResult);

        List<Attachment> attachments = new ArrayList<Attachment>() {
            /** serial */
            private static final long serialVersionUID = -3555935315175013727L;

            {
                add(new Attachment("files", MediaType.APPLICATION_OCTET_STREAM, csvFile));
            }
        };

        // call
        final Response response = this.client().accept(MediaType.TEXT_PLAIN).type(MediaType.MULTIPART_FORM_DATA).path("/v1/import/authority").query("dryRun", false)
                .post(new MultipartBody(attachments));

        // verify
        assertThat(response.getStatus()).isEqualTo(Status.OK.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo("4");

    }

    /**
     * Tests import communes avec un cas de merge
     *
     * @throws Exception
     *             whatever
     */
    @Test
    public void testImport3Communes() throws Exception {
        // prepare data
        InputStream csvFileInputStream = this.getClass().getClassLoader().getResourceAsStream("authorities/communes_extrait.csv");
        byte[] csvFile = IOUtils.toByteArray(csvFileInputStream);

        SearchResult<Object> emptySearchResult = new SearchResult<>();
        emptySearchResult.setContent(new ArrayList<AuthorityBean>());
        emptySearchResult.setTotalResults(0);
        SearchResult<Object> commune01001SearchResult = new SearchResult<>();
        commune01001SearchResult.setContent(new ArrayList<ReferentialItemBean>() {
            /** serial */
            private static final long serialVersionUID = 6537325758068416168L;

            {
                ReferentialItemBean authorityItem = new ReferentialItemBean();
                authorityItem.setId(123L);
                authorityItem.setLabel("ABERGEMENT-CLEMENCIAT_TEST-MERGE-LABEL");
                authorityItem.setEntityId("01001");
                authorityItem.setDetails(
                        objectMapper.readTree("{\"codeCommune\": \"01001\",\"CCI\": \"C0101\",\"CMA\": \"M0101\",\"GREFFE\": \"G0101\",\"CA\": \"\",\"URSSAF\": \"U0101\",\"parent\": \"ZONES\"}"));
                this.add(authorityItem);
            }
        });
        commune01001SearchResult.setTotalResults(1);

        // mock data service
        when(this.referentialItemDataService.search(any(SearchQuery.class), any(), anyString())).then(new Answer<SearchResult<Object>>() {

            @Override
            public SearchResult<Object> answer(InvocationOnMock invocation) throws Throwable {
                // retourne un résultat pour la première commune
                String searchArgumentToString = ((SearchQuery) invocation.getArgument(0)).toString();
                if (searchArgumentToString.contains("01001")) {
                    return commune01001SearchResult;
                }
                if (searchArgumentToString.matches(".*(M0101|C0101|X0101|U0101|G0101|Z0000|M9741|C9742|X9741|U9741|G9742|M9871|C9871|X9871){1}.*")) {
                    return createResult(((SearchQuery) invocation.getArgument(0)).getFilters().get(0).getValue());
                }
                return emptySearchResult;
            }
        });

        // ajouter les réponses du dataservice

        List<Attachment> attachments = new ArrayList<Attachment>() {
            /** serial */
            private static final long serialVersionUID = -1035607140140189719L;
            {
                add(new Attachment("files", MediaType.APPLICATION_OCTET_STREAM, csvFile));
            }
        };

        // call
        final Response response = this.client().accept(MediaType.TEXT_PLAIN).type(MediaType.MULTIPART_FORM_DATA).path("/v1/import/authority").query("dryRun", false)
                .post(new MultipartBody(attachments));

        // verify
        assertThat(response.getStatus()).isEqualTo(Status.OK.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo("3");

        // verify d'appel sur la couche dataservice
        ArgumentCaptor<AuthorityBean> savedAuthority = ArgumentCaptor.forClass(AuthorityBean.class);
        verify(this.authoritiesDataService, times(3)).save(savedAuthority.capture());
        List<AuthorityBean> storedAuthorities = savedAuthority.getAllValues();
        // authority ABERGEMENT-CLEMENCIAT;01001
        assertThat(storedAuthorities.get(0).getEntityId()).isEqualTo("01001");
        assertThat(storedAuthorities.get(0).getLabel()).isEqualTo("ABERGEMENT-CLEMENCIAT");
        assertThat(storedAuthorities.get(0).getDetails().get("CMA").asText()).isEqualTo("2018-06-AAA-L" + "M0101");
        assertThat(storedAuthorities.get(0).getDetails().get("CCI").asText()).isEqualTo("2018-06-AAA-L" + "C0101");
        assertThat(storedAuthorities.get(0).getDetails().get("CA").asText()).isEqualTo("2018-06-AAA-L" + "X0101");
        assertThat(storedAuthorities.get(0).getDetails().get("URSSAF").asText()).isEqualTo("2018-06-AAA-L" + "U0101");
        assertThat(storedAuthorities.get(0).getDetails().get("GREFFE").asText()).isEqualTo("2018-06-AAA-L" + "G0101");
        assertThat(storedAuthorities.get(0).getDetails().get("CNBA").asText()).isEqualTo("2018-06-AAA-L" + "Z0000");

        // authority SAINT-PIERRE;97416
        assertThat(storedAuthorities.get(1).getEntityId()).isEqualTo("97416");
        assertThat(storedAuthorities.get(1).getDetails().get("CMA").asText()).isEqualTo("2018-06-AAA-L" + "M9741");
        assertThat(storedAuthorities.get(1).getDetails().get("CCI").asText()).isEqualTo("2018-06-AAA-L" + "C9742");
        assertThat(storedAuthorities.get(1).getDetails().get("CA").asText()).isEqualTo("2018-06-AAA-L" + "X9741");
        assertThat(storedAuthorities.get(1).getDetails().get("URSSAF").asText()).isEqualTo("2018-06-AAA-L" + "U9741");
        assertThat(storedAuthorities.get(1).getDetails().get("GREFFE").asText()).isEqualTo("2018-06-AAA-L" + "G9742");
        assertThat(storedAuthorities.get(1).getDetails().get("CNBA").asText()).isEqualTo("2018-06-AAA-L" + "Z0000");

        // authority HITIAA O TE RA;98722
        assertThat(storedAuthorities.get(2).getEntityId()).isEqualTo("98722");
        assertThat(storedAuthorities.get(2).getDetails().get("CMA").asText()).isEqualTo("2018-06-AAA-L" + "M9871");
        assertThat(storedAuthorities.get(2).getDetails().get("CCI").asText()).isEqualTo("2018-06-AAA-L" + "C9871");
        assertThat(storedAuthorities.get(2).getDetails().get("CA").asText()).isEqualTo("2018-06-AAA-L" + "X9871");
        assertThat(storedAuthorities.get(2).getDetails().get("URSSAF")).isEmpty();
        assertThat(storedAuthorities.get(2).getDetails().get("GREFFE")).isEmpty();
        assertThat(storedAuthorities.get(2).getDetails().get("CNBA").asText()).isEqualTo("2018-06-AAA-L" + "Z0000");
        // TODO finir les assert
    }

    private SearchResult<Object> createResult(String ediCode) throws IOException {
        SearchResult<Object> u0101SearchResult = new SearchResult<>();
        u0101SearchResult.setContent(new ArrayList<ReferentialItemBean>() {
            /** serial */
            private static final long serialVersionUID = 6537325758068416168L;

            {
                ReferentialItemBean authorityItem = new ReferentialItemBean();
                authorityItem.setId(123L);
                authorityItem.setLabel("Autorité " + ediCode);
                authorityItem.setEntityId("2018-06-AAA-L" + ediCode);
                authorityItem.setDetails(objectMapper.readTree("{\"edicode\":\"" + ediCode + "\",\"parent\":\"GE/CODE\"" //
                        + ", \"profile\" : {\"tel\":\"00000000000\"}}"));
                this.add(authorityItem);
            }
        });
        u0101SearchResult.setTotalResults(1);
        return u0101SearchResult;
    }

}
