/**
 * 
 */
package fr.ge.directory.ws.v1.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.ge.common.utils.test.AbstractRestTest;

/**
 * @author mmouhous
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:spring/application-context.xml", "classpath*:spring/test-context.xml" })
@WebAppConfiguration
public class ManagedWebServiceImplTest extends AbstractRestTest {

    @Autowired
    @Qualifier("managedRestServer")
    private JAXRSServerFactoryBean restServerFactory;

    @Override
    protected JAXRSServerFactoryBean getRestServerFactory() {
        return this.restServerFactory;
    }

    @Test
    public void healthcheckTest() {

        Response response = this.client().accept(MediaType.TEXT_PLAIN).path("/directory/healthcheck").get();
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

    }
}