/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.ws.v1.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.type.TypeReference;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.IAlgoDataService;
import fr.ge.directory.service.bean.AlgoBean;
import fr.ge.directory.ws.v1.bean.ResponseAlgoBean;

/**
 * Tests {@link AlgoServiceImpl}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(locations = { "classpath:spring/data-context.xml",
// "classpath:spring/test-context.xml",
// "classpath:spring/ws-server-cxf-context.xml" })
@ContextConfiguration(locations = { "classpath:spring/application-context.xml", "classpath:spring/test-context.xml" })
public class AlgoServiceImplTest extends AbstractRestTest {

    /** Algo data service. */
    @Autowired
    private IAlgoDataService algoDataService;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        Mockito.reset(this.algoDataService);
    }

    /**
     * Tests {@link AlgoServiceImpl#get(long)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testGet() throws Exception {
        // prepare
        final AlgoBean algo = new AlgoBean();
        algo.setId(42L);
        algo.setCode("debitBoisson");
        algo.setContent("return 'Mairie' + args.codeInsee;".getBytes());
        when(this.algoDataService.get(eq(42L))).thenReturn(algo);

        // call
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/algo/42").get();

        // verify
        verify(this.algoDataService).get(eq(42L));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(this.readAsBean(response, ResponseAlgoBean.class), allOf( //
                hasProperty("id", equalTo(42L)), //
                hasProperty("code", equalTo("debitBoisson")), //
                hasProperty("content", equalTo("return 'Mairie' + args.codeInsee;")) //
        ) //
        );
    }

    /**
     * Tests {@link AlgoServiceImpl#getByCode(String)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testGetByCode() throws Exception {
        // prepare
        final AlgoBean algo = new AlgoBean();
        algo.setId(42L);
        algo.setCode("debitBoisson");
        algo.setContent("return 'Mairie' + args.codeInsee;".getBytes());
        when(this.algoDataService.getByCode(eq("debitBoisson"))).thenReturn(algo);

        // call
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/algo/debitBoisson").get();

        // verify
        verify(this.algoDataService).getByCode(eq("debitBoisson"));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(this.readAsBean(response, ResponseAlgoBean.class), allOf( //
                hasProperty("id", equalTo(42L)), //
                hasProperty("code", equalTo("debitBoisson")), //
                hasProperty("content", equalTo("return 'Mairie' + args.codeInsee;")) //
        ) //
        );
    }

    /**
     * Tests
     * {@link AlgoServiceImpl#save(fr.ge.directory.ws.v1.bean.ResponseAlgoBean)}.
     */
    @Test
    public void testSave() {
        // call
        final Map<String, String> data = new HashMap<>();
        data.put("code", "debitBoisson");
        data.put("content", "return 'Mairie' + args.codeInsee;");
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/algo").post(data);

        // verify
        final AlgoBean algo = new AlgoBean();
        algo.setCode("debitBoisson");
        algo.setContent("return 'Mairie' + args.codeInsee;".getBytes());
        verify(this.algoDataService).save(refEq(algo));
        assertThat(response.getStatus(), equalTo(Status.NO_CONTENT.getStatusCode()));
    }

    /**
     * Tests
     * {@link AlgoServiceImpl#search(long, long, java.util.List, java.util.List)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testSearch() throws Exception {
        // prepare
        final ResponseAlgoBean bean = new ResponseAlgoBean();
        bean.setId(42L);
        bean.setCode("debitBoisson");
        bean.setContent("return 'Mairie' + args.codeInsee;");
        final SearchResult<ResponseAlgoBean> dataSearchResult = new SearchResult<>(0, 5);
        dataSearchResult.setTotalResults(3L);
        dataSearchResult.setContent(Arrays.asList(bean, bean, bean));
        when(this.algoDataService.search(any(SearchQuery.class), eq(ResponseAlgoBean.class))).thenReturn(dataSearchResult);

        // call
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/algo").query("startIndex", 0).query("maxResults", 5).get();

        // verify
        verify(this.algoDataService).search(any(SearchQuery.class), eq(ResponseAlgoBean.class));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(this.readAsBean(response, new TypeReference<SearchResult<ResponseAlgoBean>>() {
        }), allOf( //
                hasProperty("totalResults", equalTo(3L)), //
                hasProperty("content", hasSize(3)), hasProperty("content", hasItem( //
                        allOf( //
                                hasProperty("id", equalTo(42L)), //
                                hasProperty("code", equalTo("debitBoisson")), //
                                hasProperty("content", equalTo("return 'Mairie' + args.codeInsee;")) //
                        ) //
                ) //
                ) //
        ));
    }

    /**
     * Tests {@link AlgoServiceImpl#remove(long)}.
     */
    @Test
    public void testRemove() {
        // prepare
        when(this.algoDataService.remove(eq(42L))).thenReturn(true);

        // call
        final Response response = this.client().path("/v1/algo/42").delete();

        // verify
        verify(this.algoDataService).remove(eq(42L));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(response.readEntity(boolean.class), equalTo(true));
    }

    /**
     * Tests {@link AlgoServiceImpl#execute(String, java.util.Map)}.
     */
    @Test
    public void testExecute() {
        // prepare
        final Map<String, String> metas = new HashMap<>();
        metas.put("codeInsee", "75012");
        when(this.algoDataService.execute(eq("debitBoisson"), eq(metas))).thenReturn("Mairie75012");

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).accept(MediaType.TEXT_PLAIN).path("/v1/algo/debitBoisson/execute").post(metas);

        // verify
        verify(this.algoDataService).execute(eq("debitBoisson"), eq(metas));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(response.readEntity(String.class), equalTo("Mairie75012"));
    }

    /**
     * {@link AlgoServiceImpl#importAlgo(byte[], boolean)}.
     *
     * @throws Exception
     *             whatever
     */
    @Test
    public void testImportAlgo() throws Exception {
        // prepare data
        InputStream zipFileInputStream = this.getClass().getClassLoader().getResourceAsStream("algo/import.zip");
        byte[] resourceAsBytes = IOUtils.toByteArray(zipFileInputStream);

        // call
        final Response response = this.client().accept(MediaType.TEXT_PLAIN) //
                .type("application/zip") //
                .path("/v1/algo/import") //
                .query("reset", false) //
                .post(resourceAsBytes);

        // verify
        assertThat(response.getStatus()).isEqualTo(Status.OK.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo("2");

        // verify d'appel sur la couche dataservice
        ArgumentCaptor<AlgoBean> savedAlgo = ArgumentCaptor.forClass(AlgoBean.class);
        verify(this.algoDataService, times(2)).save(savedAlgo.capture());
        List<AlgoBean> storedAlgo = savedAlgo.getAllValues();
        assertThat(storedAlgo.get(0).getCode()).isEqualTo("algo1");
        assertThat(storedAlgo.get(0).getContent()).isEqualTo("return \"Hello World\";".getBytes());
        assertThat(storedAlgo.get(1).getCode()).isEqualTo("algo2");
        assertThat(storedAlgo.get(1).getContent()).isEqualTo("return \"Hello World\";".getBytes());
    }

    /**
     * {@link AlgoServiceImpl#importAlgo(byte[], boolean)}.
     *
     * @throws Exception
     *             whatever
     */
    @Test
    public void testImportAlgoWithReset() throws Exception {
        // prepare data
        InputStream zipFileInputStream = this.getClass().getClassLoader().getResourceAsStream("algo/import.zip");
        byte[] resourceAsBytes = IOUtils.toByteArray(zipFileInputStream);

        doNothing().when(this.algoDataService).removeAll();

        // call
        final Response response = this.client().accept(MediaType.TEXT_PLAIN) //
                .type("application/zip") //
                .path("/v1/algo/import") //
                .query("reset", true) //
                .post(resourceAsBytes);

        // verify
        assertThat(response.getStatus()).isEqualTo(Status.OK.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo("2");

        verify(this.algoDataService).removeAll();

        // verify d'appel sur la couche dataservice
        ArgumentCaptor<AlgoBean> savedAlgo = ArgumentCaptor.forClass(AlgoBean.class);
        verify(this.algoDataService, times(2)).save(savedAlgo.capture());
        List<AlgoBean> storedAlgo = savedAlgo.getAllValues();
        assertThat(storedAlgo.get(0).getCode()).isEqualTo("algo1");
        assertThat(storedAlgo.get(0).getContent()).isEqualTo("return \"Hello World\";".getBytes());
        assertThat(storedAlgo.get(1).getCode()).isEqualTo("algo2");
        assertThat(storedAlgo.get(1).getContent()).isEqualTo("return \"Hello World\";".getBytes());
    }
}
