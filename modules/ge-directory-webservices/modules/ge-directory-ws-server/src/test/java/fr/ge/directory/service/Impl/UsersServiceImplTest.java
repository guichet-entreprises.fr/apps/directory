package fr.ge.directory.service.Impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.ct.authentification.bean.AccountUserBean;
import fr.ge.ct.authentification.facade.IAccountFacade;
import fr.ge.directory.service.IAuthorityDataService;
import fr.ge.directory.service.IReferentialItemDataService;
import fr.ge.directory.service.IUsersService;
import fr.ge.directory.service.bean.AuthorityBean;
import fr.ge.directory.service.bean.ReferentialItemBean;
import fr.ge.directory.ws.bean.LegacyAuthorityUserLink;
import fr.ge.directory.ws.constant.enumeration.LegacyAuthorityLvl2CodeEnum;
import fr.ge.directory.ws.constant.enumeration.LegacyUserRoleEnum;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/persistence-context.xml", "classpath:spring/application-context.xml", "classpath:spring/test-context.xml" })
public class UsersServiceImplTest {

    @Autowired
    private IUsersService service;

    @Autowired
    private IAuthorityDataService authorityDataService;

    @Autowired
    private IReferentialItemDataService referentialItemDataService;

    @Autowired
    private IAccountFacade accountFacade;

    @Test
    public void testImportUsers() throws Exception {

        String nbLines = "";
        List<LegacyAuthorityUserLink> listUsersToImport = new ArrayList<LegacyAuthorityUserLink>();
        boolean dryRun = true;

        final StringBuilder logPrefixBuilder = new StringBuilder();
        logPrefixBuilder.append("[Import]");
        final String logPrefix = logPrefixBuilder.toString();

        LegacyAuthorityUserLink userLevel1 = new LegacyAuthorityUserLink();
        userLevel1.setLvl1Code("GE");
        userLevel1.setLvl2Code("");
        userLevel1.setLvl3Code("");
        userLevel1.setLvl3Label("");
        userLevel1.setUserEmail("pro.loic@yopmail.com");
        userLevel1.setUserFirstName("Loic");
        userLevel1.setUserId("1");
        userLevel1.setUserLastName("abemonty");
        userLevel1.setUserRole(LegacyUserRoleEnum.ADMINISTRATEUR);

        LegacyAuthorityUserLink userLevel2 = new LegacyAuthorityUserLink();
        userLevel2.setLvl1Code("");
        userLevel2.setLvl2Code("CMA");
        userLevel2.setUserLvl2Code("CMA");
        userLevel2.setLvl3Code("");
        userLevel2.setLvl3Label("");
        userLevel2.setUserEmail("kamal@yopmail.com");
        userLevel2.setUserFirstName("Kamal");
        userLevel2.setUserId("2");
        userLevel2.setUserLastName("EL MOURAHIB");
        userLevel2.setUserRole(LegacyUserRoleEnum.REFERENT);

        LegacyAuthorityUserLink userLevel3 = new LegacyAuthorityUserLink();
        userLevel3.setId("1");
        userLevel3.setLvl1Code("");
        userLevel3.setLvl2Code("");
        userLevel3.setLvl3Code("M3101");
        userLevel3.setLvl3Label("CMA DE HTE GARONNE");
        userLevel3.setUserEmail("kamal@yopmail.com");
        userLevel3.setUserFirstName("Mohamed Ahmed");
        userLevel3.setUserId("3");
        userLevel3.setUserLastName("TAKERRABET");
        userLevel3.setUserRole(LegacyUserRoleEnum.UTILISATEUR);

        listUsersToImport.add(userLevel1);
        listUsersToImport.add(userLevel2);
        listUsersToImport.add(userLevel3);

        SearchResult<ReferentialItemBean> authorityFound = new SearchResult<ReferentialItemBean>();
        List<ReferentialItemBean> content = new ArrayList<ReferentialItemBean>();
        String contentString = "{                                                                    " + //
                "           \"id\": 1,                                                                " + //
                "           \"label\": \"CMA DE HTE GARONNE\",                                        " + //
                "           \"entityId\": \"CMA31555\",                                               " + //
                "           \"details\": {                                                            " + //
                "               \"ediCode\": \"M3101\",                                               " + //
                "               \"profile\": {                                                        " + //
                "                   \"tel\": \"00561104747\",                                         " + //
                "                   \"fax\": \"0561104748\",                                          " + //
                "                   \"emails\": [                                                     " + //
                "                       null                                                          " + //
                "                   ],                                                                " + //
                "                   \"url\": null,                                                    " + //
                "                   \"observation\": null,                                            " + //
                "                   \"email\": \"ekamal@yopmail.com\",                                " + //
                "                   \"address\": {                                                    " + //
                "                       \"recipientName\": \"CMA DE HTE GARONNE\",                    " + //
                "                       \"addressName\": \"18B BD LASCROSSES\",                       " + //
                "                       \"addressNameCompl\": null,                                   " + //
                "                       \"special\": \"BP 496\",                                      " + //
                "                       \"postalCode\": \"31010\",                                    " + //
                "                       \"cedex\": null,                                              " + //
                "                       \"cityNumber\": \"31555\",                                    " + //
                "                       \"cityName\": \"Paris\"                                       " + //
                "                   }                                                                 " + //
                "               },                                                                    " + //
                "               \"parent\": \"GE/CMA\",                                               " + //
                "               \"email\": \"kamal@yopmail.com\"                                      " + //
                "           },                                                                        " + //
                "           \"updated\": 1529335611473                                                " + //
                "}";
        ReferentialItemBean contentJsonNode = new ObjectMapper().readValue(contentString, ReferentialItemBean.class);
        content.add(contentJsonNode);
        authorityFound.setMaxResults(10);
        authorityFound.setStartIndex(0);
        authorityFound.setTotalResults(1);
        authorityFound.setContent(content);

        // Mocking the returned authorities
        // => GE Authority
        AuthorityBean authorityGe = new AuthorityBean();
        authorityGe.setEntityId("GE");
        authorityGe.setLabel("GE");
        authorityGe.setPath("GE");

        String geDetails = "{                                                                         " + //
                "               \"users\": [                                                          " + //
                "                   {                                                                 " + //
                "                       \"roles\": [                                                  " + //
                "                           \"referent\",                                             " + //
                "                           \"all\"                                                   " + //
                "                       ],                                                            " + //
                "                       \"id\": \"2018-01-AZV-EPW-15\"                                " + //
                "                   },                                                                " + //
                "                   {                                                                 " + //
                "                       \"roles\": [                                                  " + //
                "                           \"referent\",                                             " + //
                "                           \"all\"                                                   " + //
                "                       ],                                                            " + //
                "                       \"id\": \"2018-01-AZV-EPW-10\"                                " + //
                "                   }                                                                 " + //
                "               ],                                                                    " + //
                "               \"parent\": \"GE/CMA\"                                                " + //
                "}";
        JsonNode geDetailsJsonNode = new ObjectMapper().readValue(geDetails, JsonNode.class);
        authorityGe.setDetails(geDetailsJsonNode);

        // => Authority of level 2
        AuthorityBean authorityLevel2 = new AuthorityBean();
        authorityLevel2.setEntityId("CMA");
        authorityLevel2.setLabel("CMA");
        authorityLevel2.setPath("GE/CMA");
        authorityLevel2.setDetails(geDetailsJsonNode);

        // => Authority of level 3
        AuthorityBean authorityLevel3 = new AuthorityBean();
        authorityLevel3.setEntityId("CMA31555");
        authorityLevel3.setLabel("CMA DE HTE GARONNE");
        authorityLevel3.setPath("GE/CMA/CMA31555");
        authorityLevel3.setDetails(contentJsonNode.getDetails());

        // Mock the user returned by Account call :

        AccountUserBean userAccount = new AccountUserBean();
        userAccount.setEmail("kamal@yopmail.com");
        userAccount.setFirstName("Kamal");
        userAccount.setLastName("EL MOURAHIB");
        userAccount.setCivility("Monsieur");
        userAccount.setLanguage("fr_FR");
        userAccount.setPhone("+33600000000");
        userAccount.setTrackerId("018-01-AZV-EPW-25");

        // Mocking the Calls to data service
        when(this.accountFacade.readUser(Mockito.any(String.class))).thenReturn(userAccount);
        when(this.authorityDataService.getByEntityId("GE")).thenReturn(authorityGe);
        when(this.authorityDataService.getByEntityId(LegacyAuthorityLvl2CodeEnum.valueOf(userLevel2.getLvl2Code()).getCode())).thenReturn(authorityLevel2);
        when(this.referentialItemDataService.search(Mockito.any(SearchQuery.class), Mockito.any(Class.class), Mockito.any(String.class))).thenReturn(authorityFound);
        // Mockito.doReturn(authorityFound).when(this.referentialItemDataService.search(Mockito.any(SearchQuery.class),
        // Mockito.any(), Mockito.any(String.class)));
        when(this.authorityDataService.getByEntityId("CMA31555")).thenReturn(authorityLevel3);

        nbLines = this.service.importUsers(listUsersToImport, false, logPrefix);

        assertThat(nbLines).isEqualTo("3");
    }
}
