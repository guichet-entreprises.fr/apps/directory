package fr.ge.directory.ws.v1.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.StreamUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.directory.service.IAuthorityDataService;
import fr.ge.directory.service.bean.AuthorityBean;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/application-context.xml", "classpath:spring/test-context.xml" })
public class AuthorityServicesWebServiceImplTest extends AbstractRestTest {

    /** Authority data service. */
    @Autowired
    private IAuthorityDataService authorityDataService;

    /**
     * Setup.
     */
    @Before
    public void setup() {

        Mockito.reset(this.authorityDataService);

    }

    /** not private server factory. */
    @Autowired
    private JAXRSServerFactoryBean privateRestServer;

    /** {@inheritDoc} */
    @Override
    protected JAXRSServerFactoryBean getRestServer() {
        return this.privateRestServer;
    }

    /**
     * Tests {@link AuthorityServicesWebServiceImpl#findUserRights(String)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testFindAuthoritiesPathAndUserRole() throws Exception {
        // prepare
        final Map<String, List<String>> rights = new HashMap();
        rights.put("GE/Mairies/Mairie78646", Arrays.asList("user", "referent"));

        when(this.authorityDataService.findUserRights("2017-10-USR-XXX-42")).thenReturn(rights);

        // call
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/user/2017-10-USR-XXX-42/rights").get();

        // verify
        verify(this.authorityDataService).findUserRights(eq("2017-10-USR-XXX-42"));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        Map<String, List<String>> actual = this.readAsBean(response, new TypeReference<HashMap<String, List<String>>>() {
        });

        assertThat(actual.get("GE/Mairies/Mairie78646").get(0), equalTo("user"));
        assertThat(actual.get("GE/Mairies/Mairie78646").get(1), equalTo("referent"));
    }

    /**
     * Tests {@link AuthorityServicesWebServiceImpl#findUserRights(String)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testFindUserRightsUserDoesntExist() throws Exception {
        // prepare
        when(this.authorityDataService.findUserRights("12")).thenReturn(new HashMap());

        // call
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/user/12/rights").get();

        // verify
        verify(this.authorityDataService).findUserRights(eq("12"));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        Map<String, List<String>> actual = this.readAsBean(response, new TypeReference<HashMap<String, List<String>>>() {
        });

        assertTrue(actual.isEmpty());
    }

    /**
     * Test an email configuration enabled channel
     * 
     * @throws Exception
     */
    @Test
    public void testComputeActiveChannelEmailConfiguration() throws Exception {

        AuthorityBean authority = new AuthorityBean();

        InputStream resourceAsStream = this.getClass().getResourceAsStream("/jsonFilesTest/emailConfiguration.json");

        String authorityDetailString = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));

        ObjectMapper mapper = new ObjectMapper();
        JsonNode authorityDetails = mapper.readTree(authorityDetailString);

        authority.setDetails(authorityDetails);
        authority.setEntityId("2018-06-VMB-AM7501");
        authority.setLabel("CA DE PARIS");
        authority.setPath("CA/authority");

        when(this.authorityDataService.getByEntityId(Mockito.anyString())).thenReturn(authority);
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/2018-06-VMB-AM7501/channel/active").get();

        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));

        String returnedActifChannel = "{\"entityId\":\"2018-06-VMB-AM7501\",\"label\":\"CA DE PARIS\",\"path\":\"CA/authority\",\"email\":{\"emailAddress\":\"kamal@yopmail.com\",\"state\":\"enabled\"}}";

        byte[] responseAsByte = response.readEntity(byte[].class);

        String returnedJsonAsString = new String(responseAsByte, StandardCharsets.UTF_8);

        assertThat(returnedJsonAsString, equalTo(returnedActifChannel));
    }

    /**
     * 
     * @throws Exception
     */
    @Test
    public void testComputeActiveChannelAllChannel() throws Exception {

        AuthorityBean authority = new AuthorityBean();
        AuthorityBean delegatedAuthority = new AuthorityBean();
        AuthorityBean delegatedLastAuthority = new AuthorityBean();

        InputStream resourceAsStream = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthorityAllChannelActivated.json");
        String authorityDetailString = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));
        ObjectMapper mapper = new ObjectMapper();
        JsonNode authorityDetails = mapper.readTree(authorityDetailString);
        authority.setDetails(authorityDetails);
        authority.setEntityId("2018-06-VMB-AM7501");
        authority.setLabel("CA DE PARIS");
        authority.setPath("CA/authority");

        InputStream resourceAsStreamDelegated = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthoritySecondRedirection.json");
        String authorityDetailStringDelegated = StreamUtils.copyToString(resourceAsStreamDelegated, Charset.forName("UTF-8"));
        JsonNode delegatedAuthorityDetails = mapper.readTree(authorityDetailStringDelegated);
        delegatedAuthority.setDetails(delegatedAuthorityDetails);

        InputStream resourceAsStreamDelegatedLast = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthorityLastRedirection.json");
        String authorityDetailStringDelegatedLast = StreamUtils.copyToString(resourceAsStreamDelegatedLast, Charset.forName("UTF-8"));
        JsonNode delegatedAuthorityDetailsLast = mapper.readTree(authorityDetailStringDelegatedLast);
        delegatedAuthority.setDetails(delegatedAuthorityDetailsLast);

        when(this.authorityDataService.getByEntityId("2018-06-VMB-AM7501")).thenReturn(authority);
        when(this.authorityDataService.getByEntityId("2018-06-VMB-XXXXXX")).thenReturn(delegatedAuthority);
        when(this.authorityDataService.getByEntityId("2018-07-NXF-CX7501X")).thenReturn(delegatedLastAuthority);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/2018-06-VMB-AM7501/channel/active").get();

        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));

        String returnedActifChannel = "{\"entityId\":\"2018-06-VMB-AM7501\",\"label\":\"CA DE PARIS\",\"path\":\"CA/authority\",\"backoffice\":{\"state\":\"enabled\"},\"email\":{\"emailAddress\":\"kamal@yopmail.com\",\"state\":\"enabled\"},\"address\":{\"addressDetail\":{\"recipientName\":\"EL MOURAHIB\",\"recipientNameCompl\":\"Kamal\",\"addressName\":\"129 Rue du Gén de gaulle\",\"addressNameCompl\":\"APPT XXX\",\"cityName\":\"ERMONT\",\"postalCode\":\"95120\"},\"state\":\"enabled\"},\"ftp\":{\"state\":\"enabled\",\"ftpMode\":\"delegatedChannel\",\"pathFolder\":\"/eddie/testPathFolder\",\"comment\":null,\"delegationAuthority\":\"2018-06-VMB-XXXXXX\",\"token\":\"79b7c1ba-543c-4f1a-afda-ca0782f9c00b\"}}";

        byte[] responseAsByte = response.readEntity(byte[].class);

        String returnedJsonAsString = new String(responseAsByte, StandardCharsets.UTF_8);

        assertThat(returnedJsonAsString, equalTo(returnedActifChannel));

    }

    /**
     * Test a backOffice configuration enabled channel
     * 
     * @throws Exception
     */
    @Test
    public void testComputeActiveChannelBackofficeConfiguration() throws Exception {

        AuthorityBean authority = new AuthorityBean();

        InputStream resourceAsStream = this.getClass().getResourceAsStream("/jsonFilesTest/backofficeConfiguration.json");

        String authorityDetailString = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));

        ObjectMapper mapper = new ObjectMapper();
        JsonNode authorityDetails = mapper.readTree(authorityDetailString);

        authority.setDetails(authorityDetails);
        authority.setEntityId("2018-06-VMB-AM7501");
        authority.setLabel("CA DE PARIS");
        authority.setPath("CA/authority");

        when(this.authorityDataService.getByEntityId(Mockito.anyString())).thenReturn(authority);
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/2018-06-VMB-AM7501/channel/active").get();

        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));

        String returnedActifChannel = "{\"entityId\":\"2018-06-VMB-AM7501\",\"label\":\"CA DE PARIS\",\"path\":\"CA/authority\",\"backoffice\":{\"state\":\"enabled\"}}";

        byte[] responseAsByte = response.readEntity(byte[].class);

        String returnedJsonAsString = new String(responseAsByte, StandardCharsets.UTF_8);

        assertThat(returnedJsonAsString, equalTo(returnedActifChannel));
    }

    /**
     * Test an address configuration enabled channel
     * 
     * @throws Exception
     */
    @Test
    public void testComputeActiveChannelAddressConfiguration() throws Exception {

        AuthorityBean authority = new AuthorityBean();
        authority.setEntityId("2018-06-VMB-AM7501");
        authority.setLabel("CA DE PARIS");
        authority.setPath("CA/authority");

        InputStream resourceAsStream = this.getClass().getResourceAsStream("/jsonFilesTest/addressConfiguration.json");

        String authorityDetailString = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));

        ObjectMapper mapper = new ObjectMapper();
        JsonNode authorityDetails = mapper.readTree(authorityDetailString);

        authority.setDetails(authorityDetails);

        when(this.authorityDataService.getByEntityId(Mockito.anyString())).thenReturn(authority);
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/2018-06-VMB-AM7501/channel/active").get();

        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));

        String returnedActifChannel = "{\"entityId\":\"2018-06-VMB-AM7501\",\"label\":\"CA DE PARIS\",\"path\":\"CA/authority\",\"address\":{\"addressDetail\":{\"recipientName\":\"EL MOURAHIB\",\"recipientNameCompl\":\"Kamal\",\"addressName\":\"129 Rue du Gén de gaulle\",\"addressNameCompl\":\"APPT XXX\",\"cityName\":\"ERMONT\",\"postalCode\":\"95120\"},\"state\":\"enabled\"}}";

        byte[] responseAsByte = response.readEntity(byte[].class);

        String returnedJsonAsString = new String(responseAsByte, StandardCharsets.UTF_8);

        assertThat(returnedJsonAsString, equalTo(returnedActifChannel));
    }

    /**
     * Test when the configured authority is null
     * 
     * @throws IOException
     * 
     *
     */
    @Test
    public void testComputeActiveChannelNoAuthority() throws IOException {

        when(this.authorityDataService.getByEntityId("2018-06-VMB-AM7501")).thenReturn(null);
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/2018-06-VMB-AM7501/channel/active").get();

        assertThat(response.getStatus(), equalTo(Status.NOT_FOUND.getStatusCode()));

        byte[] responseAsByte = response.readEntity(byte[].class);

        String errorMessage = new String(responseAsByte, StandardCharsets.UTF_8);

        assertThat(errorMessage, equalTo("No authority exists with the entityId 2018-06-VMB-AM7501"));

    }

    /**
     * Test when the authority been configured got no transfer channel bloc
     * 
     * @throws IOException
     * 
     *
     */
    @Test
    public void testComputeActiveChannelNoTransferChannel() throws IOException {

        AuthorityBean authority = new AuthorityBean();
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/jsonFilesTest/configuredAuthorityNoTransferChannel.json");
        String authorityDetailString = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));
        ObjectMapper mapper = new ObjectMapper();
        JsonNode authorityDetails = mapper.readTree(authorityDetailString);

        authority.setDetails(authorityDetails);

        when(this.authorityDataService.getByEntityId("2018-06-VMB-AM7501")).thenReturn(authority);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/2018-06-VMB-AM7501/channel/active").get();

        assertThat(response.getStatus(), equalTo(Status.NOT_FOUND.getStatusCode()));

        byte[] responseAsByte = response.readEntity(byte[].class);

        String errorMessage = new String(responseAsByte, StandardCharsets.UTF_8);

        assertThat(errorMessage, equalTo("the channels configured are not Activated for the authority that have entityId : 2018-06-VMB-AM7501"));

    }

    /**
     * Test when there is no configuration for any channel
     * 
     * @throws IOException
     * 
     *
     */
    @Test
    public void testComputeActiveChannelNoChannelEnabled() throws IOException {

        AuthorityBean authority = new AuthorityBean();
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/jsonFilesTest/configuredAuthorityNoConfigurationEnabled.json");
        String authorityDetailString = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));
        ObjectMapper mapper = new ObjectMapper();
        JsonNode authorityDetails = mapper.readTree(authorityDetailString);

        authority.setDetails(authorityDetails);

        when(this.authorityDataService.getByEntityId("2018-06-VMB-AM7501")).thenReturn(authority);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/2018-06-VMB-AM7501/channel/active").get();

        assertThat(response.getStatus(), equalTo(Status.NOT_FOUND.getStatusCode()));

        byte[] responseAsByte = response.readEntity(byte[].class);

        String errorMessage = new String(responseAsByte, StandardCharsets.UTF_8);

        assertThat(errorMessage, equalTo("the channels configured are not Activated for the authority that have entityId : 2018-06-VMB-AM7501"));

    }

    /**
     * Test when there is no entityId for a delegated channel
     * 
     * @throws IOException
     * 
     *
     */
    @Test
    public void testComputeActiveChannelNoDelegatedEntityId() throws IOException {

        AuthorityBean authority = new AuthorityBean();
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthorityNoEntityId.json");
        String authorityDetailString = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));
        ObjectMapper mapper = new ObjectMapper();
        JsonNode authorityDetails = mapper.readTree(authorityDetailString);

        authority.setDetails(authorityDetails);

        when(this.authorityDataService.getByEntityId("2018-06-VMB-AM7501")).thenReturn(authority);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/2018-06-VMB-AM7501/channel/active").get();

        assertThat(response.getStatus(), equalTo(Status.NOT_FOUND.getStatusCode()));

        byte[] responseAsByte = response.readEntity(byte[].class);

        String errorMessage = new String(responseAsByte, StandardCharsets.UTF_8);

        assertThat(errorMessage, equalTo("No delegated authority entityID found in the ftp configuration for the authority with the entityId 2018-06-VMB-AM7501."));

    }

    /**
     * Test when the delegated authority given in the ftp configuration is null
     * 
     * @throws IOException
     * 
     *
     */
    @Test
    public void testComputeActiveChannelNoDelegatedAuthority() throws IOException {

        AuthorityBean authority = new AuthorityBean();
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthority.json");
        String authorityDetailString = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));
        ObjectMapper mapper = new ObjectMapper();
        JsonNode authorityDetails = mapper.readTree(authorityDetailString);

        authority.setDetails(authorityDetails);

        when(this.authorityDataService.getByEntityId("2018-06-VMB-AM7501")).thenReturn(authority);
        when(this.authorityDataService.getByEntityId("2018-06-VMB-XXXXXX")).thenReturn(null);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/2018-06-VMB-AM7501/channel/active").get();

        assertThat(response.getStatus(), equalTo(Status.NOT_FOUND.getStatusCode()));

        byte[] responseAsByte = response.readEntity(byte[].class);

        String errorMessage = new String(responseAsByte, StandardCharsets.UTF_8);

        assertThat(errorMessage, equalTo("No delegated authority exists with the entityId 2018-06-VMB-XXXXXX"));

    }

    /**
     * Test when the delegated authority's ftp configuration is not enabled
     * 
     * @throws IOException
     * 
     *
     */
    @Test
    public void testComputeActiveChannelDisabledFtpDelegatedAuthority() throws IOException {

        AuthorityBean authority = new AuthorityBean();
        AuthorityBean delegatedAuthority = new AuthorityBean();

        InputStream resourceAsStream = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthority.json");
        String authorityDetailString = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));
        ObjectMapper mapper = new ObjectMapper();
        JsonNode authorityDetails = mapper.readTree(authorityDetailString);
        authority.setDetails(authorityDetails);

        InputStream resourceAsStreamDelegated = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthorityNotEnabled.json");
        String authorityDetailStringDelegated = StreamUtils.copyToString(resourceAsStreamDelegated, Charset.forName("UTF-8"));
        ObjectMapper mapperDelegated = new ObjectMapper();
        JsonNode delegatedAuthorityDetails = mapperDelegated.readTree(authorityDetailStringDelegated);
        delegatedAuthority.setDetails(delegatedAuthorityDetails);

        when(this.authorityDataService.getByEntityId("2018-06-VMB-AM7501")).thenReturn(authority);
        when(this.authorityDataService.getByEntityId("2018-06-VMB-XXXXXX")).thenReturn(delegatedAuthority);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/2018-06-VMB-AM7501/channel/active").get();

        assertThat(response.getStatus(), equalTo(Status.NOT_FOUND.getStatusCode()));

        byte[] responseAsByte = response.readEntity(byte[].class);

        String errorMessage = new String(responseAsByte, StandardCharsets.UTF_8);

        assertThat(errorMessage, equalTo("The delegated Authority that have the entityId 2018-06-VMB-XXXXXX, have no Ftp channel enabled."));

    }

    /**
     * Test when the delegated authority'shave no transferChannel bloc
     * 
     * @throws IOException
     * 
     *
     */
    @Test
    public void testComputeActiveChannelDisabledNoConfigurationDelegatedAuthority() throws IOException {

        AuthorityBean authority = new AuthorityBean();
        AuthorityBean delegatedAuthority = new AuthorityBean();

        InputStream resourceAsStream = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthority.json");
        String authorityDetailString = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));
        ObjectMapper mapper = new ObjectMapper();
        JsonNode authorityDetails = mapper.readTree(authorityDetailString);
        authority.setDetails(authorityDetails);

        InputStream resourceAsStreamDelegated = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthorityNoTransferChannel.json");
        String authorityDetailStringDelegated = StreamUtils.copyToString(resourceAsStreamDelegated, Charset.forName("UTF-8"));
        ObjectMapper mapperDelegated = new ObjectMapper();
        JsonNode delegatedAuthorityDetails = mapperDelegated.readTree(authorityDetailStringDelegated);
        delegatedAuthority.setDetails(delegatedAuthorityDetails);

        when(this.authorityDataService.getByEntityId("2018-06-VMB-AM7501")).thenReturn(authority);
        when(this.authorityDataService.getByEntityId("2018-06-VMB-XXXXXX")).thenReturn(delegatedAuthority);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/2018-06-VMB-AM7501/channel/active").get();
        assertThat(response.getStatus(), equalTo(Status.NOT_FOUND.getStatusCode()));

        byte[] responseAsByte = response.readEntity(byte[].class);

        String errorMessage = new String(responseAsByte, StandardCharsets.UTF_8);

        assertThat(errorMessage, equalTo("The delegated Authority that have the entityId 2018-06-VMB-XXXXXX, have no configuration set."));

    }

    /**
     * Test when the delegated authority have the same id as the configured
     * authority
     * 
     * @throws IOException
     * 
     *
     */
    @Test
    public void testComputeActiveChannelLoopDelegatedAuthority() throws IOException {

        AuthorityBean authority = new AuthorityBean();
        AuthorityBean delegatedAuthority = new AuthorityBean();

        InputStream resourceAsStream = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthority.json");
        String authorityDetailString = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));
        ObjectMapper mapper = new ObjectMapper();
        JsonNode authorityDetails = mapper.readTree(authorityDetailString);
        authority.setDetails(authorityDetails);

        InputStream resourceAsStreamDelegated = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthorityLoop.json");
        String authorityDetailStringDelegated = StreamUtils.copyToString(resourceAsStreamDelegated, Charset.forName("UTF-8"));
        ObjectMapper mapperDelegated = new ObjectMapper();
        JsonNode delegatedAuthorityDetails = mapperDelegated.readTree(authorityDetailStringDelegated);
        delegatedAuthority.setDetails(delegatedAuthorityDetails);

        when(this.authorityDataService.getByEntityId("2018-06-VMB-AM7501")).thenReturn(authority);
        when(this.authorityDataService.getByEntityId("2018-06-VMB-XXXXXX")).thenReturn(delegatedAuthority);
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/2018-06-VMB-AM7501/channel/active").get();

        assertThat(response.getStatus(), equalTo(Status.NOT_FOUND.getStatusCode()));

        byte[] responseAsByte = response.readEntity(byte[].class);

        String errorMessage = new String(responseAsByte, StandardCharsets.UTF_8);

        assertThat(errorMessage,
                equalTo("The delegated Authority that have the entityId 2018-06-VMB-AM7501, have the same entityId of an authority that was referred as a delegation authority which is not correct."));

    }

    /**
     * Test when all is OK and returns the actif ftp channel of a delegated
     * authority authority that have pathFolder and token equal to "" (empty)
     * 
     * @throws IOException
     * 
     *
     */
    @Test
    public void testComputeActiveChannelDelegatedAuthorityOkWithEmpty() throws IOException {

        AuthorityBean authority = new AuthorityBean();
        AuthorityBean delegatedAuthority = new AuthorityBean();
        AuthorityBean delegatedLastAuthority = new AuthorityBean();

        InputStream resourceAsStream = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthority.json");
        String authorityDetailString = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));
        ObjectMapper mapper = new ObjectMapper();
        JsonNode authorityDetails = mapper.readTree(authorityDetailString);
        authority.setDetails(authorityDetails);
        authority.setEntityId("2018-06-VMB-AM7501");
        authority.setLabel("CA DE PARIS");
        authority.setPath("CA/authority");

        InputStream resourceAsStreamDelegated = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthoritySecondRedirection.json");
        String authorityDetailStringDelegated = StreamUtils.copyToString(resourceAsStreamDelegated, Charset.forName("UTF-8"));
        JsonNode delegatedAuthorityDetails = mapper.readTree(authorityDetailStringDelegated);
        delegatedAuthority.setDetails(delegatedAuthorityDetails);

        InputStream resourceAsStreamDelegatedLast = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthorityEmptyLastRedirection.json");
        String authorityDetailStringDelegatedLast = StreamUtils.copyToString(resourceAsStreamDelegatedLast, Charset.forName("UTF-8"));
        JsonNode delegatedAuthorityDetailsLast = mapper.readTree(authorityDetailStringDelegatedLast);
        delegatedAuthority.setDetails(delegatedAuthorityDetailsLast);

        when(this.authorityDataService.getByEntityId("2018-06-VMB-AM7501")).thenReturn(authority);
        when(this.authorityDataService.getByEntityId("2018-06-VMB-XXXXXX")).thenReturn(delegatedAuthority);
        when(this.authorityDataService.getByEntityId("2018-07-NXF-CX7501X")).thenReturn(delegatedLastAuthority);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/2018-06-VMB-AM7501/channel/active").get();

        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));

        String returnedActifChannel = "{\"entityId\":\"2018-06-VMB-AM7501\",\"label\":\"CA DE PARIS\",\"path\":\"CA/authority\",\"ftp\":{\"state\":\"enabled\",\"ftpMode\":\"delegatedChannel\",\"pathFolder\":\"\",\"comment\":null,\"delegationAuthority\":\"2018-06-VMB-XXXXXX\",\"token\":\"\"}}";

        byte[] responseAsByte = response.readEntity(byte[].class);

        String returnedJsonAsString = new String(responseAsByte, StandardCharsets.UTF_8);

        assertThat(returnedJsonAsString, equalTo(returnedActifChannel));
    }

    /**
     * Test when all is OK and returns the actif ftp channel of a delegated
     * authority authority that have pathFolder and token equal to NULL
     * 
     * @throws IOException
     * 
     *
     */
    @Test
    public void testComputeActiveChannelDelegatedAuthorityOkWithNull() throws IOException {

        AuthorityBean authority = new AuthorityBean();
        AuthorityBean delegatedAuthority = new AuthorityBean();
        AuthorityBean delegatedLastAuthority = new AuthorityBean();

        InputStream resourceAsStream = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthority.json");
        String authorityDetailString = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));
        ObjectMapper mapper = new ObjectMapper();
        JsonNode authorityDetails = mapper.readTree(authorityDetailString);
        authority.setDetails(authorityDetails);
        authority.setEntityId("2018-06-VMB-AM7501");
        authority.setLabel("CA DE PARIS");
        authority.setPath("CA/authority");

        InputStream resourceAsStreamDelegated = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthoritySecondRedirection.json");
        String authorityDetailStringDelegated = StreamUtils.copyToString(resourceAsStreamDelegated, Charset.forName("UTF-8"));
        JsonNode delegatedAuthorityDetails = mapper.readTree(authorityDetailStringDelegated);
        delegatedAuthority.setDetails(delegatedAuthorityDetails);

        InputStream resourceAsStreamDelegatedLast = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthorityNullLastRedirection.json");
        String authorityDetailStringDelegatedLast = StreamUtils.copyToString(resourceAsStreamDelegatedLast, Charset.forName("UTF-8"));
        JsonNode delegatedAuthorityDetailsLast = mapper.readTree(authorityDetailStringDelegatedLast);
        delegatedAuthority.setDetails(delegatedAuthorityDetailsLast);

        when(this.authorityDataService.getByEntityId("2018-06-VMB-AM7501")).thenReturn(authority);
        when(this.authorityDataService.getByEntityId("2018-06-VMB-XXXXXX")).thenReturn(delegatedAuthority);
        when(this.authorityDataService.getByEntityId("2018-07-NXF-CX7501X")).thenReturn(delegatedLastAuthority);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/2018-06-VMB-AM7501/channel/active").get();

        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));

        String returnedActifChannel = "{\"entityId\":\"2018-06-VMB-AM7501\",\"label\":\"CA DE PARIS\",\"path\":\"CA/authority\",\"ftp\":{\"state\":\"enabled\",\"ftpMode\":\"delegatedChannel\",\"pathFolder\":null,\"comment\":null,\"delegationAuthority\":\"2018-06-VMB-XXXXXX\",\"token\":null}}";

        byte[] responseAsByte = response.readEntity(byte[].class);

        String returnedJsonAsString = new String(responseAsByte, StandardCharsets.UTF_8);

        assertThat(returnedJsonAsString, equalTo(returnedActifChannel));
    }

    /**
     * Test when all is OK and returns the actif ftp channel of a delegated
     * authority authority
     * 
     * @throws IOException
     * 
     *
     */
    @Test
    public void testComputeActiveChannelDelegatedAuthorityOK() throws IOException {

        AuthorityBean authority = new AuthorityBean();
        AuthorityBean delegatedAuthority = new AuthorityBean();
        AuthorityBean delegatedLastAuthority = new AuthorityBean();

        InputStream resourceAsStream = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthority.json");
        String authorityDetailString = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));
        ObjectMapper mapper = new ObjectMapper();
        JsonNode authorityDetails = mapper.readTree(authorityDetailString);
        authority.setDetails(authorityDetails);

        authority.setEntityId("2018-06-VMB-AM7501");
        authority.setLabel("CA DE PARIS");
        authority.setPath("CA/authority");

        InputStream resourceAsStreamDelegated = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthoritySecondRedirection.json");
        String authorityDetailStringDelegated = StreamUtils.copyToString(resourceAsStreamDelegated, Charset.forName("UTF-8"));
        JsonNode delegatedAuthorityDetails = mapper.readTree(authorityDetailStringDelegated);
        delegatedAuthority.setDetails(delegatedAuthorityDetails);

        InputStream resourceAsStreamDelegatedLast = this.getClass().getResourceAsStream("/jsonFilesTest/delegationAuthorityLastRedirection.json");
        String authorityDetailStringDelegatedLast = StreamUtils.copyToString(resourceAsStreamDelegatedLast, Charset.forName("UTF-8"));
        JsonNode delegatedAuthorityDetailsLast = mapper.readTree(authorityDetailStringDelegatedLast);
        delegatedAuthority.setDetails(delegatedAuthorityDetailsLast);

        when(this.authorityDataService.getByEntityId("2018-06-VMB-AM7501")).thenReturn(authority);
        when(this.authorityDataService.getByEntityId("2018-06-VMB-XXXXXX")).thenReturn(delegatedAuthority);
        when(this.authorityDataService.getByEntityId("2018-07-NXF-CX7501X")).thenReturn(delegatedLastAuthority);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/services/authority/2018-06-VMB-AM7501/channel/active").get();

        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));

        String returnedActifChannel = "{\"entityId\":\"2018-06-VMB-AM7501\",\"label\":\"CA DE PARIS\",\"path\":\"CA/authority\",\"ftp\":{\"state\":\"enabled\",\"ftpMode\":\"delegatedChannel\",\"pathFolder\":\"/eddie/testPathFolder\",\"comment\":null,\"delegationAuthority\":\"2018-06-VMB-XXXXXX\",\"token\":\"79b7c1ba-543c-4f1a-afda-ca0782f9c00b\"}}";

        byte[] responseAsByte = response.readEntity(byte[].class);

        String returnedJsonAsString = new String(responseAsByte, StandardCharsets.UTF_8);

        assertThat(returnedJsonAsString, equalTo(returnedActifChannel));
    }

}
