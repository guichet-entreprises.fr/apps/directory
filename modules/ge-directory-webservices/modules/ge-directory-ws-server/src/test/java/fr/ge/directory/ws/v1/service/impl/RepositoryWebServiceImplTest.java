package fr.ge.directory.ws.v1.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.ibatis.session.RowBounds;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.query.sql.QueryBuilder;
import fr.ge.directory.service.mapper.RepositoryDaoMapper;
import fr.ge.directory.ws.v1.model.RepositoryElement;

/**
 * Test {@link RepositoryWebServiceImpl}.
 * 
 * @author mtakerra
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/application-context.xml", "classpath:spring/test-context.xml" })
public class RepositoryWebServiceImplTest extends AbstractRestTest {

    @Autowired
    private RepositoryDaoMapper mapper;

    /** not private server factory. */
    @Autowired
    private JAXRSServerFactoryBean privateRestServer;

    /** {@inheritDoc} */
    protected JAXRSServerFactoryBean getRestServer() {
        return this.privateRestServer;
    }

    /**
     * Setup.
     */
    @Before
    public void setup() {

        Mockito.reset(this.mapper);

    }

    @Test
    public void testAddElement() throws Exception {
        String repositoryId = "organisme_conventionne";
        RepositoryElement element = new RepositoryElement();
        element.setEntityId("N150");
        element.setLabel("organisme1");

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/repository/organisme_conventionne").post(element);

        final ArgumentCaptor<RepositoryElement> captorElement = ArgumentCaptor.forClass(RepositoryElement.class);

        verify(this.mapper).create(eq(repositoryId), captorElement.capture());
        RepositoryElement capturedElemet = captorElement.getValue();
        assertThat(capturedElemet.getEntityId(), equalTo(element.getEntityId()));
        assertThat(capturedElemet.getLabel(), equalTo(element.getLabel()));

        // verify
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
    }

    @Test
    public void testAddListOfElements() throws Exception {
        String repositoryId = "organisme_conventionne";
        RepositoryElement element1 = new RepositoryElement();
        element1.setEntityId("N150");
        element1.setLabel("organisme1");
        RepositoryElement element2 = new RepositoryElement();
        element2.setEntityId("N151");
        element2.setLabel("organisme2");
        List<RepositoryElement> listElements = new ArrayList<RepositoryElement>();

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/repository/organisme_conventionne").replaceQueryParam("reset", "true").put(listElements);

        verify(this.mapper).delete(eq(repositoryId));

        // verify
        assertThat(response.getStatus(), equalTo(Status.NO_CONTENT.getStatusCode()));
    }

    @Test
    public void testReadElement() throws Exception {
        String repositoryId = "organisme_conventionne";
        RepositoryElement element = new RepositoryElement();
        element.setId(1L);
        element.setEntityId("N150");
        element.setLabel("organisme1");

        when(this.mapper.read(repositoryId, element.getId())).thenReturn(element);
        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/repository/organisme_conventionne/1").get();

        verify(this.mapper).read(eq(repositoryId), eq(element.getId()));

        // verify
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
    }

    @Test
    public void testSearchElements() throws Exception {
        String repositoryId = "organisme_conventionne";
        RepositoryElement element1 = new RepositoryElement();
        element1.setEntityId("N150");
        element1.setLabel("organisme1");
        RepositoryElement element2 = new RepositoryElement();
        element2.setEntityId("N151");
        element2.setLabel("organisme2");
        List<RepositoryElement> listElements = new ArrayList<RepositoryElement>();

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/repository/organisme_conventionne").get();

        final ArgumentCaptor<RowBounds> captorRow = ArgumentCaptor.forClass(RowBounds.class);
        final ArgumentCaptor<QueryBuilder> captorQuery = ArgumentCaptor.forClass(QueryBuilder.class);

        verify(this.mapper).search(eq(repositoryId), captorQuery.capture(), captorRow.capture());

        // verify
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
    }

    @Test
    public void testUpdate() throws Exception {

        RepositoryElement element = new RepositoryElement();

        Mockito.doNothing().when(this.mapper).update(Mockito.anyString(), Mockito.any(RepositoryElement.class));

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/repository/invitations/15").put(element);

        // verify
        assertThat(response.getStatus(), equalTo(Status.NO_CONTENT.getStatusCode()));

    }

    @Test
    public void testListRepositories() throws Exception {
        List<String> listRepositories = new ArrayList<>();
        listRepositories.add("IRCS001");
        listRepositories.add("IRSEI01");
        listRepositories.add("NRCDP01");
        listRepositories.add("DDMEI01");
        listRepositories.add("IRCSB08");
        listRepositories.add("IRCS002");

        when(this.mapper.listRepositories()).thenReturn(listRepositories);

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/repository/repositories").get();

        // verify
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));

    }
}
