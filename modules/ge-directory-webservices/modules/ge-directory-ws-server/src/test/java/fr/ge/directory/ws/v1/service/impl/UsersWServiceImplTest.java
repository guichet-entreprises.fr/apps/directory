package fr.ge.directory.ws.v1.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import fr.ge.directory.service.IUsersService;
import fr.ge.directory.ws.bean.LegacyAuthorityUserLink;
import fr.ge.directory.ws.constant.enumeration.LegacyUserRoleEnum;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/service-context.xml", "classpath:spring/application-context.xml", "classpath:spring/test-context.xml" })
public class UsersWServiceImplTest extends AbstractRestTest {

    @Autowired
    private IUsersService usersService;

    @Test
    public void testImportUsers() throws Exception {
        InputStream csvFileInputStream = this.getClass().getClassLoader().getResourceAsStream("users/oneUser.csv");
        List<LegacyAuthorityUserLink> listUsersToImport = new ArrayList<LegacyAuthorityUserLink>();
        // Mock the user that is gotten from the csv file
        LegacyAuthorityUserLink userLevel1 = new LegacyAuthorityUserLink();
        userLevel1.setLvl1Code("GE");
        userLevel1.setLvl2Code("");
        userLevel1.setLvl3Code("");
        userLevel1.setLvl3Label("");
        userLevel1.setUserEmail("pro.loic@yopmail.com");
        userLevel1.setUserFirstName("Loic");
        userLevel1.setUserId("1");
        userLevel1.setUserLastName("ABEMONTY");
        userLevel1.setUserRole(LegacyUserRoleEnum.ADMINISTRATEUR);

        listUsersToImport.add(userLevel1);

        // Mock the calls to the userService
        when(this.usersService.getUser(Mockito.any(String[].class), Mockito.any(HashMap.class))).thenReturn(userLevel1);
        when(this.usersService.importUsers(Mockito.any(List.class), Mockito.any(boolean.class), Mockito.any(String.class))).thenReturn("1");

        // Call the WS using it's Path "/v1/import/users"
        Response response = this.client()//
                .type(MediaType.APPLICATION_OCTET_STREAM)//
                .path("/v1/import/users")//
                .query("dryRun", true)//
                .post(csvFileInputStream);

        // Assert the response is OK & it returned one line processed
        assertThat(response.getStatus()).isEqualTo(Status.OK.getStatusCode());
        assertThat(response.readEntity(String.class)).isEqualTo("1");

    }

    @Test
    public void testImportUsersKo() throws TechniqueException, FunctionalException {
        InputStream csvFileInputStream = this.getClass().getClassLoader().getResourceAsStream("users/oneUser.csv");
        List<LegacyAuthorityUserLink> listUsersToImport = new ArrayList<LegacyAuthorityUserLink>();
        // Mock the user that is gotten from the csv file
        LegacyAuthorityUserLink userLevel1 = new LegacyAuthorityUserLink();
        userLevel1.setLvl1Code("GE");
        userLevel1.setLvl2Code("");
        userLevel1.setLvl3Code("");
        userLevel1.setLvl3Label("");
        userLevel1.setUserEmail("pro.loic@yopmail.com");
        userLevel1.setUserFirstName("Loic");
        userLevel1.setUserId("1");
        userLevel1.setUserLastName("ABEMONTY");
        userLevel1.setUserRole(LegacyUserRoleEnum.ADMINISTRATEUR);

        listUsersToImport.add(userLevel1);
        // Mock the calls to the userService
        when(this.usersService.getUser(Mockito.any(String[].class), Mockito.any(HashMap.class))).thenReturn(userLevel1);
        when(this.usersService.importUsers(Mockito.any(List.class), Mockito.any(boolean.class), Mockito.any(String.class))).thenThrow(TechnicalException.class);

        // Call the WS using it's Path "/v1/import/users"
        Response response = this.client()//
                .type(MediaType.APPLICATION_OCTET_STREAM)//
                .path("/v1/import/users")//
                .query("dryRun", true)//
                .post(csvFileInputStream);

        assertThat(response.getStatus()).isEqualTo(Status.INTERNAL_SERVER_ERROR.getStatusCode());
    }
}
