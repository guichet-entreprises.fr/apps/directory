package fr.ge.directory.ws.v2.service.impl;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.IReferentialItemDataService;
import fr.ge.directory.service.bean.ReferentialItemBean;
import fr.ge.directory.service.bean.ReferentialTableEnum;
import fr.ge.directory.service.impl.RepositoryService;
import fr.ge.directory.ws.v1.model.RepositoryElement;

/**
 * Class Test of {@link RefServiceImpl}.
 * 
 * @author $Author: ijijon $
 * @version $Revision: 0 $
 */

public class RefServiceImplTest {

    @Mock
    private IReferentialItemDataService referentialItemDataService;

    @InjectMocks
    private RefServiceImpl refServiceImpl;

    /** The repository service. */
    @Mock
    private RepositoryService repositoryService;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSave() throws Exception {

        // Prepare mock
        doNothing().when(referentialItemDataService).save("refTableName", null);
        // Execute
        refServiceImpl.save("refTableName", null);
        // Verify
        verify(this.referentialItemDataService).save("refTableName", null);
    }

    @Test
    public void testSearch() throws Exception {

        // Prepare mock
        ReferentialItemBean rib = new ReferentialItemBean();
        rib.setLabel("label");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.createObjectNode();
        rib.setDetails(node);
        List<ReferentialItemBean> resultReferantial = new ArrayList<ReferentialItemBean>();
        resultReferantial.add(rib);

        when(referentialItemDataService.find(ReferentialTableEnum.findTechnicalTable("refTableName"), "searchedValue")).thenReturn(resultReferantial);

        // Execute
        refServiceImpl.search("refTableName", "searchedValue");

        // Verify
        verify(this.referentialItemDataService).find(ReferentialTableEnum.findTechnicalTable("refTableName"), "searchedValue");
    }

    @Test
    public void testTruncate() throws Exception {

        // Prepare mock
        doNothing().when(referentialItemDataService).truncate(ReferentialTableEnum.findTechnicalTable("refTableName"));

        // Execute
        refServiceImpl.truncate("refTableName");

        // Verify
        verify(this.referentialItemDataService).truncate(ReferentialTableEnum.findTechnicalTable("refTableName"));
    }

    @Test
    public void testFindAll() throws Exception {
        // Prepare mock
        final SearchQuery searchQuery = new SearchQuery(0, 1).setFilters(null).setOrders(null).setSearchTerms("searchTerms");
        when(this.repositoryService.search("refTableName", searchQuery)).thenReturn(new SearchResult<RepositoryElement>());

        // Execute
        refServiceImpl.findAll("refTableName", 0, 1, null, null, "searchTerms");

        // Verify
        verify(this.repositoryService).search("refTableName", searchQuery);
    }

    @Test
    public void testGet() throws Exception {
        // Prepare mock
        ReferentialItemBean ref = new ReferentialItemBean();
        when(referentialItemDataService.get(ReferentialTableEnum.findTechnicalTable("refTableName"), 0L)).thenReturn(ref);

        // Execute
        refServiceImpl.get(0L, "refTableName");

        // Verify
        verify(this.referentialItemDataService).get(ReferentialTableEnum.findTechnicalTable("refTableName"), 0L);
    }

}
