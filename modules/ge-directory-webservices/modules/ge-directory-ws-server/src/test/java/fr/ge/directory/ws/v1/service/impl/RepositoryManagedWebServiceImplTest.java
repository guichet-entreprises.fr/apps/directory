package fr.ge.directory.ws.v1.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.test.AbstractRestTest;
import fr.ge.directory.service.RepositoryInternalService;
import fr.ge.directory.ws.v1.model.RepositoryElement;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/application-context.xml", "classpath:spring/test-context.xml", "classpath:spring/service-context.xml" })
public class RepositoryManagedWebServiceImplTest extends AbstractRestTest {

    @Autowired
    @Qualifier("managedRestServer")
    private JAXRSServerFactoryBean restServerFactory;

    /** The repository service. */
    @Autowired
    private RepositoryInternalService repositoryInternalService;

    @Override
    protected JAXRSServerFactoryBean getRestServerFactory() {
        return this.restServerFactory;
    }

    /**
     * {@inheritDoc}
     */
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        reset(this.repositoryInternalService);
    }

    /**
     * Test managed search repository service.
     *
     * @throws Exception
     */
    @Test
    public void testSearchRepository() throws Exception {
        // prepare
        RepositoryElement element = new RepositoryElement();
        element.setEntityId("E1");
        element.setLabel("L1");

        final SearchResult<RepositoryElement> result = new SearchResult<RepositoryElement>(0L, 20L);
        result.setContent(Arrays.asList(element));
        result.setTotalResults(1L);

        when(this.repositoryInternalService.search("frais", 0L, 20L, Arrays.asList(new SearchQueryFilter("details.entityId:test")), Arrays.asList(new SearchQueryOrder("created:desc"))))
                .thenReturn(result);

        // call
        final Response response = this.client().path("/v1/repository/{repositoryId}", "frais") //
                .query("filters", "details.entityId:test") //
                .query("orders", "created:desc") //
                .get();

        // verify
        verify(this.repositoryInternalService).search(eq("frais"), eq(0L), eq(20L), eq(Arrays.asList(new SearchQueryFilter("details.entityId:test"))),
                eq(Arrays.asList(new SearchQueryOrder("created:desc"))));

        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
    }
}
