package fr.ge.directory.service.authority;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.directory.service.IAuthorityDataService;
import fr.ge.directory.service.IReferentialItemDataService;
import fr.ge.directory.service.bean.AuthorityBean;
import fr.ge.tracker.facade.ITrackerFacade;

/**
 * Specialization for Commune type authority.
 * 
 * @author $Author: LABEMONT $
 */
class AuthorityCommuneImporter extends AuthorityImporter {
    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorityCommuneImporter.class);

    /** Json Mapper to create Authority's details. */
    private ObjectMapper objectMapper = new ObjectMapper();

    /** AuthorityImport utilisé pour faire une recherche de CFE. */
    private AuthorityImporter authorityImportForSearchPurposes;

    /**
     * Constructeur de la classe.
     *
     * @param AuthorityCsvIterator
     *            the iterator over the input data using a custom unmarshaller
     * @param authorityDataService
     *            services CRUD des autorités
     * @param referentialItemDataService
     *            service de recherche
     * @param trackerFacade
     *            service de création d'un identifiant unique
     */
    public AuthorityCommuneImporter(AuthorityCsvIterator authorityIterator, IAuthorityDataService authorityDataService, IReferentialItemDataService referentialItemDataService,
            ITrackerFacade trackerFacade) {
        super(authorityIterator, authorityDataService, referentialItemDataService, trackerFacade);
    }

    /**
     * {@inheritDoc}<br/>
     * Overridden to use codeCommune
     */
    @Override
    protected SearchQuery findByUniqueCriteria(AuthorityBean authority) {
        return new AuthorityCommuneSearchFilterBuilder(authority).filterByFunctionalCode().build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected AuthorityBean merge(AuthorityBean authority, AuthorityBean storedAuthority) {

        if (storedAuthority == null) {
            // pas de autorité originelle, on retourne la nouveauté
            return authority;
        }
        storedAuthority.setDetails(authority.getDetails());
        if (StringUtils.isNoneBlank(authority.getLabel())) {
            storedAuthority.setLabel(authority.getLabel());
        }

        return storedAuthority;
    }

    /**
     * The codeCommune has to be present, the referenced authorities are
     * searched for and liked to the element.
     * 
     * @param the
     *            authority to create, not null
     * @return the check element, completed with the linked authorities
     * @throws TechnicalException
     *             if the treatment can't be finished
     */
    @Override
    protected AuthorityBean functionalControl(AuthorityBean authority) throws TechnicalException {

        // Création de l'entityId si besoin
        if (StringUtils.isBlank(authority.getEntityId())) {
            // positionnement de l'entityId d'après les données du détail
            try {
                if (authority.getDetails() == null || authority.getDetails().get("codeCommune") == null || StringUtils.isBlank(authority.getDetails().get("codeCommune").asText())) {
                    throw new TechnicalException(String.format("Unable to create a unique id without a commune code (%s).", authority.getLabel()));
                }
                // construction d'un id avec comme base le code EDI
                String baseId = authority.getDetails().get("codeCommune").asText();
                authority.setEntityId(baseId);
                LOGGER.info(String.format("Set entityId to %s for %s.", baseId, authority.getLabel()));
            } catch (Exception e) {
                String msg = String.format("Error occured when calling Tracker to generate Uid for authority %s : %s.", authority.getLabel(), e.getMessage());
                LOGGER.error(msg);
                throw new TechnicalException(msg, e);
            }
        }
        // TODO rechercher les autorités de chaque domaine
        Iterator<Entry<String, JsonNode>> cfeIds = authority.getDetails().fields();
        // Map<String, String> cfeFunctionalIds = new HashMap<>();
        List<String> cfeEntries = Arrays.asList("CCI", "CA", "GREFFE", "URSSAF", "CMA", "CNBA");
        while (cfeIds.hasNext()) {
            Entry<String, JsonNode> entry = cfeIds.next();
            if (cfeEntries.contains(entry.getKey())) {
                AuthorityBean cfeAuthority = searchAuthority(entry.getValue());
                if (cfeAuthority != null) {
                    ((ObjectNode) authority.getDetails()).put(entry.getKey(), cfeAuthority.getEntityId());
                    LOGGER.debug("Updated for {} : field {} = {}.", authority.getEntityId(), entry.getKey(), cfeAuthority.getEntityId());
                    // cfeFunctionalIds.put(entry.getKey(),
                    // cfeAuthority.getEntityId());
                }
            }
        }

        LOGGER.warn("TODO rechercher les autorités de chaque domaine pour {}.", authority.getEntityId());
        return authority;
    }

    /**
     * Search an Authority based on a JsonNode value.
     * 
     * @param jsonNode
     *            the json value
     * @return null if not found
     */
    private AuthorityBean searchAuthority(JsonNode jsonNode) {
        AuthorityBean searchAuthority = null;
        if (jsonNode != null) {
            try {
                searchAuthority = new AuthorityBean();
                searchAuthority.setDetails(objectMapper.readTree(String.format("{\"ediCode\":\"%s\"}", jsonNode.asText())));
                searchAuthority = getImportForSearchPurposes().findOriginalItem(searchAuthority);
            } catch (IOException e) {
                String msg = String.format("Unable to search for authority %s : %s.", jsonNode, e.getMessage());
                LOGGER.error(msg);
                throw new TechnicalException(msg, e);
            }
        }
        return searchAuthority;
    }

    /**
     * Method to create the Importer after the setup. Lazyness.
     * 
     * @return the AuthorityImport, CFE level.
     */
    private AuthorityImporter getImportForSearchPurposes() {
        if (this.authorityImportForSearchPurposes == null) {
            this.authorityImportForSearchPurposes = new AuthorityImporter(null, authorityDataService, referentialItemDataService, trackerFacade);
        }
        return this.authorityImportForSearchPurposes;
    }

}
