/**
 *
 */
package fr.ge.directory.ws.constant.enumeration;

/**
 * Codes of the level 2 structure for legacy authorities.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public enum LegacyAuthorityLvl2CodeEnum {

    /** Agricole. */
    AGRICOLE("CA", ""),

    /** Artisanal. */
    ARTISANAL("CMA", ""),

    /** Commercial. */
    COMMERCIAL("CCI", ""),

    /** Fluvial. */
    FLUVIAL("FLUVIAL", ""),

    /** URSSAF. */
    URSSAF("URSSAF", ""),

    /** Greffe. */
    GREFFE("GREFFE", ""),

    /**
     * œuvres d'écrivains, de photographes, illustrateurs, auteurs multi
     * média,…(AGESSA).
     */
    AGESSA("AGESSA", ""),

    /** Maison des artistes. */
    ARTIST("ARTIST", ""),

    /**
     * Commission d'agrément des contrôleurs techniques (Ministère de
     * l'écologie).
     */
    CACT("CACT", ""),

    /**
     * Chambre de commerce et de l'industrie (CCI) ou Président de Chambre de
     * commerce et de l'industrie.
     */
    CCI("CCI", ""),

    /**
     * Commission départementale d'aménagement commercial (CDAC) (Préfecture).
     */
    CDAC("CDAC", ""),

    /**
     * Chambre des Métiers et de l'Artisanat (CMA) ou Président de la Chambre de
     * métiers et de l'artisanat.
     */
    CMA("CMA", ""),

    /**
     * Conseil national de l'expertise foncière, agricole et forestière
     * (CNEFAF).
     */
    CNEFAF("CNEFAF", ""),

    /** Conseil régional de l'ordre des Architectes. */
    CROA("CROA", ""),

    /** Conseil régional de l'ordre des experts-comptables. */
    CROEC("CROEC", ""),

    /** Conseil régional de l'ordre des géomètres-experts. */
    CROGE("CROGE", ""),

    /** Conseil régional de l'ordre des Architectes. */
    CROV("CROV", ""),

    /** Conseil des ventes volontaires de meubles aux enchères publiques. */
    CVVMEP("CVVMEP", ""),

    /**
     * Direction départementales de la cohésion sociale et de la protection des
     * populations (DDCSPP) ou Direction départementale de la protection des
     * populations (DDPP).
     */
    DDCSPP("DDCSPP", ""),

    /**
     * Direction générale de la création artistique (DGCA) (Ministère de la
     * Culture).
     */
    DGCA("DGCA", ""),

    /**
     * Direction régionale des entreprises, de la concurrence, de la
     * consommation, du travail et de l'emploi (DIRECCTE) - Préfecture.
     */
    DIRECCTE("DIRECCTE", ""),

    /**
     * Direction régionale des affaires culturelles [DRAC] (préfecture de
     * département).
     */
    DRAC("DRAC", ""),

    /**
     * Direction régionale de l’environnement, de l’aménagement et du logement.
     */
    DREAL("DREAL", ""),

    /**
     * Greffe du tribunal de commerce (ou Greffe du tribunal d'instance
     * Bas-Rhin, Haut-Rhin et Moselle).
     */
    GREFFE_AH("GREFFE", ""),

    /** Mairie de la commune. */
    MAIRIE("COMMUNE", "_"),

    /** Préfecture du département (Préfecture de police à Paris). */
    PREFDEP("PREFDEP", ""),

    /** Préfecture de région (Préfecture de police à Paris). */
    PREFREG("PREFREG", ""),

    /** Procureur de la République près le tribunal de grande instance. */
    PROC("PROC", "");

    /** Code. */
    private String code;

    /** Level 3 separator. */
    private String lvl3Separator;

    /**
     * Constructor.
     *
     * @param code
     *            the code
     * @param lvl3Separator
     *            level 3 separator
     */
    LegacyAuthorityLvl2CodeEnum(final String code, final String lvl3Separator) {
        this.code = code;
        this.lvl3Separator = lvl3Separator;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Gets the lvl 3 separator.
     *
     * @return the lvl 3 separator
     */
    public String getLvl3Separator() {
        return this.lvl3Separator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return this.code;
    }

}
