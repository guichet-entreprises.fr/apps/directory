package fr.ge.directory.service.authority;

import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import fr.ge.directory.service.bean.AuthorityBean;

/**
 * Iterator over a csv file containing authority data.
 * 
 * @author $Author: LABEMONT $
 */
class AuthorityCsvIterator implements Iterator<AuthorityBean> {
    /** the inner Parser. */
    private CSVParser csvParser;
    /** the inner Iterator. */
    private Iterator<CSVRecord> recordsIterator;
    private AuthorityCsvReader authorityCsvReader;

    /**
     * The data to use for the mapping.
     * 
     * @param recordsIterator
     *            the iterator over the csv file
     */
    public AuthorityCsvIterator(CSVParser csvParser, AuthorityCsvReader authorityCsvReader) {
        this.csvParser = csvParser;
        this.recordsIterator = csvParser.iterator();
        // creation du reader selon le type de fichier déterminé par les
        // headers
        this.authorityCsvReader = authorityCsvReader;
    }

    @Override
    public boolean hasNext() {
        if (recordsIterator != null) {
            if (recordsIterator.hasNext()) {
                return true;
            } else {
                try {
                    this.csvParser.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public AuthorityBean next() {
        return recordsIterator != null ? this.authorityCsvReader.unmarshall(recordsIterator.next()) : null;
    }

}