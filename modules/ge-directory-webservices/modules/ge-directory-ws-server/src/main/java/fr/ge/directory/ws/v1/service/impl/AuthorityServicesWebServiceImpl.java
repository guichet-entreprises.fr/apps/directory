
package fr.ge.directory.ws.v1.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import fr.ge.core.rest.exception.RestResponseException;
import fr.ge.directory.service.IAuthorityDataService;
import fr.ge.directory.service.bean.AuthorityBean;
import fr.ge.directory.ws.v1.service.AuthorityServicesWebService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Service for User's rights in authorities.
 * 
 * @author mtakerra
 *
 */
@Api("Authority User Rights REST Services")
@Path("/v1")
public class AuthorityServicesWebServiceImpl implements AuthorityServicesWebService {

    /** Authority data service. */
    @Autowired
    private IAuthorityDataService authorityDataService;

    /** Mapper */
    @Autowired
    private ObjectMapper classicJaxbJsonMapper;

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorityServicesWebServiceImpl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Get authorities rights for a user")
    public Map<String, List<String>> findUserRights(final String userId) throws TechniqueException, FunctionalException {
        return this.authorityDataService.findUserRights(userId);
    }

    /**
     * {@inheritDoc}
     * 
     * @throws JsonProcessingException
     */
    @Override
    @ApiOperation("Compute an authority's active channel")
    public JsonNode computeActiveChannel(String entityId) throws RestResponseException {
        String ftpMode = "";
        boolean flagActivatedChannel = false;
        ObjectNode retour = JsonNodeFactory.instance.objectNode();
        // Get the authority by it's entityId
        final AuthorityBean authority = this.authorityDataService.getByEntityId(entityId);
        if (authority == null) {
            LOGGER.error("no authority exists with the entityId {}", entityId);
            String msgError = String.format("No authority exists with the entityId %s", entityId);
            throw new RestResponseException(msgError, Status.NOT_FOUND);
        } else {
            JsonNode authorityDetails = authority.getDetails();
            JsonNode transferChannels = authorityDetails.get("transferChannels");
            retour.put("entityId", authority.getEntityId());
            retour.put("label", authority.getLabel());
            retour.put("path", authority.getPath());
            if (transferChannels != null) {
                JsonNode backOffice = transferChannels.get("backoffice");
                JsonNode email = transferChannels.get("email");
                JsonNode address = transferChannels.get("address");
                JsonNode ftp = transferChannels.get("ftp");
                // Test which channel have been activated
                if (backOffice.get("state").asText().equals("enabled")) {
                    retour.put("backoffice", backOffice);
                    flagActivatedChannel = true;
                }
                if (email.get("state").asText().equals("enabled")) {
                    retour.put("email", email);
                    flagActivatedChannel = true;
                }
                if (address.get("state").asText().equals("enabled")) {
                    retour.put("address", address);
                    flagActivatedChannel = true;
                }

                if (ftp.get("state").asText().equals("enabled")) {

                    ObjectNode ftpOjectNode = (ObjectNode) ftp;
                    boolean flagErreurChannelFTP = false;
                    ftpMode = ftp.get("ftpMode").asText();
                    // manage the case there is a use of the delegated authority
                    if (ftpMode.equals("delegatedChannel")) {
                        // variables for the delegation ftp configuration
                        boolean ftpTokenFound = false;
                        String delegatedAuthorityEntityId = ftp.get("delegationAuthority").asText();
                        String delegatedAuthorityFtpMode = "";
                        String delegatedAuthorityToken = "";
                        String delegatedAuthoritypathFolder = "";

                        JsonNode delegatedAuthorityDetails;
                        JsonNode delegatedAuthorityTransferChannels;
                        JsonNode delegatedAuthorityFtp;
                        List<String> delegatesAuthoritiesList = new ArrayList<String>();
                        delegatesAuthoritiesList.add(entityId);
                        do {
                            // The delegated authority should
                            // not be the same as the authority been
                            // configured
                            if (delegatesAuthoritiesList.contains(delegatedAuthorityEntityId)) {
                                LOGGER.error(
                                        "The delegated Authority that have the entityId {}, have the same entityId of an authority that was referred as a delegation authority which is not correct.",
                                        delegatedAuthorityEntityId);
                                String msgError = String.format(
                                        "The delegated Authority that have the entityId %s, have the same entityId of an authority that was referred as a delegation authority which is not correct.",
                                        delegatedAuthorityEntityId);
                                throw new RestResponseException(msgError, Status.NOT_FOUND);
                            }
                            delegatesAuthoritiesList.add(delegatedAuthorityEntityId);
                            // Get the authority chosen as delegated authority
                            // and its configuration's detail
                            if (delegatedAuthorityEntityId == null || delegatedAuthorityEntityId.isEmpty()) {
                                LOGGER.error("No delegated authority entityID found in the ftp configuration for the authority with the entityId {}.", entityId);
                                String msgError = String.format("No delegated authority entityID found in the ftp configuration for the authority with the entityId %s.", entityId);
                                throw new RestResponseException(msgError, Status.NOT_FOUND);
                            }
                            // get the delegated authority
                            AuthorityBean delegatedAuthority = authorityDataService.getByEntityId(delegatedAuthorityEntityId);
                            if (delegatedAuthority == null) {
                                LOGGER.error("no delegated authority exists with the entityId {}", delegatedAuthorityEntityId);
                                String msgError = String.format("No delegated authority exists with the entityId %s", delegatedAuthorityEntityId);
                                throw new RestResponseException(msgError, Status.NOT_FOUND);
                            }
                            delegatedAuthorityDetails = delegatedAuthority.getDetails();
                            delegatedAuthorityTransferChannels = delegatedAuthorityDetails.get("transferChannels");
                            // Test that the transfer channel exists in the
                            // delegated authority json's detail
                            if (delegatedAuthorityTransferChannels != null) {
                                // Test the status of the FTP channel of the
                                // delegated authority
                                delegatedAuthorityFtp = delegatedAuthorityTransferChannels.get("ftp");
                                if (delegatedAuthorityFtp.get("state").asText().equals("enabled")) {
                                    delegatedAuthorityFtpMode = delegatedAuthorityFtp.get("ftpMode").asText();
                                    // Test if there is no other redirection
                                    // then we update in the returned actif
                                    // channel the token + folder path of the
                                    // delegated authority found
                                    if (!delegatedAuthorityFtpMode.equals("delegatedChannel")) {

                                        if (delegatedAuthorityFtp.get("token").isNull()) {
                                            ftpOjectNode.putNull("token");
                                        } else {
                                            ftpOjectNode.put("token", delegatedAuthorityFtp.get("token").asText());
                                        }

                                        if (delegatedAuthorityFtp.get("pathFolder").isNull()) {
                                            ftpOjectNode.putNull("pathFolder");
                                        } else {
                                            ftpOjectNode.put("pathFolder", delegatedAuthorityFtp.get("pathFolder").asText());
                                        }
                                        ftpTokenFound = true;

                                    } else {
                                        delegatedAuthorityEntityId = delegatedAuthorityFtp.get("delegationAuthority").asText();
                                    }

                                } else {
                                    LOGGER.error("The delegated Authority that have the entityId {}, have no Ftp channel enabled.", delegatedAuthorityEntityId);
                                    String msgError = String.format("The delegated Authority that have the entityId %s, have no Ftp channel enabled.", delegatedAuthorityEntityId);
                                    throw new RestResponseException(msgError, Status.NOT_FOUND);
                                }
                            } else {

                                LOGGER.error("The delegated Authority that have the entityId {}, have no configuration set.", delegatedAuthorityEntityId);
                                String msgError = String.format("The delegated Authority that have the entityId %s, have no configuration set.", delegatedAuthorityEntityId);
                                throw new RestResponseException(msgError, Status.NOT_FOUND);

                            }
                        } while (delegatedAuthorityFtpMode.equals("delegatedChannel") && ftpTokenFound == false);

                    }

                    retour.put("ftp", ftpOjectNode);
                    flagActivatedChannel = true;

                }

            }
            if (!flagActivatedChannel) {
                LOGGER.error("the channels configured are not Activated for the authority that have entityId : {}.", entityId);
                String msgError = String.format("the channels configured are not Activated for the authority that have entityId : %s", entityId);
                throw new RestResponseException(msgError, Status.NOT_FOUND);
            }

        }
        return retour;
    }

}
