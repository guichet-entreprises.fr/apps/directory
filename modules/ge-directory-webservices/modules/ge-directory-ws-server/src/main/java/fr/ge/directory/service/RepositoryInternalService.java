/**
 * 
 */
package fr.ge.directory.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.impl.RepositoryService;
import fr.ge.directory.ws.v1.model.RepositoryElement;

/**
 * Repository internal service.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Service
public class RepositoryInternalService {

    /** The repository service. */
    @Autowired
    private RepositoryService repositoryService;

    public SearchResult<RepositoryElement> search(String repositoryId, long startIndex, long maxResults, List<SearchQueryFilter> filters, List<SearchQueryOrder> orders) {
        return this.repositoryService.search(repositoryId, new SearchQuery(startIndex, maxResults).setFilters(filters).setOrders(orders));
    }
}
