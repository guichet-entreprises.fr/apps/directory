/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.ws.v1.service.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import fr.ge.core.rest.exception.RestResponseException;
import fr.ge.directory.service.IUsersService;
import fr.ge.directory.ws.bean.LegacyAuthorityUserLink;
import fr.ge.directory.ws.v1.service.IUsersWService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Service for authorities.
 *
 * @author kelmoura $
 * @version $Revision: 0 $
 */
@Api("Users REST Services")
@Path("/v1")
public class UsersWServiceImpl implements IUsersWService {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(UsersWServiceImpl.class);

    /** Authority data service. */
    @Autowired
    private IUsersService usersService;

    /**
     * {@inheritDoc}
     * 
     * @throws RestResponseException
     */
    @Override
    @ApiOperation("Imports a list of users")
    public String importUsers(byte[] importedUsers, boolean dryRun) throws RestResponseException {
        // Construction du Préfix du log pour tout les traitements d'imports à
        // venir

        final StringBuilder logPrefixBuilder = new StringBuilder();
        logPrefixBuilder.append("[Import]");
        if (dryRun) {
            logPrefixBuilder.append("[Dry run]");
        }

        final String logPrefix = logPrefixBuilder.toString();
        String nbLines = "";
        final Map<String, Integer> colsIndexes = new HashMap<>();
        final List<LegacyAuthorityUserLink> listUsersToImport = new ArrayList<>();

        LOGGER.info("{} Importing users", logPrefix);
        InputStream importedUsersInputStream = new ByteArrayInputStream(importedUsers);
        LOGGER.debug("{} starting the scanner to get imported users", logPrefix);
        try (Scanner scanner = new Scanner(importedUsersInputStream, StandardCharsets.UTF_8.name());) {
            if (scanner.hasNextLine()) {
                LOGGER.debug("{} Getting the Scanner's first line ", logPrefix);
                String[] cols = scanner.nextLine().split(";");
                // Getting the columns and there indexes from the file
                for (int colIndex = 0; colIndex < cols.length; ++colIndex) {
                    colsIndexes.put(cols[colIndex], colIndex);
                    LOGGER.debug("{} The splitted column is :{}.", logPrefix, cols);
                    LOGGER.debug("{} The columnns' indexe is :{}.", logPrefix, colsIndexes.get(cols[colIndex]));
                }
                LOGGER.info("{} The columnns' indexes extracetd are :{}.", logPrefix, colsIndexes);

                // Getting user by user from the file
                while (scanner.hasNextLine()) {

                    cols = scanner.nextLine().split(";");
                    final LegacyAuthorityUserLink user = this.usersService.getUser(cols, colsIndexes);
                    listUsersToImport.add(user);
                    LOGGER.info("{} The user {} {} {} was extracted from the csv file.", logPrefix, user.getUserLastName(), user.getUserFirstName(), user.getUserEmail());
                }

                LOGGER.info("{} The list of the extracted users from the CSV file has {} elements.", logPrefix, listUsersToImport.size());

                try {
                    nbLines = this.usersService.importUsers(listUsersToImport, dryRun, logPrefix);
                } catch (TechnicalException | TechniqueException | FunctionalException e) {
                    String msg = String.format("Unable to import the users %s because : %s.", listUsersToImport, e.getMessage());
                    LOGGER.error(msg);
                    throw new RestResponseException(msg, Status.INTERNAL_SERVER_ERROR);
                }
            }
        } catch (Exception e) {
            String msg = String.format("%s Error reading the CSV file : %s", logPrefix, e);
            LOGGER.error(msg);
            throw new RestResponseException(msg, Status.INTERNAL_SERVER_ERROR);
        }
        return nbLines;
    }
}
