/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.ws.bean;

import org.apache.commons.lang3.StringUtils;

import fr.ge.directory.ws.constant.enumeration.LegacyUserRoleEnum;

/**
 * Legacy authority user link.
 *
 * @author jpauchet
 */
public class LegacyAuthorityUserLink {

    /** Identifier. */
    private String id;

    /** Level 1 code. */
    private String lvl1Code;

    /** Level 2 code. */
    private String lvl2Code;

    /** Level 3 code. */
    private String lvl3Code;

    /** Level 3 label. */
    private String lvl3Label;

    /** User id. */
    private String userId;

    /** User last name. */
    private String userLastName;

    /** User first name. */
    private String userFirstName;

    /** User email. */
    private String userEmail;

    /** User role. */
    private LegacyUserRoleEnum userRole;

    /** User level 2 code. */
    private String userLvl2Code;

    /**
     * Constructor.
     */
    public LegacyAuthorityUserLink() {
        // Nothing to do.
    }

    /**
     * Is the user linked to a level 2 authority ?
     *
     * @return a boolean
     */
    public boolean isUserLinkedToLvl2() {
        return StringUtils.isEmpty(this.getId());
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id
     * @return the legacy authority user link
     */
    public LegacyAuthorityUserLink setId(final String id) {
        this.id = id;
        return this;
    }

    /**
     * Gets the lvl 1 code.
     *
     * @return the lvl 1 code
     */
    public String getLvl1Code() {
        return this.lvl1Code;
    }

    /**
     * Sets the lvl 1 code.
     *
     * @param lvl1Code
     *            the lvl 1 code
     * @return the legacy authority user link
     */
    public LegacyAuthorityUserLink setLvl1Code(final String lvl1Code) {
        this.lvl1Code = lvl1Code;
        return this;
    }

    /**
     * Gets the lvl 2 code.
     *
     * @return the lvl 2 code
     */
    public String getLvl2Code() {
        return this.lvl2Code;
    }

    /**
     * Sets the lvl 2 code.
     *
     * @param lvl2Code
     *            the lvl 2 code
     * @return the legacy authority user link
     */
    public LegacyAuthorityUserLink setLvl2Code(final String lvl2Code) {
        this.lvl2Code = lvl2Code;
        return this;
    }

    /**
     * Gets the lvl 3 code.
     *
     * @return the lvl 3 code
     */
    public String getLvl3Code() {
        return this.lvl3Code;
    }

    /**
     * Sets the lvl 3 code.
     *
     * @param lvl3Code
     *            the lvl 3 code
     * @return the legacy authority user link
     */
    public LegacyAuthorityUserLink setLvl3Code(final String lvl3Code) {
        this.lvl3Code = lvl3Code;
        return this;
    }

    /**
     * Gets the lvl 3 label.
     *
     * @return the lvl 3 label
     */
    public String getLvl3Label() {
        return this.lvl3Label;
    }

    /**
     * Sets the lvl 3 label.
     *
     * @param lvl3Label
     *            the lvl 3 label
     * @return the legacy authority user link
     */
    public LegacyAuthorityUserLink setLvl3Label(final String lvl3Label) {
        this.lvl3Label = lvl3Label;
        return this;
    }

    /**
     * Gets the user id.
     *
     * @return the user id
     */
    public String getUserId() {
        return this.userId;
    }

    /**
     * Sets the user id.
     *
     * @param userId
     *            the user id
     * @return the legacy authority user link
     */
    public LegacyAuthorityUserLink setUserId(final String userId) {
        this.userId = userId;
        return this;
    }

    /**
     * Gets the user last name.
     *
     * @return the user last name
     */
    public String getUserLastName() {
        return this.userLastName;
    }

    /**
     * Sets the user last name.
     *
     * @param userLastName
     *            the user last name
     * @return the legacy authority user link
     */
    public LegacyAuthorityUserLink setUserLastName(final String userLastName) {
        this.userLastName = userLastName;
        return this;
    }

    /**
     * Gets the user first name.
     *
     * @return the user first name
     */
    public String getUserFirstName() {
        return this.userFirstName;
    }

    /**
     * Sets the user first name.
     *
     * @param userFirstName
     *            the user first name
     * @return the legacy authority user link
     */
    public LegacyAuthorityUserLink setUserFirstName(final String userFirstName) {
        this.userFirstName = userFirstName;
        return this;
    }

    /**
     * Gets the user email.
     *
     * @return the user email
     */
    public String getUserEmail() {
        return this.userEmail;
    }

    /**
     * Sets the user email.
     *
     * @param userEmail
     *            the user email
     * @return the legacy authority user link
     */
    public LegacyAuthorityUserLink setUserEmail(final String userEmail) {
        this.userEmail = userEmail;
        return this;
    }

    /**
     * Gets the user role.
     *
     * @return the user role
     */
    public LegacyUserRoleEnum getUserRole() {
        return this.userRole;
    }

    /**
     * Sets the user role.
     *
     * @param userRole
     *            the user role
     * @return the legacy authority user link
     */
    public LegacyAuthorityUserLink setUserRole(final LegacyUserRoleEnum userRole) {
        this.userRole = userRole;
        return this;
    }

    /**
     * Gets the user lvl 2 code.
     *
     * @return the user lvl 2 code
     */
    public String getUserLvl2Code() {
        return this.userLvl2Code;
    }

    /**
     * Sets the user lvl 2 code.
     *
     * @param userLvl2Code
     *            the user lvl 2 code
     * @return the legacy authority user link
     */
    public LegacyAuthorityUserLink setUserLvl2Code(final String userLvl2Code) {
        this.userLvl2Code = userLvl2Code;
        return this;
    }

}
