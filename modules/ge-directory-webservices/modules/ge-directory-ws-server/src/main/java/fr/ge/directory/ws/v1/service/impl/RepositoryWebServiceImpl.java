/**
 * 
 */
package fr.ge.directory.ws.v1.service.impl;

import java.util.List;

import javax.ws.rs.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.impl.RepositoryService;
import fr.ge.directory.ws.v1.model.RepositoryElement;
import fr.ge.directory.ws.v1.model.RepositoryId;
import fr.ge.directory.ws.v1.service.RepositoryWebService;

/**
 * @author $Author: LABEMONT $
 */
@Path("/v1")
public class RepositoryWebServiceImpl implements RepositoryWebService {

    /** le logger */
    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryWebServiceImpl.class);

    /** The repository service. */
    @Autowired
    private RepositoryService repositoryService;

    /**
     * {@inheritDoc}
     */
    @Override
    public RepositoryId add(String repositoryId, RepositoryElement element) {
        RepositoryId id = this.repositoryService.add(repositoryId, element);
        return id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RepositoryElement read(String repositoryId, long id) {
        RepositoryElement element = this.repositoryService.read(repositoryId, id);
        return element;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchResult<RepositoryElement> search(String repositoryId, long startIndex, long maxResults, List<SearchQueryFilter> filters, List<SearchQueryOrder> orders) {
        final SearchQuery searchQuery = new SearchQuery(startIndex, maxResults).setFilters(filters).setOrders(orders);
        final SearchResult<RepositoryElement> results = this.repositoryService.search(repositoryId, searchQuery);
        LOGGER.info("Found {} results in {} for {}.", results.getTotalResults(), repositoryId, filters);

        return results;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(String repositoryId, boolean reset, List<RepositoryElement> repositoryElements) {
        this.repositoryService.injectRepository(repositoryId, repositoryElements, reset);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(String repositoryId, RepositoryElement repositoryElement) {
        this.repositoryService.updateElement(repositoryId, repositoryElement);

    }

    public List<String> listRepositories() {
        final List<String> listDistinctRepositoriesId = this.repositoryService.listRepositories();

        return listDistinctRepositoriesId;
    }

}
