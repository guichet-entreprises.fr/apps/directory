/**
 * 
 */
package fr.ge.directory.ws.v1.service.impl;

import java.util.List;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.RepositoryInternalService;
import fr.ge.directory.ws.v1.model.RepositoryElement;
import fr.ge.directory.ws.v1.service.RepositoryManagedWebService;

/**
 * Generic managed repository web services implementation.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Path("/v1")
public class RepositoryManagedWebServiceImpl implements RepositoryManagedWebService {

    /** The repository service. */
    @Autowired
    private RepositoryInternalService repositoryInternalService;

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchResult<RepositoryElement> search(final String repositoryId, final long startIndex, final long maxResults, final List<SearchQueryFilter> filters, final List<SearchQueryOrder> orders) {
        return this.repositoryInternalService.search(repositoryId, startIndex, maxResults, filters, orders);
    }
}
