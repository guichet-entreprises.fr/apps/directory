/**
 * 
 */
package fr.ge.directory.ws.v1.service.impl;

import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.ICommuneDataService;
import fr.ge.directory.service.IPriceDataService;
import fr.ge.directory.ws.v1.bean.ResponseRefCommuneBean;
import fr.ge.directory.ws.v1.bean.ResponseRefPriceBean;
import fr.ge.directory.ws.v1.service.IRefService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author bsadil
 *
 */

@Api("Referential REST Services")
@Path("/v1/refService")
public class RefServiceImpl implements IRefService {
    /**
     * Tarif data Service
     */
    @Autowired
    private IPriceDataService priceDataServie;
    /**
     * Commune data service
     */
    @Autowired
    private ICommuneDataService CommuneDataServie;

    /**
     * {@inheritDoc}
     * 
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Override
    @ApiOperation(value = "Search for Zip code", notes = "Multiple filters can be specified using pattern\n" //
            + "&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt, where \"operator\" can be :\n" //
            + "- \":\" = equals\n" //
            + "- \"&gt;\" = greater than\n" //
            + "- \"&gt;=\" = greater than or equals\n" //
            + "- \"&lt;\" = less than\n" //
            + "- \"&lt;=\" = less than or equals\n")
    public SearchResult<ResponseRefCommuneBean> search(@ApiParam("page's first element index") final long startIndex, //
            @ApiParam("max element per page") final long maxResults, //
            @ApiParam("filters as \"&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt;\"") final List<SearchQueryFilter> filters, //
            @ApiParam("orders as \"&lt;fieldName&gt;:&lt;asc|desc&gt;\"") final List<SearchQueryOrder> orders, @ApiParam("searched value on all field ") final String searchedValue) {
        final SearchQuery searchQuery = new SearchQuery(startIndex, maxResults).setFilters(filters).setOrders(orders).setSearchTerms(searchedValue);
        return this.CommuneDataServie.search(searchQuery, ResponseRefCommuneBean.class);

    }

    /**
     * 
     * {@inheritDoc}
     */

    @Override
    @ApiOperation(value = "Search for tarifs", notes = "Multiple filters can be specified using pattern\n" //
            + "&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt, where \"operator\" can be :\n" //
            + "- \":\" = equals\n" //
            + "- \"&gt;\" = greater than\n" //
            + "- \"&gt;=\" = greater than or equals\n" //
            + "- \"&lt;\" = less than\n" //
            + "- \"&lt;=\" = less than or equals\n")
    public SearchResult<ResponseRefPriceBean> searchPrice(@ApiParam("page's first element index") final long startIndex, //
            @ApiParam("max element per page") final long maxResults, //
            @ApiParam("filters as \"&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt;\"") final List<SearchQueryFilter> filters, //
            @ApiParam("orders as \"&lt;fieldName&gt;:&lt;asc|desc&gt;\"") final List<SearchQueryOrder> orders, @ApiParam("searched value on all field ") final String searchedValue) {
        final SearchQuery searchQuery = new SearchQuery(startIndex, maxResults).setFilters(filters).setOrders(orders).setSearchTerms(searchedValue);
        return this.priceDataServie.search(searchQuery, ResponseRefPriceBean.class);

    }

}
