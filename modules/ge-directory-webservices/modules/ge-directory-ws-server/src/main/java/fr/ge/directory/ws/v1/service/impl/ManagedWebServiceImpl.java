/**
 * 
 */
package fr.ge.directory.ws.v1.service.impl;

import javax.ws.rs.Path;

import fr.ge.common.utils.service.IHealthCheckService;
import fr.ge.directory.ws.v1.service.IManagedWebService;
import io.swagger.annotations.Api;

/**
 * @author mmouhous
 *
 */
@Api("managed directory services")
@Path("/directory")
public class ManagedWebServiceImpl implements IManagedWebService, IHealthCheckService {

}
