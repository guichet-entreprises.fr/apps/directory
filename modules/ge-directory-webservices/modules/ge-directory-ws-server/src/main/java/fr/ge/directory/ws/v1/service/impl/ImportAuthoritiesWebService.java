/**
 * 
 */
package fr.ge.directory.ws.v1.service.impl;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.core.rest.exception.RestResponseException;
import fr.ge.directory.service.authority.AuthorityImporter;
import fr.ge.directory.service.authority.AuthorityImporterFactory;
import fr.ge.directory.ws.v1.service.IImportAuthoritiesWebService;

/**
 * @author $Author: LABEMONT $
 */
@Path("/v1")
public class ImportAuthoritiesWebService implements IImportAuthoritiesWebService {

    /** le logger */
    private static final Logger LOGGER = LoggerFactory.getLogger(ImportAuthoritiesWebService.class);

    @Autowired
    private AuthorityImporterFactory authorityImportFactory;

    /**
     * {@inheritDoc}
     */
    @Override
    public String importAuthorities(List<Attachment> authoritiesFiles, boolean dryRun) throws RestResponseException {
        final StringBuilder logPrefixBuilder = new StringBuilder();
        logPrefixBuilder.append("[Import]");
        if (dryRun) {
            logPrefixBuilder.append("[Dry run]");
        }

        // XXX gérer le dryRun dans toute la chaine
        final String logPrefix = logPrefixBuilder.toString();
        int nbLines = 0;
        LOGGER.info("{} Importing authorities", logPrefix);

        for (Attachment attachement : authoritiesFiles) {
            try {
                // recognize the input : which extractor to use
                AuthorityImporter authorityImporter = authorityImportFactory.read(attachement.getDataHandler().getInputStream());
                nbLines += authorityImporter.importAuthorities(dryRun);
            } catch (TechnicalException | IOException e) {
                String msg = String.format("Unable to insert authorities for %s : %s.", attachement.getContentId(), e.getMessage());
                LOGGER.error(msg);
                throw new RestResponseException(msg, Status.INTERNAL_SERVER_ERROR);
            }
        }
        return Integer.toString(nbLines);
    }

}
