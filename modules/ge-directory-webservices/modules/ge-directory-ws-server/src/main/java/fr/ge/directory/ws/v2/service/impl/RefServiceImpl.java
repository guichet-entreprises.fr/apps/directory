/**
 *
 */
package fr.ge.directory.ws.v2.service.impl;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.ws.rs.Path;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.directory.service.IAuthorityDataService;
import fr.ge.directory.service.IReferentialItemDataService;
import fr.ge.directory.service.bean.ReferentialItemBean;
import fr.ge.directory.service.bean.ReferentialTableEnum;
import fr.ge.directory.service.impl.RepositoryService;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityBean;
import fr.ge.directory.ws.v1.bean.ResponseReferentialItemBean;
import fr.ge.directory.ws.v2.service.IRefService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Referential REST Services V2
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Api("Referential REST Services V2")
@Path("/v2/ref")
public class RefServiceImpl implements IRefService {

    @Autowired
    private IReferentialItemDataService referentialItemDataService;

    /** The repository service. */
    @Autowired
    private RepositoryService repositoryService;

    /** Authority data service. */
    @Autowired
    private IAuthorityDataService authorityDataService;

    @Override
    @ApiOperation("Save referential data")
    public void save(@ApiParam("functional referential name") final String refTableName, //
            @ApiParam("The details as JSON") final List<JsonNode> data) {
        this.referentialItemDataService.save(refTableName, data);
    }

    @Override
    @ApiOperation(value = "Search data into referential")
    public List<JsonNode> search(@ApiParam("functional referential name") final String refTableName, @ApiParam("searched value") final String searchedValue) {
        final List<ReferentialItemBean> resultReferantial = this.referentialItemDataService.find(ReferentialTableEnum.findTechnicalTable(refTableName), searchedValue);

        return resultReferantial.stream() //
                .map(ReferentialItemBean::getDetails) //
                .filter(detail -> detail.has("label")) // yep
                .collect(Collectors.toList());
    }

    @Override
    @ApiOperation(value = "Truncate referential")
    public void truncate(@ApiParam("functional referential name") final String refTableName) {
        this.referentialItemDataService.truncate(ReferentialTableEnum.findTechnicalTable(refTableName));
    }

    /**
     * {@inheritDoc}
     *
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Override
    @ApiOperation(value = "Search for referential", notes = "Multiple filters can be specified using pattern\n" //
            + "&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt, where \"operator\" can be :\n" //
            + "- \":\" = equals\n" //
            + "- \"&gt;\" = greater than\n" //
            + "- \"&gt;=\" = greater than or equals\n" //
            + "- \"&lt;\" = less than\n" //
            + "- \"&lt;=\" = less than or equals\n")
    public SearchResult<JsonNode> findAll(@ApiParam("functional referential name") final String refTableName, @ApiParam("page's first element index") final long startIndex, //
            @ApiParam("max element per page") final long maxResults, //
            @ApiParam("filters as \"&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt;\"") final List<SearchQueryFilter> filters, //
            @ApiParam("orders as \"&lt;fieldName&gt;:&lt;asc|desc&gt;\"") final List<SearchQueryOrder> orders, @ApiParam("search terms on all field ") final String searchTerms) {

        final SearchQuery searchQuery = new SearchQuery(startIndex, maxResults).setFilters(filters).setOrders(orders).setSearchTerms(searchTerms);

        SearchResult<?> searchResult = null;
        final String targetTable = ReferentialTableEnum.findTechnicalTable(refTableName);
        if (StringUtils.isNotEmpty(targetTable) && ReferentialTableEnum.AUTHORITY.getTechnicalTable().equals(targetTable)) {
            searchResult = this.authorityDataService.search(searchQuery, ResponseAuthorityBean.class);
        } else {
            searchResult = this.repositoryService.search(refTableName, searchQuery);
        }

        final SearchResult<JsonNode> results = new SearchResult<JsonNode>(startIndex, maxResults);
        results.setTotalResults(searchResult.getTotalResults());
        results.setContent(searchResult.getContent().stream() //
                .map(repo -> new ObjectMapper().convertValue(repo, JsonNode.class)) //
                .collect(Collectors.toList()));

        return results;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseReferentialItemBean get(final long id, String refTableName) {
        final ResponseReferentialItemBean bean = new ResponseReferentialItemBean();
        refTableName = ReferentialTableEnum.findTechnicalTable(refTableName);
        final ReferentialItemBean ref = this.referentialItemDataService.get(refTableName, id);
        if (ref != null) {
            bean.setId(ref.getId());
            bean.setDetails(ref.getDetails());
            bean.setEntityId(ref.getEntityId());
            bean.setLabel(ref.getLabel());
            bean.setCreated(ref.getUpdated());
        }
        return bean;
    }
}
