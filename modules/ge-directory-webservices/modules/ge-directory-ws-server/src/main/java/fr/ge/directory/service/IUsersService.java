/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.directory.service;

import java.util.List;
import java.util.Map;

import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import fr.ge.directory.ws.bean.LegacyAuthorityUserLink;

/**
 * Users services.
 *
 * @author kelmoura $
 * @version $Revision: 0 $
 */
public interface IUsersService {

    /**
     * For each user present in the param listUsersToImport, we will import it
     * after doing the necessary checks on it
     * 
     * @param listUsersToImport
     *            : List of the users to import
     * 
     * @param dryrun
     *            : a boolean that allows us to check if this is a test run or
     *            the real deal
     *
     * @param logPrefix
     *            : The prefix to identify wich users the logs relates to
     * @throws FunctionalException
     * @throws TechniqueException
     * 
     */
    public String importUsers(List<LegacyAuthorityUserLink> listUsersToImport, boolean dryRun, String logPrefix) throws TechniqueException, FunctionalException;

    /**
     * Gets a user from columns.
     *
     * @param cols
     *            the columns
     * @param colsIndexes
     *            the column indexes
     * @return the user
     */
    public LegacyAuthorityUserLink getUser(final String[] cols, final Map<String, Integer> colsIndexes);

}
