/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.ws.v1.service.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.core.exception.FunctionalException;
import fr.ge.core.rest.exception.RestResponseException;
import fr.ge.directory.core.exception.TechnicalException;
import fr.ge.directory.service.IAlgoDataService;
import fr.ge.directory.service.bean.AlgoBean;
import fr.ge.directory.ws.v1.bean.ResponseAlgoBean;
import fr.ge.directory.ws.v1.service.IAlgoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;

/**
 * Service for algorithms.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@Api("Algo REST Services")
@Path("/v1/algo")
public class AlgoServiceImpl implements IAlgoService {

    /** Algo data service. */
    @Autowired
    private IAlgoDataService algoDataService;

    /** Dozer. */
    @Autowired
    private DozerBeanMapper dozer;

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Retrieves an algo from its `ID`")
    public ResponseAlgoBean get(@ApiParam("algo id") final long id) {
        ResponseAlgoBean responseAlgo;
        final AlgoBean algo = this.algoDataService.get(id);
        if (algo == null) {
            responseAlgo = new ResponseAlgoBean();
        } else {
            responseAlgo = this.dozer.map(algo, ResponseAlgoBean.class);
        }
        return responseAlgo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Retrieves an algo from its authority type")
    public ResponseAlgoBean getByCode(@ApiParam("algo authority type") final String authorityType) {
        ResponseAlgoBean responseAlgo;
        final AlgoBean algo = this.algoDataService.getByCode(authorityType);
        if (algo == null) {
            responseAlgo = new ResponseAlgoBean();
        } else {
            responseAlgo = this.dozer.map(algo, ResponseAlgoBean.class);
        }
        return responseAlgo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Saves an algo")
    public void save(@ApiParam(value = "algo", examples = @Example(value = @ExampleProperty(value = "{\n" //
            + "  \"code\": \"debitBoisson\",\n" //
            + "  \"content\": \"return 'Mairie' + args.codeInsee;\"\n" //
            + "}"))) final ResponseAlgoBean data) {
        final AlgoBean algo = this.dozer.map(data, AlgoBean.class);
        this.algoDataService.save(algo);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Search for queue messages", notes = "Multiple filters can be specified using pattern\n" //
            + "&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt, where \"operator\" can be :\n" //
            + "- \":\" = equals\n" //
            + "- \"&gt;\" = greater than\n" //
            + "- \"&gt;=\" = greater than or equals\n" //
            + "- \"&lt;\" = less than\n" //
            + "- \"&lt;=\" = less than or equals\n")
    public SearchResult<ResponseAlgoBean> search(@ApiParam("page's first element index") final long startIndex, //
            @ApiParam("max element per page") final long maxResults, //
            @ApiParam("filters as \"&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt;\"") final List<SearchQueryFilter> filters, //
            @ApiParam("orders as \"&lt;fieldName&gt;:&lt;asc|desc&gt;\"") final List<SearchQueryOrder> orders) {
        final SearchQuery searchQuery = new SearchQuery(startIndex, maxResults).setFilters(filters).setOrders(orders);
        return this.algoDataService.search(searchQuery, ResponseAlgoBean.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Removes an algo")
    public boolean remove(@ApiParam("algo id") final long id) {
        return this.algoDataService.remove(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Retrieves an authority from algo")
    public String execute(@ApiParam("algo authority type") final String code, @ApiParam(value = "metas", examples = @Example(value = @ExampleProperty(value = "{\r\n" //
            + "  \"codeInsee\": \"75012\"\r\n" //
            + "}"))) final Map<String, String> metas) {
        return this.algoDataService.execute(code, metas);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String importAlgo(final byte[] algoAsBytes, final boolean reset) throws FunctionalException, RestResponseException {
        try (ZipInputStream stream = new ZipInputStream(new ByteArrayInputStream(algoAsBytes))) {
            if (null == stream.getNextEntry()) {
                throw new TechnicalException("The input file is not a zip file");
            }
        } catch (final TechnicalException | ZipException ex) {
            throw new RestResponseException("The input file is not a zip file", Status.INTERNAL_SERVER_ERROR);
        } catch (final IOException ex) {
            throw new RestResponseException("Error while read zip entries", Status.INTERNAL_SERVER_ERROR);
        }

        if (reset) {
            this.algoDataService.removeAll();
        }

        int nbIntegrated = 0;
        try (ZipInputStream stream = new ZipInputStream(new ByteArrayInputStream(algoAsBytes))) {
            ZipEntry zipEntry;
            while (null != (zipEntry = stream.getNextEntry())) {
                final ResponseAlgoBean data = new ResponseAlgoBean();
                data.setCode(FilenameUtils.removeExtension(zipEntry.getName()));
                data.setContent(IOUtils.toString(stream, StandardCharsets.UTF_8.name()));
                this.save(data);
                nbIntegrated++;
            }
        } catch (final IOException ex) {
            throw new RestResponseException("Error while read zip entries", Status.INTERNAL_SERVER_ERROR);
        }
        return Integer.toString(nbIntegrated);
    }

}
