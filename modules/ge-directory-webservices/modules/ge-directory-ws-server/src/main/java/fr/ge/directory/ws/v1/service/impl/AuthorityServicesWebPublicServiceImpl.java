
package fr.ge.directory.ws.v1.service.impl;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import fr.ge.directory.service.IAuthorityDataService;
import fr.ge.directory.ws.v1.service.AuthorityServicesWebPublicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Service for User's rights in authorities.
 * 
 * @author mtakerra
 *
 */
@Api("Authority User Rights REST Services")
@Path("/public/services")
public class AuthorityServicesWebPublicServiceImpl implements AuthorityServicesWebPublicService {

    /** Authority data service. */
    @Autowired
    private IAuthorityDataService authorityDataService;

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Get authorities rights for a user")
    public Map<String, List<String>> findUserRights(final String userId) throws TechniqueException, FunctionalException {
        return this.authorityDataService.findUserRights(userId);
    }

}
