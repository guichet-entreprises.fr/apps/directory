/**
 * 
 */
package fr.ge.directory.service.authority;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.directory.service.IAuthorityDataService;
import fr.ge.directory.service.IReferentialItemDataService;
import fr.ge.tracker.facade.ITrackerFacade;

/**
 * Factory to create the right Import for a feed.
 * 
 * @author $Author: LABEMONT $
 */
@Service
public class AuthorityImporterFactory {

    /** Authority data access */
    @Autowired
    private IAuthorityDataService authorityDataService;

    @Autowired
    private IReferentialItemDataService referentialItemDataService;

    /** Traker facade. */
    @Autowired
    private ITrackerFacade trackerFacade;

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorityImporterFactory.class);

    /** Defaut simple resilient csv reader format. */
    private static CSVFormat BASIC_RESILIENT_FORMAT = CSVFormat.newFormat(';').withFirstRecordAsHeader().withIgnoreEmptyLines().withIgnoreSurroundingSpaces().withIgnoreHeaderCase()
            .withAllowMissingColumnNames();

    /**
     * Read the inputStream to find the right importer.
     * 
     * @param inputStream
     *            the data feed
     * @return the right importer
     */
    public AuthorityImporter read(InputStream inputStream) throws TechnicalException {

        try {
            // TODO gérer le Closeable
            CSVParser csvParser = new CSVParser(new InputStreamReader(inputStream), BASIC_RESILIENT_FORMAT);
            String[] headers = csvParser.getHeaderMap().keySet().toArray(new String[csvParser.getHeaderMap().keySet().size()]);
            if (LegacyAuthorityCsvReader.readable(headers)) {
                return new AuthorityImporter(new AuthorityCsvIterator(csvParser, new LegacyAuthorityCsvReader()), this.authorityDataService, this.referentialItemDataService, this.trackerFacade);
            }
            if (LegacyCommuneCsvReader.readable(headers)) {
                return new AuthorityCommuneImporter(new AuthorityCsvIterator(csvParser, new LegacyCommuneCsvReader()), this.authorityDataService, this.referentialItemDataService, this.trackerFacade);
            }
        } catch (IOException e) {
            LOGGER.error("Unable to create an Authority Importer for this inputStream : {}", e.getMessage());
            throw new TechnicalException(e);
        }

        return null;
    }

}
