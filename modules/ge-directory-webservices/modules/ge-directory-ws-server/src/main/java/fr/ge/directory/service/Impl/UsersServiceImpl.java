/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.directory.service.Impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import fr.ge.ct.authentification.bean.AccountUserBean;
import fr.ge.ct.authentification.facade.IAccountFacade;
import fr.ge.directory.service.IAuthorityDataService;
import fr.ge.directory.service.IReferentialItemDataService;
import fr.ge.directory.service.IUsersService;
import fr.ge.directory.service.bean.AuthorityBean;
import fr.ge.directory.service.bean.ReferentialItemBean;
import fr.ge.directory.service.bean.ReferentialTableEnum;
import fr.ge.directory.ws.bean.LegacyAuthorityUserLink;
import fr.ge.directory.ws.constant.enumeration.LegacyAuthorityLvl2CodeEnum;
import fr.ge.directory.ws.constant.enumeration.LegacyUserRoleEnum;

/**
 * Users Services Implementation
 *
 * @author $Author: kelmoura $
 * @version $Revision: 0 $
 */
@Service
public class UsersServiceImpl implements IUsersService {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(UsersServiceImpl.class);

    /** Authority DataService */
    @Autowired
    private IAuthorityDataService authorityDataService;

    /** referentialItem DataService */
    @Autowired
    private IReferentialItemDataService referentialItemDataService;

    /** Account facade. */
    @Autowired
    private IAccountFacade accountFacade;

    /**
     * {@inheritDoc}
     * 
     * @throws FunctionalException
     * @throws TechniqueException
     */
    @Override
    public String importUsers(List<LegacyAuthorityUserLink> listUsersToImport, boolean dryRun, String logPrefix) throws TechniqueException, FunctionalException {
        // Differenciate between using the dryrun or real import in the logs
        AuthorityBean authority = null;
        int nbLines = 0;
        final Map<String, AccountUserBean> importedUsersBeans = new HashMap<>();
        if (listUsersToImport != null && !listUsersToImport.isEmpty()) {
            for (LegacyAuthorityUserLink user : listUsersToImport) {
                // Using new Prefix for the user's proper log
                final StringBuilder userLogPrefixBuilder = new StringBuilder();
                userLogPrefixBuilder.append(logPrefix);
                userLogPrefixBuilder.append("[user '");
                userLogPrefixBuilder.append(user.getUserId());
                userLogPrefixBuilder.append("']'");

                String userlogPrefix = userLogPrefixBuilder.toString();
                // Getting the authority that relates to the user
                if (user.getUserRole().isRootAuthority()) {
                    // Level 1 (Root level)
                    LOGGER.info("{} The user : {} {} ({}), is linked to a level 1 authority => GE.", userlogPrefix, user.getUserLastName(), user.getUserFirstName(), user.getUserEmail());
                    authority = this.authorityDataService.getByEntityId("GE");
                } else if (user.isUserLinkedToLvl2()) {
                    // Level 2
                    LOGGER.info("{} The user : {} {} ({}), is linked to a level 2 authority with the code :{}.", userlogPrefix, user.getUserLastName(), user.getUserFirstName(), user.getUserEmail(),
                            user.getUserLvl2Code());
                    authority = this.authorityDataService.getByEntityId(this.getNewCode(user.getUserLvl2Code(), false));
                    LOGGER.debug("{} The authority returned by the call of getByEntityId :{}.", userlogPrefix, authority);
                } else {
                    // Level 3
                    LOGGER.info("{} The user : {} {} ({}), is linked to a level 3 authority that have the ediCode {}.", userlogPrefix, user.getUserLastName(), user.getUserFirstName(),
                            user.getUserEmail(), user.getLvl3Code());
                    List<SearchQueryFilter> filters = new ArrayList<>();
                    SearchQueryFilter filter = new SearchQueryFilter();
                    filter.setColumn("details").setPath("ediCode").setOperator(":").setValue(user.getLvl3Code());
                    filters.add(filter);
                    final SearchQuery searchQuery = new SearchQuery(0, 10).setFilters(filters).setOrders(new ArrayList<SearchQueryOrder>()).setSearchTerms("");
                    LOGGER.debug("{} Calling the search that gets the authority by it's ediCode using the following parameters=> filter:{} || SearchQuery:{}.", userlogPrefix, filter, searchQuery);
                    final SearchResult<ReferentialItemBean> result = this.referentialItemDataService.search(searchQuery, ReferentialItemBean.class,
                            ReferentialTableEnum.findTechnicalTable("authority"));
                    LOGGER.debug("{} The result of the call of the method search that gets authority by it's ediCode is : {}.", userlogPrefix, result);
                    if (result.getTotalResults() > 1) {
                        LOGGER.warn("{} more than one authority have the ediCode equals to :{} for the user {} {} ({}).", userlogPrefix, user.getLvl3Code(), user.getUserLastName(),
                                user.getUserFirstName(), user.getUserEmail());
                    }
                    if (result.getTotalResults() == 1 && !result.getContent().isEmpty()) {
                        LOGGER.debug("{} One result was found", userlogPrefix);
                        authority = new AuthorityBean();
                        // en utilisant le resultat on doit récupérer le
                        // functionnal ID
                        LOGGER.debug("{} Getting the contentList {}", userlogPrefix, result.getContent());
                        String entityId = result.getContent().get(0).getEntityId();
                        LOGGER.debug("{} Getting the entityId", entityId);
                        if (entityId != null) {
                            authority = this.authorityDataService.getByEntityId(entityId);
                        } else {
                            LOGGER.warn("{} An error occured while getting the functionalId from the result's content: The functional ID was empty or null", userlogPrefix);
                        }
                    }
                } // End bloc :"Getting the authority that relates to the user"
                LOGGER.info("{} getting the users's authority : {}.", userlogPrefix, authority.getEntityId());

                // Create the user and then check the authority if it exist then
                // if he find it check if the user is already imported in it if
                // no imports the user
                if (authority != null) {
                    // Get the users of the authority
                    JsonNode users = authority.getDetails().get("users");
                    List<String> authorityUsersIds = new ArrayList<String>();
                    if (users != null && users.isArray()) {
                        for (final JsonNode userNode : users) {
                            authorityUsersIds.add((userNode.get("id").asText()));
                        }
                    }
                    // Call Account to create the user and get his new Id
                    LOGGER.info("{} Call Account to read the user {} {} ({}).", userlogPrefix, user.getUserLastName(), user.getUserFirstName(), user.getUserEmail());

                    String userTrackerId = null;
                    try {
                        userTrackerId = this.createUser(user, dryRun, userlogPrefix, importedUsersBeans);
                    } catch (TechnicalException e) {
                        LOGGER.error(String.format("%s The user %s was not imported because an error occured when contacting Account. Proceeding to the next user.", userlogPrefix, user), e);
                        // Go to the next User.
                        continue;
                    }

                    LOGGER.info("{} Result of the call Account to read the user {} {} ({}) returned the tracker Id : {}.", userlogPrefix, user.getUserLastName(), user.getUserFirstName(),
                            user.getUserEmail(), userTrackerId);

                    // Check if the List of Ids contains the user's id
                    LOGGER.debug("{} checking if the list of the authority {} user's contains the user's id {} ", userlogPrefix, authority.getEntityId(), userTrackerId);
                    if (!authorityUsersIds.contains(userTrackerId)) {
                        LOGGER.debug("{} The authority {} does not contain the user's id {}, adding the user to the authority ", userlogPrefix, authority.getEntityId(), userTrackerId);
                        if (!dryRun) {
                            // Add the user to the table authority_user&
                            // Json

                            final String role = user.getUserRole().getRole();
                            if (StringUtils.isBlank(role)) {
                                LOGGER.debug("{}  Calling method create link with the parameter ==> EntityId :{} || userTrackerId :{}. The user been added doesn't have any role", userlogPrefix,
                                        authority.getEntityId(), userTrackerId);
                                this.authorityDataService.createLink(authority.getEntityId(), userTrackerId, null);
                                nbLines++;
                            } else {
                                LOGGER.debug("{}  Calling method create link with the parameter ==> EntityId :{} || userTrackerId :{} || userRole:{}", userlogPrefix, authority.getEntityId(),
                                        userTrackerId, user.getUserRole().getRole());
                                this.authorityDataService.createLink(authority.getEntityId(), userTrackerId, user.getUserRole().getRole());
                                nbLines++;
                            }
                        }
                    } else {
                        LOGGER.warn("{} The authority {} already contain's the user's id {}, skipping the import of this user ", userlogPrefix, authority.getEntityId(), userTrackerId);
                    }
                } else {
                    LOGGER.error("{} The user {} was not imported because no Authority was found for this user.", userlogPrefix, user);
                } // End bloc : Create the user and then check the authority if
                  // it exist then if he find it check if the user is already
                  // imported in it if no imports the user
            } // Fin boucle sur les users à importer

            LOGGER.info("{} Process finished, {} lines processed", logPrefix, nbLines);
            return Integer.toString(nbLines);
        } else {
            String msg = String.format("The list of users given as a parameter is either null or empty thus there is no action to be done on the file");
            LOGGER.error(msg);
            throw new TechnicalException(msg);
        }
    }

    /**
     * {@inheritDoc}
     * 
     */
    @Override
    public LegacyAuthorityUserLink getUser(final String[] cols, final Map<String, Integer> colsIndexes) {
        final LegacyAuthorityUserLink user = new LegacyAuthorityUserLink() //
                .setId(this.getCol(cols, colsIndexes, "id")) //
                .setLvl1Code(this.getCol(cols, colsIndexes, "niv1_code")) //
                .setLvl2Code(this.getCol(cols, colsIndexes, "niv2_code")) //
                .setLvl3Code(this.getCol(cols, colsIndexes, "niv3_code")) //
                .setLvl3Label(this.getCol(cols, colsIndexes, "niv3_lib")) //
                .setUserId(this.getCol(cols, colsIndexes, "u_id")) //
                .setUserLastName(this.getCol(cols, colsIndexes, "u_nom")) //
                .setUserFirstName(this.getCol(cols, colsIndexes, "u_prenom")) //
                .setUserEmail(this.getCol(cols, colsIndexes, "u_email")) //
                .setUserRole(LegacyUserRoleEnum.valueOf(this.getCol(cols, colsIndexes, "u_role"))) //
                .setUserLvl2Code(this.getCol(cols, colsIndexes, "u_niv2_code"));
        return user;
    }

    /**
     * Gets a column value for the import.
     *
     * @param cols
     *            the columns of a line
     * @param colsIndexes
     *            the columns indexed with their names
     * @param col
     *            the column name
     * @return the column value
     */
    private String getCol(final String[] cols, final Map<String, Integer> colsIndexes, final String col) {
        String colValue = null;
        final Integer colIndex = colsIndexes.get(col);
        if (colIndex != null && colIndex >= 0 && colIndex < cols.length) {
            colValue = StringUtils.trim(cols[colIndex]);
            if (colValue.startsWith("\uFEFF")) {
                colValue = colValue.substring(1);
            }
        }
        return colValue;
    }

    /**
     * Gets the new code from a legacy code.
     *
     * @param code
     *            the legacy code
     * @param isLevel3
     *            is level 3
     * @return the new code
     */
    private String getNewCode(final String code, final boolean isLevel3) {
        String newCode = code;
        final LegacyAuthorityLvl2CodeEnum legacyCodeEnum = LegacyAuthorityLvl2CodeEnum.valueOf(code);
        if (legacyCodeEnum != null) {
            newCode = legacyCodeEnum.getCode();
            if (isLevel3) {
                newCode += legacyCodeEnum.getLvl3Separator();
            }
        }
        return newCode;
    }

    /**
     * Calling account to read the user using it's @mail and then
     * 
     * @param user
     * @param dryRun
     * @param logPrefix
     * @param authority
     * @param importedUsers
     */
    private String createUser(final LegacyAuthorityUserLink user, final boolean dryRun, final String logPrefix, final Map<String, AccountUserBean> importedUsers) throws TechnicalException {
        try {
            AccountUserBean accountUser = importedUsers.get(user.getUserEmail());
            if (accountUser == null) {
                accountUser = this.accountFacade.readUser(user.getUserEmail());
            }
            if (accountUser == null || StringUtils.isEmpty(accountUser.getTrackerId())) {
                accountUser = new AccountUserBean();
                accountUser.setFirstName(user.getUserFirstName());
                accountUser.setLastName(user.getUserLastName());
                accountUser.setCivility("Monsieur");
                accountUser.setLanguage("fr_FR");
                accountUser.setEmail(user.getUserEmail());
                accountUser.setPhone("+33600000000");
                if (LegacyUserRoleEnum.ADMINISTRATEUR.equals(user.getUserRole())) {
                    accountUser.setMailType(StringUtils.EMPTY);
                } else if (LegacyUserRoleEnum.REFERENT.equals(user.getUserRole())) {
                    accountUser.setMailType("1");
                } else {
                    accountUser.setMailType("0");
                }
                if (!dryRun) {
                    accountUser = this.accountFacade.registerUser(accountUser);
                }
                LOGGER.info("{} Created user '{}' ({})", logPrefix, accountUser.getEmail(), accountUser.getTrackerId());
                importedUsers.put(accountUser.getEmail(), accountUser);
            } else {
                LOGGER.info("{} Found user '{}' ({})", logPrefix, accountUser.getEmail(), accountUser.getTrackerId());
            }

            return accountUser.getTrackerId();

        } catch (final RuntimeException e) {
            String msg = String.format("%s An error occurred when calling Account  for user '%s'.", logPrefix, user.getUserEmail());
            LOGGER.error(msg);
            throw new TechnicalException(msg, e);
        }
    }
}