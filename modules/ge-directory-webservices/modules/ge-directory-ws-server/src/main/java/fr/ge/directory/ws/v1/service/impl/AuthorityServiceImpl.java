package fr.ge.directory.ws.v1.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Path;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JsonNode;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import fr.ge.directory.service.IAlgoDataService;
import fr.ge.directory.service.IAuthorityDataService;
import fr.ge.directory.service.bean.AuthorityBean;
import fr.ge.directory.service.bean.AuthorityRoleBean;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityBean;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityRoleBean;
import fr.ge.directory.ws.v1.service.IAuthorityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;

/**
 * Service for authorities.
 *
 * @author Christian Cougourdan
 */
@Api("Authority REST Services")
@Path("/v1/authority")
public class AuthorityServiceImpl implements IAuthorityService {

    /**
     * 
     */
    private static final String PARENT = "parent";

    /** Authority data service. */
    @Autowired
    private IAuthorityDataService authorityDataService;

    /** Algo data service. */
    @Autowired
    private IAlgoDataService algoDataService;

    /** Dozer. */
    @Autowired
    private DozerBeanMapper dozer;
    // TODO Add logs to this interface
    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorityServiceImpl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Retrieves an authority from its `ID`")
    public ResponseAuthorityBean get(@ApiParam("authority id") final long id) {
        ResponseAuthorityBean responseAuthority;
        final AuthorityBean authority = this.authorityDataService.get(id);
        if (authority == null) {
            responseAuthority = this.getByEntityId(Long.toString(id));
        } else {
            responseAuthority = this.dozer.map(authority, ResponseAuthorityBean.class);
        }
        JsonNode details = responseAuthority.getDetails();
        JsonNode parentFromDetails = details.findValue(PARENT);
        String entityId = responseAuthority.getEntityId();

        String path = null;
        if (parentFromDetails != null && !StringUtils.isEmpty(parentFromDetails.asText())) {
            path = parentFromDetails.asText().concat("/").concat(entityId);
            responseAuthority.setPath(path);
        } else {
            path = entityId;
        }
        responseAuthority.setPath(path);
        return responseAuthority;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Retrieves an authority from its functional id")
    public ResponseAuthorityBean getByEntityId(@ApiParam("authority functional id") final String entityId) {
        ResponseAuthorityBean responseAuthority;
        final AuthorityBean authority = this.authorityDataService.getByEntityId(entityId);
        if (authority == null) {
            responseAuthority = new ResponseAuthorityBean();
        } else {
            responseAuthority = this.dozer.map(authority, ResponseAuthorityBean.class);
        }
        return responseAuthority;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Retrieves an authority from metas")
    public ResponseAuthorityBean find(@ApiParam(value = "metas", examples = @Example(value = @ExampleProperty(value = "{\r\n" //
            + "  \"serviceCompetent\": \"debitBoisson\",\r\n" //
            + "  \"codeInsee\": \"78646\"\r\n" //
            + "}"))) final Map<String, String> metas) {
        final String authorityType = metas.get("serviceCompetent");
        final String entityId = this.algoDataService.execute(authorityType, metas);
        return this.getByEntityId(entityId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Saves an authority")
    public Long save(@ApiParam(value = "authority data", examples = @Example(value = @ExampleProperty(value = "{\r\n" //
            + "  \"entityId\": \"Mairie78646\",\r\n" //
            + "  \"label\": \"Mairie de Versailles\",\r\n" //
            + "  \"details\": \"{\\\"email\\\": \\\"xxx@xxx.com\\\", \\\"address\\\": {\\\"zipCode\\\": 78646}}\",\r\n" //
            + "  \"path\": \"GE/Mairies/Mairie78646\"\r\n" //
            + "  \"updated\": \"2018-06-12 15:02:04.51+02\"\r\n"//
            + "}"))) final ResponseAuthorityBean data) {

        // pas d'utilisation du copieur qui plante
        // final AuthorityBean authority = this.dozer.map(data,
        // AuthorityBean.class);
        AuthorityBean authority = new AuthorityBean();
        authority.setId(data.getId());
        authority.setEntityId(data.getEntityId());
        authority.setLabel(data.getLabel());
        authority.setDetails(data.getDetails());
        authority.setUpdated(data.getUpdated());
        // // generate the functional identifier from the path
        // final String[] pathSplit = authority.getPath().split("/");
        // authority.setEntityId(pathSplit[pathSplit.length - 1]);
        return this.authorityDataService.save(authority);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Merges an authority")
    public Long merge(@ApiParam(value = "authority", examples = @Example(value = @ExampleProperty(value = "{\r\n" //
            + "  \"entityId\": \"Mairie78646\",\r\n" //
            + "  \"details\": \"{\\\"email\\\": \\\"xxx@xxx.com\\\", \\\"address\\\": {\\\"zipCode\\\": 78646}}\"\r\n" //
            + "}"))) final ResponseAuthorityBean data) {
        final AuthorityBean authority = this.dozer.map(data, AuthorityBean.class);
        return this.authorityDataService.merge(authority);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Search for queue messages", notes = "Multiple filters can be specified using pattern\n" //
            + "&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt, where \"operator\" can be :\n" //
            + "- \":\" = equals\n" //
            + "- \"&gt;\" = greater than\n" //
            + "- \"&gt;=\" = greater than or equals\n" //
            + "- \"&lt;\" = less than\n" //
            + "- \"&lt;=\" = less than or equals\n")
    public SearchResult<ResponseAuthorityBean> search(@ApiParam("page's first element index") final long startIndex, //
            @ApiParam("max element per page") final long maxResults, //
            @ApiParam("filters as \"&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt;\"") final List<SearchQueryFilter> filters, //
            @ApiParam("orders as \"&lt;fieldName&gt;:&lt;asc|desc&gt;\"") final List<SearchQueryOrder> orders, @ApiParam("searched value on all field ") final String searchedValue) {

        // limiter l'injection sql et le plantage de requête
        String searchTerm = searchedValue.replace("'", "''");
        final SearchQuery searchQuery = new SearchQuery(startIndex, maxResults).setFilters(filters).setOrders(orders).setSearchTerms(searchTerm);
        return this.authorityDataService.search(searchQuery, ResponseAuthorityBean.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Removes an authority")
    public boolean remove(@ApiParam("authority id") final long id) {
        return this.authorityDataService.remove(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Links an user to an authority")
    public void createLink(@ApiParam("authority functional id") final String authorityEntityId, @ApiParam("user id") final String userId) {
        this.authorityDataService.createLink(authorityEntityId, userId, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Links an user to an authority, with a role")
    public void createLinkWithRole(@ApiParam("authority functional id") final String authorityEntityId, @ApiParam("user id") final String userId, @ApiParam("role") final String role) {
        this.authorityDataService.createLink(authorityEntityId, userId, role);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Checks a user has a role for an authority")
    public boolean hasRoleForAuthority(final String userId, final String role, final String authorityId) {
        return this.authorityDataService.hasRoleForAuthority(userId, role, authorityId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Gets authorities for a partner user")
    public List<ResponseAuthorityRoleBean> findAuthoritiesByUserId(@ApiParam("partner user id") final String userId) throws TechniqueException, FunctionalException {
        final List<AuthorityRoleBean> authoritiesRoles = this.authorityDataService.findAuthoritiesByUserId(userId);
        final List<ResponseAuthorityRoleBean> responseAuthoritiesRoles = new ArrayList<>();
        for (final AuthorityRoleBean authorityRole : authoritiesRoles) {
            responseAuthoritiesRoles.add(this.dozer.map(authorityRole, ResponseAuthorityRoleBean.class));
        }
        return responseAuthoritiesRoles;
    }

}
