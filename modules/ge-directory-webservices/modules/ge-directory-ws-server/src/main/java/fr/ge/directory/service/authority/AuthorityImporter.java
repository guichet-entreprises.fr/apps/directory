/**
 * 
 */
package fr.ge.directory.service.authority;

import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.directory.service.IAuthorityDataService;
import fr.ge.directory.service.IReferentialItemDataService;
import fr.ge.directory.service.bean.AuthorityBean;
import fr.ge.directory.service.bean.ReferentialItemBean;
import fr.ge.directory.service.bean.ReferentialTableEnum;
import fr.ge.tracker.facade.ITrackerFacade;

/**
 * @author $Author: LABEMONT $
 */
public class AuthorityImporter {
    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorityImporter.class);

    /** Authority external data reader */
    private AuthorityCsvIterator authorityIterator;

    /** Authority data access */
    @Autowired
    protected IAuthorityDataService authorityDataService;

    @Autowired
    protected IReferentialItemDataService referentialItemDataService;

    /** Traker facade. */
    @Autowired
    protected ITrackerFacade trackerFacade;

    /**
     * Constructeur de la classe.
     * 
     * @param AuthorityCsvIterator
     *            the iterator over the input data using a custom unmarshaller
     * @param authorityDataService
     *            services CRUD des autorités
     * @param referentialItemDataService
     *            service de recherche
     * @param trackerFacade
     *            service de création d'un identifiant unique
     */
    public AuthorityImporter(AuthorityCsvIterator authorityIterator, IAuthorityDataService authorityDataService, IReferentialItemDataService referentialItemDataService, ITrackerFacade trackerFacade) {
        this.authorityIterator = authorityIterator;
        this.authorityDataService = authorityDataService;
        this.referentialItemDataService = referentialItemDataService;
        this.trackerFacade = trackerFacade;
    }

    /**
     * Start the import with the authority iterator from the constructor.
     * 
     * @param dryRun
     *            whether to actually save the data
     * @return the number of imported authority
     */
    public int importAuthorities(boolean dryRun) {

        return importAuthorities(this.authorityIterator, dryRun);
    }

    /**
     * Import an Authority : create if not present, merge if exists.<br/>
     * Search is made on ediCode, the imported fields win over the legacy
     * values, except for the users, the backoffice parameters.
     * 
     * @param authority
     *            the authority, not null
     * @return the number of imported authority
     */
    protected int importAuthorities(Iterator<AuthorityBean> authoritiesIterator, boolean dryRun) {
        int nbIntegrated = 0;

        AuthorityBean authority = null;

        while (authoritiesIterator != null && authoritiesIterator.hasNext()) {
            authority = authoritiesIterator.next();
            try {
                // rechercher si l'autorité est déjà stockée
                AuthorityBean storedAuthority = findOriginalItem(authority);

                // Si on a un résultat c'est une mise à jour
                if (storedAuthority != null) {
                    // merge => conservation du entityId, des users, du
                    // paramétrage des canaux
                    // écriture dans le bean d'origine des nouvelles valeurs
                    authority = merge(authority, storedAuthority);
                }

                // mise à jour métier des valeurs de l'autorité
                // certaines valeurs doivent subir une transformation
                authority = functionalControl(authority);

                // insérer le bean
                if (!dryRun) {
                    this.authorityDataService.save(authority);
                    nbIntegrated++;
                }
            } catch (RuntimeException e) {
                LOGGER.error(String.format("Unable to process the authority %s : %s. Proceeding to the next element.", authority.getEntityId(), e.getMessage()), e);
            }
        }
        return nbIntegrated;
    }

    /**
     * Search if the authority is already present in the system.
     * 
     * @param authority
     *            the authority to find in the system, not null
     * @return the previous authority, null if not found
     */
    protected AuthorityBean findOriginalItem(AuthorityBean authority) {

        SearchQuery searchQuery = findByUniqueCriteria(authority);
        SearchResult<ReferentialItemBean> searchResult = this.referentialItemDataService.search(searchQuery, ReferentialItemBean.class, ReferentialTableEnum.AUTHORITY.getTechnicalTable());

        // Si on a un résultat c'est une mise à jour
        if (searchResult.getTotalResults() >= 1) {
            ReferentialItemBean storedAuthorityItem = searchResult.getContent().get(0);
            AuthorityBean storedAuthority = new AuthorityBean();
            storedAuthority.setId(storedAuthorityItem.getId());
            storedAuthority.setEntityId(storedAuthorityItem.getEntityId());
            storedAuthority.setLabel(storedAuthorityItem.getLabel());
            storedAuthority.setDetails(storedAuthorityItem.getDetails());

            return storedAuthority;

        }
        return null;
    }

    /**
     * A search filter meant to retrieve one value; or none.
     * 
     * @return the filter
     */
    protected SearchQuery findByUniqueCriteria(AuthorityBean authority) {
        return new AuthoritySearchFilterBuilder(authority).filterByFunctionalCode().build();
    }

    /**
     * {@inheritDoc} <br/>
     * Conservation du entityId, des users, du paramétrage des canaux écriture
     * dans le bean d'origine des nouvelles valeurs => mise à jour du profile
     * seul.
     */
    protected AuthorityBean merge(AuthorityBean authority, AuthorityBean storedAuthority) {

        if (storedAuthority == null) {
            // pas de autorité originelle, on retourne la nouveauté
            return authority;
        }

        // Mise à jour du libellé
        if (StringUtils.isNoneBlank(authority.getLabel())) {
            storedAuthority.setLabel(authority.getLabel());
        }

        // relecture du Json d'origine
        // mise à jour du Details avec uniquement le profile
        if (storedAuthority.getDetails() instanceof ObjectNode) {
            ObjectNode storedDetails = (ObjectNode) storedAuthority.getDetails();
            // copie du nouveau profile dans l'ancien
            storedDetails.set("profile", authority.getDetails().path("profile"));
        } else {
            LOGGER.warn("Unable to read the details from {}, the merge is not happening the original is conserved.", storedAuthority.getEntityId());
        }
        return storedAuthority;
    }

    /**
     * Business check : the ediCode has to be present, and the trackerId is
     * created.
     * 
     * @param authority
     *            the authority, not null
     * @return the checked authority, trackerId augmented
     * @throws TechnicalException
     *             if the treatment can't be finished
     */
    protected AuthorityBean functionalControl(AuthorityBean authority) throws TechnicalException {
        if (StringUtils.isBlank(authority.getEntityId())) {
            // génération d'un Id si besoin
            try {
                if (authority.getDetails() == null || authority.getDetails().get("ediCode") == null || StringUtils.isBlank(authority.getDetails().get("ediCode").asText())) {
                    throw new TechnicalException(String.format("Unable to create a unique id without an edi code (%s).", authority.getLabel()));
                }
                // construction d'un id avec comme base le code EDI
                String baseId = authority.getDetails().get("ediCode").asText();

                String generatedTrackerId = this.trackerFacade.createUid();
                LOGGER.info(String.format("Generated Tracker id : %s, for authority : %s.", generatedTrackerId, baseId));
                String entityId = String.format("%s%s", generatedTrackerId.substring(0, generatedTrackerId.length() - baseId.length()), baseId);
                authority.setEntityId(entityId);
                return authority;
            } catch (Exception e) {
                String msg = String.format("Error occured when calling Tracker to generate Uid for authority %s : %s.", authority.getLabel(), e.getMessage());
                LOGGER.error(msg);
                throw new TechnicalException(msg, e);
            }
        }
        return authority;
    }

}