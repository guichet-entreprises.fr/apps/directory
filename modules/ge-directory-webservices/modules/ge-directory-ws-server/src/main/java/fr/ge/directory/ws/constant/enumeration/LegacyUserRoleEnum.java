/**
 *
 */
package fr.ge.directory.ws.constant.enumeration;

/**
 * Roles for legacy users.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public enum LegacyUserRoleEnum {

    /** Administrateur. */
    ADMINISTRATEUR("referent", true),

    /** Referent. */
    REFERENT("referent", false),

    /** Utilisateur. */
    UTILISATEUR("user", false);

    /** Role. */
    private String role;

    /** Root authority. */
    private boolean rootAuthority;

    /**
     * Constructor.
     *
     * @param role
     *            the role
     * @param rootAuthority
     *            the root authority
     */
    LegacyUserRoleEnum(final String role, final boolean rootAuthority) {
        this.role = role;
        this.rootAuthority = rootAuthority;
    }

    /**
     * Gets the role.
     *
     * @return the role
     */
    public String getRole() {
        return this.role;
    }

    /**
     * Checks if is root authority.
     *
     * @return true, if is root authority
     */
    public boolean isRootAuthority() {
        return this.rootAuthority;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return this.role;
    }

}
