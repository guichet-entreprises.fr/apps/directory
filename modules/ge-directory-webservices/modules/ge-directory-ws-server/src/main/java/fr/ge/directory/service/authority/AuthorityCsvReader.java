/**
 * 
 */
package fr.ge.directory.service.authority;

import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.directory.service.bean.AuthorityBean;

/**
 * The class that constructs an Authority from a CSV, merges it from another
 * entity, exposes the search filter to find this authority, gives tips on how
 * to compute the functional id (entity id).
 * 
 * @author $Author: LABEMONT $
 */
public abstract class AuthorityCsvReader {
    /** Static logger. */
    protected final static Logger LOGGER = LoggerFactory.getLogger(AuthorityCsvReader.class);

    /** simple mapper to create JsonNode. */
    protected ObjectMapper mapper = new ObjectMapper();

    /**
     * Read the headers of a csv file and choose the right model reader.
     * 
     * @param the
     *            headers
     * @throws TechnicalException
     *             if some wrong occurs, or if the reader can't be created.
     * @return the functional objet reader
     */
    public static boolean readable(String[] array) throws TechnicalException {
        // TODO gérer le controle en capitales
        if (Arrays.asList(array).containsAll(Arrays.asList("ch_cfe", "ch_nom", "ch_nompostal", "ch_commune"))) {
            return true;
        }
        return false;
    }

    /**
     * Read a csv line and create an Authority bean
     * 
     * @param record
     *            the CSV line
     * @return the created bean, null if unable to create the bean
     */
    public abstract AuthorityBean unmarshall(CSVRecord record);

    /**
     * Merge a new Authority bean and one that is stored
     * 
     * @param authority
     *            the authority's new version
     * @param storedAuthority
     *            the authority's old version, nullable
     * @return the merged authority
     */
    public abstract AuthorityBean merge(AuthorityBean authority, AuthorityBean storedAuthority);

    /**
     * A search filter meant to retrieve one value; or none.
     * 
     * @return the filter
     */
    public SearchQuery findByUniqueCriteria(AuthorityBean authority) {
        return new AuthoritySearchFilterBuilder(authority).filterByFunctionalCode().build();
    }

    /**
     * Says if the authority's functional id need a complement.
     * 
     * @return true by default
     */
    public boolean doNeedFunctionalIdCompletion() {
        return true;
    }

}

/**
 * Reader for legacy Authority.
 * 
 * @author $Author: LABEMONT $
 */
final class LegacyAuthorityCsvReader extends AuthorityCsvReader {

    /** {@inheritDoc} */
    @Override
    public AuthorityBean unmarshall(CSVRecord record) {
        StringBuilder detailJson = new StringBuilder();
        detailJson.append(String.format("{\"ediCode\": \"%s\",\"parent\": \"%s\",", record.get("ch_cfe"), computeParentPath(record.get("ch_cfe"))));
        // début profile
        detailJson.append(" \"profile\": {");
        detailJson.append(String.format("\"tel\": \"%s\",", record.get("ch_tel")));
        detailJson.append(String.format("\"fax\": \"%s\",", record.get("ch_fax")));
        detailJson.append(String.format("\"email\": \"%s\",", record.get("ch_mail")));
        detailJson.append(String.format("\"url\": \"%s\",", record.get("ch_url")));
        detailJson.append(String.format("\"observation\": \"%s\",", record.get("ch_obs")));
        // début address
        detailJson.append(" \"address\": {");
        detailJson.append(String.format("\"recipientName\": \"%s\",", record.get("ch_nompostal"), ","));
        StringBuilder addressNameCompl = new StringBuilder();
        addressNameCompl.append(record.get("ch_num"));
        addressNameCompl.append(" ");
        String rep = record.get("ch_rep");
        if (StringUtils.isNotBlank(rep)) {
            if (rep.equals("B")) {
                addressNameCompl.append(" Bis ");
            }
            if (rep.equals("T")) {
                addressNameCompl.append(" Ter ");
            }
        }
        addressNameCompl.append(record.get("ch_typ"));
        addressNameCompl.append(" ");
        addressNameCompl.append(record.get("ch_voi"));
        detailJson.append(String.format("\"addressNameCompl\": \"%s\",", addressNameCompl));
        detailJson.append(String.format("\"compl\": \"%s\",", record.get("ch_cpl")));
        detailJson.append(String.format("\"special\": \"%s\",", record.get("ch_spe")));
        detailJson.append(String.format("\"postalCode\": \"%s\",", record.get("ch_pos")));
        detailJson.append(String.format("\"cedex\": \"%s\",", record.get("ch_cdx")));
        detailJson.append(String.format("\"cityNumber\": \"%s\"", record.get("ch_commune")));
        detailJson.append("}");
        // fin address
        detailJson.append(" }");
        // fin profile
        detailJson.append("}");
        // fin details
        JsonNode details;
        try {
            details = this.mapper.readTree(detailJson.toString());
        } catch (IOException e) {
            LOGGER.error(String.format("Unable to read the detail for >>%s<< : %s. Returning null.", detailJson, e.getMessage()), e);
            return null;
        }
        AuthorityBean authority = new AuthorityBean();
        // ceci est mis en commentaire
        // l'entity id sera positionné avec des règles métiers
        // authority.setEntityId(record.get("ch_cfe"));
        authority.setLabel(record.get("ch_nom"));
        authority.setDetails(details);
        return authority;
    }

    /**
     * Compute the parent path for a CFE.
     * 
     * @param string
     *            the edi code
     * @return the parent path
     */
    private String computeParentPath(String codeCfe) {

        if (StringUtils.isBlank(codeCfe)) {
            LOGGER.warn("Impossible to search a CFE with code null or empty.");
            return null;
        }

        String authorityPrefix = null;

        switch (codeCfe.substring(0, 1)) {
        case "C":
            authorityPrefix = "CCI";
            break;
        case "M":
            authorityPrefix = "CMA";
            break;
        case "G":
            authorityPrefix = "GREFFE";
            break;
        case "U":
            authorityPrefix = "URSSAF";
            break;
        case "X":
            authorityPrefix = "CA";
            break;
        }
        return "GE/" + authorityPrefix;
    }

    /**
     * {@inheritDoc} <br/>
     * Conservation du entityId, des users, du paramétrage des canaux écriture
     * dans le bean d'origine des nouvelles valeurs.
     */
    @Override
    public AuthorityBean merge(AuthorityBean authority, AuthorityBean storedAuthority) {

        if (storedAuthority == null) {
            // pas de autorité originelle, on retourne la nouveauté
            return authority;
        }

        // relecture du Json d'origine
        if (storedAuthority.getDetails() instanceof ObjectNode) {
            ObjectNode storedDetails = (ObjectNode) storedAuthority.getDetails();
            // copie du nouveau profile dans l'ancien
            storedDetails.set("profile", authority.getDetails().path("profile"));
        } else {
            LOGGER.warn("Unable to read the details from {}, the merge is not happening the original is conserved.", storedAuthority.getEntityId());
        }
        return storedAuthority;
    }

}

/**
 * Reader for Commune.
 * 
 * @author $Author: LABEMONT $
 */
final class LegacyCommuneCsvReader extends AuthorityCsvReader {

    public static boolean readable(String[] array) throws TechnicalException {
        // TODO gérer le controle en capitales
        if (Arrays.asList(array).containsAll(Arrays.asList("ch_nom", "ch_commune", "ch_cma", "ch_cci", "ch_ca", "ch_urssaf", "ch_greffe", "ch_cnba"))) {
            return true;
        }
        return false;
    }

    /** {@inheritDoc} */
    @Override
    public AuthorityBean unmarshall(CSVRecord record) {
        AuthorityBean authority = new AuthorityBean();
        // ceci est mis en commentaire
        // l'entity id sera positionné avec des règles métiers
        // authority.setEntityId(record.get("ch_commune"));
        authority.setLabel(record.get("ch_nom"));
        String detailsJson = String.format("{\"codeCommune\": \"%s\",\"CCI\": \"%s\",\"CMA\": \"%s\",\"GREFFE\": \"%s\",\"CA\": \"%s\",\"URSSAF\": \"%s\",\"CNBA\":\"%s\",\"parent\": \"%s\"}",
                record.get("ch_commune"), StringUtils.stripToEmpty(record.get("ch_cci")), StringUtils.stripToEmpty(record.get("ch_cma")), StringUtils.stripToEmpty(record.get("ch_greffe")),
                StringUtils.stripToEmpty(record.get("ch_ca")), StringUtils.stripToEmpty(record.get("ch_urssaf")), StringUtils.stripToEmpty(record.get("ch_cnba")),
                computeParentPath(StringUtils.stripToEmpty(record.get("ch_commune"))));
        try {
            authority.setDetails(this.mapper.readTree(detailsJson));
        } catch (IOException e) {
            LOGGER.error(String.format("Unable to read the detail for >>%s<< : %s. Returning null.", detailsJson, e.getMessage()), e);
            return null;
        }
        return authority;
    }

    /**
     * Compute the parent path for a CFE.
     * 
     * @param string
     *            the functionnal code
     * @return the parent path
     */
    private String computeParentPath(String code) {
        return "ZONES";
    }

    /** {@inheritDoc} */
    @Override
    public AuthorityBean merge(AuthorityBean authority, AuthorityBean storedAuthority) {

        if (storedAuthority == null) {
            // pas de autorité originelle, on retourne la nouveauté
            return authority;
        }
        storedAuthority.setDetails(authority.getDetails());

        return storedAuthority;
    }

    /**
     * {@inheritDoc}<br/>
     * Overridden to use codeCommune
     */
    @Override
    public SearchQuery findByUniqueCriteria(AuthorityBean authority) {
        return new AuthorityCommuneSearchFilterBuilder(authority).filterByFunctionalCode().build();
    }

    /**
     * {@inheritDoc}<br/>
     * The commune don't need it.
     */
    public boolean doNeedFunctionalIdCompletion() {
        return false;
    }
}
